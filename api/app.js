export default ($axios, isDev) => ({
  fetchInitialData: async () => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/metadata/initial`
    )
    return data
  },

  fetchVersion: async () => {
    const data = await $axios.$get('/version.json')
    return data
  },

  fetchCountryInfo: async (ip) => {
    const apiStack = $axios.create({
      baseURL: process.env.ipStackHost,
      port: 443
    })

    const data = await apiStack.$get(
      `/${ip}?access_key=${process.env.ipStackKey}`
    )

    return data
  }
})
