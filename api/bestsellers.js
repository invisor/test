export default ($axios) => ({
  fetchDeals: async () => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/promotion/bestsellers`
    )
    return data
  }
})
