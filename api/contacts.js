export default ($axios) => ({
  sendContactsForm: async (query) => {
    const data = await $axios.$post(
      `public/${process.env.siteName}/cart/message`,
      query
    )
    return data
  }
})
