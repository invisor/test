export default ($axios) => ({
  fetchJewelryFilters: async (query) => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/jewelry/filters/${query}`
    )
    return data
  },

  fetchSettingsFilters: async (query) => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/setting/filters${query}`
    )
    return data
  },

  fetchPlainBandsFilters: async () => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/band/plain/filters`
    )
    return data
  },

  fetchBandsFilters: async () => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/band/filters`
    )
    return data
  },

  fetchStonesFilters: async (query) => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/gemstone/filters${query}`
    )
    return data
  },

  fetchPairsFilters: async (query) => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/gemstone/pairs/filters${query}`
    )
    return data
  }
})
