export default ($axios) => ({
  getCurrency: async () => {
    const data = await $axios.$get(
      `public/${process.env.siteName}/account/currency`
    )
    return data
  }
})
