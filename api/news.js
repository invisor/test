export default ($axios) => ({
  subscribeToNewsletters: async (query) => {
    const data = await $axios.$post(
      `public/${process.env.siteName}/account/newsletter/subscribe`,
      query
    )
    return data
  }
})
