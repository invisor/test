import { formatAccountDateUTC } from '~~/utils/formatters'

export default class CustomCellRenderer {
  init(params) {
    this.eGui = document.createElement('div')
    this.eGui.innerHTML = `
        <span>${params.context.$t(
          'account.preview.dateRequested'
        )}:<br>${formatAccountDateUTC(params.value)}</span>`
  }

  getGui() {
    return this.eGui
  }
}
