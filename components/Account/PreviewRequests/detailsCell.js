import { caratFormatter } from '~~/utils/formatters'

export default class CustomCellRenderer {
  init(params) {
    this.params = params
    this.eGui = document.createElement('div')
    this.eGui.classList.add('cell-wrapper')
    this.eGui.innerHTML = `
        <div class="half-item is-flex">
           <div class="item-param">${this.params.context.$t(
             'account.preview.itemID'
           )}<br>${this.params.value.stone.id}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.color'
           )}<br>${this.params.value.stone.color}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.carat'
           )}<br>${caratFormatter(this.params.value.stone.weight)}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.shape'
           )}<br>${this.params.value.stone.shape}</div>
                </div>
        <div class="half-item is-flex">
           <div class="item-param">${this.params.context.$t(
             'account.preview.itemID'
           )}<br>${this.params.value.setting.id}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.metal'
           )}<br>${this.params.value.setting.metalName}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.prong'
           )}<br>${this.params.value.setting.prongType || 'N/A'}</div>
           <div class="item-param">${this.params.context.$t(
             'account.preview.productTime'
           )}<br>${this.params.context.$t('account.preview.range', [
      this.params.value.setting.productionTimeFrom,
      this.params.value.setting.productionTimeTo
    ])}</div>
        </div>
    `
  }

  getGui() {
    return this.eGui
  }
}
