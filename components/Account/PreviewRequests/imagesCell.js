export default class CustomCellRenderer {
  init(params) {
    this.eGui = document.createElement('div')
    this.eGui.classList.add('cell-wrapper')
    this.eGui.innerHTML = `
        <div class="half-item is-flex">
          <img class='item-image' src='${params.value.stone.imagePath}'>
        </div>
        <div class="half-item is-flex">
          <img class='item-image' src='${params.value.setting.imagePath}'>
        </div>`
  }

  getGui() {
    return this.eGui
  }
}
