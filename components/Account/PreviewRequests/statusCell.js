export default class CustomCellRenderer {
  init(params) {
    this.params = params
    this.eGui = document.createElement('div')
    this.eGui.innerHTML = !this.params.value
      ? `
           <span class="item-status-awaiting">${this.params.context.$t(
             'account.preview.awaitingPreview'
           )}</span>
       `
      : `
           <button onclick="${
             this.onButtonClick
           }" class="button gold-color light">${this.params.context.$t(
          'account.preview.readyToView'
        )}</button>
       `

    if (this.params.value) this.eGui.onclick = this.onButtonClick.bind(this)
  }

  onButtonClick() {
    this.params.context.$emit('show-previews', this.params.value)
  }

  getGui() {
    return this.eGui
  }
}
