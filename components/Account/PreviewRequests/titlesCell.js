import { itemNameFormatter } from '~~/utils/formatters'

export default class CustomCellRenderer {
  init(params) {
    this.params = params
    this.eGui = document.createElement('div')
    this.eGui.classList.add('cell-wrapper')
    this.eGui.innerHTML = `
        <div class="half-item is-flex">
          <div class="item-title">${this.itemNameFormatter(
            this.params.value.stone
          )}</div>
          </div>
          <div class="half-item is-flex">
            <div class="item-title">${this.itemNameFormatter(
              this.params.value.setting
            )}
          </div>
        </div>`
  }

  itemNameFormatter(item) {
    return itemNameFormatter.call(this.params.context, item)
  }

  getGui() {
    return this.eGui
  }
}
