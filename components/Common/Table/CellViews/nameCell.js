export default class CustomCellRenderer {
  init(params) {
    this.params = params
    this.eGui = document.createElement('div')
    const paramName = document.createElement('div')
    paramName.classList.add('param-name', 'is-flex')
    const paramNameValue = document.createElement('span')
    paramNameValue.innerHTML = params.value
    const icon = params.data.learnMore ? document.createElement('i') : null
    if (icon) {
      icon.classList.add('material-icons')
      icon.innerHTML = 'help'
    }

    paramName.appendChild(paramNameValue)

    if (icon) {
      icon.addEventListener(
        'click',
        () => {
          params.context.onInfoClick(params.data)
        },
        false
      )
      paramName.appendChild(icon)
    }

    this.eGui.appendChild(paramName)
  }

  getGui() {
    return this.eGui
  }
}
