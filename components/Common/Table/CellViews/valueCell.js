export default class CustomCellRenderer {
  init(params) {
    this.eGui = document.createElement('div')
    this.eGui.innerHTML =
      typeof params.value === 'object'
        ? `<div class="param-value">${this.getObjValueHTML(params.value)}</div>`
        : `<div class="param-value">${params.value}</div>`
  }

  getObjValueHTML(obj) {
    let html = ''
    Object.keys(obj).forEach((key) => {
      html += `<div class="param-value__row">${obj[key]}</div>`
    })
    return html
  }

  getGui() {
    return this.eGui
  }
}
