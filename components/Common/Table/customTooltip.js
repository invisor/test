export default class CustomTooltip {
  init(params) {
    const eGui = (this.eGui = document.createElement('div'))
    const color = params.color || 'white'
    const data = params.api.getDisplayedRowAtIndex(params.rowIndex).data
    if (!data.tooltip) return

    eGui.classList.add('custom-tooltip')
    // @ts-ignore
    eGui.style['background-color'] = color
    eGui.innerHTML = `
            <p>
                <span class"name">${data.tooltip}</span>
            </p>
        `
  }

  getGui() {
    return this.eGui
  }
}
