﻿import numeral from 'numeral'
import throttle from 'lodash/throttle'
import { getSiteColor } from '~~/utils/utils'

const integerFormat = '000'

export default function (el, options) {
  if (!el) return null
  const rotateBackTime = 1000
  const startDelay = 500
  const threads = 13
  const deltaBorder = 0.3 // after load 30% of images we should increase frames frequency
  let getSequenceStarted = false
  let stopExecuting = false
  let pauseAnimation = false
  let syncProcess = false
  let loadedEventEmited = false
  let loadedImagesCount = 0
  let allImagesAreLoaded = false
  let reverseCover = null
  const arrayOfImages = []
  const xmlhttp = new XMLHttpRequest()
  const loadedEvent = document.createEvent('Event')
  loadedEvent.initEvent('loaded', true, true)

  const axisOffset = {
    x: 0,
    y: 0
  }
  let yTouch = true
  const positionStart = new Event('positionStart')
  const positionStop = new Event('positionStop')

  const progress = document.createElement('div')

  const checkYAxisDrag = (event) => {
    if (event) {
      const xTouch =
        Math.abs(axisOffset.x - event.changedTouches[0].clientX) > 10
      if (xTouch) {
        event.stopPropagation()
        return
      }
      const yOffset = Math.abs(axisOffset.y - event.changedTouches[0].clientY)
      yTouch = yOffset > 30
    }
    el.children[0].style.touchAction = yTouch ? null : 'none'
    el.children[0].style['-webkit-user-drag'] = yTouch ? null : 'none'
  }

  const setPromiseMap = () => {
    Promise.map = function (iterable, mapper, options = {}) {
      let concurrency = options.concurrency || Infinity

      let index = 0
      const results = []
      const pending = []
      const iterator = iterable[Symbol.iterator]()

      while (concurrency-- > 0) {
        const thread = wrappedMapper()
        if (thread) pending.push(thread)
        else break
      }

      return Promise.all(pending).then(() => results)

      function wrappedMapper() {
        const next = iterator.next()
        if (next.done) return null
        const i = index++
        const mapped = mapper(next.value, i)
        return Promise.resolve(mapped).then((resolved) => {
          results[i] = resolved
          return wrappedMapper()
        })
      }
    }
  }

  const emitPosition = () => {
    const { direction, currentIdx, framesCount } = settings

    const position = new CustomEvent('position', {
      detail: {
        direction,
        currentIdx,
        framesCount
      }
    })

    el.dispatchEvent(position)
  }

  const canvasRenderImage = (canvas, image) => {
    if (image) {
      canvas.width = image.width
      canvas.height = image.height
      const context = canvas.getContext('2d')
      context.drawImage(image, 0, 0)
    }
  }

  const resetRollbackTimer = (settings, canvas) => {
    if (settings.behavior !== 'swing' && settings.cover) {
      clearTimeout(reverseCover)
      reverseCover = setTimeout(() => {
        stopExecuting = true
        setCover(settings, canvas)
      }, rotateBackTime)
    }
  }

  const detectSwipeOrDrag = (canvas) => {
    // eslint-disable-next-line no-undef
    const manager = new Hammer.Manager(canvas)
    // eslint-disable-next-line no-undef
    const Swipe = new Hammer.Swipe()
    manager.add(Swipe)
    manager.on('swipe', function (e) {
      if (!settings.onSwipe) return
      if (e.direction === 4) settings.onSwipe(1)
      if (e.direction === 2) settings.onSwipe(0)
    })
  }

  const attachStaticListeners = (settings, canvas) => {
    if (settings.onSwipe) detectSwipeOrDrag(canvas)

    if (settings.startOn === 'auto') {
      getSequence(settings, canvas)
    } else if (settings.startOn === 'mousedown') {
      const onMouseDown = (event) => {
        if (
          canvas === event.currentTarget &&
          event.button === 0 &&
          (event.buttons === 1 || event.which)
        ) {
          getSequence(settings, canvas)
          canvas.removeEventListener('mousedown', onMouseDown, false)

          if (event.cancelable) event.preventDefault()
        }
      }

      canvas.addEventListener('mousedown', onMouseDown, false)
    } else {
      let startDelayTimer = null
      const onMouseOver = (event) => {
        startDelayTimer = setTimeout(() => {
          if (!allImagesAreLoaded) {
            stopExecuting = false
            getSequenceStarted = false
          }
          getSequence(settings, canvas)
          canvas.removeEventListener('click', onMouseClick, false)
          canvas.removeEventListener('mouseover', onMouseOver, false)
          canvas.removeEventListener('mouseout', onMouseOut, false)
          canvas.removeEventListener('touchmove', onTouchMove, false)
        }, startDelay)
      }

      const onMouseOut = (event) => {
        if (startDelayTimer) clearTimeout(startDelayTimer)
      }

      const onMouseClick = (event) => {
        if (settings.onclick != null) {
          settings.onclick(settings.id, event)
        }
      }

      const onTouchMove = (event) => {
        if (canvas === event.currentTarget && event.touches.length === 1) {
          getSequence(settings, canvas, true)

          canvas.removeEventListener('click', onMouseClick, false)
          canvas.removeEventListener('mouseover', onMouseOver, false)
          canvas.removeEventListener('mouseout', onMouseOut, false)
          canvas.removeEventListener('touchmove', onTouchMove, false)
          // Do not prevent default to keep scrolling behaviour.
        }
      }

      canvas.addEventListener('click', onMouseClick, false)
      canvas.addEventListener('mouseover', onMouseOver, false)
      canvas.addEventListener('mouseout', onMouseOut, false)
      canvas.addEventListener('touchmove', onTouchMove, false)
    }
  }

  let start = -1
  let ping = -1
  let done = -1

  const attachDynamicListeners = (settings, canvas, doAnimate) => {
    const getSpeed = (settings) => {
      if (!settings) return 1
      let speed = 100 - settings.speed * 100
      if (speed > 100) speed = 100
      if (speed < 0) speed = 0

      return speed
    }

    const showNextImage = throttle(function (dx) {
      let deltaIdx = 1
      if (dx) {
        dx = dx > -3 ? -3 : dx
        deltaIdx = getDeltaIdx(settings, dx)
      }

      const currentIdx = settings.currentIdx

      if (pauseAnimation && !syncProcess) deltaIdx = 0

      const target = currentIdx + deltaIdx
      const nextIdx = target % settings.images.length
      canvasRenderImage(canvas, settings.images[nextIdx])

      settings.currentIdx = nextIdx
      settings.currentImg = settings.images[nextIdx]
    }, getSpeed(settings))

    const showPrevImage = throttle(function (dx) {
      let deltaIdx = 1
      if (dx) {
        dx = dx < 3 ? 3 : dx
        deltaIdx = getDeltaIdx(settings, dx)
      }

      const currentIdx = settings.currentIdx

      if (pauseAnimation && !syncProcess) deltaIdx = 0

      let target = currentIdx - deltaIdx
      while (target < 0) {
        if (!settings.images.length) {
          target = 0
        }
        target += settings.images.length
      }
      const nextIdx = target

      canvasRenderImage(canvas, settings.images[nextIdx])

      settings.currentIdx = nextIdx
      settings.currentImg = settings.images[nextIdx]
    }, getSpeed(settings))

    const getDeltaIdx = (settings, dx) => {
      let pixel

      switch (settings.size) {
        case 'full':
          pixel = 3
          break
        case 'medium':
          pixel = 2
          break
        case 'small':
          pixel = 1
          break
        default:
          pixel = 2
      }

      const weidth = settings.images.length / 70.0
      const imagesLoadingProgress = loadedImagesCount / settings.images.length
      const reducer = imagesLoadingProgress > deltaBorder ? 1 : 3 // We have to reduce the size of the delta until more images are loaded. Reducing the delta to start showing the video faster
      const delta = Math.floor((weidth * Math.abs(dx / pixel)) / reducer)

      return 2 * Math.floor(delta / 2) + 1
    }

    const animate = () => {
      if (stopExecuting) {
        clearTimeout(settings.timeoutID)
        return
      }

      if (settings.direction < 0) {
        showNextImage()
      } else {
        showPrevImage()
      }

      const maxDelta = settings.targetDeltaTime + 50

      const normalDelta = 1000 / 24 // 24 frames per second
      const loadingDelta = // delta while loading
        settings.deltaTime -
        (settings.deltaTime - settings.targetDeltaTime) * 0.025
      const imagesLoadingProgress = loadedImagesCount / settings.images.length
      const minDelta =
        imagesLoadingProgress > deltaBorder ? normalDelta : loadingDelta // if 30% of images are loading we will start use delta for 24 frames per second
      settings.deltaTime = settings.deltaTime > maxDelta ? maxDelta : minDelta
      settings.timeoutID = setTimeout(animate, settings.deltaTime)
    }

    const onMouseOver = (event) => {
      settings.prevX = event.clientX
    }

    const onMouseMove = (event) => {
      const dX = event.clientX - settings.prevX
      settings.prevX = event.clientX
      if (dX < 0) {
        showNextImage(dX)
      } else if (dX > 0) {
        showPrevImage(dX)
      }
    }

    const swMouseDown = (event) => {
      const buttons = event.buttons || event.which
      if (event.button === 0 && buttons === 1) {
        settings.swinging = true
        settings.moved = false
        settings.prevX = event.clientX
        settings.dX = 0

        if (settings.timeoutID !== 0) {
          clearTimeout(settings.timeoutID)
        }

        window.document.addEventListener('mouseup', bodyMouseUp, false)

        if (settings.changeCursor === 'swing') {
          event.currentTarget.style.cursor = 'ew-resize'
        }

        if (settings.dream) {
          syncProcess = true
          el.dispatchEvent(positionStart)
        }

        // To avoid default image dragging.
        if (event.cancelable) event.preventDefault()
      }
    }

    const swMouseMove = (event) => {
      if (!settings.swinging) {
        return
      }

      const dX = event.clientX - settings.prevX

      settings.prevX = event.clientX
      if (dX < 0) {
        showNextImage()
      } else if (dX > 0) {
        showPrevImage()
      }

      // Exponential mooving average of last increment, to have an idea of speed.
      const alpha = 2.0 / (5 + 1)
      settings.dX = alpha * dX + (1 - alpha) * settings.dX
      settings.moved = true

      if (settings.changeCursor === 'swing') {
        event.currentTarget.style.cursor = 'ew-resize'
      }

      if (settings.dream) {
        if (settings.dX > 0) settings.direction = 1
        if (settings.dX < 0) settings.direction = -1
        emitPosition()
      }

      if (event.cancelable) event.preventDefault()
    }

    const swMouseUp = (event) => {
      if (event.button === 0 && settings.swinging) {
        settings.swinging = false

        if (settings.dX > 0) {
          settings.direction = 1
          settings.deltaTime =
            settings.targetDeltaTime / Math.abs(settings.dX * 0.2)
          animate()
        } else if (settings.dX < 0) {
          settings.direction = -1
          settings.deltaTime =
            settings.targetDeltaTime / Math.abs(settings.dX * 0.2)
          animate()
        } else if (settings.timeoutID === 0) {
          settings.deltaTime = settings.targetDeltaTime
          animate()
        } else {
          clearTimeout(settings.timeoutID)
          settings.timeoutID = 0
        }

        if (settings.dream) {
          syncProcess = false
          el.dispatchEvent(positionStop)
        }

        if (settings.changeCursor === 'swing') {
          event.currentTarget.style.cursor = 'default'
        }
        if (event.cancelable) event.preventDefault()
      }
    }

    const swMouseClick = (event) => {
      const buttons = event.buttons || event.which
      if (event.button === 0 && buttons === 1 && !settings.moved) {
        if (settings.onclick != null) {
          settings.onclick(settings.id, event)
        }
      }
    }

    const touchStart = (event) => {
      if (event.targetTouches.length === 1 && event.touches.length === 1) {
        checkYAxisDrag(event)

        settings.swinging = true
        settings.moved = false
        settings.prevX = event.touches[0].clientX
        axisOffset.y = event.touches[0].clientY
        axisOffset.x = event.touches[0].clientX
        settings.dX = 0

        if (settings.timeoutID !== 0) {
          clearTimeout(settings.timeoutID)
        }
      }
    }

    const touchMove = (event) => {
      checkYAxisDrag(event)

      if (yTouch) return

      if (event.cancelable) {
        event.stopPropagation()
        event.preventDefault()
      }

      if (!settings.swinging) {
        return
      }

      if (event.targetTouches.length === 1 && event.touches.length === 1) {
        const dX = event.touches[0].clientX - settings.prevX
        settings.prevX = event.touches[0].clientX
        if (dX < 0) {
          showNextImage()
        } else if (dX > 0) {
          showPrevImage()
        }

        // Exponential moving average of last increment, to have an idea of speed.
        const alpha = 2.0 / (5 + 1)
        settings.dX = alpha * dX + (1 - alpha) * settings.dX
        settings.moved = true
      }
    }

    const touchEnd = (event) => {
      if (!settings.swinging) {
        return
      }

      if (event.targetTouches.length === 0 && event.touches.length === 0) {
        // yTouch = true
        // el.children[0].style.touchAction = 'none'
        // settings.prevY = 0
        settings.swinging = false

        if (settings.dX > 0) {
          settings.direction = 1
          settings.deltaTime =
            settings.targetDeltaTime / Math.abs(settings.dX * 0.2)
          animate()
        } else if (settings.dX < 0) {
          settings.direction = -1
          settings.deltaTime =
            settings.targetDeltaTime / Math.abs(settings.dX * 0.2)
          animate()
        } else if (settings.timeoutID === 0) {
          settings.deltaTime = settings.targetDeltaTime
          animate()
        } else {
          clearTimeout(settings.timeoutID)
          settings.timeoutID = 0
        }
      }

      return true
    }

    const bodyMouseUp = (event) => {
      if (event.button === 0) {
        settings.swinging = false
        window.document.removeEventListener('mouseup', bodyMouseUp, false)
      }
    }

    const onMouseOut = (event) => {
      resetRollbackTimer(settings, canvas)
    }

    if (settings.behavior === 'mouseover') {
      canvas.addEventListener('mouseover', onMouseOver, true)
      canvas.addEventListener('mouseout', onMouseOut, true)
      canvas.addEventListener('mousemove', onMouseMove, true)
    } else {
      canvas.addEventListener('mousedown', swMouseDown, true)
      canvas.addEventListener('mousemove', swMouseMove, true)
      canvas.addEventListener('mouseup', swMouseUp, true)
    }

    if (doAnimate) {
      if (arrayOfImages[0]) {
        const image = new Image()
        image.src = arrayOfImages[0].image
        canvasRenderImage(canvas, image)
      }
      // wait for 1 second to preload some images
      setTimeout(() => {
        animate()
      }, 1000)
    }

    // Regardless of behavior, add touch listeners.
    canvas.addEventListener('touchstart', touchStart, true)
    canvas.addEventListener('touchmove', touchMove, true)
    canvas.addEventListener('touchend', touchEnd, true)

    // Click allows following links or whatever, touch-punch makes it work on mobile.
    canvas.addEventListener('click', swMouseClick, true)
  }

  const getSequence = (settings, canvas, doAnimate) => {
    if (getSequenceStarted) return
    getSequenceStarted = true
    settings.currentIdx = 0
    const ph =
      settings.size !== 'small'
        ? settings.progressHeight * 2
        : settings.progressHeight

    if (!el.contains(progress)) el.appendChild(progress)
    progress.className = 'progress'
    progress.style.height = ph + 'px'
    progress.style.width = '0%'
    progress.style.position = 'absolute'
    progress.style.bottom = 0

    progress.style.zIndex = '99'
    progress.style.backgroundColor = settings.progressColor

    let firstImage = null
    let count = 0

    /**
     * @param {Object} responseText
     * @param {Integer} from
     */
    const incrementalParse = (obj, from) => {
      return new Promise((resolve, reject) => {
        if (stopExecuting) {
          resolve()
          return
        }

        const index = obj.index
        const of = obj.of

        // Init arrays.
        if (typeof settings.images === 'undefined') {
          settings.images = new Array(of)
          settings.arrived = []
          settings.arrived.length = of
          for (let i = 0; i < of; i++) {
            settings.arrived[i] = false
          }
        }

        const image = new Image()
        image.onload = ((theSettings, theCanvas, theImage, theIndex) => {
          return () => {
            theSettings.offContext.drawImage(theImage, 0, 0)

            progress.style.width = (theCanvas.offsetWidth * count) / of + 'px'
            count++
            if (stopExecuting && el.contains(progress)) el.removeChild(progress)
            if (theSettings.framesCount === count) {
              resetRollbackTimer(settings, canvas)
              allImagesAreLoaded = true
              if (el.contains(progress)) el.removeChild(progress)
            }

            // Add image to the arrays.
            theSettings.images[theIndex] = theImage
            theSettings.arrived[theIndex] = true
            loadedImagesCount++
            onLoadedHandler()
            resolve()
          }
        })(settings, canvas, image, index)
        image.src = obj.image

        if (firstImage == null) {
          firstImage = image
          settings.currentImg = image
          if (settings.changeCursor === 'swing') {
            canvas.style.cursor = 'default'
          } else {
            canvas.style.cursor = 'ew-resize'
          }
          doAnimate = doAnimate || settings.behavior === 'swing'
          // wait for 100ms to preload some first images
          setTimeout(() => {
            attachDynamicListeners(settings, canvas, doAnimate)
          }, 100)
        }
        settings.targetDeltaTime = settings.targetPeriod / of
        settings.deltaTime = settings.targetDeltaTime
      })
    }

    prepareImages()
    ;(function () {
      if (!Promise.map) setPromiseMap()
      Promise.map(
        arrayOfImages,
        (img) => {
          return incrementalParse(img)
        },
        { concurrency: threads }
      )
    })()
  }

  const prepareImages = (step = 25, addedImages = []) => {
    let index = arrayOfImages[0] ? Math.round(step) : 0
    while (index < settings.framesCount) {
      if (!addedImages.includes(index)) {
        const image = getImagePath(settings, index + 1)
        const obj = {
          image: image.toLowerCase(),
          index: index + 1,
          of: settings.framesCount
        }
        arrayOfImages.push(obj)
        addedImages.push(index)
      }
      index += Math.round(step)
    }
    if (arrayOfImages.length < settings.framesCount)
      prepareImages(Math.round(step) / 2, addedImages)
  }

  const getImagePath = (setting, index) => {
    if (settings.itemType === 'stone')
      return `${settings.host}/${settings.itemType}/${settings.id}/video360/${
        settings.id
      }-video360-${numeral(index).format(integerFormat)}.jpg${getImageSize(
        settings.size
      )}`
    if (settings.itemType === 'setting')
      return `${settings.host}/${settings.itemType}/${settings.id}/video360/${
        settings.id + settings.metalTypeCode
      }-video360-${numeral(index).format(integerFormat)}.jpg${getImageSize(
        settings.size
      )}`
    if (settings.itemType === 'jewelry')
      return `${settings.host}/${settings.itemType}/${settings.id}/video360/${
        settings.id
      }-video360-${numeral(index).format(integerFormat)}.jpg${getImageSize(
        settings.size
      )}`
    if (settings.itemType === 'band')
      return `${settings.host}/${settings.itemType}/${settings.id}/video360/${
        settings.id + settings.metalTypeCode
      }-video360-${numeral(index).format(integerFormat)}.jpg${getImageSize(
        settings.size
      )}`
    return ''
  }

  const getImageSize = (size) => {
    if (size === 'full') return '?d=600x600'
    if (size === 'medium') return '?d=365x365'
    if (size === 'small') return '?d=200x200'
    return '?d=600x600'
  }

  const getImage = (settings, canvas) => {
    xmlhttp.onreadystatechange = () => {
      if (xmlhttp.readyState === 1 && start === -1) {
        start = Date.now()
      } else if (xmlhttp.readyState === 2 && ping === -1) {
        ping = Date.now()
      } else if (
        xmlhttp.readyState === 4 &&
        xmlhttp.status === 200 &&
        done === -1
      ) {
        done = Date.now()
        start = -1
        ping = -1
        done = -1

        const result = JSON.parse(xmlhttp.responseText)
        const image = new Image()
        image.src = result.image
        canvasRenderImage(canvas, image)
        image.className += ' video360 cover'
        attachStaticListeners(settings, canvas)
      }
    }

    xmlhttp.open(
      'GET',
      settings.host + settings.id + '/' + settings.prefix + '/' + '1'
    )
    xmlhttp.send()
  }

  const onLoadedHandler = () => {
    if (loadedEventEmited) return
    loadedEventEmited = true
    el.dispatchEvent(loadedEvent)
  }

  const setCover = (settings, canvas) => {
    const image = new Image()
    image.onload = () => {
      if (settings.behavior === 'swing') return // cover not needed if behavior is swing
      canvasRenderImage(canvas, image)
      onLoadedHandler()
    }
    image.src = settings.cover
    canvas.className += ' video360 cover'
    attachStaticListeners(settings, canvas)
  }

  // This is the easiest way to have default options, while not modifying the input: settings becomes an internal.
  const defaultSettings = {
    size: 'Full', // supported: Full, Medium, Small
    behavior: 'swing', // supported: swing, mouseover
    backgroundColor: 'white', // any css supported expression, default white
    progressColor: getSiteColor('darkPrimary'), // any css supported expression, default sapphire
    progressHeight: '1', // in pixels, default 3
    targetPeriod: 25000, // time to show all images in sequence, in milliseconds
    speed: 1,
    startOn: 'mouseover', // supported: mouseover, mousedown, auto
    onclick: null, // function(id, event)
    changeCursor: 'mouseover', // supported: mouseover, swing
    host: '', // api url
    itemType: '', // stone, setting, etc
    framesCount: 0,
    onSwipe: null,
    cover: '',
    prefix: '',
    positionEvent: false,
    dream: false
  }

  const stateInitialization = {
    // This is state initialization
    dX: 0,
    direction: -1,
    deltaTime: 125,
    periodTime: 5000,
    prevFrameTime: 0,
    timeoutID: 0,
    moved: false,
    offContext: document.createElement('canvas').getContext('2d')
  }

  const settings = {
    ...defaultSettings,
    ...stateInitialization,
    ...options
  }

  el.style.backgroundColor = settings.backgroundColor

  el.classList.remove('video360')
  el.classList.add('video360')

  // Create canvas and append it to el
  const canvas = document.createElement('canvas')

  el.appendChild(canvas)

  if (settings.cover) {
    setCover(settings, canvas)
  } else {
    getImage(settings, canvas)
  }

  checkYAxisDrag()

  const pause = () => {
    pauseAnimation = true
  }

  const play = () => {
    pauseAnimation = false
  }

  const setPosition = ({ direction, currentIdx }) => {
    settings.direction = direction
    settings.currentIdx = currentIdx
  }

  const reset = () => {
    settings.direction = -1
    settings.currentIdx = 0
  }

  const kill = () => {
    stopExecuting = true
    settings.images = []
    settings.arrived = []
  }

  return { kill, pause, play, reset, setPosition }
}
