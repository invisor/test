export default class StoneAnimation {
  constructor(el, anchor, options) {
    this.el = el
    this.offsetTop = el.offsetTop
    this.anchor = anchor
    this.start = options.start
    this.end = options.end
    this.directionBottom = false
    this.scrollPos = Number.POSITIVE_INFINITY
    this.bottomBorder = this.windowHeight * (1 - this.end / 100)
    this.framesCount = options.framesCount

    this.length =
      this.anchor.getBoundingClientRect().top -
      this.el.getBoundingClientRect().top -
      this.anchor.clientHeight / 2 -
      10

    const anchorTopOffset = this.anchor.getBoundingClientRect().top
    if (this.bottomBorder > anchorTopOffset) {
      this.bottomBorder = anchorTopOffset
    }
  }

  init() {
    if (!window) return
    this.addListeners()
  }

  get currentFrame() {
    const path = this.bottomBorder - this.anchorPosition
    const percent = path / this.windowLength
    const frame = Math.round(this.framesCount * percent)
    if (frame < 1) return 1
    if (frame > this.framesCount) return this.framesCount
    return frame
  }

  get windowHeight() {
    return window.innerHeight
  }

  get topBorder() {
    return (this.windowHeight * this.start) / 100
  }

  get inRange() {
    return (
      this.topBorder < this.anchorPosition &&
      this.anchorPosition < this.bottomBorder
    )
  }

  get windowLength() {
    return this.bottomBorder - this.topBorder
  }

  get anchorPosition() {
    const rect = this.anchor.getBoundingClientRect()
    return (rect.bottom - rect.top) / 2 + rect.top
  }

  get elTopPos() {
    if (this.inRange) {
      const path = this.bottomBorder - this.anchorPosition
      const percent = path / this.windowLength
      return percent * this.length
    }
    if (this.anchorPosition > this.bottomBorder) return 0
    if (this.anchorPosition < this.topBorder) return this.length
  }

  destroy() {
    window.removeEventListener('scroll', this.onScrollHandler, false)
  }

  addListeners() {
    window.addEventListener('scroll', this.onScrollHandler.bind(this), false)
  }

  detectScrollDirection() {
    this.directionBottom =
      document.body.getBoundingClientRect().top <= this.scrollPos
    this.scrollPos = document.body.getBoundingClientRect().top
  }

  onScrollHandler(e) {
    this.detectScrollDirection()

    this.el.dispatchEvent(
      new CustomEvent('frame', { detail: this.currentFrame })
    )
    this.el.style.top = `${this.offsetTop + this.elTopPos}px`
  }
}
