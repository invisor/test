export default class Zoom {
  constructor(el, options) {
    this.el = el
    this.userOptions = options
    this.zoomArea = null
    this.zoomAreaWrapper = null
    this.preventContextLayer = null
    this.zoomLens = null
    this.clonedSource = null
    this.elBoundingRect = null
    this.allowTouchEvents = false
    this.zoomStarted = false
    this.cx = 1
    this.cy = 1

    this.updateAreasPositions = this.updateAreasPositions.bind(this)
    this.moveLens = this.moveLens.bind(this)
    this.startZoom = this.startZoom.bind(this)
    this.stopZoom = this.stopZoom.bind(this)
    this.update = this.update.bind(this)
    this.startTouchZoom = this.startTouchZoom.bind(this)
    this.touchMoveLens = this.touchMoveLens.bind(this)
    this.stopTouchZoom = this.stopTouchZoom.bind(this)
  }

  get defaultOptions() {
    return {
      inline: false,
      zoomFactor: 2,
      touchDelay: 300,
      zoomAreaWidth: 500,
      zoomAreaHeight: 500,
      zoomAreaWrapperClass: 'zoom-area--wrapper',
      zoomAreaWrapperInlineClass: 'zoom-area--inline',
      zoomAreaClass: 'zoom-area',
      zoomLensClass: 'zoom-lens',
      preventContextClass: 'prevent-context'
    }
  }

  get options() {
    return {
      ...this.defaultOptions,
      ...this.userOptions
    }
  }

  init() {
    this.elBoundingRect = this.el.getBoundingClientRect()
    this.createPreventContextLayer()
    this.addEventListeners()
  }

  update() {
    this.elBoundingRect = this.el.getBoundingClientRect()
  }

  destroy() {
    this.removeEventListeners()
    this.removeLensArea()
    this.removeZoomAreaWrapper()
    this.removePreventContextLayer()
  }

  updateAreasPositions(e) {
    this.calcZoomAreaWrapperPos()
    this.moveLens(e)
  }

  calcZoomAreaWrapperPos() {
    this.update()
    if (!this.zoomAreaWrapper) return
    this.zoomAreaWrapper.style.top =
      this.elBoundingRect.top + document.documentElement.scrollTop + 'px'
    this.zoomAreaWrapper.style.left = this.options.inline
      ? this.elBoundingRect.left + 'px'
      : this.elBoundingRect.left + this.el.offsetWidth + 50 + 'px'
  }

  createZoomAreaFull() {
    this.createZoomAreaWrapper()
    this.createZoomArea()
    this.addSourceToZoomArea()
  }

  createPreventContextLayer() {
    this.preventContextLayer = document.createElement('div')
    this.preventContextLayer.style.width = '100%'
    this.preventContextLayer.style.height = '100%'
    this.preventContextLayer.style.position = 'absolute'
    this.preventContextLayer.style.top = 0
    this.preventContextLayer.style.left = 0
    this.preventContextLayer.style.zIndex = 999

    this.preventContextLayer.style.touchAction = 'none'
    this.preventContextLayer.style.userSelect = 'none'

    this.preventContextLayer.setAttribute(
      'class',
      this.options.preventContextClass
    )
    this.el.append(this.preventContextLayer)
  }

  createZoomAreaWrapper() {
    this.zoomAreaWrapper = document.createElement('div')
    const classes = [this.options.zoomAreaWrapperClass]
    if (this.options.inline)
      classes.push(this.options.zoomAreaWrapperInlineClass)
    this.zoomAreaWrapper.classList.add(...classes)

    this.zoomAreaWrapper.style.width = this.options.inline
      ? this.el.offsetWidth + 'px'
      : this.options.zoomAreaWidth + 'px'
    this.zoomAreaWrapper.style.height = this.options.inline
      ? this.el.offsetHeight + 'px'
      : this.options.zoomAreaHeight + 'px'
    this.calcZoomAreaWrapperPos()
    document.body.append(this.zoomAreaWrapper)
  }

  createZoomArea() {
    this.zoomArea = document.createElement('div')
    this.zoomArea.setAttribute('class', this.options.zoomAreaClass)
    this.zoomAreaWrapper.append(this.zoomArea)
  }

  createZoomLens(e) {
    this.zoomLens = document.createElement('div')
    this.zoomLens.setAttribute('class', this.options.zoomLensClass)
    const xRatio = this.zoomAreaWrapper.offsetWidth / this.el.offsetWidth
    const yRatio = this.zoomAreaWrapper.offsetHeight / this.el.offsetHeight
    this.zoomLens.style.width =
      (this.el.offsetWidth / this.options.zoomFactor) * xRatio + 'px'
    this.zoomLens.style.height =
      (this.el.offsetHeight / this.options.zoomFactor) * yRatio + 'px'
    this.el.parentElement.insertBefore(this.zoomLens, this.el)
    this.updatePositions(e)
  }

  removeLensArea() {
    if (this.zoomLens) this.zoomLens.remove()
  }

  removePreventContextLayer() {
    if (this.preventContextLayer) this.preventContextLayer.remove()
  }

  removeZoomAreaWrapper() {
    if (this.zoomAreaWrapper) this.zoomAreaWrapper.remove()
  }

  addSourceToZoomArea() {
    this.clonedSource = this.deepCloneWithStyles(this.el)

    this.zoomArea.style.width =
      this.el.offsetWidth * this.options.zoomFactor + 'px'
    this.zoomArea.style.height =
      this.el.offsetHeight * this.options.zoomFactor + 'px'

    this.zoomArea.append(this.clonedSource)
  }

  deepCloneWithStyles(node) {
    const style = document.defaultView.getComputedStyle(node, null)
    const clone = node.cloneNode(false)
    if (clone.style && style.cssText) clone.style.cssText = style.cssText
    clone.style.width = '100%'
    clone.style.height = '100%'
    clone.style.pointerEvents = 'none'
    clone.style.maxHeight = 'none' // safari fix
    clone.style.maxBlockSize = 'none' // safari fix
    if (node.tagName === 'IMG')
      node.addEventListener('contextmenu', this.preventContextMenu, false)
    for (const child of node.childNodes)
      clone.appendChild(this.deepCloneWithStyles(child))
    return clone
  }

  applyStylesDeep(el, index = 0) {
    el.style.cssText = document.defaultView.getComputedStyle(
      this.el.childNodes[index],
      ''
    ).cssText
    el.style.width = '100%'
    el.style.height = '100%'
    el.childNodes.forEach((node, index) => {
      this.applyStylesDeep(node, index)
    })
  }

  addEventListeners() {
    this.preventContextLayer.addEventListener('mousemove', this.moveLens, false)
    this.preventContextLayer.addEventListener(
      'mouseenter',
      this.startZoom,
      false
    )
    this.preventContextLayer.addEventListener('mouseout', this.stopZoom, false)
    this.preventContextLayer.addEventListener('resize', this.update, false)
    this.preventContextLayer.addEventListener(
      'contextmenu',
      this.preventContextMenu,
      false
    )
    this.preventContextLayer.addEventListener(
      'touchstart',
      this.startTouchZoom,
      false
    )
    this.preventContextLayer.addEventListener(
      'touchmove',
      this.touchMoveLens,
      false
    )
    this.preventContextLayer.addEventListener(
      'touchend',
      this.stopTouchZoom,
      false
    )
    window.addEventListener('scroll', this.updateAreasPositions, false)
  }

  removeEventListeners() {
    this.preventContextLayer.removeEventListener(
      'mousemove',
      this.moveLens,
      false
    )
    this.preventContextLayer.removeEventListener(
      'mouseenter',
      this.startZoom,
      false
    )
    this.preventContextLayer.removeEventListener(
      'mouseout',
      this.stopZoom,
      false
    )
    this.preventContextLayer.removeEventListener(
      'touchstart',
      this.startTouchZoom,
      false
    )
    this.preventContextLayer.removeEventListener(
      'touchmove',
      this.touchMoveLens,
      false
    )
    this.preventContextLayer.removeEventListener(
      'touchend',
      this.stopTouchZoom,
      false
    )
    this.preventContextLayer.removeEventListener('resize', this.update, false)
    this.preventContextLayer.removeEventListener(
      'contextmenu',
      this.preventContextMenu,
      false
    )
    window.removeEventListener('scroll', this.updateAreasPositions, false)
  }

  preventContextMenu(e) {
    e.preventDefault()
    e.stopPropagation()
    return false
  }

  moveLens(e) {
    e.preventDefault()
    e.stopPropagation()
    if (!this.zoomLens) return
    this.updatePositions(e)
  }

  touchMoveLens(e) {
    if (!this.allowTouchEvents) return
    this.moveLens(e)
  }

  calcLensPosition(e) {
    // Get the cursor's x and y positions
    const pos = this.getCursorPos(e)

    // Calculate the position of the lens
    let x = pos.x - this.zoomLens.offsetWidth / 2
    let y = pos.y - this.zoomLens.offsetHeight / 2
    // Prevent the lens from being positioned outside the image
    if (x > this.el.offsetWidth - this.zoomLens.offsetWidth) {
      x = this.el.offsetWidth - this.zoomLens.offsetWidth
    }
    if (x < 0) {
      x = 0
    }
    if (y > this.el.offsetHeight - this.zoomLens.offsetHeight) {
      y = this.el.offsetHeight - this.zoomLens.offsetHeight
    }
    if (y < 0) {
      y = 0
    }
    return { x, y }
  }

  updatePositions(e) {
    const { x, y } = this.calcLensPosition(e)
    // Set the position of the lens
    this.zoomLens.style.left = x + 'px'
    this.zoomLens.style.top = y + 'px'

    this.zoomArea.style.transform = `translate(-${x * this.cx}px, -${y *
      this.cy}px)`
  }

  startZoom(e) {
    if (this.zoomStarted) return
    this.zoomStarted = true
    this.createZoomAreaFull()
    this.createZoomLens(e)
    this.calcZoomAreaWrapperPos()
    this.cx = this.zoomArea.offsetWidth / this.el.offsetWidth
    this.cy = this.zoomArea.offsetHeight / this.el.offsetHeight
  }

  startTouchZoom(e) {
    e.preventDefault() // prevent mouse events simulation
    this.timer = setTimeout(() => {
      this.allowTouchEvents = true
      this.startZoom(e)
    }, this.options.touchDelay)
  }

  stopZoom() {
    this.zoomStarted = false
    this.removeZoomAreaWrapper()
    this.removeLensArea()
  }

  stopTouchZoom(e) {
    e.preventDefault()
    this.allowTouchEvents = false
    if (this.timer) clearTimeout(this.timer)
    this.stopZoom()
  }

  getCursorPos(e) {
    let x = 0
    let y = 0
    e = e || window.event
    const xOffset = ['touchstart', 'touchmove', 'touchend'].includes(e.type)
      ? e.touches[0].pageX
      : e.pageX
    const yOffset = ['touchstart', 'touchmove', 'touchend'].includes(e.type)
      ? e.touches[0].pageY
      : e.pageY
    // Calculate the cursor's x and y coordinates, relative to the image
    x = xOffset - this.elBoundingRect.left
    y = yOffset - this.elBoundingRect.top
    // Consider any page scrolling
    x = x - window.pageXOffset
    y = y - window.pageYOffset
    return { x, y }
  }
}
