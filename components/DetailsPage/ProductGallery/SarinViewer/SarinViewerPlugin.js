﻿/*
// Sarin .srn Web 3D Viewer in JS - Based off of Robert Strickland v0.72
// Modified by: John Richardson // Swank Internet Business Solutions LLC // 2017-02-22
// Original Credits Follow...
//
// Sarine .srn web 3D viewer in JavaScript using HTML canvas element
// for The Natural Sapphire Company
// by Robert W. Strickland
//
// Version 0.72 Mon Dec 19 08:17:25 CST 2016
//   Tweaked the mouse handlers
//
// Version 0.71 Sat Dec 17 11:49:48 CST 2016
//   Turned off some features for cabochons
//
// Version 0.70 Fri Dec 16 12:53:03 CST 2016
//   Added to table of dimensions
//   This version reads from a server or client .SRN file.
//   Added let ratio pixel scaling for high resolution displays
//
// Version 0.61 Fri Dec  9 00:58:52 CST 2016
//   Removed Math.hypot() calls (not supported by Explorer)
//   Fixed some undeclared variables
//
// Version 0.6 Thu Dec  8 23:13:32 CST 2016
//   Added trackball axes, HTML table tags in output
//
// Version 0.5 Wed Dec  7 10:43:36 CST 2016
//
*/

/* eslint-disable no-alert, no-unused-vars */

export default function(el, options) {
  ;(function() {
    if (typeof window.CustomEvent === 'function') return false

    function CustomEvent(event, params) {
      params = params || {
        bubbles: false,
        cancelable: false,
        detail: undefined
      }
      const evt = document.createEvent('CustomEvent')
      evt.initCustomEvent(
        event,
        params.bubbles,
        params.cancelable,
        params.detail
      )
      return evt
    }
    CustomEvent.prototype = window.Event.prototype

    window.CustomEvent = CustomEvent
  })()

  let loaded
  const loadedEvent = new CustomEvent('loaded')
  let dimension
  let maxDimension = 900
  let padding = 40
  let ratio = 2
  let ix = 1.0
  let iy = 0.0
  let iz = 0.0
  let jx = 0.0
  let jy = 1.0
  let jz = 0.0
  let kx = 0.0
  let ky = 0.0
  let kz = 0.0
  let trackball = rolycyl // try rolycyl and rolypoly to see which trackball you prefer.
  const srnHeight = 0.0 // Height of stone from Sarine file
  let cabochon = false
  const bgColor = '#FFFFFF' // white
  // let hltColor = '#FFFF00' // yellow
  const Red = '#A00000' // Dark Red. girdle highlight color
  const DEG = '\u00B0' // Unicode degree symbol (little superscript circle)
  let tht // erase box text height
  let col = [] /* array of 255 colors from black to stone color plus white */
  const PIXSCL = 1.0 // bigger than one will enlarge several things. Obsolete?
  const SNAP3D = 0.35
  let snap = true
  let AHWID = 3 * PIXSCL // /** arrowhead half width in pixels */
  let AHLEN = 9 * PIXSCL /** arrowhead half length in pixels */
  let DIMBAR = 7 * PIXSCL /** dimension bar half length, pixels */
  let labeledFacet = -1
  let extraInfo = false
  let frontOnly = false
  let pxlPerMm = 100
  let nVert
  let xvert = []
  let yvert = []
  let zvert = []
  const angle = []
  const azi = []
  let nFacet
  const facet = []
  let nEdge
  const f0 = []
  const f1 = []
  const v0 = []
  const v1 = []
  const anorm = []
  const bnorm = []
  const cnorm = []
  const dnorm = []
  const cnorm1 = []
  const CRN = 0
  const GRD = 1
  const PAV = 2
  const facetType = [] // PAV, CRN or GRD
  const xvertP = []
  const yvertP = []
  const pavGirdLoThrsh = Math.cos((60.0 * Math.PI) / 180.0)
  const crnGirdLoThrsh = Math.cos((55.0 * Math.PI) / 180.0)
  const pavGirdThrsh = Math.cos((75.0 * Math.PI) / 180.0)
  const crnGirdThrsh = Math.cos((75.0 * Math.PI) / 180.0)
  let xmin, xmax, ymin, ymax, zmin, zmax
  let maxRadius
  let minGirdle = 0.0
  let maxGirdle = 0.0
  let xcenmin, xcenmax
  let ycenmin, ycenmax
  let tableFacet = -1
  let txmin, txmax, tymin, tymax, tz0
  // let avgTableDia = 0.0
  let minTableDia = 0.0
  let maxTableDia = 0.0
  let txcen, tycen, tzcen
  let culetFacet = -1
  let cxmin, cxmax, cymin, cymax, cz0
  let cxcen, cycen, czcen
  let zTopGirdAvg, zBotGirdAvg
  let totalDepth
  const xPol = []
  const yPol = []
  let ncg /** number of crown girdle outline points */
  let xcg = [] /** x coordinate of crown girdle outline points */
  let ycg = [] /** y coordinate of crown girdle outline points */
  let zcg = [] /** z coordinate of crown girdle outline points */
  let fcg = [] /** Facet numbers for crown girdle outlines */
  let npg /** number of pavilion girdle outline points */
  let xpg = [] /** x coordinate of pavilion girdle outline points */
  let ypg = [] /** y coordinate of pavilion girdle outline points */
  let zpg = [] /** z coordinate of pavilion girdle outline points */
  let fpg = [] /** Facet numbers for pavilion girdle outlines */
  const xcgP = []
  const ycgP = []
  const xpgP = []
  const ypgP = []
  let w, h, wBy2, hBy2
  let volume
  const dang = 0.1
  let xcos = 1.0
  let ycos = 1.0
  let xsin = 0.0
  let ysin = 0.0
  const ncos = 20

  let xpgm = 0
  let ypgm = 0
  let zpgm = 0
  const xcgm = 0
  const ycgm = 0
  const zcgm = 0
  let rdim = 0
  let xdim0 = 0
  let ydim0 = 0
  let zdim0 = 0
  let xdim1 = 0
  let ydim1 = 0
  let zdim1 = 0
  let xMouse = 0
  let yMouse = 0

  let costab = []
  let m00, m01, m02, m10, m11, m12, m20, m21, m22
  let bm00, bm01, bm02, bm10, bm11, bm12, bm20, bm21, bm22
  let xrmin = 1e10
  let xrmax = -1e10
  let yrmin = 1e10
  let yrmax = -1e10
  let trackballShown = false
  let mouseDownStartTime = new Date().getTime()
  let mousedown = false
  const mousePrev = {
    x: 0,
    y: 0
  }
  let mouseCur = {
    x: 0,
    y: 0
  }
  let center = {
    x: 0,
    y: 0,
    z: 0
  }
  let radius = 1

  const settings = {
    url: '',
    autoResize: true,
    maxDimension: 900,
    padding: 100,
    ratio: 2,
    clickFacet: true,
    ...options
  }

  // Assign Vars
  loaded = false
  // sarinObj = this
  const sarinObj = el
  // cvs = document.getElementById(sarinObj.attr('id'))
  const cvs = sarinObj
  const ctx = cvs.getContext('2d')

  // Defaults
  const fname = settings.url
  trackball = rolypoly // try rolycyl and rolypoly to see which trackball you prefer.
  maxDimension = settings.maxDimension
  padding = settings.padding
  ratio = settings.ratio

  // Calculate Dimensions and Load Sarin 3D Object
  loadServerSrn()

  // Add Event Listeners
  if (settings.autoResize) {
    let SarinResizeTimeout
    window.addEventListener(
      'resize',
      function(evt) {
        clearTimeout(SarinResizeTimeout)
        SarinResizeTimeout = setTimeout(loadServerSrn, 150)
      },
      false
    )
  }
  cvs.addEventListener(
    'mousemove',
    function(evt) {
      mouseCur = getMousePos(cvs, evt)
      if (mousedown) {
        const r = trackball(mousePrev, mouseCur, center, radius)
        if (r !== undefined) {
          rotate(r)
          ctx.clearRect(0, 0, cvs.width, cvs.height)
          paint(ctx)
          drawball(ctx)
          trackballShown = true
        }
        mousePrev.x = mouseCur.x
        mousePrev.y = mouseCur.y
      } else if (trackballShown) {
        mouseCur = getMousePos(cvs, evt)
        mousedown = false
        ctx.clearRect(0, 0, cvs.width, cvs.height)
        paint(ctx)
        trackballShown = false
      }
    },
    false
  )
  cvs.addEventListener(
    'mousedown',
    function(evt) {
      mousedown = evt.which & 1
      if (!mousedown) {
        evt.preventDefault()
        return
      }
      mouseDownStartTime = new Date().getTime()
      mouseCur = getMousePos(cvs, evt)
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      paint(ctx)
      drawball(ctx)
      trackballShown = true
      mousePrev.x = mouseCur.x
      mousePrev.y = mouseCur.y
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'mouseup',
    function(evt) {
      mousedown = false
      mouseCur = getMousePos(cvs, evt)
      const elapsed = new Date().getTime() - mouseDownStartTime
      if (
        elapsed < 250 &&
        Math.abs(mouseCur.x - mousePrev.x) < 3 * ratio &&
        Math.abs(mouseCur.y - mousePrev.y) < 3 * ratio
      ) {
        if (settings.clickFacet) {
          mouseClicked(mouseCur)
        }
      }
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      paint(ctx)
      trackballShown = false
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'mouseenter',
    function(evt) {
      mousedown = false
      mouseCur = getMousePos(cvs, evt)
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      mousePrev.x = mouseCur.x
      mousePrev.y = mouseCur.y
      paint(ctx)
      trackballShown = false
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'mouseleave',
    function(evt) {
      mousedown = false
      mouseCur = getMousePos(cvs, evt)
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      paint(ctx)
      trackballShown = false
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'touchmove',
    function(evt) {
      mouseCur = getTouchPos(cvs, evt)
      const r = trackball(mousePrev, mouseCur, center, radius)
      if (r !== undefined) {
        rotate(r)
        ctx.clearRect(0, 0, cvs.width, cvs.height)
        paint(ctx)
        drawball(ctx)
        trackballShown = true
      }
      mousePrev.x = mouseCur.x
      mousePrev.y = mouseCur.y
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'touchstart',
    function(evt) {
      mouseCur = getTouchPos(cvs, evt)
      mouseDownStartTime = new Date().getTime()
      mousePrev.x = mouseCur.x
      mousePrev.y = mouseCur.y
      evt.preventDefault()
    },
    false
  )
  cvs.addEventListener(
    'touchend',
    function(evt) {
      mouseCur = getTouchPos(cvs, evt)
      const elapsed = new Date().getTime() - mouseDownStartTime
      if (
        elapsed < 330 &&
        Math.abs(mouseCur.x - mousePrev.x) < 6 * ratio &&
        Math.abs(mouseCur.y - mousePrev.y) < 6 * ratio
      ) {
        mouseClicked(mouseCur)
      }
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      paint(ctx)
      trackballShown = false
      mousePrev.x = mouseCur.x
      mousePrev.y = mouseCur.y
      evt.preventDefault()
    },
    false
  )

  function loadServerSrn() {
    // Calculate Dimensions
    dimension =
      (window.innerWidth > window.innerHeight
        ? window.innerHeight
        : window.innerWidth) - padding
    if (dimension > maxDimension) {
      dimension = maxDimension - padding
    }
    cvs.width = dimension * ratio
    cvs.height = dimension * ratio
    cvs.style.width = dimension + 'px'
    cvs.style.height = dimension + 'px'
    center = {
      x: cvs.width / 2,
      y: cvs.height / 2
    }
    radius = cvs.width / 2 - 60 * PIXSCL
    costab = []
    for (let i = 0; i < ncos; i++) {
      const ang = i * dang
      costab[i] = Math.sqrt(1.0 - ang * ang)
    }

    // Load 3D Object
    const req = new XMLHttpRequest()
    if (!loaded) {
      req.onreadystatechange = function() {
        if (req.readyState === 4 && req.status === 200) {
          document.body.style.cursor = 'progress'
          parseSarin(req.responseText, cvs.width, cvs.height)
          document.body.style.cursor = 'default'
          ctx.clearRect(0, 0, cvs.width, cvs.height)
          rotate()
          paint(ctx)
          trackballShown = false
          cvs.dispatchEvent(loadedEvent)
        }
      }
      req.open('GET', fname, true)
      req.send()
      loaded = true
    } else {
      parseSarin(req.responseText, cvs.width, cvs.height)
      ctx.clearRect(0, 0, cvs.width, cvs.height)
      rotate()
      paint(ctx)
      trackballShown = false
    }
  }

  function parseSarin(srn, width, height) {
    w = width
    h = height
    wBy2 = w / 2
    hBy2 = h / 2
    labeledFacet = -1
    nFacet = 0
    nVert = 0
    nEdge = 0
    const lines = srn ? srn.split('\n') : [] // Split file into lines
    let i, j
    let caratWeight = 0
    let stoneColor = 'Undefined'
    let stoneClarity = 'Undefined'
    let stoneName = 'Undefined'
    let foundmesh = false
    let srnHeight = 0
    let rgb
    for (i = 0; i < lines.length; i++) {
      if (lines[i].includes('stoneWeight')) {
        caratWeight = lines[i].split(/[=,;]/)[1]
      }
      if (lines[i].includes('stoneName')) {
        stoneName = lines[i].split(/[=,;]/)[1]
      }
      if (lines[i].includes('stoneColor')) {
        stoneColor = lines[i].split(/[=,;]/)[1]
        rgb = rgbcolor(stoneColor)
      }
      if (lines[i].includes('stoneClarity')) {
        stoneClarity = lines[i].split(/[=,;]/)[1]
      }
      if (lines[i].includes('stoneHeight')) {
        srnHeight = lines[i].split(/[=,;]/)[1]
      }
      if (lines[i].match(/Mesh/)) {
        foundmesh = true
        break
      }
    }
    srnHeight = Number(srnHeight)
    caratWeight = Number(caratWeight)
    i++
    if (lines[i]) nVert = lines[i].split(';')[0]
    nVert = Number(nVert)
    i++
    let szmax = -1e10
    let szmin = 1e10
    xvert = []
    yvert = []
    zvert = []
    for (j = 0; j < nVert; j++, i++) {
      let v = []
      if (lines[i]) v = lines[i].split(';')
      xvert.push(Number(v[0]))
      yvert.push(Number(v[1]))
      const z = Number(v[2])
      zvert.push(z)
      if (z < szmin) szmin = z
      if (z > szmax) szmax = z
    }
    let nf = 0
    if (lines[i]) nf = lines[i].split(';')[0]
    nf = Number(nf)
    nFacet = nf
    i++
    for (j = 0; j < nf; j++, i++) {
      let verts = []
      if (lines[i]) verts = lines[i].split(/[;,]/)
      let ne = verts[0]
      ne = Number(ne)
      let k
      facet[j] = []
      for (k = 0; k < ne; k++) {
        facet[j].push(Number(verts[k + 1]))
      }
    }
    let scale = 1000
    if (srnHeight !== 0) {
      scale = (szmax - szmin) / srnHeight
    }
    if (scale < 900 || scale > 1100) {
      scale = 1000
    }
    const zcen = (szmin + szmax) / 2
    for (j = 0; j < nVert; j++) {
      xvert[j] /= -scale
      yvert[j] /= scale
      zvert[j] -= zcen
      zvert[j] /= -scale
    }
    initEdges()
    unitNormals(false) // Calculate unit normal vectors for each facet
    domeCheck() // Check to see if stone is a cabochon
    centerOutline(false)
    const isRound = checkIfRound()
    if (isRound && !cabochon) {
      orientRound()
    } else {
      orientStone()
    }
    unitNormals(true) // redo constant term of plane eqns. (dnorm)
    // Calculate elevation angles
    for (i = 0; i < nFacet; i++) {
      if (Math.abs(anorm[i]) < 1e-6 && Math.abs(bnorm[i]) < 1e-6) {
        azi[i] = 0
      } else {
        azi[i] = (Math.atan2(anorm[i], bnorm[i]) * 180) / Math.PI
      }
      angle[i] = cnorm[i] > 1 ? 0 : (Math.acos(cnorm[i]) * 180) / Math.PI
      while (angle[i] > 90) {
        angle[i] -= 180
      }
    }
    // Identify girdle facets
    if (cabochon) {
      for (i = 0; i < nFacet; i++) {
        facetType[i] = cnorm[i] > 0 ? PAV : CRN
      }
    } else {
      for (i = 0; i < nFacet; i++) {
        if (cnorm[i] < crnGirdThrsh && -cnorm[i] < pavGirdThrsh) {
          facetType[i] = GRD // High angle. Definitely a girdle
          continue
        }
        if (cnorm[i] < crnGirdLoThrsh && -cnorm[i] < pavGirdLoThrsh) {
          // Maybe a girdle, but only if it is short. Measure height.
          let fzmax = -1e10
          let fzmin = 1e10
          for (j = 0; j < facet[i].length; j++) {
            const zv = zvert[facet[i][j]]
            if (zv > fzmax) {
              fzmax = zv
            }
            if (zv < fzmin) {
              fzmin = zv
            }
          }
          if (fzmax - fzmin < srnHeight * 0.1) {
            // short: facet height < 10% of stone height
            facetType[i] = GRD
            continue
          }
        }
        facetType[i] = cnorm[i] > 0 ? CRN : PAV
      }
      // Short test above can sometimes fail.
      // No girdle facet can be an island completely surrounded by crown or
      // pavilion facets.
      for (i = 0; i < nFacet; i++) {
        if (facetType[i] === GRD) {
          let island = true
          let ntype = -1
          let ftype
          for (j = 0; j < nEdge; j++) {
            if (f0[j] !== i && f1[j] !== i) {
              continue
            }
            if (f0[j] === i) {
              ftype = facetType[f1[j]]
              if (ntype === -1) {
                ntype = ftype
              }
              if (ftype !== ntype) {
                island = false
                break
              }
            } else {
              ftype = facetType[f0[j]]
              if (ntype === -1) {
                ntype = ftype
              }
              if (ftype !== ntype) {
                island = false
                break
              }
            }
          }
          if (island) {
            facetType[i] = ntype
          }
        }
      }
    }
    dimensions() // Calculate dimensions
    // Initialize the rotation matrix coefficients
    const xang = -Math.PI / 2
    const yang = 0
    const ct = Math.cos(xang)
    const st = Math.sin(xang)
    const cf = Math.cos(yang)
    const sf = Math.sin(yang)
    xsin = 0
    ysin = 0
    xcos = 1
    ycos = 1
    m00 = cf
    m01 = 0
    m02 = sf
    m10 = st * sf
    m11 = ct
    m12 = -st * cf
    m20 = -ct * sf
    m21 = st
    m22 = ct * cf
    bm00 = m00
    bm01 = m01
    bm02 = m02
    bm10 = m10
    bm11 = m11
    bm12 = m12
    bm20 = m20
    bm21 = m21
    bm22 = m22
    // Scale so maximum radius of stone defines a sphere touching border.
    // Calculate maximum stone radius
    maxRadius = 0
    for (i = 0; i < nVert; i++) {
      const vv = xvert[i] * xvert[i] + yvert[i] * yvert[i] + zvert[i] * zvert[i]
      if (vv > maxRadius) {
        maxRadius = vv
      }
    }
    maxRadius = Math.sqrt(maxRadius)
    // Calculate plot scale in pixels per mm. Leave 50 pixel border for
    // dimension lines and labels
    pxlPerMm = (wBy2 - 50 * PIXSCL) / maxRadius
    // Find stone volume
    volume = findVolume(null)
    const specificGravity = (200 * caratWeight) / volume
  }

  function rolypoly(p0, p1, center, rad) {
    if (
      p0 === undefined ||
      p1 === undefined ||
      center === undefined ||
      rad === undefined ||
      rad < 1 ||
      rad > 4096
    ) {
      return
    }
    let x0 = (p0.x - center.x) / rad
    let y0 = -(p0.y - center.y) / rad
    let x1 = (p1.x - center.x) / rad
    let y1 = -(p1.y - center.y) / rad
    const L0sqr = x0 * x0 + y0 * y0
    const L1sqr = x1 * x1 + y1 * y1
    let Lmax = L0sqr > L1sqr ? L0sqr : L1sqr
    let z0
    let z1
    if (Lmax > 1.0) {
      // Outside of trackball circle. Rotate about z only. Use point of
      // intersection with bounding circke of the ray from center to mouse
      // cursor. Set z to zero so rotation is about z axis only.
      Lmax = Math.sqrt(Lmax)
      x0 /= Lmax
      x1 /= Lmax
      y0 /= Lmax
      y1 /= Lmax
      z0 = 0.0
      z1 = 0.0
    } else {
      // Inside trackball circle. Project in z direction to find point of
      // intersection with trackball sphere. Since we have normalized to the
      // radius of the sphere, we have a unit vector and can find the
      // missing z coordinate
      z0 = Math.sqrt(1.0 - L0sqr)
      z1 = Math.sqrt(1.0 - L1sqr)
    }
    // Now we have two unit vectors from center to trackball. The vector
    // product of these two vectors gives us a third vector, perpendicular
    // to the other two pointing in the direction of the rotation. The length
    // of the vector product will be the sine of the angle of rotation.
    let x = y0 * z1 - z0 * y1
    let y = z0 * x1 - x0 * z1
    let z = x0 * y1 - y0 * x1
    const s2 = x * x + y * y + z * z // square of the sine of the rotation angle
    if (s2 < 1e-12) {
      // No rotation.
      return
    }
    // the sine, cosine and one minus the cosine
    const s = Math.sqrt(s2) // sine of rotation angle
    const c = Math.sqrt(1.0 - s2) // cosine of rotation angle without calling cos()
    const t = 1.0 - c // one minus cosine of rotation angle

    // Make (x, y, z) a unit vector
    //
    x /= s
    y /= s
    z /= s
    //
    // Calculate Rotation matrix of an angle around a vector. See, for instance,
    // Taylor, Camillo J; Kriegman, David J. (1994). 'Minimization on the Lie
    // Group SO(3) and Related Manifolds' (PDF). Technical Report No. 9405. Yale
    // University as cited in the Wikipedia article on matrix rotation.
    //
    return {
      r00: t * x * x + c,
      r01: t * x * y - s * z,
      r02: t * x * z + s * y,
      r10: t * x * y + s * z,
      r11: t * y * y + c,
      r12: t * y * z - s * x,
      r20: t * x * z - s * y,
      r21: t * y * z + s * x,
      r22: t * z * z + c
    }
  }

  function rolycyl(p0, p1, center, rad) {
    if (
      p0 === undefined ||
      p1 === undefined ||
      center === undefined ||
      rad === undefined ||
      rad < 1 ||
      rad > 4096
    ) {
      return
    }
    let x0 = (p0.x - center.x) / rad
    let y0 = -(p0.y - center.y) / rad
    let x1 = (p1.x - center.x) / rad
    let y1 = -(p1.y - center.y) / rad
    const L0sqr = x0 * x0 + y0 * y0
    const L1sqr = x1 * x1 + y1 * y1
    let Lmax = L0sqr > L1sqr ? L0sqr : L1sqr
    let z0
    let z1
    let x, y, s, s2, c, t
    if (Lmax > 1.0) {
      // Outside of trackball circle. Rotate about z only. Use point of
      // intersection with bounding circle of the ray from center to mouse
      // cursor. Set z to zero so rotation is about z axis only.
      Lmax = Math.sqrt(Lmax)
      x0 /= Lmax
      x1 /= Lmax
      y0 /= Lmax
      y1 /= Lmax
      s = x0 * y1 - y0 * x1
      s2 = s * s
      if (s2 < 1e-12) {
        // No rotation.
        return
      }
      c = Math.sqrt(1.0 - s2) // cosine of rotation angle without calling cos()
      t = 1.0 - c // one minus cosine of rotation angle
      //
      // Calculate Rotation matrix of an angle around a vector. See, for instance,
      // Taylor, Camillo J; Kriegman, David J. (1994). 'Minimization on the Lie
      // Group SO(3) and Related Manifolds' (PDF). Technical Report No. 9405. Yale
      // University as cited in the Wikipedia article on matrix rotation.
      // x and y are both zero.
      //
      return {
        r00: c,
        r01: -s,
        r02: 0.0,
        r10: s,
        r11: c,
        r12: 0.0,
        r20: 0.0,
        r21: 0.0,
        r22: 1.0
      }
    } else {
      // Inside trackball circle.
      const dx = x1 - x0
      const dy = y1 - y0
      if (dx === 0.0 && dy === 0.0) {
        // No rotation.
        return
      }
      //    return {r00:1.,r01:0.,r02:0.,r10:0.,r11:1.,r12:0.,r20:0.,r21:0.,r22:1.}; identity matrix
      if (Math.abs(dx) > Math.abs(dy)) {
        x = 0.0
        y = dx
        if (Math.abs(dx / dy) > 0.707) {
          y *= 1.414
        } // this hack makes diagonal rotations better
        s = y
      } else {
        x = -dy
        y = 0.0
        if (Math.abs(dy / dx) > 0.707) {
          x *= 1.414
        } // this hack makes diagonal rotations better
        s = x
      }
      // Now we have two unit vectors from center to trackball. The vector
      // product of these two vectors gives us a third vector, perpendicular
      // to the other two pointing in the direction of the rotation. The length
      // of the vector product will be the sine of the angle of rotation.
      s2 = s * s
      if (s2 < 1e-12) {
        // No rotation.
        return
      }
      c = Math.sqrt(1.0 - s2) // cosine of rotation angle without calling cos()
      t = 1.0 - c // one minus cosine of rotation angle

      // Make (x, y) a unit vector
      x /= s
      y /= s
      // Calculate Rotation matrix of an angle around a vector. See, for instance,
      // Taylor, Camillo J; Kriegman, David J. (1994). 'Minimization on the Lie
      // Group SO(3) and Related Manifolds' (PDF). Technical Report No. 9405. Yale
      // University as cited in the Wikipedia article on matrix rotation.
      //
      return {
        r00: t * x * x + c,
        r01: 0.0,
        r02: s * y,
        r10: 0.0,
        r11: t * y * y + c,
        r12: -s * x,
        r20: -s * y,
        r21: s * x,
        r22: c
      }
    }
  }

  function initEdges() {
    nEdge = nFacet + nVert - 2 // Euler's relationship for polyhedra
    if (nEdge < 0) {
      return
    }
    let found

    let m = 0
    for (let i = 0; i < nEdge; i++) {
      f1[i] = -1
    }
    for (let i = 0; i < nFacet; i++) {
      const nv = facet[i].length
      for (let j = 0; j < nv; j++) {
        //
        // go through all edges and see if the facet that shares it is
        // already tablulated
        //
        found = false
        for (let k = 0; k < m; k++) {
          if (f1[k] !== -1) {
            continue
          }
          if (facet[i][(j + 1) % nv] === v0[k] && facet[i][j] === v1[k]) {
            f1[k] = i
            found = true
            break
          }
        }
        if (found) {
          continue
        }
        v0[m] = facet[i][j]
        v1[m] = facet[i][(j + 1) % nv]
        f0[m] = i
        m++
      }
    }
  }

  function ColorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, '')
    if (hex.length < 6) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2]
    }
    lum = lum || 0

    // convert to decimal and change luminosity
    let rgb = '#'
    let c
    // let i
    for (let i = 0; i < 3; i++) {
      c = parseInt(hex.substr(i * 2, 2), 16)
      c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16)
      rgb += ('00' + c).substr(c.length)
    }

    return rgb
  }

  function rgbcolor(Color) {
    const c = Color.toLowerCase()
    let rgb = '#FFFFFF' /* default to white */
    if (c.includes('green') && c.includes('blu')) {
      /* blue green, bluish green, greenish blue, etc */
      rgb = '#40FFFF'
    } else if (c.includes('orang') && c.includes('yellow')) {
      rgb = '#FFCC00'
    } /* orangy yellow, orangish yellow, yellow-orange, etc */ else if (
      c.includes('green') &&
      c.includes('yellow')
    ) {
      rgb = '#C0FF00'
    } /* greenish yellow */ else if (c.includes('light') && c.includes('blu')) {
      rgb = '#90A0FF'
    } /* light blue */ else if (c.includes('padpar')) {
      /* padparadscha */
      rgb = '#FF674B'
    } else if (c.includes('orang') && c.includes('pink')) {
      rgb = '#FF674B'
    } /* orangy pink, pinky orange same as padparadscha */ else if (
      c.includes('red')
    ) {
      rgb = '#FF4080'
    } else if (c.includes('ruby')) {
      rgb = '#FF0000'
    } else if (c.includes('green')) {
      rgb = '#80FF80'
    } else if (c.includes('blu')) {
      // bluish anything else will be read as blue
      rgb = '#8080FF'
    } else if (c.includes('yellow')) {
      rgb = '#FFD000'
    } else if (c.includes('orange')) {
      rgb = '#FF9000'
    } else if (c.includes('brown')) {
      rgb = '#905050'
    } else if (c.includes('purple')) {
      rgb = '#9370DB'
    } else if (c.includes('violet')) {
      rgb = '#EE82EE'
    } else if (c.includes('pink')) {
      rgb = '#FF88A5'
    } else if (c.includes('peach')) {
      rgb = '#FF9080'
    } else if (c.includes('grey') || c.includes('gray')) {
      rgb = '#A0A0A0'
    }
    col = []
    let i
    for (i = 1; i < 255; i++) {
      col[i] = ColorLuminance(rgb, ((i - 127) * 0.7) / 127.0)
    }
    col[0] = '#000000'
    col[255] = '#FFFFFF'

    return rgb
  }

  function unitNormals(dOnly) {
    let i, j, k
    let a, b, c, d

    for (i = 0; i < nFacet; i++) {
      const nv = facet[i].length
      if (dOnly) {
        a = anorm[i]
        b = bnorm[i]
        c = cnorm[i]
      } else {
        a = 0.0
        b = 0.0
        c = 0.0
        for (j = 0; j < nv; j++) {
          const v0 = facet[i][j]
          k = (j + 1) % nv
          const v1 = facet[i][k]
          a += (zvert[v0] + zvert[v1]) * (yvert[v1] - yvert[v0])
          b += (xvert[v0] + xvert[v1]) * (zvert[v1] - zvert[v0])
          c += (yvert[v0] + yvert[v1]) * (xvert[v1] - xvert[v0])
        }
        d = -Math.sqrt(a * a + b * b + c * c)
        a /= d
        b /= d
        c /= d
        anorm[i] = a
        bnorm[i] = b
        cnorm[i] = c
      }
      // Constant term d such that a*x + b*y + c*z = d
      d = 0.0
      for (j = 0; j < nv; j++) {
        const v = facet[i][j]
        d += a * xvert[v] + b * yvert[v] + c * zvert[v]
      }
      d /= nv
      dnorm[i] = d
    }
  }

  function tiltStone(ax, ay, az) {
    let i
    // Rotate this stone to make girdle normal plane point along
    // the Z axis.
    const s = Math.sqrt(ax * ax + ay * ay) // sin of rotation angle
    if (s < 0.0001) {
      return
    }
    const c = az // cos of rotation angle is the z coordinate
    // Cross product of unit normal with k=(0,0,1) is the axis of
    // rotation vector. Make it a unit vector.
    let ux = ay / s
    let uy = -ax / s
    let uz = 0.0
    const d = Math.sqrt(ux * ux + uy * uy + uz * uz)
    ux /= d
    uy /= d
    uz /= d
    const lmc = 1.0 - c
    // General 3x3 rotation matrix for rotation about the unit vector
    // (ux, uy, uz)
    const gm00 = ux * ux + (1.0 - ux * ux) * c
    const gm01 = ux * uy * lmc - uz * s
    const gm02 = ux * uz * lmc + uy * s
    const gm10 = ux * uy * lmc + uz * s
    const gm11 = uy * uy + (1.0 - uy * uy) * c
    const gm12 = uy * uz * lmc - ux * s
    const gm20 = ux * uz * lmc - uy * s
    const gm21 = uy * uz * lmc + ux * s
    const gm22 = uz * uz + (1.0 - uz * uz) * c
    let xx, yy, zz
    for (i = 0; i < nVert; i++) {
      xx = xvert[i] * gm00 + yvert[i] * gm01 + zvert[i] * gm02
      yy = xvert[i] * gm10 + yvert[i] * gm11 + zvert[i] * gm12
      zz = xvert[i] * gm20 + yvert[i] * gm21 + zvert[i] * gm22
      xvert[i] = xx
      yvert[i] = yy
      zvert[i] = zz
    }
    for (i = 0; i < nFacet; i++) {
      xx = anorm[i] * gm00 + bnorm[i] * gm01 + cnorm[i] * gm02
      yy = anorm[i] * gm10 + bnorm[i] * gm11 + cnorm[i] * gm12
      zz = anorm[i] * gm20 + bnorm[i] * gm21 + cnorm[i] * gm22
      anorm[i] = xx
      bnorm[i] = yy
      cnorm[i] = zz
    }
    unitNormals(true) // redo constant term of plane eqns. (dnorm)
  }

  function centerOutline(levelGirdle) {
    const outline = []
    const flip = []
    let i, j, k, m
    let phi
    let ymin, ymax

    m = 0
    let tiltCount = 0
    let tiltang = 0.0
    do {
      //
      // Find top and bottom extent of stone
      //
      zmin = zvert[0]
      zmax = zvert[0]
      for (i = 1; i < nVert; i++) {
        if (zvert[i] < zmin) zmin = zvert[i]
        if (zvert[i] > zmax) zmax = zvert[i]
      }
      //
      // Find crown top view outline points
      //
      m = 0
      for (i = 0; i < nEdge; i++) {
        if (cnorm[f0[i]] > crnGirdThrsh && cnorm[f1[i]] <= crnGirdThrsh) {
          outline[m] = i
          flip[m] = false
          m++
        } else if (
          cnorm[f0[i]] <= crnGirdThrsh &&
          cnorm[f1[i]] > crnGirdThrsh
        ) {
          outline[m] = i
          flip[m] = true
          m++
        }
      }
      //
      // Sort the outline and make a copy of it
      //
      const vx = []
      const vy = []
      const vz = []
      sortOutline(outline, flip, m, vx, vy, vz, null)
      //
      // Find the inclination of the plane fitted to the top girdle outline.
      // Tilt stone to make this plane level.
      //
      if (levelGirdle && !cabochon) {
        //
        // Best fit plane: calculate area of girdle outline trajectory
        // projected onto each coordinate plane:
        //    ax = area projected onto yz plane
        //    ay = area projected onto xz plane
        //    az = area projected onto xy plane
        //
        let ax, ay, az
        ax = 0.0
        ay = 0.0
        az = 0.0
        for (i = 0; i < m; i++) {
          j = (i + 1) % m
          ax += (vz[i] + vz[j]) * (vy[j] - vy[i])
          ay += (vx[i] + vx[j]) * (vz[j] - vz[i])
          az += (vy[i] + vy[j]) * (vx[j] - vx[i])
        }
        const d = Math.sqrt(ax * ax + ay * ay + az * az)
        ax /= d
        ay /= d
        az /= d
        if (az < 0.0) {
          ax = -ax
          ay = -ay
          az = -az
        }
        tiltang = az > 1.0 ? 0.0 : (Math.acos(az) * 180) / Math.PI
        if (tiltang !== 0.0) {
          tiltStone(ax, ay, az)
        }
      }
      //
      // Calculate center of area of outline
      //
      let area, sa, sx, sy
      sa = 0
      sx = 0.0
      sy = 0.0
      area = 0.0
      for (i = 0; i < m; i++) {
        j = (i + 1) % m
        const x0 = vx[i]
        const y0 = vy[i]
        const x1 = vx[j]
        const y1 = vy[j]
        area = x0 * y1 - x1 * y0
        sa += area
        sx += (x1 * x1 + x1 * x0 + x0 * x0) * (y1 - y0)
        sy -= (y1 * y1 + y1 * y0 + y0 * y0) * (x1 - x0)
      }
      sa /= 2.0
      sx /= 6.0 * sa
      sy /= 6.0 * sa
      const outlineArea = sa
      const zhalf = (zmax + zmin) / 2.0
      //
      // Recenter XY about center of area and Z about the halfway point
      //
      for (i = 0; i < nVert; i++) {
        xvert[i] -= sx
        yvert[i] -= sy
        zvert[i] -= zhalf
      }
      //
      // Recenter outline copy
      //
      for (i = 0; i < m; i++) {
        vx[i] -= sx
        vy[i] -= sy
      }
    } while (++tiltCount < 3 && tiltang > 0.1)
  }

  function rotatez(ang, m, x, y) {
    let i
    let xp, yp

    const rad = (ang * Math.PI) / 180.0 // to radians
    const s = Math.sin(rad)
    const c = Math.cos(rad)
    //
    // Rotate vertices
    //
    for (i = 0; i < nVert; i++) {
      xp = xvert[i] * c - yvert[i] * s
      yp = yvert[i] * c + xvert[i] * s
      xvert[i] = xp
      yvert[i] = yp
    }
    //
    // Rotate facet normals
    //
    for (i = 0; i < nFacet; i++) {
      xp = anorm[i] * c - bnorm[i] * s
      yp = bnorm[i] * c + anorm[i] * s
      anorm[i] = xp
      bnorm[i] = yp
    }
    //
    // Rotate auxilliary arrays
    //
    if (m === 0) {
      return
    }
    for (i = 0; i < m; i++) {
      xp = x[i] * c - y[i] * s
      yp = y[i] * c + x[i] * s
      x[i] = xp
      y[i] = yp
    }
  }

  function rotate(r) {
    //
    // Incremental rotation matrix coefficients are r.rij
    // from rolypoly()
    //
    // Multiply the rotation matrix by the incremental rotation
    // matrix
    //
    let rm00, rm01, rm02, rm10, rm11, rm12, rm20, rm21, rm22
    if (r !== undefined) {
      rm00 = r.r00 * bm00 + r.r01 * bm10 + r.r02 * bm20
      rm01 = r.r00 * bm01 + r.r01 * bm11 + r.r02 * bm21
      rm02 = r.r00 * bm02 + r.r01 * bm12 + r.r02 * bm22

      rm10 = r.r10 * bm00 + r.r11 * bm10 + r.r12 * bm20
      rm11 = r.r10 * bm01 + r.r11 * bm11 + r.r12 * bm21
      rm12 = r.r10 * bm02 + r.r11 * bm12 + r.r12 * bm22

      rm20 = r.r20 * bm00 + r.r21 * bm10 + r.r22 * bm20
      rm21 = r.r20 * bm01 + r.r21 * bm11 + r.r22 * bm21
      rm22 = r.r20 * bm02 + r.r21 * bm12 + r.r22 * bm22

      bm00 = rm00
      bm01 = rm01
      bm02 = rm02
      bm10 = rm10
      bm11 = rm11
      bm12 = rm12
      bm20 = rm20
      bm21 = rm21
      bm22 = rm22
    }

    /*
     * Check to see if the line of sight is close to one of the
     * coordinte axes. If so, set orthog = true
     */
    let orthog = false
    rm00 = Math.round(bm00)
    const orthogthresh = 0.1
    if (Math.abs(bm00 - rm00) < orthogthresh) {
      rm01 = Math.round(bm01)
      if (Math.abs(bm01 - rm01) < orthogthresh) {
        rm02 = Math.round(bm02)
        if (Math.abs(bm02 - rm02) < orthogthresh) {
          rm10 = Math.round(bm10)
          if (Math.abs(bm10 - rm10) < orthogthresh) {
            rm11 = Math.round(bm11)
            if (Math.abs(bm11 - rm11) < orthogthresh) {
              rm12 = Math.round(bm12)
              if (Math.abs(bm12 - rm12) < orthogthresh) {
                rm20 = Math.round(bm20)
                if (Math.abs(bm20 - rm20) < orthogthresh) {
                  rm21 = Math.round(bm21)
                  if (Math.abs(bm21 - rm21) < orthogthresh) {
                    rm22 = Math.round(bm22)
                    if (Math.abs(bm22 - rm22) < orthogthresh) {
                      orthog = true
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    /*
     * If the view is nearly parallel to an axis and if the snap flag
     * is on (not displaying table or culet center info) then snap
     * the view to the nearest coordinate axis
     */
    if (snap && orthog) {
      m00 = rm00
      m01 = rm01
      m02 = rm02
      m10 = rm10
      m11 = rm11
      m12 = rm12
      m20 = rm20
      m21 = rm21
      m22 = rm22
    } else {
      m00 = bm00
      m01 = bm01
      m02 = bm02
      m10 = bm10
      m11 = bm11
      m12 = bm12
      m20 = bm20
      m21 = bm21
      m22 = bm22
    }

    //    showStatus(String.format('%.3f %.3f %.3f  %.3f %.3f %.3f  %.3f %.3f %.3f',
    //      m00, m01, m02, m10, m11, m12, m20, m21, m22))
    /*
          let check = Math.abs(rm00-1.) + Math.abs(rm01) + Math.abs(rm02)
          check += Math.abs(rm10) + Math.abs(rm11-1.) + Math.abs(rm12)
          check += Math.abs(rm20) + Math.abs(rm21) + Math.abs(rm22-1.)
          showStatus(String.format('Check %e', check))
      */
    //
    // Rotate the stone's vertices. Calculate the bounding box dimensions
    // in the rotated coordinate system. These are indicated with dimension
    // lines that grow and shrink as the stone is rotated. This gives the
    // user a way to measure diagonal dimensions.
    //
    xrmin = 1e10 // initialize bounding box
    xrmax = -1e10
    yrmin = 1e10
    yrmax = -1e10
    let i
    for (i = 0; i < nVert; i++) {
      const xr = m00 * xvert[i] + m01 * yvert[i] + m02 * zvert[i]
      const yr = m10 * xvert[i] + m11 * yvert[i] + m12 * zvert[i]
      if (xr < xrmin) xrmin = xr
      if (xr > xrmax) xrmax = xr
      if (yr < yrmin) yrmin = yr
      if (yr > yrmax) yrmax = yr
      xvertP[i] = Math.floor(pxlPerMm * xr) // convert from mm to pixels
      yvertP[i] = Math.floor(pxlPerMm * yr)
    }
    //
    // Rotate the facet's unit normal z components. This is used to
    // decide if a facet is visible and if so, how much to shade it.
    //
    for (i = 0; i < nFacet; i++) {
      cnorm1[i] = m20 * anorm[i] + m21 * bnorm[i] + m22 * cnorm[i]
      cnorm1[i] *= 254.5 // convert to grayscale index value
    }
    //
    // Rotate the crown girdle outline plotting points
    //
    for (i = 0; i < ncg; i++) {
      xcgP[i] = Math.floor(
        pxlPerMm * (m00 * xcg[i] + m01 * ycg[i] + m02 * zcg[i])
      )
      ycgP[i] = Math.floor(
        pxlPerMm * (m10 * xcg[i] + m11 * ycg[i] + m12 * zcg[i])
      )
    }
    //
    // Rotate the pavilion girdle outline plotting points
    //
    for (i = 0; i < npg; i++) {
      xpgP[i] = Math.floor(
        pxlPerMm * (m00 * xpg[i] + m01 * ypg[i] + m02 * zpg[i])
      )
      ypgP[i] = Math.floor(
        pxlPerMm * (m10 * xpg[i] + m11 * ypg[i] + m12 * zpg[i])
      )
    }
    ix = m00
    iy = -m10
    iz = m20
    jx = -m01
    jy = m11
    jz = -m21
    kx = m02
    ky = -m12
    kz = m22
  }

  function checkIfRound() {
    const outline = []
    const flip = []
    let i, j, k, m

    m = 0
    //
    // Find top and bottom extent of stone. (Why??)
    //
    zmin = zvert[0]
    zmax = zvert[0]
    for (i = 1; i < nVert; i++) {
      if (zvert[i] < zmin) zmin = zvert[i]
      if (zvert[i] > zmax) zmax = zvert[i]
    }
    //
    // Find crown top view outline points
    //
    m = 0
    for (i = 0; i < nEdge; i++) {
      if (cnorm[f0[i]] > crnGirdThrsh && cnorm[f1[i]] <= crnGirdThrsh) {
        outline[m] = i
        flip[m] = false
        m++
      } else if (cnorm[f0[i]] <= crnGirdThrsh && cnorm[f1[i]] > crnGirdThrsh) {
        outline[m] = i
        flip[m] = true
        m++
      }
    }
    //
    // Sort the outline and make a copy of it
    //
    const vx = []
    const vy = []
    const vz = []
    sortOutline(outline, flip, m, vx, vy, vz, null)

    let rmin = 1e10
    let rmax = 0
    for (i = 0; i < m; i++) {
      let x0, y0
      j = (i + 1) % m
      x0 = vx[i]
      y0 = vy[i]
      const x1 = vx[j]
      const y1 = vy[j]
      //
      // Calculate the point of intersection from the origin to
      // the current edge joining vertices (x0, y0) to (x1, y1).
      // Find the number p such that the point
      // (x0 + p*(x1-x0), y0 + p*(y1-y0))
      // is the point of intersection. If p is between 0 and 1 then
      // the point of intersection is between points 0 and 1.
      // Then the distance from the origin to the edge is less than
      // the distance to either point 0 or 1, and the distance is the
      // distance to the point of intersection.
      //
      const dx = x1 - x0
      const dy = y1 - y0
      if (dx === 0 && dy === 0) {
        continue
      }
      //
      // minimizing square of radius is same as minimizing radius,
      // so no need for sqrt()
      //
      let r = x0 * x0 + y0 * y0 // square of radius. no need for sqrt()
      if (r > rmax) {
        rmax = r
      }
      const p = -(dx * x0 + dy * y0) / (dx * dx + dy * dy)
      if (p > 0 && p < 1) {
        // between points 0 and 1?
        //
        // Interpolate
        //
        x0 += p * dx
        y0 += p * dy
        r = x0 * x0 + y0 * y0 // square of radius to perpendicular distance
      }
      if (r < rmin) {
        // shortest so far
        rmin = r
      }
    }
    const rat = Math.sqrt(rmax / rmin)
    return rat < 1.05 // True if shape is round to within 5%
  }

  function orientRound() {
    let iter
    let i, j, k, ii, jj, kk
    //
    // Find table facet
    //
    tableFacet = -1
    for (i = 0; i < nFacet; i++) {
      if (cnorm[i] > 0.996) {
        // < about 5 degrees. found table
        tableFacet = i
        break
      }
    }
    //
    // Find table maximum radius rmax
    //
    let rmax = 0
    let phi = 0
    if (tableFacet !== -1) {
      const nv = facet[tableFacet].length
      let ysav = 1
      let xsav = 0
      for (i = 0; i < nv; i++) {
        j = facet[tableFacet][i]
        const x0 = xvert[j]
        const y0 = yvert[j]
        const r = x0 * x0 + y0 * y0 // square of radius. no need for sqrt()
        if (r > rmax) {
          rmax = r
          xsav = x0
          ysav = y0
        }
      }
      //
      // Rotate so max table radius is up
      //
      phi = Math.atan2(xsav, ysav)
      phi *= 180 / Math.PI
      rotatez(phi, 0, null, null)
    }
    const xvr = []
    const yvr = []
    const zvr = []
    let distmin, distsum
    for (iter = 0; iter < 2; iter++) {
      let phi0, phi1, phi2
      phi0 = -10
      phi1 = 10.1
      phi2 = 1.0
      if (iter === 1) {
        phi0 = -1.0
        phi1 = 1.01
        phi2 = 0.05
      }
      let phibest = 0
      let distbest = 1e10
      for (phi = phi0; phi <= phi1; phi += phi2) {
        const rad = (phi * Math.PI) / 180
        const s = Math.sin(rad)
        const c = Math.cos(rad)
        for (i = 0; i < nVert; i++) {
          const xt = xvert[i] * c - yvert[i] * s
          const yt = yvert[i] * c + xvert[i] * s
          xvr[i] = xt
          yvr[i] = yt
          zvr[i] = zvert[i]
        }
        distsum = 0
        for (i = 0; i < nFacet; i++) {
          if (cnorm[i] <= crnGirdThrsh) {
            continue
          } // Consider crown facets only
          distmin = 1e10
          for (j = 0; j < facet[i].length; j++) {
            k = facet[i][j]
            for (ii = 0; ii < nFacet; ii++) {
              if (cnorm[ii] <= crnGirdThrsh) {
                continue
              }
              for (jj = 0; jj < facet[ii].length; jj++) {
                kk = facet[ii][jj]
                const dx = xvr[k] + xvr[kk] // + because mirror
                const dy = yvr[k] - yvr[kk]
                const dz = zvr[k] - zvr[kk]
                const dist = dx * dx + dy * dy + dz * dz
                if (dist < distmin) {
                  distmin = dist
                }
              }
            }
          }
          distsum += distmin
        }
        if (distsum < distbest) {
          distbest = distsum
          phibest = phi
        }
      }
      rotatez(phibest, 0, null, null)
    }
  }

  function orientStone() {
    let m, i, s, c, xt, yt
    const outline = []
    const flip = []
    // Find crown top view outline points
    m = 0
    for (i = 0; i < nEdge; i++) {
      if (cnorm[f0[i]] > crnGirdThrsh && cnorm[f1[i]] <= crnGirdThrsh) {
        outline[m] = i
        flip[m] = false
        m++
      } else if (cnorm[f0[i]] <= crnGirdThrsh && cnorm[f1[i]] > crnGirdThrsh) {
        outline[m] = i
        flip[m] = true
        m++
      }
    }
    // Sort the outline and make a copy of it
    const vx = []
    const vy = []
    const vz = []
    sortOutline(outline, flip, m, vx, vy, vz, null)
    const xrot = []
    const yrot = []
    // Search for the angle that makes this stone's outline  most symmetrical
    // about the Y axis. Do three passes of decreasing angle increment.
    for (let iter = 0; iter < 3; iter++) {
      let phi, phi0, phi1, phi2
      phi0 = -90
      phi1 = 91
      phi2 = 10
      if (iter === 1) {
        phi0 = -10
        phi1 = 10.1
        phi2 = 1.0
      } else if (iter === 2) {
        phi0 = -1.0
        phi1 = 1.01
        phi2 = 0.05
      }

      phi = 0
      let testphi
      let maxarea = 0
      for (testphi = phi0; testphi < phi1; testphi += phi2) {
        // Copy the outline again since foldedArea() consumes it
        for (i = 0; i < m; i++) {
          xrot[i] = vx[i]
          yrot[i] = vy[i]
        }
        // Rotate the outline
        const rad = (testphi * Math.PI) / 180 // to radians
        s = Math.sin(rad)
        c = Math.cos(rad)
        for (i = 0; i < m; i++) {
          xt = xrot[i] * c - yrot[i] * s
          yt = yrot[i] * c + xrot[i] * s
          xrot[i] = xt
          yrot[i] = yt
        }
        // Find the area of the outline folded on the Y axis and
        // clipped away where they overlap.
        const testarea = foldedArea(xrot, yrot, m)
        if (testarea > maxarea) {
          maxarea = testarea
          phi = testphi
        }
      }
      // let nsym = (2 * maxarea / outlineArea > 0.95) ? 1 : 0
      // Rotate this stone coordinates and normals
      rotatez(phi, m, vx, vy)
    }
    // Find crown top view outline points
    m = 0
    for (i = 0; i < nEdge; i++) {
      if (cnorm[f0[i]] > crnGirdThrsh && cnorm[f1[i]] <= crnGirdThrsh) {
        outline[m] = i
        flip[m] = false
        m++
      } else if (cnorm[f0[i]] <= crnGirdThrsh && cnorm[f1[i]] > crnGirdThrsh) {
        outline[m] = i
        flip[m] = true
        m++
      }
    }
    //
    // Sort the outline and make a copy of it
    //
    sortOutline(outline, flip, m, vx, vy, vz, null)
    //
    // Now stone has symmetry axis vertical, but might have other ones
    // Rotate from 0 to 135 degrees in steps of 45 degrees
    // Do folded area
    //
    const vxr = []
    const vyr = []
    for (i = 0; i < m; i++) {
      vxr[i] = vx[i]
      vyr[i] = vy[i]
    }
    s = Math.sqrt(2) / 2
    c = s
    const width = []
    const area = []
    for (let irot = 0; irot < 4; irot++) {
      let oxmin, oxmax
      oxmin = 0
      oxmax = 0
      for (i = 0; i < m; i++) {
        xrot[i] = vxr[i]
        yrot[i] = vyr[i]
        if (vxr[i] < oxmin) {
          oxmin = vxr[i]
        }
        if (vxr[i] > oxmax) {
          oxmax = vxr[i]
        }
      }
      width[irot] = oxmax - oxmin
      area[irot] = foldedArea(xrot, yrot, m)
      //
      // Rotate
      //
      for (i = 0; i < m; i++) {
        xt = vxr[i] * c - vyr[i] * s
        yt = vyr[i] * c + vxr[i] * s
        vxr[i] = xt
        vyr[i] = yt
      }
    }
    //  showStatus(String.format('Areas: %.2f %.2f %.2f  Widths: %.2f %.2f %.2f',
    //    area[0], area[1], area[2], width[0], width[1], width[2]))
    let rat01 = area[0] / area[1]
    let rat12 = area[1] / area[2]
    let rat02 = area[0] / area[2]
    if (rat01 > 0.97 && rat01 < 1.03 && rat12 > 0.97 && rat12 < 1.03) {
      // nsym = 4
      // 4-fold, mirror symmetry
      // Check to see if  width is greater than diagonal width
      //
      if (width[0] > width[1]) {
        rotatez(45, 0, null, null) // Rotate stone 45 degrees
        for (i = 0; i < 3; i++) {
          area[i] = area[i + 1]
          width[i] = width[i + 1]
        }
        rat01 = area[0] / area[1]
        rat12 = area[1] / area[2]
        rat02 = area[0] / area[2]
      }
      //    showStatus('4-fold, mirror')
    } else if (rat02 > 0.97 && rat02 < 1.03) {
      // nsym = 2
    }
    //
    // Make length bigger than width and make length vertical in top view
    //
    if (rat02 > 0.95 && rat02 < 1.05) {
      if (width[0] > width[2]) {
        rotatez(90, 0, null, null)
      }
    }
    //
    // Find Y minimum and maximum of vertices
    //
    ymin = yvert[0]
    ymax = yvert[0]
    for (i = 1; i < nVert; i++) {
      if (yvert[i] < ymin) {
        ymin = yvert[i]
      }
      if (yvert[i] > ymax) {
        ymax = yvert[i]
      }
    }
    //
    // Orient pears so that the point is down
    //
    if ((ymax + ymin) / 2 > 0) {
      rotatez(180, 0, null, null)
    }
  }

  function sortOutline(outline, flip, m, vx, vy, vz, f) {
    //
    // Sort the outline so the edges are contiguous
    //
    let i = 0
    let o
    for (i = 0; i < m - 1; i++) {
      let onext, vnext
      const ocur = outline[i]
      const vcur = flip[i] ? v0[ocur] : v1[ocur]
      vnext = 0
      for (let j = i + 1; j < m; j++) {
        onext = outline[j]
        vnext = flip[j] ? v1[onext] : v0[onext]
        if (vcur === vnext) {
          //
          // swap outline i+1 and j
          //
          if (j === i + 1) {
            break
          }
          const k = outline[i + 1]
          outline[i + 1] = outline[j]
          outline[j] = k
          const b = flip[i + 1]
          flip[i + 1] = flip[j]
          flip[j] = b
          break
        }
      }
      if (vcur !== vnext) {
        // gap; outline not contiguous
        //      showStatus(String.format('Gap in girdle outline'))
        //      return
      }
    }
    //
    // Make a copy of the outline
    //
    for (i = 0; i < m; i++) {
      o = outline[i]
      if (flip[i]) {
        vx[i] = xvert[v1[o]]
        vy[i] = yvert[v1[o]]
        vz[i] = zvert[v1[o]]
      } else {
        vx[i] = xvert[v0[o]]
        vy[i] = yvert[v0[o]]
        vz[i] = zvert[v0[o]]
      }
    }
    if (f === null) {
      return
    }
    for (i = 0; i < m; i++) {
      o = outline[i]
      f[i] = Math.abs(cnorm[f0[o]]) < Math.abs(cnorm[f1[o]]) ? f0[o] : f1[o]
    }
  }

  function foldedArea(x, y, n) {
    let i, j, k
    let p
    let ybot = 0
    let ytop = 0
    let npos, nneg
    let leftstart, rightstart, leftstop, rightstop
    let ileft, iright, jleft, jright
    let yy
    let xxprev
    let area
    //
    // Find y coordinates ybot ytop where outline crosses the fold line, the
    // y axis.
    //
    for (i = 0; i < n; i++) {
      j = (i + 1) % n
      if (x[i] > 0 && x[j] <= 0) {
        p = -x[i] / (x[j] - x[i])
        ytop = y[i] + p * (y[j] - y[i])
      } else if (x[i] <= 0 && x[j] > 0) {
        p = -x[j] / (x[i] - x[j])
        ybot = y[j] + p * (y[i] - y[j])
      }
    }
    //
    // Clip horizontally at ytop and ybot
    //
    rightstart = -1
    rightstop = -1
    leftstart = -1
    leftstop = -1
    for (i = 0; i < n; i++) {
      j = (i + 1) % n
      if (y[i] >= ytop && y[j] < ytop) {
        p = (ytop - y[j]) / (y[i] - y[j])
        x[i] = x[j] + p * (x[i] - x[j])
        if (Math.abs(x[i]) < 1e-3) {
          x[i] = 0
        }
        y[i] = ytop
        leftstop = i
      }
      if (y[j] >= ytop && y[i] < ytop) {
        p = (ytop - y[i]) / (y[j] - y[i])
        x[j] = x[i] + p * (x[j] - x[i])
        if (Math.abs(x[j]) < 1e-3) {
          x[j] = 0
        }
        y[j] = ytop
        rightstop = j
      }
      if (y[j] > ybot && y[i] <= ybot) {
        p = (ybot - y[j]) / (y[i] - y[j])
        x[i] = x[j] + p * (x[i] - x[j])
        if (Math.abs(x[i]) < 1e-3) {
          x[i] = 0
        }
        y[i] = ybot
        rightstart = i
      }
      if (y[i] > ybot && y[j] <= ybot) {
        p = (ybot - y[i]) / (y[j] - y[i])
        x[j] = x[i] + p * (x[j] - x[i])
        if (Math.abs(x[j]) < 1e-3) {
          x[j] = 0
        }
        y[j] = ybot
        leftstart = j
      }
    }

    //
    // Divide the area from ybot to ytop into trapezoids and sum up the
    // area common to the two halves.
    //
    const ninc = 200
    const dy = (ytop - ybot) / ninc
    yy = ybot + dy
    xxprev = 0
    iright = rightstart
    jright = (iright + 1) % n
    ileft = leftstart
    jleft = (ileft + n - 1) % n
    area = 0
    for (i = 0; i < ninc; i++) {
      while (yy > y[jright]) {
        iright = jright
        jright = (iright + 1) % n
        if (jright === rightstop) {
          break
        }
      }
      while (yy > y[jleft]) {
        ileft = jleft
        jleft = (ileft + n - 1) % n
        if (jleft === leftstop) {
          break
        }
      }
      p = (yy - y[iright]) / (y[jright] - y[iright])
      //    assert(p >= 0. && p <= 1.)
      const xright = x[iright] + p * (x[jright] - x[iright])
      p = (yy - y[ileft]) / (y[jleft] - y[ileft])
      //    assert(p >= 0. && p <= 1.)
      const xleft = -(x[ileft] + p * (x[jleft] - x[ileft]))
      const xx = xright < xleft ? xright : xleft
      area += (dy * (xx + xxprev)) / 2
      xxprev = xx
      yy += dy
    }
    return area
  }

  function domeCheck() {
    cabochon = false
    let ndome = 0
    let dotavg = 0
    let i
    //
    // Count the number of edges on the bottom whose two facet's angles are
    // both less than about 65 degrees. Average the dot product of the
    // edge's two facets (the cosine of the dihedral angle).
    //
    for (i = 0; i < nEdge; i++) {
      const jf0 = f0[i]
      const jf1 = f1[i] // the two facets that form this edge
      if (cnorm[jf0] > -0.9 || cnorm[f1[i]] > -0.9) {
        // sin(65 deg.) ~ 0.9
        continue
      }
      ndome++
      dotavg +=
        anorm[jf0] * anorm[jf1] +
        bnorm[jf0] * bnorm[jf1] +
        cnorm[jf0] * cnorm[jf1] // dot product of normal vectors
    }
    if (ndome !== 0) {
      dotavg /= ndome
    }
    if (ndome < 100 || dotavg < 0.99) {
      return
    }
    cabochon = true
    //
    // Flip stone over
    //
    for (i = 0; i < nVert; i++) {
      xvert[i] = -xvert[i]
      zvert[i] = -zvert[i]
    }
    for (i = 0; i < nFacet; i++) {
      anorm[i] = -anorm[i]
      cnorm[i] = -cnorm[i]
    }
  }

  function dimensions() {
    // Minimum and maximum bounding box dimensions
    xmin = xvert[0]
    xmax = xvert[0]
    ymin = yvert[0]
    ymax = yvert[0]
    zmin = zvert[0]
    zmax = zvert[0]
    let i = 1
    let nv,
      j,
      dr,
      o,
      p,
      k,
      c,
      x0,
      x1,
      x2,
      x3,
      x4,
      x5,
      xx,
      dx,
      y0,
      y1,
      y2,
      y3,
      y4,
      y5,
      yy,
      dy,
      z0,
      z1,
      z2,
      z3,
      z4,
      z5,
      zz,
      dz,
      u12,
      u32,
      u34,
      area,
      center,
      thickness,
      den
    for (i = 1; i < nVert; i++) {
      if (xvert[i] < xmin) {
        xmin = xvert[i]
      }
      if (xvert[i] > xmax) {
        xmax = xvert[i]
      }
      if (yvert[i] < ymin) {
        ymin = yvert[i]
      }
      if (yvert[i] > ymax) {
        ymax = yvert[i]
      }
      if (zvert[i] < zmin) {
        zmin = zvert[i]
        cxcen = xvert[i]
        cycen = yvert[i]
        czcen = zvert[i]
      }
      if (zvert[i] > zmax) {
        zmax = zvert[i]
        txcen = xvert[i]
        tycen = yvert[i]
        tzcen = zvert[i]
      }
    }
    //
    // Table dimensions
    //
    tableFacet = -1
    tz0 = zmax
    if (!cabochon) {
      for (i = 0; i < nFacet; i++) {
        if (cnorm[i] > 0.996) {
          // < about 5 degrees. found table
          tableFacet = i
          nv = facet[i].length
          txmin = xvert[facet[i][0]]
          txmax = xvert[facet[i][0]]
          tymin = yvert[facet[i][0]]
          tymax = yvert[facet[i][0]]
          tz0 = dnorm[i] / cnorm[i]
          for (j = 1; j < nv; j++) {
            if (xvert[facet[i][j]] < txmin) txmin = xvert[facet[i][j]]
            if (xvert[facet[i][j]] > txmax) txmax = xvert[facet[i][j]]
            if (yvert[facet[i][j]] < tymin) tymin = yvert[facet[i][j]]
            if (yvert[facet[i][j]] > tymax) tymax = yvert[facet[i][j]]
          }
          break // found table, so no need to keep looking
        }
      }
      if (tableFacet !== -1) {
        center = [0, 0, 0]
        area = facetArea(tableFacet, center)
        txcen = center[0]
        tycen = center[1]
        tzcen = center[2]
      }
    }
    //
    // Culet dimensions
    //
    culetFacet = -1
    cz0 = zmin
    if (!cabochon) {
      for (i = 0; i < nFacet; i++) {
        if (cnorm[i] < -0.996) {
          // < about 5 degrees. found culet candidate.
          nv = facet[i].length
          //
          // Make sure the candidate culet contains the bottom vertex
          //
          let bottom = false
          for (j = 0; j < nv; j++) {
            if (zvert[facet[i][j]] === zmin) {
              bottom = true
              break
            }
          }
          if (!bottom) {
            continue
          }
          culetFacet = i
          cxmin = xvert[facet[i][0]]
          cxmax = xvert[facet[i][0]]
          cymin = yvert[facet[i][0]]
          cymax = yvert[facet[i][0]]
          cz0 = dnorm[i] / cnorm[i]
          for (j = 1; j < nv; j++) {
            if (xvert[facet[i][j]] < cxmin) cxmin = xvert[facet[i][j]]
            if (xvert[facet[i][j]] > cxmax) cxmax = xvert[facet[i][j]]
            if (yvert[facet[i][j]] < cymin) cymin = yvert[facet[i][j]]
            if (yvert[facet[i][j]] > cymax) cymax = yvert[facet[i][j]]
          }
          break // found culet, so no need to keep looking
        }
      }
      if (culetFacet !== -1) {
        center = [0, 0, 0]
        area = facetArea(culetFacet, center)
        cxcen = center[0]
        cycen = center[1]
        czcen = center[2]
        //      showStatus(String.format('Culet center at %.2f %.2f %.2f',
        //            cxcen, cycen, czcen))
      }
      //
      // Search for a keel. Note that the presence of a keel will
      // override the culet facet. This is because stones with keels
      // will often have a tiny little culet facet on one end of the
      // keel that will corrupt the bottom center measurement.
      //
      for (i = 0; i < nEdge; i++) {
        z0 = zvert[v0[i]]
        z1 = zvert[v1[i]]
        if (f0[i] === culetFacet || f1[i] === culetFacet) {
          continue
        } // ignore edges on the culet facet
        if (z0 === zmin || z1 === zmin) {
          // one vertex is lowest
          x0 = xvert[v0[i]]
          x1 = xvert[v1[i]]
          y0 = yvert[v0[i]]
          y1 = yvert[v1[i]]
          dx = x0 - x1
          dy = y0 - y1
          dz = z0 - z1
          dr = Math.sqrt(dx * dx + dy * dy)
          //
          // If length of edge is > 0.5 mm and elevation angle of edge
          // is less than about 5 degrees, then let's call it a keel.
          //
          if (dr > 0.5 && Math.abs(dz) / dr < 0.0875) {
            cxcen = (x0 + x1) / 2
            cycen = (y0 + y1) / 2
            czcen = (z0 + z1) / 2
            //          showStatus(String.format('Keel center at %.2f %.2f %.2f',
            //              cxcen, cycen, czcen))
            break
          }
        }
      }
    }
    //
    // Find crown outline points
    //
    const cgOutline = []
    const cgflip = []
    ncg = 0
    for (i = 0; i < nEdge; i++) {
      if (facetType[f0[i]] === CRN && facetType[f1[i]] !== CRN) {
        cgOutline[ncg] = i
        cgflip[ncg] = false
        ncg++
      } else if (facetType[f1[i]] === CRN && facetType[f0[i]] !== CRN) {
        cgOutline[ncg] = i
        cgflip[ncg] = true
        ncg++
      }
    }
    //
    // Find area under a vertical curtain hung from upper girdle edge.
    // This area divided by the length projected on the XY plane is the
    // average crown girdle height.
    //
    area = 0
    let rsum = 0
    for (i = 0; i < ncg; i++) {
      o = cgOutline[i]
      dx = xvert[v0[o]] - xvert[v1[o]]
      dy = yvert[v0[o]] - yvert[v1[o]]
      dr = Math.sqrt(dx * dx + dy * dy)
      dz = (zvert[v0[o]] + zvert[v1[o]]) / 2
      rsum += dr
      area += dr * dz
    }
    if (rsum !== 0) {
      area /= rsum
    }
    zTopGirdAvg = area
    //
    // Find pavilion outline points
    //
    const pgOutline = []
    const pgflip = []
    npg = 0

    for (i = 0; i < nEdge; i++) {
      if (facetType[f0[i]] === PAV && facetType[f1[i]] !== PAV) {
        pgOutline[npg] = i
        pgflip[npg] = false
        npg++
      } else if (facetType[f1[i]] === PAV && facetType[f0[i]] !== PAV) {
        pgOutline[npg] = i
        pgflip[npg] = true
        npg++
      }
    }
    //
    // Find area under a vertical curtain hung from lower girdle edge.
    // This area divided by the length projected on the XY plane is the
    // average pavilion girdle height.
    //
    area = 0
    rsum = 0
    for (i = 0; i < npg; i++) {
      o = pgOutline[i]
      dx = xvert[v0[o]] - xvert[v1[o]]
      dy = yvert[v0[o]] - yvert[v1[o]]
      dr = Math.sqrt(dx * dx + dy * dy)
      dz = (zvert[v0[o]] + zvert[v1[o]]) / 2
      rsum += dr
      area += dr * dz
    }
    if (rsum !== 0) {
      area /= rsum
    }
    zBotGirdAvg = area
    //
    // Calculate width at y=0 and length at x=0
    //
    xcenmin = 1e10
    xcenmax = -1e10
    ycenmin = 1e10
    ycenmax = -1e10
    for (i = 0; i < nEdge; i++) {
      x0 = xvert[v0[i]]
      x1 = xvert[v1[i]]
      y0 = yvert[v0[i]]
      y1 = yvert[v1[i]]
      if ((y0 <= 0 && y1 > 0) || (y1 <= 0 && y0 > 0)) {
        dy = y1 - y0
        if (dy === 0) {
          xx = x0
        } else {
          p = -y0 / dy
          xx = x0 + p * (x1 - x0)
        }
        if (xx < xcenmin) {
          xcenmin = xx
        }
        if (xx > xcenmax) {
          xcenmax = xx
        }
      }
      if ((x0 <= 0 && x1 > 0) || (x1 <= 0 && x0 > 0)) {
        dx = x1 - x0
        if (dx === 0) {
          yy = y0
        } else {
          p = -x0 / dx
          yy = y0 + p * (y1 - y0)
        }
        if (yy < ycenmin) {
          ycenmin = yy
        }
        if (yy > ycenmax) {
          ycenmax = yy
        }
      }
    }
    //
    // Measure girdle thicknesses.
    //
    xcg = []
    ycg = []
    zcg = []
    fcg = []
    sortOutline(cgOutline, cgflip, ncg, xcg, ycg, zcg, fcg)
    xpg = []
    ypg = []
    zpg = []
    fpg = []
    sortOutline(pgOutline, pgflip, npg, xpg, ypg, zpg, fpg)
    //
    // Measure down from crown girdle vertices to pavilion girdle edges.
    //
    minGirdle = 1e10
    maxGirdle = -1e10
    for (i = 0; i < ncg; i++) {
      x1 = 0
      y1 = 0
      x2 = xcg[i]
      y2 = ycg[i]
      z2 = zcg[i]
      for (j = 0; j < npg; j++) {
        // inefficent
        k = (j + 1) % npg
        x3 = xpg[j]
        y3 = ypg[j]
        z3 = zpg[j]
        x4 = xpg[k]
        y4 = ypg[k]
        z4 = zpg[k]
        den = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
        if (den === 0) {
          continue
        }
        u12 = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / den
        if (u12 < 0) {
          continue
        } // on other side of stone
        u34 = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / den
        //
        // Thickness
        //
        if (u34 >= 0 && u34 <= 1) {
          dx = x3 + u34 * (x4 - x3) - x2
          dy = y3 + u34 * (y4 - y3) - y2
          dz = z3 + u34 * (z4 - z3) - z2
          thickness = Math.sqrt(dx * dx + dy * dy + dz * dz)
          if (thickness > maxGirdle) {
            maxGirdle = thickness
          }
          if (thickness < minGirdle) {
            minGirdle = thickness
          }
          break
        }
      }
    }
    //
    // Measure up from pavilion girdle vertices to crown girdle edges
    //
    for (i = 0; i < npg; i++) {
      x1 = 0
      y1 = 0
      x2 = xpg[i]
      y2 = ypg[i]
      z2 = zpg[i]
      for (j = 0; j < ncg; j++) {
        // inefficent
        k = (j + 1) % ncg
        x3 = xcg[j]
        y3 = ycg[j]
        z3 = zcg[j]
        x4 = xcg[k]
        y4 = ycg[k]
        z4 = zcg[k]
        den = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
        if (den === 0) {
          continue
        }
        u12 = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / den
        if (u12 < 0) {
          continue
        } // on other side of stone
        u34 = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / den
        if (u34 >= 0 && u34 <= 1) {
          dx = x3 + u34 * (x4 - x3) - x2
          dy = y3 + u34 * (y4 - y3) - y2
          dz = z3 + u34 * (z4 - z3) - z2
          thickness = Math.sqrt(dx * dx + dy * dy + dz * dz)
          if (thickness > maxGirdle) {
            maxGirdle = thickness
          }
          if (thickness < minGirdle) {
            minGirdle = thickness
          }
        }
      }
    }
    //  showStatus(String.format('Girdle thickness min, max = %.2f %.2f',
    //      minGirdle, maxGirdle))
    //
    // Total depth: maximum distance from plane of table to furthest vertex
    //
    if (tableFacet === -1) {
      totalDepth = zmax - zmin
    } else {
      totalDepth = 0
      const a = anorm[tableFacet]
      const b = bnorm[tableFacet]
      c = cnorm[tableFacet]
      const d = dnorm[tableFacet]
      for (i = 0; i < nVert; i++) {
        const dist = Math.abs(a * xvert[i] + b * yvert[i] + c * zvert[i] - d)
        if (dist > totalDepth) {
          totalDepth = dist
        }
      }
      //    System.out.println(String.format('totalDepth = %.3f, zmax-zmin = %.3f', totalDepth, zmax-zmin))
    }
    //
    // Find total stone outline as opposed to just pavilion or girdle
    //
    const outline = []
    const flip = []
    let m = 0
    for (i = 0; i < nEdge; i++) {
      if (cnorm[f0[i]] > 0 && cnorm[f1[i]] <= 0) {
        outline[m] = i
        flip[m] = false
        m++
      } else if (cnorm[f0[i]] <= 0 && cnorm[f1[i]] > 0) {
        outline[m] = i
        flip[m] = true
        m++
      }
    }
    //
    // Sort the outline and make a copy of it
    //
    const vx = []
    const vy = []
    const vz = []
    sortOutline(outline, flip, m, vx, vy, vz, null)
    //
    // Find the area of the outline
    //
    let outlineArea = 0
    for (i = 0; i < m; i++) {
      j = (i + 1) % m
      outlineArea += 0.5 * (vx[i] * vy[j] - vx[j] * vy[i])
    }
    //
    // Find minimum and maximum diameters of round stones
    //
    //
    // Rotate the outline in increments from 0 to 89 degree in increments
    // of 1 degree and find minimum and maximum diameters. 90 degrees is
    // sufficient since we look in x and y directions for each angle
    //
    let ang = Math.PI / 180
    let s = Math.sin(ang)
    c = Math.cos(ang)
    let minDia = 1e10
    let maxDia = -1e10
    let diaPerpMax = 0
    let diaPerpMin = 0
    let oxmin, oxmax, oymin, oymax
    for (let iang = 0; iang < 90; iang++) {
      oxmin = 1e10
      oymin = 1e10
      oxmax = -1e10
      oymax = -1e10
      for (i = 0; i < m; i++) {
        if (vx[i] < oxmin) oxmin = vx[i]
        if (vy[i] < oymin) oymin = vy[i]
        if (vx[i] > oxmax) oxmax = vx[i]
        if (vy[i] > oymax) oymax = vy[i]
      }
      const xdia = oxmax - oxmin
      const ydia = oymax - oymin
      if (xdia < minDia) {
        minDia = xdia
        diaPerpMin = ydia
      }
      if (ydia < minDia) {
        minDia = ydia
        diaPerpMin = xdia
      }
      if (xdia > maxDia) {
        maxDia = xdia
        diaPerpMax = ydia
      }
      if (ydia > maxDia) {
        maxDia = ydia
        diaPerpMax = xdia
      }
      //
      // Rotate
      //
      for (i = 0; i < m; i++) {
        const xt = vx[i] * c - vy[i] * s
        const yt = vy[i] * c + vx[i] * s
        vx[i] = xt
        vy[i] = yt
      }
    }
    const isRound = checkIfRound()
    if (!isRound) {
      return
    }
    //
    // Find 4 table diameters assuming an octagonal table
    //
    if (tableFacet !== -1) {
      //
      // Find table outline
      //
      m = facet[tableFacet].length
      xx = []
      yy = []
      const vxr = []
      const vyr = []
      const vpr = []
      const vqr = []
      for (i = 0; i < m; i++) {
        xx[i] = xvert[facet[tableFacet][i]]
        yy[i] = yvert[facet[tableFacet][i]]
      }
      const c45 = Math.sqrt(2) / 2
      /*
       * Rotate the table outline from -5 to +5 degrees to find
       * four table diameters
       */
      let tDia0 = 0
      let tDia1 = 0
      let tDia2 = 0
      let tDia3 = 0
      for (let iang = -5; iang < 5; iang++) {
        ang = (iang * Math.PI) / 180
        s = Math.sin(ang)
        c = Math.cos(ang)
        for (i = 0; i < m; i++) {
          vxr[i] = xx[i] * c - yy[i] * s
          vyr[i] = yy[i] * c + xx[i] * s
          vpr[i] = vyr[i] * c45 + vxr[i] * c45
          vqr[i] = vxr[i] * c45 - vyr[i] * c45
        }
        oxmin = 1e10
        oxmax = -1e10
        oymin = 1e10
        oymax = -1e10
        let pmin = 1e10
        let pmax = -1e10
        let qmin = 1e10
        let qmax = -1e10
        for (i = 0; i < m; i++) {
          if (vxr[i] < oxmin) {
            oxmin = vxr[i]
          }
          if (vyr[i] < oymin) {
            oymin = vyr[i]
          }
          if (vpr[i] < pmin) {
            pmin = vpr[i]
          }
          if (vqr[i] < qmin) {
            qmin = vqr[i]
          }
          if (vxr[i] > oxmax) {
            oxmax = vxr[i]
          }
          if (vyr[i] > oymax) {
            oymax = vyr[i]
          }
          if (vpr[i] > pmax) {
            pmax = vpr[i]
          }
          if (vqr[i] > qmax) {
            qmax = vqr[i]
          }
        }
        const d0 = oxmax - oxmin
        const d1 = pmax - pmin
        const d2 = oymax - oymin
        const d3 = qmax - qmin
        if (d0 > tDia0) {
          tDia0 = d0
        }
        if (d1 > tDia1) {
          tDia1 = d1
        }
        if (d2 > tDia2) {
          tDia2 = d2
        }
        if (d3 > tDia3) {
          tDia3 = d3
        }
      }
      minTableDia = tDia0
      if (tDia1 < minTableDia) {
        minTableDia = tDia1
      }
      if (tDia2 < minTableDia) {
        minTableDia = tDia2
      }
      if (tDia3 < minTableDia) {
        minTableDia = tDia3
      }
      maxTableDia = tDia0
      if (tDia1 > maxTableDia) {
        maxTableDia = tDia1
      }
      if (tDia2 > maxTableDia) {
        maxTableDia = tDia2
      }
      if (tDia3 > maxTableDia) {
        maxTableDia = tDia3
      }
      const avgTableDia = (tDia0 + tDia1 + tDia2 + tDia3) / 4
      //   System.err.println(String.format('Table diamters = %.2f %.2f %.2f %.2f',
      //       tDia0, tDia1, tDia2, tDia3))
      //   System.err.println(String.format('Table avg. dia = %.2f',
      //       avgTableDia))
    }
    //  showStatus(String.format('Dia. min, max = %.3f %.3f', minDia, maxDia))
  }

  function facetArea(iFacet, cen) {
    let xcen = 0
    let ycen = 0
    let zcen = 0
    let area = 0
    const nv = facet[iFacet].length
    const v0 = facet[iFacet][0]
    const x0 = xvert[v0]
    const y0 = yvert[v0]
    const z0 = zvert[v0]
    for (let j = 1; j < nv - 1; j++) {
      const v1 = facet[iFacet][j]
      const x1 = xvert[v1]
      const y1 = yvert[v1]
      const z1 = zvert[v1]
      const v2 = facet[iFacet][j + 1]
      const x2 = xvert[v2]
      const y2 = yvert[v2]
      const z2 = zvert[v2]
      const delx = y0 * z1 - y1 * z0
      const dely = z0 * x1 - z1 * x0
      const delz = x0 * y1 - x1 * y0
      const ax = delx + y1 * z2 - y2 * z1 + y2 * z0 - y0 * z2
      const ay = dely + z1 * x2 - z2 * x1 + z2 * x0 - z0 * x2
      const az = delz + x1 * y2 - x2 * y1 + x2 * y0 - x0 * y2
      const a = Math.sqrt(ax * ax + ay * ay + az * az)
      xcen += (a * (x0 + x1 + x2)) / 3 // weighted sum
      ycen += (a * (y0 + y1 + y2)) / 3 // of centroids of
      zcen += (a * (z0 + z1 + z2)) / 3 // triangles
      area += a
    }
    if (area !== 0) {
      xcen /= area
      ycen /= area
      zcen /= area
    }
    if (cen !== null) {
      cen[0] = xcen
      cen[1] = ycen
      cen[2] = zcen
    }
    return area / 2
  }

  function findVolume(cen) {
    let xcen, ycen, zcen
    let vol
    let tvol
    let i, f
    let v0, v1, v2
    let x0, x1, x2
    let y0, y1, y2
    let z0, z1, z2
    let d0x1y, d1x0z, d0y1z, d0x1z, d1x0y, d1y0z
    let tx, ty, tz

    xcen = 0
    ycen = 0
    zcen = 0
    vol = 0
    for (f = 0; f < nFacet; f++) {
      //
      // Divide facet into triangles with vertex v0 as the pivot common
      // to all triangles.
      //
      v0 = facet[f][0]
      x0 = xvert[v0]
      y0 = yvert[v0]
      z0 = zvert[v0]
      for (i = 1; i < facet[f].length - 1; i++) {
        v1 = facet[f][i]
        x1 = xvert[v1]
        y1 = yvert[v1]
        z1 = zvert[v1]
        v2 = facet[f][i + 1]
        x2 = xvert[v2]
        y2 = yvert[v2]
        z2 = zvert[v2]
        //
        // For each triangle, compute volume of tetrahedron with the
        // origin as the fourth point
        //
        d0x1y = x0 * y1
        d1x0z = x1 * z0
        d0y1z = y0 * z1
        d0x1z = x0 * z1
        d1x0y = x1 * y0
        d1y0z = y1 * z0
        tvol = Math.abs(
          d0x1y * z2 +
            d1x0z * y2 +
            d0y1z * x2 -
            d0x1z * y2 -
            d1x0y * z2 -
            d1y0z * x2
        )
        tx = (x0 + x1 + x2) / 4
        ty = (y0 + y1 + y2) / 4
        tz = (z0 + z1 + z2) / 4
        xcen += tvol * tx
        ycen += tvol * ty
        zcen += tvol * tz
        vol += tvol
      }
    }
    xcen /= vol
    ycen /= vol
    zcen /= vol
    vol /= 6
    if (cen !== null) {
      cen[0] = xcen
      cen[1] = ycen
      cen[2] = zcen
    }
    return vol
  }

  function fillPolygon(g, x, y, n) {
    g.beginPath()
    g.moveTo(x[0], y[0])
    for (let i = 1; i < n; i++) {
      g.lineTo(x[i], y[i])
    }
    g.closePath()
    g.fill()
  }

  function drawPolygon(g, x, y, n) {
    g.beginPath()
    g.moveTo(x[0], y[0])
    for (let i = 1; i < n; i++) {
      g.lineTo(x[i], y[i])
    }
    g.closePath()
    g.stroke()
  }

  function stringFormat(x, ndig) {
    return x.toFixed(ndig)
  }

  function drawLine(g, x0, y0, x1, y1, color) {
    if (color !== undefined) {
      g.strokeStyle = color
    }
    g.beginPath()
    g.moveTo(x0, y0)
    g.lineTo(x1, y1)
    g.stroke()
  }

  function fillRoundRect(g, x, y, wid, ht, r1, r2) {
    x -= 2
    wid += 4
    g.fillRect(x, y, wid, ht)
    //  g.fill()
  }

  function drawLine3d(g, x0, y0, z0, x1, y1, z1) {
    let xp0, yp0, xp1, yp1
    xp0 = Math.floor(pxlPerMm * (m00 * x0 + m01 * y0 + m02 * z0))
    yp0 = Math.floor(pxlPerMm * (m10 * x0 + m11 * y0 + m12 * z0))
    xp1 = Math.floor(pxlPerMm * (m00 * x1 + m01 * y1 + m02 * z1))
    yp1 = Math.floor(pxlPerMm * (m10 * x1 + m11 * y1 + m12 * z1))
    xp0 += wBy2
    yp0 = hBy2 - yp0
    xp1 += wBy2
    yp1 = hBy2 - yp1
    drawLine(g, xp0, yp0, xp1, yp1)
  }

  function drawArrow3d(g, x0, y0, z0, x1, y1, z1) {
    if (frontOnly) {
      const zp0 = m20 * x0 + m21 * y0 + m22 * z0
      const zp1 = m20 * x1 + m21 * y1 + m22 * z1
      if (zp0 < 0 || zp1 < 0) {
        return
      }
    }
    let xp0, yp0, xp1, yp1
    xp0 = Math.floor(pxlPerMm * (m00 * x0 + m01 * y0 + m02 * z0))
    yp0 = Math.floor(pxlPerMm * (m10 * x0 + m11 * y0 + m12 * z0))
    xp1 = Math.floor(pxlPerMm * (m00 * x1 + m01 * y1 + m02 * z1))
    yp1 = Math.floor(pxlPerMm * (m10 * x1 + m11 * y1 + m12 * z1))
    xp0 += wBy2
    yp0 = hBy2 - yp0
    xp1 += wBy2
    yp1 = hBy2 - yp1
    drawArrow2d(g, xp0, yp0, xp1, yp1)
  }

  function drawArrow2d(g, xp0, yp0, xp1, yp1) {
    const dx = xp0 - xp1
    const dy = yp0 - yp1
    if (dx === 0 && dy === 0) {
      return
    }
    drawLine(g, xp0, yp0, xp1, yp1) // Draw the shaft of arrow.
    const adx = dx < 0 ? -dx : dx
    const ady = dy < 0 ? -dy : dy
    const dr = adx > ady ? adx + ady / 2 : ady + adx / 2
    //
    // Draw arrowhead
    //
    xp1 = xp0 - (AHLEN * dx - AHWID * dy) / dr
    yp1 = yp0 - (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp1, yp1)
    xp1 = xp0 - (AHLEN * dx + AHWID * dy) / dr
    yp1 = yp0 - (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp1, yp1)
  }

  function dimLine3d(g, x0, y0, z0, x1, y1, z1, label, labcolor, bgcolor) {
    if (frontOnly) {
      const zp0 = m20 * x0 + m21 * y0 + m22 * z0
      const zp1 = m20 * x1 + m21 * y1 + m22 * z1
      if (zp0 < 0 || zp1 < 0) {
        return
      }
    }
    let xp, yp, xp0, yp0, xp1, yp1
    xp0 = Math.floor(pxlPerMm * (m00 * x0 + m01 * y0 + m02 * z0))
    yp0 = Math.floor(pxlPerMm * (m10 * x0 + m11 * y0 + m12 * z0))
    xp1 = Math.floor(pxlPerMm * (m00 * x1 + m01 * y1 + m02 * z1))
    yp1 = Math.floor(pxlPerMm * (m10 * x1 + m11 * y1 + m12 * z1))
    xp0 += wBy2
    yp0 = hBy2 - yp0
    xp1 += wBy2
    yp1 = hBy2 - yp1
    const dx = xp0 - xp1
    const dy = yp0 - yp1
    if (dx === 0 && dy === 0) {
      return
    }
    //
    // draw the shaft of the arrows
    //
    drawLine(g, xp0, yp0, xp1, yp1) // Draw the shaft of arrow.
    const adx = dx < 0 ? -dx : dx
    const ady = dy < 0 ? -dy : dy
    const dr = adx > ady ? adx + ady / 2 : ady + adx / 2

    //
    // draw arrowheads on both ends
    //
    let xp2, yp2
    xp2 = xp0 - (AHLEN * dx - AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    xp2 = xp0 - (AHLEN * dx + AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    xp2 = xp1 + (AHLEN * dx - AHWID * dy) / dr
    yp2 = yp1 + (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp1, yp1, xp2, yp2)
    xp2 = xp1 + (AHLEN * dx + AHWID * dy) / dr
    yp2 = yp1 + (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp1, yp1, xp2, yp2)

    //
    // draw the dimension bars on both ends
    //
    let xp3, yp3
    xp2 = xp0 - (DIMBAR * dy) / dr
    yp2 = yp0 + (DIMBAR * dx) / dr
    xp3 = xp0 + (DIMBAR * dy) / dr
    yp3 = yp0 - (DIMBAR * dx) / dr
    drawLine(g, xp2, yp2, xp3, yp3)

    xp2 = xp1 - (DIMBAR * dy) / dr
    yp2 = yp1 + (DIMBAR * dx) / dr
    xp3 = xp1 + (DIMBAR * dy) / dr
    yp3 = yp1 - (DIMBAR * dx) / dr
    drawLine(g, xp2, yp2, xp3, yp3)

    //
    // draw the label in the middle if it fits
    //
    const wid = g.measureText(label).width
    if (adx > ady) {
      if (adx > wid + 14) {
        //
        // label fits so draw it halfway between two ends
        //
        g.fillStyle = bgcolor
        xp = (xp0 + xp1) / 2
        yp = (yp0 + yp1) / 2
        fillRoundRect(g, xp - wid / 2, yp - tht / 2, wid, tht, 8, 8)
        g.fillStyle = labcolor
        g.fillText(label, xp - wid / 2, yp + tht / 3)
      } else {
        //
        // label won't fit between bars so draw an extension line and
        // put the label outside on point 1 end
        //
        xp2 = xp1 - (10 * dx) / dr
        yp2 = yp1 - (10 * dy) / dr
        drawLine(g, xp1, yp1, xp2, yp2)
        g.fillStyle = bgcolor
        fillRoundRect(g, xp2 - wid, yp2 - tht / 2, wid, tht, 8, 8)
        g.fillStyle = labcolor
        g.fillText(label, xp2 - wid, yp2 + tht / 3)
      }
    } else if (ady > tht + 14) {
      //
      // label fits so draw it halfway between two ends
      //
      g.fillStyle = bgcolor
      xp = (xp0 + xp1) / 2
      yp = (yp0 + yp1) / 2
      fillRoundRect(g, xp - wid / 2, yp - tht / 2, wid, tht, 8, 8)
      g.fillStyle = labcolor
      g.fillText(label, xp - wid / 2, yp + tht / 3)
    } else {
      //
      // label won't fit between bars so draw an extension line and
      // put the label outside on point 1 end
      //
      xp2 = xp1 - (15 * dx) / dr
      yp2 = yp1 - (15 * dy) / dr
      drawLine(g, xp1, yp1, xp2, yp2)
      g.fillStyle = bgcolor
      fillRoundRect(g, xp2 - wid / 2, yp2 - tht / 2, wid, tht, 8, 8)
      g.fillStyle = labcolor
      g.fillText(label, xp2 - wid / 2, yp2 + tht / 3)
    }
  }

  function dimLine2d(g, xp0, yp0, xp1, yp1, label, labcolor, bgcolor) {
    const dx = xp0 - xp1
    const dy = yp0 - yp1
    if (dx === 0 && dy === 0) {
      return
    }
    //
    // draw the shaft of the arrows
    //
    drawLine(g, xp0, yp0, xp1, yp1) // Draw the shaft of arrow.
    const adx = dx < 0 ? -dx : dx
    const ady = dy < 0 ? -dy : dy
    const dr = adx > ady ? adx + ady / 2 : ady + adx / 2

    //
    // draw arrowheads on both ends
    //
    let xp2, yp2, xp, yp, xp3, yp3
    xp2 = xp0 - (AHLEN * dx - AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    xp2 = xp0 - (AHLEN * dx + AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    xp2 = xp1 + (AHLEN * dx - AHWID * dy) / dr
    yp2 = yp1 + (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp1, yp1, xp2, yp2)
    xp2 = xp1 + (AHLEN * dx + AHWID * dy) / dr
    yp2 = yp1 + (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp1, yp1, xp2, yp2)

    //
    // draw the dimension bars on both ends
    //
    xp2 = xp0 - (DIMBAR * dy) / dr
    yp2 = yp0 + (DIMBAR * dx) / dr
    xp3 = xp0 + (DIMBAR * dy) / dr
    yp3 = yp0 - (DIMBAR * dx) / dr
    drawLine(g, xp2, yp2, xp3, yp3)

    xp2 = xp1 - (DIMBAR * dy) / dr
    yp2 = yp1 + (DIMBAR * dx) / dr
    xp3 = xp1 + (DIMBAR * dy) / dr
    yp3 = yp1 - (DIMBAR * dx) / dr
    drawLine(g, xp2, yp2, xp3, yp3)

    //
    // draw the label in the middle if it fits
    //
    if (label === null) {
      return
    }
    const wid = g.measureText(label).width
    if (adx > ady) {
      if (adx > wid + 14) {
        //
        // label fits so draw it halfway between two ends
        //
        g.fillStyle = bgcolor
        xp = (xp0 + xp1) / 2
        yp = (yp0 + yp1) / 2
        fillRoundRect(g, xp - wid / 2, yp - tht / 2, wid, tht, 8, 8)
        g.fillStyle = labcolor
        g.fillText(label, xp - wid / 2, yp + tht / 3)
      } else {
        //
        // label won't fit between bars so draw an extension line and
        // put the label outside on point 1 end
        //
        xp2 = xp1 - (10 * dx) / dr
        yp2 = yp1 - (10 * dy) / dr
        drawLine(g, xp1, yp1, xp2, yp2)
        g.fillStyle = bgcolor
        fillRoundRect(g, xp2 - wid, yp2 - tht / 2, wid, tht, 8, 8)
        g.fillStyle = labcolor
        g.fillText(label, xp2 - wid, yp2 + tht / 3)
      }
    } else if (ady > tht + 14) {
      //
      // label fits so draw it halfway between two ends
      //
      g.fillStyle = bgcolor
      xp = (xp0 + xp1) / 2
      yp = (yp0 + yp1) / 2
      fillRoundRect(g, xp - wid / 2, yp - tht / 2, wid, tht, 8, 8)
      g.fillStyle = labcolor
      g.fillText(label, xp - wid / 2, yp + tht / 3)
    } else {
      //
      // label won't fit between bars so draw an extension line and
      // put the label outside on point 1 end
      //
      xp2 = xp1 - (15 * dx) / dr
      yp2 = yp1 - (15 * dy) / dr
      drawLine(g, xp1, yp1, xp2, yp2)
      g.fillStyle = bgcolor
      fillRoundRect(g, xp2 - wid / 2, yp2 - tht / 2, wid, tht, 8, 8)
      g.fillStyle = labcolor
      g.fillText(label, xp2 - wid / 2, yp2 + tht / 3)
    }
  }

  function drawArrowLabel3d(
    g,
    x0,
    y0,
    z0,
    x1,
    y1,
    z1,
    label,
    labcolor,
    bgcolor
  ) {
    if (frontOnly) {
      const zp0 = m20 * x0 + m21 * y0 + m22 * z0
      const zp1 = m20 * x1 + m21 * y1 + m22 * z1
      if (zp0 < 0 || zp1 < 0) {
        return
      }
    }
    let xp0, yp0, xp1, yp1
    xp0 = Math.floor(pxlPerMm * (m00 * x0 + m01 * y0 + m02 * z0))
    yp0 = Math.floor(pxlPerMm * (m10 * x0 + m11 * y0 + m12 * z0))
    xp1 = Math.floor(pxlPerMm * (m00 * x1 + m01 * y1 + m02 * z1))
    yp1 = Math.floor(pxlPerMm * (m10 * x1 + m11 * y1 + m12 * z1))
    xp0 += wBy2
    yp0 = hBy2 - yp0
    xp1 += wBy2
    yp1 = hBy2 - yp1
    const dx = xp0 - xp1
    const dy = yp0 - yp1
    if (dx === 0 && dy === 0) {
      return
    }
    const adx = dx < 0 ? -dx : dx
    const ady = dy < 0 ? -dy : dy
    const dr = adx > ady ? adx + ady / 2 : ady + adx / 2
    const wid = g.measureText(label).width
    //
    // Draw arrowhead
    //
    let xp2, yp2
    xp2 = xp0 - (AHLEN * dx - AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy + AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    xp2 = xp0 - (AHLEN * dx + AHWID * dy) / dr
    yp2 = yp0 - (AHLEN * dy - AHWID * dx) / dr
    drawLine(g, xp0, yp0, xp2, yp2)
    //
    // lengthen the arrow's shaft so that the label doesn't hide it
    //
    if (adx * tht > ady * wid) {
      xp1 -= (dx * wid) / (2 * dr)
      yp1 -= (dy * wid) / (2 * dr)
    } else {
      xp1 -= (dx * tht) / (2 * dr)
      yp1 -= (dy * tht) / (2 * dr)
    }
    drawLine(g, xp0, yp0, xp1, yp1) // Draw the shaft of arrow.
    //
    // Draw label
    //
    if (bgcolor !== null) {
      g.fillStyle = bgcolor
      fillRoundRect(g, xp1 - wid / 2, yp1 - tht / 2, wid, tht, 8, 8)
    }
    g.fillStyle = labcolor
    g.fillText(label, xp1 - wid / 2, yp1 + tht / 3)
  }

  function drawLabel3d(g, x, y, z, label, labcolor, bgcolor) {
    let xp = Math.floor(pxlPerMm * (m00 * x + m01 * y + m02 * z))
    let yp = Math.floor(pxlPerMm * (m10 * x + m11 * y + m12 * z))
    xp += wBy2
    yp = hBy2 - yp
    const wid = g.measureText(label).width
    if (bgcolor !== null) {
      g.fillStyle = bgcolor
      fillRoundRect(g, xp - wid / 2, yp - tht / 2, wid, tht, 8, 8)
    }
    g.fillStyle = labcolor
    g.fillText(label, xp - wid / 2, yp + tht / 3)
  }

  function dimensionLines(g) {
    //
    // Draw dimension lines
    //
    // top view
    //
    let a, b, c, xtab, ytab, ztab
    const z0 = (zTopGirdAvg + zBotGirdAvg) / 2
    if (m22 > 1 - SNAP3D) {
      //
      // y direction total length
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        xmin - (24 * PIXSCL) / pxlPerMm,
        ymin,
        z0,
        xmin - (24 * PIXSCL) / pxlPerMm,
        ymax,
        z0,
        stringFormat(ymax - ymin, 2),
        col[0],
        bgColor
      )
      //
      // x direction total width
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        xmin,
        ymin - (18 * PIXSCL) / pxlPerMm,
        z0,
        xmax,
        ymin - (18 * PIXSCL) / pxlPerMm,
        z0,
        stringFormat(xmax - xmin, 2) + ' mm',
        col[0],
        bgColor
      )
    }
    //
    // bottom view
    //
    if (m22 < SNAP3D - 1) {
      //
      // x direction center width
      //
      g.strokeStyle = col[0]
      drawArrow3d(g, xcenmin, 0, z0, xcenmin - (2 * AHLEN) / pxlPerMm, 0, z0)
      drawArrowLabel3d(
        g,
        xcenmax,
        0,
        z0,
        xcenmax + (2 * AHLEN) / pxlPerMm,
        0,
        z0,
        stringFormat(xcenmax - xcenmin, 2),
        col[0],
        bgColor /*, 5 */
      )
      //
      // y direction center width
      //
      g.strokeStyle = col[0]
      drawArrow3d(g, 0, ycenmax, z0, 0, ycenmax + (2 * AHLEN) / pxlPerMm, z0)
      drawArrowLabel3d(
        g,
        0,
        ycenmin,
        z0,
        0,
        ycenmin - (2 * AHLEN) / pxlPerMm,
        z0,
        stringFormat(ycenmax - ycenmin, 2),
        col[0],
        bgColor /*, 5 */
      )
    }
    //
    // south end view || north end view
    //
    if (m21 < SNAP3D - 1 || m21 > 1 - SNAP3D) {
      //
      // total depth
      //
      a = 0
      b = 0
      c = -totalDepth
      xtab = xmin - (18 * PIXSCL) / pxlPerMm
      ytab = 0
      ztab = tz0
      if (tableFacet !== -1) {
        a = -anorm[tableFacet] * totalDepth
        b = -bnorm[tableFacet] * totalDepth
        c = -cnorm[tableFacet] * totalDepth
        ztab =
          (dnorm[tableFacet] -
            anorm[tableFacet] * xtab -
            bnorm[tableFacet] * ytab) /
          cnorm[tableFacet]
      }
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        xtab,
        ytab,
        ztab,
        xtab + a,
        ytab + b,
        ztab + c,
        stringFormat(totalDepth, 2),
        col[0],
        bgColor
      )
      //
      // crown height
      //
      if (!cabochon) {
        g.strokeStyle = col[0]
        dimLine3d(
          g,
          xmax + AHLEN / pxlPerMm,
          0,
          zTopGirdAvg,
          xmax + AHLEN / pxlPerMm,
          0,
          tz0,
          stringFormat(tz0 - zTopGirdAvg, 2),
          col[0],
          bgColor
        )
        //
        // pavilion depth
        //
        g.strokeStyle = col[0]
        dimLine3d(
          g,
          xmax + AHLEN / pxlPerMm,
          0,
          zmin,
          xmax + AHLEN / pxlPerMm,
          0,
          zBotGirdAvg,
          stringFormat(zBotGirdAvg - zmin, 2) + ' mm',
          col[0],
          bgColor
        )
      }
      //
      // total width
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        xmin,
        0,
        zmin - AHLEN / pxlPerMm,
        xmax,
        0,
        zmin - AHLEN / pxlPerMm,
        stringFormat(xmax - xmin, 2),
        col[0],
        bgColor
      )
      //
      // table width
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        txmin,
        0,
        zmax + AHLEN / pxlPerMm,
        txmax,
        0,
        zmax + AHLEN / pxlPerMm,
        stringFormat(txmax - txmin, 2),
        col[0],
        bgColor
      )
    }
    //
    // east side view || west side view
    //
    if (m20 > 1 - SNAP3D || m20 < SNAP3D - 1) {
      //
      // total depth
      //
      a = 0
      b = 0
      c = -totalDepth
      xtab = 0
      ytab = ymin - AHLEN / pxlPerMm
      ztab = tz0
      if (tableFacet !== -1) {
        a = -anorm[tableFacet] * totalDepth
        b = -bnorm[tableFacet] * totalDepth
        c = -cnorm[tableFacet] * totalDepth
        ztab =
          (dnorm[tableFacet] -
            anorm[tableFacet] * xtab -
            bnorm[tableFacet] * ytab) /
          cnorm[tableFacet]
      }
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        xtab,
        ytab,
        ztab,
        xtab + a,
        ytab + b,
        ztab + c,
        stringFormat(totalDepth, 2),
        col[0],
        bgColor
      )
      //
      // crown height
      //
      if (!cabochon) {
        g.strokeStyle = col[0]
        dimLine3d(
          g,
          0,
          ymax + AHLEN / pxlPerMm,
          zTopGirdAvg,
          0,
          ymax + AHLEN / pxlPerMm,
          tz0,
          stringFormat(tz0 - zTopGirdAvg, 2),
          col[0],
          bgColor
        )
        //
        // pavilion depth
        //
        g.strokeStyle = col[0]
        dimLine3d(
          g,
          0,
          ymax + AHLEN / pxlPerMm,
          zmin,
          0,
          ymax + AHLEN / pxlPerMm,
          zBotGirdAvg,
          stringFormat(zBotGirdAvg - zmin, 2) + ' mm',
          col[0],
          bgColor
        )
      }
      //
      // total length
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        0,
        ymin,
        zmin - AHLEN / pxlPerMm,
        0,
        ymax,
        zmin - AHLEN / pxlPerMm,
        stringFormat(ymax - ymin, 2),
        col[0],
        bgColor
      )
      //
      // table length
      //
      g.strokeStyle = col[0]
      dimLine3d(
        g,
        0,
        tymin,
        zmax + AHLEN / pxlPerMm,
        0,
        tymax,
        zmax + AHLEN / pxlPerMm,
        stringFormat(tymax - tymin, 2),
        col[0],
        bgColor
      )
    }
  }

  function calcGirdleThickness(x2, y2) {
    let den
    let i, j, k
    const x1 = 0
    const y1 = 0
    let u12, u34, x3, y3, z3, x4, y4, z4
    //
    // Find where vector from z axis to point (x2, y2) crosses the crown
    // girdle outline
    //
    let xcgm = 0
    let ycgm = 0
    let zcgm = 0
    for (j = 0; j < ncg; j++) {
      // exhaustive search is inefficent but simple
      k = (j + 1) % ncg
      x3 = xcg[j]
      y3 = ycg[j]
      z3 = zcg[j]
      x4 = xcg[k]
      y4 = ycg[k]
      z4 = zcg[k]
      den = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
      if (den === 0) {
        continue
      }
      u12 = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / den
      if (u12 < 0) {
        continue
      } // on other side of stone
      u34 = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / den
      if (u34 >= 0 && u34 <= 1) {
        xcgm = x3 + u34 * (x4 - x3)
        ycgm = y3 + u34 * (y4 - y3)
        zcgm = z3 + u34 * (z4 - z3)
        break
      }
    }
    //
    // Find where vector from z axis to point (x2, y2) crosses the pavilion
    // girdle outline
    //
    for (j = 0; j < npg; j++) {
      k = (j + 1) % npg
      x3 = xpg[j]
      y3 = ypg[j]
      z3 = zpg[j]
      x4 = xpg[k]
      y4 = ypg[k]
      z4 = zpg[k]
      den = (y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1)
      if (den === 0) {
        continue
      }
      u12 = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / den
      if (u12 < 0) {
        continue
      } // on other side of stone
      u34 = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / den
      if (u34 >= 0 && u34 <= 1) {
        xpgm = x3 + u34 * (x4 - x3)
        ypgm = y3 + u34 * (y4 - y3)
        zpgm = z3 + u34 * (z4 - z3)
        break
      }
    }
  }

  function paint(g) {
    let j, i, n, k, q, label, fct
    //
    // Erase the canvas (now done by caller)
    // g.clearRect(0, 0, w, h)
    //
    // Instruction legend. TODO: remove when mouse event handler added
    //
    g.lineWidth = ratio
    g.lineJoin = 'round'
    g.lineCap = 'round'
    g.font = ratio * 8 + 'pt sans-serif'
    //
    // JavaScript doesn't have method to measure text height, so we fake
    // it by measuring the width of an m and fudging it. Won't take into
    // account descenders on g p, etc. This also allows us to specify
    // font in point size but measure in pixels.
    //
    tht = 1.25 * g.measureText('m').width
    AHWID = tht * 0.2 // /** arrowhead half width in pixels */
    AHLEN = tht * 0.75
    DIMBAR = tht * 0.5

    //
    // Draw all of the dimension lines. The ones in back of the stone will get
    // covered by the stone
    //
    frontOnly = false
    dimensionLines(g)
    //
    // Draw filled facets
    //
    for (i = 0; i < nFacet; i++) {
      if (cnorm1[i] > 0) {
        // visibility test
        for (j = 0; j < facet[i].length; j++) {
          xPol[j] = wBy2 + xvertP[facet[i][j]]
          yPol[j] = hBy2 - yvertP[facet[i][j]]
        }
        g.fillStyle = col[Math.floor(cnorm1[i])]
        fillPolygon(g, xPol, yPol, facet[i].length)
      }
    }
    //
    // Outline the facets (except girdles)
    //
    g.strokeStyle = col[0]
    if (!cabochon) {
      // don't outline facets if it's a cabochon
      for (i = 0; i < nFacet; i++) {
        n = facet[i].length
        if (cnorm1[i] > 0 && (facetType[i] !== GRD || i === labeledFacet)) {
          for (j = 0; j < n; j++) {
            xPol[j] = wBy2 + xvertP[facet[i][j]]
            yPol[j] = hBy2 - yvertP[facet[i][j]]
          }
          drawPolygon(g, xPol, yPol, n)
        }
      }
    }
    if (labeledFacet !== -1 && facetType[labeledFacet] === GRD) {
      g.strokeStyle = Red
      if (cnorm1[labeledFacet] > 20) {
        // visible and not too close to edge
        //
        // Draw crown girdle outline
        //
        for (i = 0; i < ncg; i++) {
          fct = fcg[i]
          if (cnorm1[fct] < 0) {
            continue
          }
          j = (i + 1) % ncg
          drawLine(
            g,
            wBy2 + xcgP[i],
            hBy2 - ycgP[i],
            wBy2 + xcgP[j],
            hBy2 - ycgP[j]
          )
        }
        //
        // Draw pavilion girdle outline
        //
        for (i = 0; i < npg; i++) {
          fct = fpg[i]
          if (cnorm1[fct] < 0) {
            continue
          }
          j = (i + 1) % npg
          drawLine(
            g,
            wBy2 + xpgP[i],
            hBy2 - ypgP[i],
            wBy2 + xpgP[j],
            hBy2 - ypgP[j]
          )
        }
        //
        // The girdle dimension is from [xpgm, ypgm, xpgm] to
        // [xcgm, ycgm, zcgm]. These are calculated by calcGirdleThickness()
        // in response to a mouse click in a girdle facet
        //
        const dx = xcgm - xpgm
        const dy = ycgm - ypgm
        const dz = zcgm - zpgm
        const dr = Math.sqrt(dx * dx + dy * dy + dz * dz)
        let x0, y0, z0
        if (dr === 0) {
          x0 = xpgm
          y0 = ypgm
          z0 = zpgm - 20 / pxlPerMm
        } else {
          x0 = xpgm - (20 * dx) / (dr * pxlPerMm)
          y0 = ypgm - (20 * dy) / (dr * pxlPerMm)
          z0 = zpgm - (20 * dz) / (dr * pxlPerMm)
        }
        g.strokeStyle = Red
        g.fillStyle = Red
        drawArrow3d(g, xpgm, ypgm, zpgm, x0, y0, z0)
        if (dr === 0) {
          x0 = xcgm
          y0 = ycgm
          z0 = zcgm + 30 / pxlPerMm
        } else {
          x0 = xcgm + (30 * dx) / (dr * pxlPerMm)
          y0 = ycgm + (30 * dy) / (dr * pxlPerMm)
          z0 = zcgm + (30 * dz) / (dr * pxlPerMm)
        }
        drawArrow3d(g, xcgm, ycgm, zcgm, x0, y0, z0)
        label = stringFormat(dr, 2) + ' mm'
        drawLabel3d(g, x0, y0, z0, label, Red, col[255])
      }
    }
    //
    // Label the facet angle of a single facet unless the facet is
    // not visible or too close to edge. The 20 threshold is on a scale of
    // 0 to 254.5
    //
    if (labeledFacet !== -1 && cnorm1[labeledFacet] > 20) {
      //
      // label at centroid of facet
      //
      i = labeledFacet
      n = facet[i].length
      let bottomFacet = false
      for (j = 0; j < n; j++) {
        k = facet[i][j]
        xPol[j] = wBy2 + xvertP[k]
        yPol[j] = hBy2 - yvertP[k]
        bottomFacet |= Math.abs(zvert[k] - zmin) < 0.1
      }

      let x = 0
      let y = 0
      let halfarea = 0
      for (j = 0; j < n; j++) {
        k = (j + 1) % n
        const da = xPol[j] * yPol[k] - xPol[k] * yPol[j]
        halfarea += da
        x += da * (xPol[j] + xPol[k])
        y += da * (yPol[j] + yPol[k])
      }
      if (halfarea !== 0) {
        x /= 3 * halfarea
        y /= 3 * halfarea
        if (facetType[labeledFacet] !== GRD) {
          g.fillStyle = col[255] /// /
          fillPolygon(g, xPol, yPol, n)
          g.strokeStyle = Red
          //        g.fillStyle=(col[0])
          drawPolygon(g, xPol, yPol, n)
        }

        if (labeledFacet !== -1 && labeledFacet === tableFacet) {
          let x2 = x
          let y2 = y
          let rmax = 0
          for (j = 0; j < n; j++) {
            const r = xvertP[facet[i][j]] + yvertP[facet[i][j]]
            if (r > rmax) {
              rmax = r
              x2 = xPol[j]
              y2 = yPol[j]
            }
          }
          x = (x + x2) / 2
          y = (y + y2) / 2
        }
        let ang = angle[labeledFacet]
        if (facetType[labeledFacet] !== GRD) {
          ang = Math.abs(ang)
        }
        if (extraInfo) {
          label =
            stringFormat(ang, 1) +
            DEG +
            ' ' +
            stringFormat(azi[labeledFacet], 1) +
            DEG
        } else {
          label = stringFormat(ang, 1) + DEG
        }
        const wid = g.measureText(label).width
        if (facetType[labeledFacet] !== GRD) {
          g.fillStyle = col[255]
          fillRoundRect(
            g,
            Math.floor(x - wid / 2),
            Math.floor(y - tht / 2),
            wid,
            tht,
            8,
            8
          )
          g.fillStyle = col[0]
          g.fillText(label, Math.floor(x - wid / 2), Math.floor(y + tht / 3))
        }
      }
      if (bottomFacet && !cabochon) {
        //      snap = false
        g.fillStyle = Red
        // Draw a line down axis of stone
        drawLine3d(g, 0, 0, tz0 + 30 / pxlPerMm, 0, 0, cz0 - 30 / pxlPerMm)
        // Draw square around axis at top and bottom
        q = 3 / pxlPerMm
        drawLine3d(g, q, q, cz0, q, -q, cz0)
        drawLine3d(g, q, -q, cz0, -q, -q, cz0)
        drawLine3d(g, -q, -q, cz0, -q, q, cz0)
        drawLine3d(g, -q, q, cz0, q, q, cz0)
        drawLine3d(g, q, q, tz0, q, -q, tz0)
        drawLine3d(g, q, -q, tz0, -q, -q, tz0)
        drawLine3d(g, -q, -q, tz0, -q, q, tz0)
        drawLine3d(g, -q, q, tz0, q, q, tz0)
        // Draw + at culet center
        q = 5 / pxlPerMm
        drawLine3d(g, cxcen - q, cycen, czcen, cxcen + q, cycen, czcen)
        drawLine3d(g, cxcen, cycen - q, czcen, cxcen, cycen + q, czcen)
        label = stringFormat(Math.sqrt(cxcen * cxcen + cycen * cycen), 2)
        drawLabel3d(
          g,
          cxcen + 30 / pxlPerMm,
          cycen + 20 / pxlPerMm,
          czcen - 10 / pxlPerMm,
          label,
          Red,
          col[255]
        )
      } else {
        snap = true
      }
    }
    //
    // Draw the table outline and dimensions
    //
    if (
      labeledFacet !== -1 &&
      labeledFacet === tableFacet &&
      cnorm1[tableFacet] > 20
    ) {
      n = facet[tableFacet].length
      //
      // Draw table outline
      //
      g.fillStyle = Red
      for (j = 0; j < n; j++) {
        xPol[j] = wBy2 + xvertP[facet[tableFacet][j]]
        yPol[j] = hBy2 - yvertP[facet[tableFacet][j]]
      }
      drawPolygon(g, xPol, yPol, n)
      //
      // Draw table diameter or table center
      //
      label = stringFormat(rdim, 2)
      if (rdim * pxlPerMm < 20) {
        //      drawLine3d(g, xdim0, ydim0, zdim0, xdim1, ydim1, zdim1)
        // Draw line down axis of stone
        drawLine3d(g, 0, 0, cz0 - 30 / pxlPerMm, 0, 0, tz0 + 30 / pxlPerMm)
        // Draw square around axis
        q = (3 * PIXSCL) / pxlPerMm
        drawLine3d(g, q, q, tz0, q, -q, tz0)
        drawLine3d(g, q, -q, tz0, -q, -q, tz0)
        drawLine3d(g, -q, -q, tz0, -q, q, tz0)
        drawLine3d(g, -q, q, tz0, q, q, tz0)
        drawLine3d(g, q, q, cz0, q, -q, cz0)
        drawLine3d(g, q, -q, cz0, -q, -q, cz0)
        drawLine3d(g, -q, -q, cz0, -q, q, cz0)
        drawLine3d(g, -q, q, cz0, q, q, cz0)
        // Draw + at table center
        q = (5 * PIXSCL) / pxlPerMm
        drawLine3d(g, txcen - q, tycen, tzcen, txcen + q, tycen, tzcen)
        drawLine3d(g, txcen, tycen - q, tzcen, txcen, tycen + q, tzcen)
        drawLabel3d(
          g,
          txcen + 30 / pxlPerMm,
          tycen + 20 / pxlPerMm,
          tzcen + 10 / pxlPerMm,
          label,
          Red,
          col[255]
        )
        //      snap = false
      } else {
        snap = true
        drawArrow3d(g, xdim0, ydim0, zdim0, xdim1, ydim1, zdim1)
        drawArrow3d(g, xdim1, ydim1, zdim1, xdim0, ydim0, zdim0)
        drawLabel3d(
          g,
          (xdim0 + xdim1) / 2,
          (ydim0 + ydim1) / 2,
          (zdim0 + zdim1) / 2,
          label,
          Red,
          col[255]
        )
      }
    }
    //
    // Draw the dimension lines that are in front of the stone
    //
    frontOnly = true
    dimensionLines(g)
    //
    // Draw the screen bounding box dimensions. These grow and shrink as
    // the stone is rotated.
    //
    let xp0, yp0, xp1, yp1
    xp0 = wBy2 + Math.floor(pxlPerMm * xrmin)
    xp1 = wBy2 + Math.floor(pxlPerMm * xrmax)
    yp0 = h - 10 * PIXSCL
    yp1 = yp0
    // yp1 = h-5*PIXSCL
    g.fillStyle = col[0]
    dimLine2d(
      g,
      xp0,
      yp0,
      xp1,
      yp1,
      stringFormat(xrmax - xrmin, 2) + ' mm',
      col[0],
      bgColor
    )

    xp0 = 10 * PIXSCL
    yp0 = Math.floor(hBy2 - yrmin * pxlPerMm)
    xp1 = xp0
    yp1 = Math.floor(hBy2 - yrmax * pxlPerMm)
    g.fillStyle = col[0]
    dimLine2d(g, xp0, yp0, xp1, yp1, null, col[0], null)

    xp0 = 10 * PIXSCL
    yp0 = yp1
    xp1 = 10 * PIXSCL
    yp1 = yp0 - 14 * PIXSCL
    drawLine(g, xp0, yp0, xp1, yp1)
    label = stringFormat(yrmax - yrmin, 2)
    g.fillText(label, 1, yp1 - 2)
  }

  function calcTableDiameter() {
    let dx, dy, dz, i
    if (tableFacet === -1) {
      return
    }
    const nv = facet[tableFacet].length
    let isav = 0
    let drmin = 32767
    for (i = 0; i <= nv; i++) {
      let xv, yv
      if (i === nv) {
        // use table center of area
        xv = Math.floor(pxlPerMm * (m00 * txcen + m01 * tycen + m02 * tzcen))
        yv = Math.floor(pxlPerMm * (m10 * txcen + m11 * tycen + m12 * tzcen))
        xv = wBy2 + xv
        yv = hBy2 - yv
      } else {
        xv = wBy2 + xvertP[facet[tableFacet][i]]
        yv = hBy2 - yvertP[facet[tableFacet][i]]
      }
      dx = xv - xMouse
      if (dx < 0) dx = -dx
      dy = yv - yMouse
      if (dy < 0) dy = -dy
      const dr = dx < dy ? dy + dx / 2 : dx + dy / 2 // approx. hypotenuse
      if (dr < drmin) {
        drmin = dr
        isav = i
      }
    }
    if (isav === nv) {
      xdim0 = txcen
      ydim0 = tycen
      zdim0 = tzcen
      xdim1 = 0
      ydim1 = 0
      zdim1 = tz0
      dx = xdim0 - xdim1
      dy = ydim0 - ydim1
      dz = zdim0 - zdim1
      rdim = dx * dx + dy * dy + dz * dz // diameter squared
    } else {
      xdim0 = xvert[facet[tableFacet][isav]]
      ydim0 = yvert[facet[tableFacet][isav]]
      zdim0 = zvert[facet[tableFacet][isav]]
      let dmax = -1e10
      let imax = 0
      for (i = 0; i < nv; i++) {
        if (i === isav) {
          continue
        }
        const x1 = xvert[facet[tableFacet][i]]
        const y1 = yvert[facet[tableFacet][i]]
        const z1 = zvert[facet[tableFacet][i]]
        // The dot product of one vertex coordinates with the negative
        // of the other coordinate will tend to select points opposite
        // the origin.
        const dot = -xdim0 * x1 - ydim0 * y1 - zdim0 * z1 // dot product
        dx = xdim0 - x1
        dy = ydim0 - y1
        dz = zdim0 - z1
        const dia2 = dx * dx + dy * dy + dz * dz // diameter squared
        const norm = dot
        if (norm > dmax) {
          rdim = dia2
          dmax = norm
          imax = i
        }
      }
      xdim1 = xvert[facet[tableFacet][imax]]
      ydim1 = yvert[facet[tableFacet][imax]]
      zdim1 = zvert[facet[tableFacet][imax]]
    }
    rdim = Math.sqrt(rdim)
  }

  function Point3dOnFacet(iFacet) {
    // The mouse pointer in stone screen coordinates is the endpoint of the line of sight vector.
    const iv = facet[iFacet][0]
    const mx = (xMouse - wBy2) / pxlPerMm
    const my = (hBy2 - yMouse) / pxlPerMm
    const mz = 0
    // line of sight direction vector
    const a = 0
    const b = 0
    const c = 1
    // rotation matrix coefficients
    const a00 = m00
    const a01 = m01
    const a02 = m02
    const a10 = m10
    const a11 = m11
    const a12 = m12
    const a20 = m20
    const a21 = m21
    const a22 = m22
    // inverse of rotation matrix
    const det =
      a00 * (a22 * a11 - a21 * a12) -
      a10 * (a22 * a01 - a21 * a02) +
      a20 * (a12 * a01 - a11 * a02)
    const b00 = (a22 * a11 - a21 * a12) / det
    const b01 = -(a22 * a01 - a21 * a02) / det
    const b02 = (a12 * a01 - a11 * a02) / det
    const b10 = -(a22 * a10 - a20 * a12) / det
    const b11 = (a22 * a00 - a20 * a02) / det
    const b12 = -(a12 * a00 - a10 * a02) / det
    const b20 = (a21 * a10 - a20 * a11) / det
    const b21 = -(a21 * a00 - a20 * a01) / det
    const b22 = (a11 * a00 - a10 * a01) / det
    // Rotate the line of sight vector endpoint
    const xr = b00 * mx + b01 * my + b02 * mz
    const yr = b10 * mx + b11 * my + b12 * mz
    const zr = b20 * mx + b21 * my + b22 * mz
    // Rotate the line of sight direction vector
    const ar = b00 * a + b01 * b + b02 * c
    const br = b10 * a + b11 * b + b12 * c
    const cr = b20 * a + b21 * b + b22 * c
    // Calculate the point of intersection
    const d = anorm[iFacet] * ar + bnorm[iFacet] * br + cnorm[iFacet] * cr
    const e = anorm[iFacet] * xr + bnorm[iFacet] * yr + cnorm[iFacet] * zr
    const q = (dnorm[iFacet] - e) / d
    return {
      x: xr + ar * q,
      y: yr + br * q,
      z: zr + cr * q
    }
  }

  function mouseClicked(pt) {
    let j
    xMouse = pt.x
    yMouse = pt.y
    const prevFacet = labeledFacet
    labeledFacet = -1
    extraInfo = false
    // Find the facet surrounding the mouse pointer
    for (let i = 0; i < nFacet; i++) {
      if (cnorm1[i] > 0) {
        for (j = 0; j < facet[i].length; j++) {
          xPol[j] = wBy2 + xvertP[facet[i][j]]
          yPol[j] = hBy2 - yvertP[facet[i][j]]
        }
        let inpoly = false
        const len = facet[i].length
        for (let k = 0; k < len; k++) {
          j = (k + 1) % len
          if (
            yPol[j] > yMouse !== yPol[k] > yMouse &&
            xMouse <
              ((xPol[k] - xPol[j]) * (yMouse - yPol[j])) / (yPol[k] - yPol[j]) +
                xPol[j]
          ) {
            inpoly = !inpoly
          }
        }
        if (inpoly) {
          extraInfo = i === prevFacet
          labeledFacet = i
          break
        }
      }
    }
    // Girdle facet?
    if (labeledFacet !== -1 && facetType[labeledFacet] === GRD) {
      pt = new Point3dOnFacet(labeledFacet)
      calcGirdleThickness(pt.x, pt.y)
    }
    // Table facet?
    if (labeledFacet !== -1 && labeledFacet === tableFacet) {
      calcTableDiameter()
    }
  }

  function getMousePos(canvas, evt) {
    const rect = canvas.getBoundingClientRect()
    return {
      x: ratio * (evt.clientX - rect.left),
      y: ratio * (evt.clientY - rect.top),
      out: false // for compatability with touch
    }
  }

  function getTouchPos(canvas, evt) {
    const touchobj = evt.changedTouches[0]
    const rect = canvas.getBoundingClientRect()
    return {
      x: ratio * (touchobj.clientX - rect.left),
      y: ratio * (touchobj.clientY - rect.top),
      out:
        touchobj.clientX < rect.left ||
        touchobj.clientX > rect.right ||
        touchobj.clientY < rect.top ||
        touchobj.clientY > rect.bottom
    }
  }

  function drawball(g) {
    g.lineWidth = ratio
    g.strokeStyle = '#A0A0A0'
    g.beginPath()
    const w = cvs.width / 2
    const h = cvs.height / 2
    const rad = radius
    g.arc(cvs.width / 2, cvs.height / 2, rad, 0, 2 * Math.PI)
    g.stroke()
    for (let i = -1; i < 2; i += 2) {
      // i is sign, -1 then +1
      g.strokeStyle = i * iz >= 0 ? 'red' : 'pink'
      g.beginPath()
      g.moveTo(w, h)
      g.lineTo(rad * i * ix + w, rad * i * iy + h)
      g.stroke()

      g.fillStyle = g.strokeStyle
      g.beginPath()
      g.arc(rad * i * ix + w, rad * i * iy + h, i + 4 * ratio, 0, 2 * Math.PI)
      g.closePath()
      g.fill()

      g.strokeStyle = i * jz >= 0 ? 'green' : 'lightGreen'
      g.beginPath()
      g.moveTo(w, h)
      g.lineTo(rad * i * jx + w, rad * i * jy + h)
      g.stroke()

      g.fillStyle = g.strokeStyle
      g.beginPath()
      g.arc(rad * i * jx + w, rad * i * jy + h, i + 4 * ratio, 0, 2 * Math.PI)
      g.closePath()
      g.fill()

      g.strokeStyle = i * kz >= 0 ? 'blue' : 'lightBlue'
      g.beginPath()
      g.moveTo(w, h)
      g.lineTo(rad * i * kx + w, rad * i * ky + h)
      g.stroke()

      g.fillStyle = g.strokeStyle
      g.beginPath()
      g.arc(rad * i * kx + w, rad * i * ky + h, i + 4 * ratio, 0, 2 * Math.PI)
      g.closePath()
      g.fill()
    }
  }

  return cvs
}
