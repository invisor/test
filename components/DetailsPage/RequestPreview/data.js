export const occasions = [
  'Engagement',
  'Anniversary',
  'Gift For Loved One',
  'Birthday',
  'Graduation',
  'Personal Purchase',
  'Vedic Astrology',
  'Wedding Need/Ring Band',
  'Other'
]

export const timeframes = [
  'As soon as possible',
  'In the next few weeks',
  'In the next few months',
  'Sometime in the next 6 months',
  '6+ months',
  'Just browsing'
]
