// Add items to favorites and compare list for module and table view

import { mapActions, mapGetters, mapState } from 'vuex'
import cloneDeep from 'lodash/cloneDeep'
import { productListTypeByCategory } from '~~/utils/definitions/defaults'

export default {
  props: {
    item: {
      type: Object,
      default: () => ({})
    },

    disabled: {
      type: Boolean,
      default: false
    }
  },

  inject: ['pageType'],

  data: () => ({
    itemLoad: false
  }),

  computed: {
    ...mapState({
      productDetails: (state) => state.productDetails.productDetails,
      fetchingItemState: (state) => state.customItem.fetchingItem
    }),

    ...mapGetters({
      selectedOptions: 'productDetails/selectedOptions',
      inListFavorites: 'favorites/inList',
      inListCompareList: 'compareList/inList'
    }),

    isDetails() {
      return this.pageType === 'details'
    },

    isReview() {
      return this.pageType === 'review'
    },

    /**
     * We don't need fetch full item details on details and review pages b/c item already fetched
     * @returns {boolean}
     */
    skipFullItemFetch() {
      return this.isDetails || this.isReview
    },

    blockControls() {
      if (this.item.dataType === 2) return this.fetchingItemState
      return this.itemLoad && this.$store.state.auth.loggedIn
    },

    productListType() {
      if (this.item.dataType === 2) return 'custom'
      return productListTypeByCategory[this.item.category]
    },

    wishlistState() {
      return this.inListFavorites(this.item)
    },

    compareListState() {
      return this.inListCompareList(this.item)
    }
  },

  methods: {
    ...mapActions({
      addToWishlist: 'favorites/addToList',
      addToCompareList: 'compareList/addToList',
      fetchDetails: 'productDetails/fetchDetails',
      fetchingItem: 'customItem/fetchingItem',
      getCustomItem: 'customItem/getCustomItem',
      getFinalOptions: 'customItem/getFinalOptions',
      fetchPrice: 'customItem/fetchPrice',
      resetCustomItem: 'customItem/resetCustomItem',
      replaceWishlistItem: 'favorites/replaceListItem',
      replaceCompareItem: 'compareList/replaceListItem'
    }),

    onAddToWishList() {
      if (this.disabled) return
      if (this.blockControls) return
      // if item not in wishlist it should be fetched
      const fetchFullItem = !this.wishlistState

      this.instantAddToWishlist()

      if (fetchFullItem && !this.skipFullItemFetch)
        this.fetchFullItem(this.replaceInstantItemWithFullItemWishlist)
    },

    onAddToCompareList() {
      if (this.disabled) return
      if (this.blockControls) return
      // if item not in compare list it should be fetched
      const fetchFullItem = !this.compareListState

      this.instantAddToCompareList()
      if (fetchFullItem && !this.skipFullItemFetch)
        this.fetchFullItem(this.replaceInstantItemWithFullItemCompare)
    },

    instantAddToWishlist() {
      this.addToWishlist({
        item: cloneDeep({
          ...this.item,
          placeholder: !this.skipFullItemFetch,
          eventsStop: !this.skipFullItemFetch // stop events emit by placeholder on visible to avoid multiple fetch details
        }),
        serverSync: this.skipFullItemFetch
      })
    },

    instantAddToCompareList() {
      this.addToCompareList({
        item: cloneDeep({
          ...this.item,
          placeholder: !this.skipFullItemFetch,
          eventsStop: !this.skipFullItemFetch // stop events emit by placeholder on visible to avoid multiple fetch details
        }),
        serverSync: this.skipFullItemFetch
      })
    },

    async fetchFullItem(replaceFunction) {
      this.itemLoad = true
      if (this.item.dataType === 2) {
        this.fetchingItem(this.itemLoad)
        replaceFunction(await this.fetchPreviewItem())
        this.resetCustomItem()
        this.itemLoad = false
        this.fetchingItem(this.itemLoad)
        return
      }
      replaceFunction(await this.fetchSimpleItem())
      this.itemLoad = false
      this.fetchingItem(this.itemLoad)
    },

    async fetchPreviewItem() {
      const regEx = /(JS\d+|js\d+)([A-Za-z\d]+)-(.+)$/gm
      const result = regEx.exec(this.item.id)
      const query = {
        id: result[1],
        stoneId: result[3],
        metalTypeCode: result[2]
        // TODO add preferable ring size
      }
      await this.getFinalOptions(query)
      await this.fetchPrice()

      const item = await this.getCustomItem()
      return item
    },

    async fetchSimpleItem() {
      const params = { ...this.item, type: this.productListType }
      await this.fetchDetails(params)
      return cloneDeep({
        ...this.productDetails,
        selectedOptions: this.selectedOptions
      })
    },

    replaceInstantItemWithFullItemWishlist(item) {
      this.replaceWishlistItem({ item })
      if (item.category === 'Custom') this.resetCustomItem() // remove custom item data on preview item add
    },

    replaceInstantItemWithFullItemCompare(item) {
      this.replaceCompareItem({ item })
      if (item.category === 'Custom') this.resetCustomItem() // remove custom item data on preview item add
    }
  }
}
