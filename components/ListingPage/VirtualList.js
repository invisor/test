import Vue from 'vue'
import VirtualList from 'vue-virtual-scroll-list'

const EVENT_TYPE = {
  ITEM: 'item_resize',
  SLOT: 'slot_resize'
}

const SLOT_TYPE = {
  HEADER: 'header', // string value also use for aria role attribute
  FOOTER: 'footer'
}

const ItemProps = {
  index: {
    type: Number
  },
  event: {
    type: String
  },
  tag: {
    type: String
  },
  horizontal: {
    type: Boolean
  },
  source: {
    type: Object
  },
  component: {
    type: [Object, Function]
  },
  uniqueKey: {
    type: [String, Number]
  },
  extraProps: {
    type: Object
  },
  scopedSlots: {
    type: Object
  }
}

const SlotProps = {
  event: {
    type: String
  },
  uniqueKey: {
    type: String
  },
  tag: {
    type: String
  },
  horizontal: {
    type: Boolean
  }
}

const Wrapper = {
  created: function created() {
    this.shapeKey = this.horizontal ? 'offsetWidth' : 'offsetHeight'
  },
  mounted: function mounted() {
    const _this = this

    if (typeof ResizeObserver !== 'undefined') {
      this.resizeObserver = new ResizeObserver(function() {
        _this.dispatchSizeChange()
      })
      this.resizeObserver.observe(this.$el)
    }
  },
  // since componet will be reused, so disptach when updated
  updated: function updated() {
    this.dispatchSizeChange()
  },
  beforeDestroy: function beforeDestroy() {
    if (this.resizeObserver) {
      this.resizeObserver.disconnect()
      this.resizeObserver = null
    }
  },
  methods: {
    getCurrentSize: function getCurrentSize() {
      return this.$el ? this.$el[this.shapeKey] : 0
    },
    // tell parent current size identify by unqiue key
    dispatchSizeChange: function dispatchSizeChange() {
      this.$parent.$emit(
        this.event,
        this.uniqueKey,
        this.getCurrentSize(),
        this.hasInitial
      )
    }
  }
}

const Item = Vue.component('virtual-list-item', {
  mixins: [Wrapper],
  props: ItemProps,
  render: function render(h) {
    const tag = this.tag
    const component = this.component
    const _this$extraProps = this.extraProps
    const extraProps = _this$extraProps === void 0 ? {} : _this$extraProps
    const index = this.index
    const _this$scopedSlots = this.scopedSlots
    const scopedSlots = _this$scopedSlots === void 0 ? {} : _this$scopedSlots
    const uniqueKey = this.uniqueKey
    extraProps.source = this.source
    extraProps.index = index
    return h(
      tag,
      {
        key: uniqueKey,
        attrs: {
          role: 'listitem'
        }
      },
      [
        h(component, {
          props: extraProps,
          scopedSlots
        })
      ]
    )
  }
}) // wrapping for slot

const Slot = Vue.component('virtual-list-slot', {
  mixins: [Wrapper],
  props: SlotProps,
  render: function render(h) {
    const tag = this.tag
    const uniqueKey = this.uniqueKey
    return h(
      tag,
      {
        key: uniqueKey,
        attrs: {
          role: uniqueKey
        }
      },
      this.$slots.default
    )
  }
})

export default {
  extends: VirtualList,

  methods: {
    getRenderSlots: function getRenderSlots(h) {
      const slots = []
      const _this$range = this.range
      const start = _this$range.start
      const end = _this$range.end
      const dataSources = this.dataSources
      const dataKey = this.dataKey
      const itemClass = this.itemClass
      const itemTag = this.itemTag
      const itemStyle = this.itemStyle
      const isHorizontal = this.isHorizontal
      const extraProps = this.extraProps
      const dataComponent = this.dataComponent
      const itemScopedSlots = this.itemScopedSlots

      for (let index = start; index <= end; index++) {
        const dataSource = dataSources[index]

        if (dataSource) {
          const uniqueKey =
            typeof dataKey === 'function'
              ? dataKey(dataSource)
              : dataSource[dataKey]

          if (typeof uniqueKey === 'string' || typeof uniqueKey === 'number') {
            slots.push(
              h(Item, {
                props: {
                  index,
                  tag: itemTag,
                  event: EVENT_TYPE.ITEM,
                  horizontal: isHorizontal,
                  uniqueKey,
                  source: dataSource,
                  extraProps,
                  component: dataComponent,
                  scopedSlots: itemScopedSlots
                },
                style: itemStyle,
                class: ''
                  .concat(itemClass)
                  .concat(
                    this.itemClassAdd ? ' ' + this.itemClassAdd(index) : ''
                  ),
                key: uniqueKey
              })
            )
          } else {
            console.warn(
              "Cannot get the data-key '".concat(
                dataKey,
                "' from data-sources."
              )
            )
          }
        } else {
          console.warn(
            "Cannot get the index '".concat(index, "' from data-sources.")
          )
        }
      }

      return slots
    }
  },

  render: function render(h) {
    const _this$$slots = this.$slots
    const header = _this$$slots.header
    const footer = _this$$slots.footer
    const _this$range2 = this.range
    const padFront = _this$range2.padFront
    const padBehind = _this$range2.padBehind
    const isHorizontal = this.isHorizontal
    // const pageMode = this.pageMode
    const rootTag = this.rootTag
    const wrapTag = this.wrapTag
    const wrapClass = this.wrapClass
    const wrapStyle = this.wrapStyle
    const headerTag = this.headerTag
    const headerClass = this.headerClass
    const headerStyle = this.headerStyle
    const footerTag = this.footerTag
    const footerClass = this.footerClass
    const footerStyle = this.footerStyle
    const paddingStyle = {
      padding: isHorizontal
        ? '0px '.concat(padBehind, 'px 0px ').concat(padFront, 'px')
        : ''.concat(padFront, 'px 0px ').concat(padBehind, 'px')
    }
    const wrapperStyle = wrapStyle
      ? Object.assign({}, wrapStyle, paddingStyle)
      : paddingStyle
    return h(
      rootTag,
      {
        ref: 'root'
        // on: {
        //   '&scroll': !pageMode && this.onScroll
        // }
      },
      [
        // header slot
        header
          ? h(
              Slot,
              {
                class: headerClass,
                style: headerStyle,
                props: {
                  tag: headerTag,
                  event: EVENT_TYPE.SLOT,
                  uniqueKey: SLOT_TYPE.HEADER
                }
              },
              header
            )
          : null, // main list
        h(
          wrapTag,
          {
            class: wrapClass,
            attrs: {
              role: 'group'
            },
            style: wrapperStyle
          },
          this.getRenderSlots(h)
        ), // footer slot
        footer
          ? h(
              Slot,
              {
                class: footerClass,
                style: footerStyle,
                props: {
                  tag: footerTag,
                  event: EVENT_TYPE.SLOT,
                  uniqueKey: SLOT_TYPE.FOOTER
                }
              },
              footer
            )
          : null, // an empty element use to scroll to bottom
        h('div', {
          ref: 'shepherd',
          style: {
            width: isHorizontal ? '0px' : '100%',
            height: isHorizontal ? '100%' : '0px'
          }
        })
      ]
    )
  }
}
