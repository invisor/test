const columnsMap = {
  stonesList: getStonesColumns,
  stonesListCompare: getStonesCompareColumns,
  stonePairsList: getStonesColumns,
  stonePairsListCompare: getStonesCompareColumns,
  ringSettings: getRingSettingsColumns,
  ringSettingsCompare: getRingSettingsCompareColumns,
  earringSettings: getRingSettingsColumns, // the same as for rings
  earringSettingsCompare: getRingSettingsCompareColumns, // the same as for rings
  pendantSettings: getRingSettingsColumns, // the same as for rings
  pendantSettingsCompare: getRingSettingsCompareColumns, // the same as for rings
  necklaceSettings: getRingSettingsColumns, // the same as for rings
  necklaceSettingsCompare: getRingSettingsCompareColumns, // the same as for rings
  braceletsList: getBraceletsColumns,
  braceletsListCompare: getBraceletsCompareColumns,
  earringsList: getBraceletsColumns, // the same as for bracelets
  earringsListCompare: getBraceletsCompareColumns, // the same as for bracelets
  necklacesPendantsList: getBraceletsColumns, // the same as for bracelets
  necklacesPendantsListCompare: getBraceletsCompareColumns, // the same as for bracelets
  ringsList: getBraceletsColumns, // the same as for bracelets
  ringsListCompare: getBraceletsCompareColumns, // the same as for bracelets
  weddingBands: getBandsColumns,
  weddingBandsPlain: getBandsPlainColumns
}

export function getWidths() {
  return {
    id: '',
    photo: '',
    weight: '',
    length: '',
    width: '',
    shape: '',
    origin: '',
    price: '',
    metal: '',
    compare: '',
    wishlist: '',
    remove: '',
    actions: '',
    metalWeight: '',
    totalCarat: '',
    style: ''
  }
}

export function getResultTableColumns() {
  return columnsMap[this.type].call(this)
}

export function getCompareTableColumns() {
  return columnsMap[`${this.type}Compare`].call(this)
}

function getStonesColumns() {
  return [
    {
      id: '944169f2-94fd-4e07-a857-6b9a2d4494d2',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.photo`),
      name: 'photo'
    },
    {
      id: '5d3ff135-03ea-4833-861b-0cbd1ae66d3f',
      title: this.$t('listingPage.common.tableHeader.id'),
      name: 'id'
    },
    {
      id: '827a023f-c7f7-45c9-ae0a-420aea90a420',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.weight`),
      name: 'weight'
    },
    {
      id: '4e3fa798-4f3f-409f-88ea-87557520eeb7',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.length`),
      name: 'length'
    },
    {
      id: '041102f6-224f-4861-84fa-32f99158fc30',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.width`),
      name: 'width'
    },
    {
      id: '525eee9f-2c85-46a5-9d2e-12fb97cd151a',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.shape`),
      name: 'shape'
    },
    {
      id: 'a496738a-4acf-4f50-94cb-71e537d7b1e5',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.origin`),
      name: 'origin'
    },
    {
      id: 'd26074c2-0e00-45cb-af52-a40e90f4c2b4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.price`),
      name: 'price'
    },
    wishlistColumn.call(this),
    compareColumn.call(this)
  ]
}

function getStonesCompareColumns() {
  const tableColumns = getStonesColumns.call(this)
  const index = tableColumns.findIndex((column) => column.name === 'compare')
  if (index > -1) tableColumns[index] = removeColumn.call(this)
  return tableColumns
}

const wishlistColumn = function () {
  return {
    id: 'd9a1b5ef-336c-4e05-bb15-e60531b9ca18',
    title: this.$t(`listingPage.common.tableHeader.${this.type}.wishlist`),
    name: 'wishlist'
  }
}

const compareColumn = function () {
  return {
    id: '355e737a-9e30-4a50-8d7e-32e0f24bca19',
    title: this.$t(`listingPage.common.tableHeader.${this.type}.compare`),
    name: 'compare'
  }
}

const removeColumn = function () {
  return {
    id: '3afa5f37-7a73-437c-9459-e3ee9077aa5c',
    title: this.$t('listingPage.common.tableHeader.remove'),
    name: 'remove'
  }
}

function getRingSettingsColumns() {
  return [
    {
      id: 'f515826f-ff41-4fb4-895c-42f50ae6b8f4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.photo`),
      name: 'photo'
    },
    {
      id: '5d3ff135-03ea-4833-861b-0cbd1ae66d3f',
      title: this.$t('listingPage.common.tableHeader.id'),
      name: 'id'
    },
    {
      id: '57e4e938-318a-432a-9f0e-62c60b43f1dc',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metal`),
      name: 'metal'
    },
    wishlistColumn.call(this),
    compareColumn.call(this)
  ]
}

function getBraceletsColumns() {
  return [
    {
      id: 'f515826f-ff41-4fb4-895c-42f50ae6b8f4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.photo`),
      name: 'photo'
    },
    {
      id: '5d3ff135-03ea-4833-861b-0cbd1ae66d3f',
      title: this.$t('listingPage.common.tableHeader.id'),
      name: 'id'
    },
    {
      id: '827a023f-c7f7-45c9-ae0a-420aea90a420',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.weight`),
      name: 'weight'
    },
    {
      id: '57e4e938-318a-432a-9f0e-62c60b43f1dc',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metal`),
      name: 'metal'
    },
    {
      id: '40934cdd-5200-41c8-90bd-6333a8dd0b4c',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.treatment`),
      name: 'treatment'
    },
    {
      id: '525eee9f-2c85-46a5-9d2e-12fb97cd151a',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.shape`),
      name: 'shape'
    },
    {
      id: 'a496738a-4acf-4f50-94cb-71e537d7b1e5',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.origin`),
      name: 'origin'
    },
    {
      id: 'd26074c2-0e00-45cb-af52-a40e90f4c2b4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.price`),
      name: 'price'
    },
    wishlistColumn.call(this),
    compareColumn.call(this)
  ]
}

function getBandsColumns() {
  return [
    {
      id: 'f515826f-ff41-4fb4-895c-42f50ae6b8f4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.photo`),
      name: 'photo'
    },
    {
      id: '5d3ff135-03ea-4833-861b-0cbd1ae66d3f',
      title: this.$t('listingPage.common.tableHeader.id'),
      name: 'id'
    },
    {
      id: '827a023f-c7f7-45c9-ae0a-420aea90a420',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metalWeight`),
      name: 'metalWeight'
    },
    {
      id: '57e4e938-318a-432a-9f0e-62c60b43f1dc',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.totalCarat`),
      name: 'totalCarat'
    },
    {
      id: '40934cdd-5200-41c8-90bd-6333a8dd0b4c',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metal`),
      name: 'metal'
    },
    {
      id: 'd26074c2-0e00-45cb-af52-a40e90f4c2b4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.price`),
      name: 'price'
    },
    wishlistColumn.call(this),
    compareColumn.call(this)
  ]
}

function getBandsPlainColumns() {
  return [
    {
      id: 'f515826f-ff41-4fb4-895c-42f50ae6b8f4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.photo`),
      name: 'photo'
    },
    {
      id: '5d3ff135-03ea-4833-861b-0cbd1ae66d3f',
      title: this.$t('listingPage.common.tableHeader.id'),
      name: 'id'
    },
    {
      id: '827a023f-c7f7-45c9-ae0a-420aea90a420',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metalWeight`),
      name: 'metalWeight'
    },
    {
      id: '57e4e938-318a-432a-9f0e-62c60b43f1dc',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.metal`),
      name: 'metal'
    },
    {
      id: '40934cdd-5200-41c8-90bd-6333a8dd0b4c',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.style`),
      name: 'style'
    },
    {
      id: 'd26074c2-0e00-45cb-af52-a40e90f4c2b4',
      title: this.$t(`listingPage.common.tableHeader.${this.type}.price`),
      name: 'price'
    },
    wishlistColumn.call(this),
    compareColumn.call(this)
  ]
}

function getBraceletsCompareColumns() {
  const tableColumns = getBraceletsColumns.call(this)
  const index = tableColumns.findIndex((column) => column.name === 'compare')
  if (index > -1) tableColumns[index] = removeColumn.call(this)
  return tableColumns
}

function getRingSettingsCompareColumns() {
  const tableColumns = getRingSettingsColumns.call(this)
  const index = tableColumns.findIndex((column) => column.name === 'compare')
  if (index > -1) tableColumns[index] = removeColumn.call(this)
  return tableColumns
}
