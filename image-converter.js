import fs from 'fs'
import path from 'path'
import imagemin from 'imagemin'
import imageminMozjpeg from 'imagemin-mozjpeg'
import imageminPngquant from 'imagemin-pngquant'
import sharp from 'sharp'
import gm from 'gm'
import rimraf from 'rimraf'

const imagesFolders = ['./assets/images/ruby', './assets/images/emerald']
const sourceSizeImagesFolder = 'hi-res'
const normalSizeImagesFolder = 'normal'
const webpFolders = [
  `${sourceSizeImagesFolder}-webp`,
  `${normalSizeImagesFolder}-webp`
]
const foldersToConvert = [sourceSizeImagesFolder, normalSizeImagesFolder]
const outputFolders = [normalSizeImagesFolder, ...webpFolders]

imagesFolders.forEach((folder) => {
  outputFolders.forEach((f) => {
    const outputFolder = path.join(folder, f)
    if (fs.existsSync(outputFolder)) {
      rimraf.sync(outputFolder)
      fs.mkdirSync(outputFolder)
    } else {
      fs.mkdirSync(outputFolder)
    }
  })
})

const walk = (dir, filelist = []) => {
  const files = fs.readdirSync(dir)

  files.forEach((file) => {
    const filepath = path.join(dir, file)
    const stat = fs.statSync(filepath)

    if (
      !stat.isDirectory() &&
      !/\.(png|jpe?g)/.test(path.extname(file).toLowerCase())
    )
      return

    if (stat.isDirectory()) {
      outputFolders.forEach((folder) => {
        const dir = filepath
          .replace(`\\${sourceSizeImagesFolder}\\`, `\\${folder}\\`)
          .replace(`/${sourceSizeImagesFolder}/`, `/${folder}/`)
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir)
        }
      })
      filelist = walk(filepath, filelist)
    } else {
      // Create normal size image
      const image = sharp(filepath)
      const outputFilename = filepath
        .replace(sourceSizeImagesFolder, normalSizeImagesFolder)
        .replace('@2x', '')
      image
        .metadata()
        .then(async function (metadata) {
          await image
            .jpeg({ quality: 100 })
            .png({ compressionLevel: 0 })
            .resize(
              Math.round(metadata.width / 2),
              Math.round(metadata.height / 2),
              {
                kernel: sharp.kernel.lanczos3
              }
            )
            .toBuffer()
            .then((buffer) => {
              imagemin
                .buffer(buffer, {
                  plugins: [
                    imageminMozjpeg({
                      quality: 85
                    }),
                    imageminPngquant({
                      quality: [0.6, 0.8]
                    })
                  ]
                })
                .then((data) => {
                  fs.writeFileSync(outputFilename, data)
                  webpConvertor(filepath)
                })
                .catch((e) => {
                  console.log(e)
                })
            })
            .catch((e) => {
              console.log(e)
            })
        })
        .catch((e) => {
          console.log(e)
        })
    }
  })
}

const webpConvertor = (filename) => {
  foldersToConvert.forEach((folder) => {
    const outputFilename = filename
      .replace(sourceSizeImagesFolder, `${folder}-webp`)
      .replace(/\.[^.]+$/, '.webp')
      .replace(folder === 'normal' ? '@2x' : '', '')
    const path = filename.replace(sourceSizeImagesFolder, folder)
    gm(path.replace(/[/\\]normal[/\\]/.test(path) ? '@2x' : '', ''))
      .stream('webp')
      .pipe(fs.createWriteStream(outputFilename))
  })
}

imagesFolders.forEach((imagesFolder) => {
  walk(imagesFolder)
})
