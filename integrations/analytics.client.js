import OnDemand from '~~/utils/helpers/onDemand'

/* global ga */
export default (ctx, inject) => {
  const loader = new OnDemand({
    script: '/js/analytics.js'
  })

  inject('analytics', {
    insert: (callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          ga(
            'create',
            process.env.isDev ? 'UA-153554273-1' : 'UA-187898-6',
            'auto'
          )
          ga('send', 'pageview', location.pathname)
          resolve()
          if (callback) callback()
        })
      })
  })
}
