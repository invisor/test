import OnDemand from '~~/utils/helpers/onDemand'

export default (ctx, inject) => {
  const loader = new OnDemand(
    {
      script:
        '//cdn.callrail.com/companies/828820948/3384d92283a3482d93c7/12/swap.js'
    },
    {
      body: true
    }
  )

  inject('callrail', {
    insert: (callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          resolve()
          if (callback) callback()
        })
      })
  })
}
