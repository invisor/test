import Vue from 'vue'
import OnDemand from '~~/utils/helpers/onDemand'

/* global VueMultiselect */
export default (ctx, inject) => {
  const loader = new OnDemand({
    script:
      'https://service.thenaturalsapphirecompany.com/cdn/vue-multiselect.min.js?v=2.1.6',
    css: [
      'https://service.thenaturalsapphirecompany.com/cdn/vue-multiselect.min.css?v=2.1.6'
    ]
  })

  inject('multiselect', {
    insert: (callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          Vue.component('multiselect', VueMultiselect.default)
          resolve()
          if (callback) callback()
        })
      })
  })
}
