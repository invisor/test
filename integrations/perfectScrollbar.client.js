import OnDemand from '~~/utils/helpers/onDemand'

/* global PerfectScrollbar */
export default (ctx, inject) => {
  const loader = new OnDemand({
    script:
      'https://service.thenaturalsapphirecompany.com/cdn/perfect-scrollbar.min.js?v=1.5.3',
    css: [
      'https://service.thenaturalsapphirecompany.com/cdn/perfect-scrollbar.min.css?v=1.5.3'
    ]
  })

  inject('perfectScrollbar', {
    insert: (el, options = {}, callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          resolve(new PerfectScrollbar(el, options))
          if (callback) callback()
        })
      })
  })
}
