import OnDemand from '~~/utils/helpers/onDemand'

export default (ctx, inject) => {
  const loader = new OnDemand({
    script: 'https://js.sentry-cdn.com/8b8a6535661a4c01a6690a8f0b79d0e7.min.js'
  })

  /* global Sentry */

  const s = {}
  inject('sentry', s)

  inject('sentryio', {
    insert: (callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          Object.assign(s, Sentry)
          resolve()
          if (callback) callback()
        })
      })
  })
}
