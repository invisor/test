import OnDemand from '~~/utils/helpers/onDemand'

export default (ctx, inject) => {
  const loader = new OnDemand({
    script:
      'https://userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/66ade3c695a58314549ca32d3626f4cffdd06773d091916ab7840527fc4ac9d7.js'
  })

  inject('userlike', {
    insert: (callback) =>
      new Promise((resolve, reject) => {
        loader.load(() => {
          setTimeout(() => {
            window.userlikeStartChat = function() {
              try {
                document
                  .querySelectorAll('*[id^="userlike"] iframe')[0]
                  .contentWindow.document.getElementsByTagName('button')[0]
                  .click()
              } catch (e) {
                console.log(e)
              }
            }
            resolve()
            if (callback) callback()
          }, 1000)
        })
      })
  })
}
