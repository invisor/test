export default function({ store, error }) {
  if (!store.state.auth.loggedIn) {
    error({
      message: 'This page could not be found',
      statusCode: 404
    })
  }
}
