export default {
  data: () => ({
    autocomplete: null
  }),

  methods: {
    initAddressAutocomplete() {
      const addressElement = document.getElementById('address-field')
      if (!addressElement) return
      this.$googleMaps.insert(() => {
        this.autocomplete = new window.google.maps.places.Autocomplete(
          addressElement,
          {
            types: ['address']
          }
        )
        this.autocomplete.addListener('place_changed', this.onPlaceChanged)
      })
    },

    onPlaceChanged() {
      const place = this.autocomplete.getPlace()
      if (!place) return
      const address = place.address_components
      if (!address) return
      const streetNumber = address.find((a) =>
        a.types.includes('street_number')
      )
      const street = address.find((a) => a.types.includes('route'))
      const city = address.find((a) => a.types.includes('locality'))
      const state = address.find((a) =>
        a.types.includes('administrative_area_level_1')
      )
      const zip = address.find((a) => a.types.includes('postal_code'))
      const country = address.find((a) => a.types.includes('country'))
      this.setFormValues({ streetNumber, street, city, state, zip, country })
      this.$nextTick(this.$forceUpdate) // to overwrite google's value in input field
    },

    setFormValues() {}
  }
}
