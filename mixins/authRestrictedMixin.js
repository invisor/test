export default {
  beforeDestroy() {
    this.$root.$off('logout', this.moveHome)
  },

  mounted() {
    this.$root.$once('logout', this.moveHome)
  },

  methods: {
    moveHome() {
      this.$router.push(this.localePath({ name: 'index' }))
    }
  }
}
