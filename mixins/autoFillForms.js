import { mapGetters } from 'vuex'

export default {
  data: () => ({
    targetForm: 'form',
    targetNameField: 'customerName',
    targetEmailField: 'email',
    targetPhoneField: 'phone'
  }),

  computed: {
    ...mapGetters({
      defaultShippingAddress: 'account/defaultShippingAddress'
    }),

    profile() {
      return this.$store.state.auth.user || {}
    }
  },

  created() {
    this.autoFillFormMixinInit()
  },

  methods: {
    autoFillFormMixinInit() {
      if (!this.$store.state.auth.loggedIn) return
      if (this.validateFields()) this.fillForm()
    },

    validateFields() {
      return !!this[this.targetForm]
    },

    fillForm() {
      if (typeof this[this.targetForm][this.targetNameField] !== 'undefined')
        this[this.targetForm][
          this.targetNameField
        ] = `${this.profile.firstName} ${this.profile.lastName}`
      if (typeof this[this.targetForm][this.targetEmailField] !== 'undefined')
        this[this.targetForm][this.targetEmailField] = this.profile.email
      if (
        typeof this[this.targetForm][this.targetPhoneField] !== 'undefined' &&
        this.defaultShippingAddress.phone
      )
        this[this.targetForm][
          this.targetPhoneField
        ] = this.defaultShippingAddress.phone
    }
  }
}
