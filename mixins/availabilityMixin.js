import { mapGetters } from 'vuex'
// Possible statuses
// 0: available
// 1: delisted
// 2: call for status
// 3: sold
// 4: onhold

export default {
  inject: ['pageType'],

  computed: {
    ...mapGetters({
      productDetails: 'productDetails/productDetails',
      customItem: 'customItem/customItem'
    }),

    itemDetails() {
      if (this.isCustomItem) return this.customItem.customStone
      return this.productDetails
    },

    itemStatus() {
      return this.itemDetails.itemStatus
    },

    isCustomItem() {
      return this.pageType === 'review'
    },

    isOutOfStock() {
      if (this.isCustomItem) return this.itemStatus === 2
      return !!this.$route.query.overview || this.itemStatus === 2
    },

    estimatedShipDate() {
      return this.itemDetails.estimatedShipDate
    },

    isOutOfStockTemporary() {
      return (
        this.isOutOfStock &&
        this.estimatedShipDate &&
        !this.$route.query.overview
      )
    },

    isSoldOut() {
      if (this.isCustomItem) return this.itemStatus === 3
      return !!this.$route.query.overview || this.itemStatus === 3
    },

    isDelisted() {
      if (this.isCustomItem) return this.itemStatus === 1
      return !!this.$route.query.overview || this.itemStatus === 1
    },

    isOnHold() {
      if (this.isCustomItem) return this.itemStatus === 4
      return !!this.$route.query.overview || this.itemStatus === 4
    }
  }
}
