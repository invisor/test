export default {
  computed: {
    breadcrumbs() {
      return []
    },

    breadcrumbsItems() {
      return {
        home: {
          id: 'd8ac6bff-ce6d-4232-a68a-ab3131728f62',
          name: this.$t('breadcrumbs.home'),
          path: '/'
        },
        stoneListing: {
          id: '11e040dd-afd3-4f6c-a73f-089f7fb0586d',
          name: this.$t('breadcrumbs.listings.stone'),
          path: '/rubies/'
        },
        stoneCabochonListing: {
          id: '5aa45749-502c-4230-9f46-14a043b05c5e',
          name: this.$t('breadcrumbs.listings.stoneCabochon'),
          path: '/rubies/ruby-cabochon/'
        },
        stoneStarListing: {
          id: '33a0fdef-c41f-4bbd-8bba-1080094b807e',
          name: this.$t('breadcrumbs.listings.stoneStar'),
          path: '/rubies/star-rubies/'
        },
        pairListing: {
          id: 'c186ea0c-b18c-4438-8d77-57e9b5746dc2',
          name: this.$t('breadcrumbs.listings.pair'),
          path: '/rubies/matched-pairs-of-rubies/'
        },
        pairCabochonListing: {
          id: 'e3d50c91-a253-45d1-80f1-fd12f97a2dc8',
          name: this.$t('breadcrumbs.listings.pairCabochon'),
          path: '/rubies/matched-pairs-of-rubies/cabochon/'
        },
        pairStarListing: {
          id: 'c9fb3c2e-3875-4f97-ad16-ccfdbfb15b8c',
          name: this.$t('breadcrumbs.listings.pairStar'),
          path: '/rubies/star-ruby-pairs/'
        },
        ringListing: {
          id: '5276c401-c75d-49cd-9f94-450c5c52ff01',
          name: this.$t('breadcrumbs.listings.rings'),
          path: '/ruby-jewelry/ruby-engagement-rings/'
        },
        earringListing: {
          id: '6d13550b-21db-4373-8077-1d4be4dc3c5b',
          name: this.$t('breadcrumbs.listings.earrings'),
          path: '/ruby-jewelry/ruby-earrings/'
        },
        pendantListing: {
          id: '243c6096-988b-42a8-b65f-044b18b9436f',
          name: this.$t('breadcrumbs.listings.nAndP'),
          path: '/ruby-jewelry/ruby-necklaces-pendants/'
        },
        necklaceListing: {
          id: '2a403e75-6cd9-4f52-bf5f-a569200a3cc3',
          name: this.$t('breadcrumbs.listings.nAndP'),
          path: '/ruby-jewelry/ruby-necklaces-pendants/'
        },
        braceletListing: {
          id: 'dcd4bc98-a1d2-4331-95f1-e2d32be34171',
          name: this.$t('breadcrumbs.listings.bracelets'),
          path: '/ruby-jewelry/ruby-bracelets/'
        },
        weddingBandListing: {
          id: 'edd3eb93-a48b-4898-9c82-cb5a7070aba4',
          name: this.$t('breadcrumbs.listings.weddingBands'),
          path: '/ruby-wedding-bands/'
        },
        plainBandListing: {
          id: '7b9f912d-93cd-4876-a691-192f27f31b92',
          name: this.$t('breadcrumbs.listings.plainBands'),
          path: '/wedding-bands-without-gemstone/'
        },
        settingMensRingListing: {
          id: '4edfc719-979f-4fd3-b809-b6bb3e3684c1',
          name: this.$t('breadcrumbs.listings.mensRingSettings'),
          path: '/design-your-own/setting/mens-ring-settings/'
        },
        settingRingListing: {
          id: '55fe8893-205f-462d-b4a4-51b59d202039',
          name: this.$t('breadcrumbs.listings.ringSettings'),
          path: '/design-your-own/setting/ruby-engagement-ring-settings/'
        },
        settingEarringListing: {
          id: 'f6bb864b-e5f9-4f0f-b2b1-31cc5b8edcbf',
          name: this.$t('breadcrumbs.listings.earringSettings'),
          path: '/design-your-own/setting/ruby-earring-settings/'
        },
        settingPendantListing: {
          id: 'e3ccd3a7-902e-4f7d-aa44-bcbeb283ae03',
          name: this.$t('breadcrumbs.listings.necklaceAndPendantSettings'),
          path: '/design-your-own/setting/ruby-pendants-settings/'
        },
        settingNecklaceListing: {
          id: '4f20542c-f547-488c-b65f-c8420d1ec061',
          name: this.$t('breadcrumbs.listings.necklaceAndPendantSettings'),
          path: '/design-your-own/setting/ruby-necklaces-settings/'
        }
      }
    }
  }
}
