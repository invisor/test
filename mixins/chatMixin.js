import { mapGetters } from 'vuex'

export default {
  methods: {
    onStartChat() {
      /* eslint-disable */
      if (window.userlikeStartChat) {
        if (localStorage) localStorage.removeItem('uslk_drag')
        window.userlikeStartChat()
        // userlike.setData(this.info)
      }
      /* eslint-enable */
    }
  },

  computed: {
    ...mapGetters({
      productDetails: 'productDetails/productDetails'
    }),

    profile() {
      return this.$store.state.auth.user || {}
    },

    info() {
      const user = {
        email: this.profile.email || '',
        name: `${this.profile.firstName || ''} ${this.profile.lastName ||
          ''}`.trim()
      }

      const custom = {
        id: this.$h.getItemFullId(this.productDetails),
        itemType: this.productDetails.category || ''
      }

      return {
        user,
        custom
      }
    }
  }
}
