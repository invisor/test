import isEmpty from 'lodash/isEmpty'

const imagesMap = {
  Ring: 'hand',
  Earring: 'ear',
  Pendant: 'neck',
  Necklace: 'neck'
}

const imagesTypesMap = {
  1: 'Image',
  3: 'ScanReport',
  10: 'Appraisal',
  11: 'LifeStyle',
  4: '3DScanReport'
}

const thumbnailsOrder = [
  'Combination', // for custom (not MTO) items image with stone and setting images
  'Preview', // preview images for MTO items
  'Video', // video 360
  'Image', // simple image
  'LifeStyle',
  'Model', // 3 color model hand
  'SizeVideo', // phone videos
  'Sarin', // 3d model for stones
  'SettingsSarin', // 3d model settings
  '3DScanReport', // report
  'ScanReport', // report
  'Appraisal' // report
]

export default {
  methods: {
    sortThumbnails(thumbnails, item) {
      const t = []
      thumbnailsOrder.forEach((type) => {
        thumbnails.forEach((thumbnail) => {
          if (thumbnail.type === type) t.push(thumbnail)
        })
      })

      return t
    },

    createThumbnails(item) {
      if (item.category === 'Custom') return this.createCustomThumbnails(item)

      const thumbnails = []

      if (item.thumbnails) {
        item.thumbnails.forEach((thumbnail) => {
          thumbnails.push(
            item.dataType === 'preview'
              ? this.createPreviewThumbnail(item, thumbnail)
              : this.createImageThumbnail(item, thumbnail)
          )
        })
      }

      const firstImage = thumbnails[0]

      if (item.phoneVideos && item.phoneVideos.length) {
        item.phoneVideos.forEach((video) => {
          thumbnails.push(this.createVideoThumbnail(item, video))
        })
      }

      if (item.modelImages && item.modelImages.length) {
        thumbnails.push(this.createModelThumbnail(item))
      }

      if (item.containsSarin) {
        thumbnails.push(this.createSarinThumbnail(item))
      }

      if (item.containsVideo360)
        thumbnails.push(this.createVideo360Thumbnail(item, firstImage))

      if (item.contains3D) thumbnails.push(this.create3dThumbnail(item))

      return this.sortThumbnails(thumbnails)
    },

    createCustomThumbnails(item) {
      let thumbnails = []

      thumbnails = thumbnails.concat(
        this.createThumbnails(item.customStone).map((t) => {
          t.custom = 'Stone'
          return t
        })
      )
      thumbnails = thumbnails.concat(
        this.createThumbnails(item.customSetting).map((t) => {
          t.custom = 'Setting'
          return t
        })
      )

      const firstThumbnails = !isEmpty(item.preview)
        ? item.preview.thumbnails.map((t) => {
            return this.createPreviewThumbnail(item, t)
          })
        : [
            this.createCombinationThumbnail(item, {
              stone: item.customStone.thumbnails[0],
              setting: item.customSetting.thumbnails[0]
            })
          ]

      return this.sortThumbnails([...firstThumbnails, ...thumbnails])
    },

    createModelThumbnail(item) {
      const category =
        item.category === 'Preview' ? item.subCategory : item.category

      return {
        image: {
          micro: `details/${imagesMap[category]}Thumbnail@2x.jpg`,
          large: item.modelImages[0].large
        },
        guid: '3c5eec59-977e-4518-ad35-f0a368975ae4',
        type: 'Model',
        ...this.getCommonParams(item)
      }
    },

    createSarinThumbnail(item) {
      const device = this.$device.isMobileOrTablet ? 'Mobile' : ''

      return {
        guid: this.$h.guid(),
        image: {
          micro: 'details/sarinThumb@2x.jpg',
          large: `details/sarin${device}@2x.jpg`
        },
        type: 'Sarin',
        ...this.getCommonParams(item)
      }
    },

    create3dThumbnail(item) {
      const device = this.$device.isMobileOrTablet ? 'Mobile' : ''

      return {
        guid: this.$h.guid(),
        image: {
          micro: 'details/sarinThumb@2x.jpg',
          large: `details/sarin${device}@2x.jpg`
        },
        type: 'SettingsSarin',
        ...this.getCommonParams(item)
      }
    },

    createCombinationThumbnail(item, image) {
      return {
        guid: this.$h.guid(),
        image,
        type: 'Combination',
        ...this.getCommonParams(item)
      }
    },

    createVideo360Thumbnail(item, firstImage) {
      return {
        ...firstImage,
        guid: this.$h.guid(),
        type: 'Video',
        ...this.getCommonParams(item)
      }
    },

    createVideoThumbnail(item, video) {
      return {
        guid: this.$h.guid(),
        image: { micro: 'details/iphoneThumb@2x.jpg' },
        type: 'SizeVideo',
        videoFileName: video,
        ...this.getCommonParams(item)
      }
    },

    createImageThumbnail(item, image) {
      return {
        guid: this.$h.guid(),
        image,
        type: imagesTypesMap[image.type] || 'Image',
        ...this.getCommonParams(item)
      }
    },

    createPreviewThumbnail(item, image) {
      return {
        guid: this.$h.guid(),
        image,
        type: image.type === 1 ? 'Preview' : imagesTypesMap[image.type],
        ...this.getCommonParams(item)
      }
    },

    getCommonParams(item) {
      const common = {
        id: item.id || item.itemRealId,
        item
      }
      if (item.metalTypeCode) common.metalTypeCode = item.metalTypeCode
      return common
    }
  }
}
