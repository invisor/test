import isEmpty from 'lodash/isEmpty'
import { mapGetters } from 'vuex'
import { productListTypeByCategory } from '~~/utils/definitions/defaults'

export default {
  computed: {
    ...mapGetters({
      productDetails: 'productDetails/productDetails',
      customSetting: 'customItem/customSetting'
    }),

    type() {
      const category = this.productDetails.category || this.item.category
      return productListTypeByCategory[category]
    },

    canAddToJewelry() {
      if (isEmpty(this.customSetting)) return true
      return !!this.settingMatch
    }
  }
}
