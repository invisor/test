export default {
  data: () => ({
    emailFields: [],
    emailValidatorInitiated: false
  }),

  watch: {
    visible(state) {
      if (state) {
        setTimeout(() => {
          this.initEmailFieldValidation()
        }, 10)
        return
      }
      this.destroyEmailFieldValidation()
    }
  },

  beforeDestroy() {
    this.destroyEmailFieldValidation()
  },

  mounted() {
    this.initEmailFieldValidation()
  },

  methods: {
    getEmailsFields() {
      this.emailFields.push(this.$refs['email-field'])
    },

    initEmailFieldValidation() {
      if (this.emailValidatorInitiated || this.visible === false) return
      this.getEmailsFields()
      if (this.emailFields.length) {
        this.emailFields.forEach((field) => {
          if (field)
            field.addEventListener('input', this.validateEmailField, false)
        })
        this.emailValidatorInitiated = true
      }
    },

    destroyEmailFieldValidation() {
      if (!this.emailValidatorInitiated) return
      if (this.emailFields.length) {
        this.emailFields.forEach((field) => {
          if (field)
            field.removeEventListener('input', this.validateEmailField, false)
        })
        this.emailFields = []
        this.emailValidatorInitiated = false
      }
    },

    validateEmailField(e) {
      const result = /[a-zA-Z0-9_.+]+@(live|hotmail)(\.com)/.test(
        e.target.value
      )

      if (result) {
        this.mountNotification(e)
        return
      }
      this.unMountNotification(e)
    },

    mountNotification(e) {
      if (!e.target.notificationEl) this.createElement(e)
    },

    unMountNotification(e) {
      this.destroyElement(e)
    },

    createElement(e) {
      e.target.notificationEl = document.createElement('span')
      e.target.notificationEl.classList.add(
        'icon',
        'is-small',
        'is-right',
        'email-warning'
      )
      const icon = document.createElement('i')
      icon.classList.add('fas', 'fa-exclamation-triangle')
      e.target.notificationEl.append(icon)

      e.target.parentElement.classList.add('has-icons-right')
      e.target.parentElement.append(e.target.notificationEl)

      this.insertTooltip(e)
    },

    insertTooltip(e) {
      if (!e.target.tooltipInstance && e.target.notificationEl)
        this.$tippy.insert(e.target.notificationEl, {
          theme: 'light',
          content:
            "<p class='email-warning__paragraph'>Please take notice that our company mailing systems are routed through Google's Gmail systems, and specifically for Hotmail/Live email recipients we have seen a very high percentage of delivery failures, or our emails going to spam folders.</p><p class='email-warning__paragraph'>If possible please submit your request using a non-Hotmail/Live email address.</p><p class='email-warning__paragraph'>If you only have a hotmail/live email address then please make a note to check your spam folder in the next 2 days for a reply from us about your inquiry.</p><p class='email-warning__paragraph'>We apologize for these possible issues for hotmail/live email accounts.</p>",
          allowHTML: true
        })
    },

    destroyTooltip(e) {
      if (e.target.tooltipInstance) {
        e.target.tooltipInstance.destroy()
        e.target.tooltipInstance = null
      }
    },

    destroyElement(e) {
      if (e.target.notificationEl) {
        this.destroyTooltip(e)
        e.target.notificationEl.remove()
        delete e.target.notificationEl
      }
    }
  }
}
