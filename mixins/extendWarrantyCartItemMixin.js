import { mapActions, mapGetters, mapState } from 'vuex'
import cloneDeep from 'lodash/cloneDeep'

export default {
  computed: {
    ...mapState({
      localCurrency: (state) => state.app.localCurrency
    }),
    ...mapGetters({
      shippingCountry: 'cart/shippingCountry',
      showLocal: 'app/showLocal',
      localCurrencyCode: 'app/localCurrencyCode',
      symbolPositionFront: 'app/symbolPositionFront'
    }),

    extendPrice() {
      if (!this.item.extendPlan) return null

      const localCurrency = cloneDeep(this.localCurrency)

      const price = this.item.extendPlan.price / 100

      if (this.showLocal) {
        const { usdRate, localRate } = localCurrency
        localCurrency.price = this.$h.getLocalPrice(price, usdRate, localRate)
      }
      return { price, localCurrency }
    },

    extendItemId() {
      return `extend-offer-${this._uid}`
    },

    showExtendWarranty() {
      if (['Wax'].includes(this.item.category)) return false
      if (this.shippingCountry !== 'US') return false
      return this.extendReferenceId
    },

    extendReferenceId() {
      return (
        this.item.finalPrice?.extendReferenceId || this.item.extendReferenceId
      )
    }
  },

  watch: {
    'item.extendPlan': {
      handler(state) {
        if (!state)
          this.$nextTick(() => {
            this.insertButtons()
          })
      }
    },
    showExtendWarranty: {
      handler(state) {
        if (state) this.$nextTick(this.insertButtons)
      }
    }
  },

  beforeDestroy() {
    this.$root.$off('items-prices-updated', this.insertButtons)
  },

  mounted() {
    this.$root.$on('items-prices-updated', this.insertButtons)
    if (!this.item.extendPlan) this.insertButtons()
  },

  methods: {
    ...mapActions({
      removeExtend: 'cart/removeExtend',
      addExtend: 'cart/addExtend'
    }),

    insertButtons() {
      this.$nextTick(() => {
        if (this.showExtendWarranty) {
          this.$extendSDK.insertSimpleButtons({
            ref: `#${this.extendItemId}`,
            referenceId: this.extendReferenceId,
            onAddToCart: this.onAddExtendToItem
          })
        }
      })
    },

    getLocalPrice(price) {
      const { usdRate, localRate } = this.localCurrency
      if (usdRate && localRate) {
        return this.$h.getLocalPrice(price, usdRate, localRate)
      }
      return price
    },

    getExtendLifeTime(plan) {
      if (!plan) return null
      const lifetime = plan.term / 12
      if (lifetime > 5) return this.$t('extendWarranty.lifetime')
      return this.$t('extendWarranty.years', [lifetime])
    },

    onAddExtendToItem({ plan, product, quantity }) {
      if (plan && product) {
        // a user has selected a plan.  Add warranty to their cart.
        this.addExtend({ item: this.item, plan })
      }
    },

    onRemoveExtend() {
      this.removeExtend({ item: this.item })
    }
  }
}
