import { mapGetters } from 'vuex'

export default {
  inject: ['pageType'],

  computed: {
    ...mapGetters({
      shippingCountry: 'cart/shippingCountry',
      productDetails: 'productDetails/productDetails',
      customItem: 'customItem/customItem',
      isPreview: 'customItem/isPreview'
    }),

    // START default computed properties
    item() {
      if (this.pageType === 'review') return this.customItem
      return this.productDetails
    },
    stoneInCart: () => true,
    inCart: () => true,
    // END default computed properties

    referenceId() {
      if (this.item.category === 'Custom')
        return this.item.finalPrice?.extendReferenceId
      return this.item.extendReferenceId
    },

    showExtendWarranty() {
      if (this.isOutOfStock || this.isSoldOut) return false // availabilityMixin
      if (this.shippingCountry !== 'US') return false
      if (this.item.category === 'Custom') {
        if (!this.isPreview) return false
        return !this.inCart && !this.stoneInCart
      }
      return !this.inCart
    }
  },

  mounted() {
    if (!this.referenceId || !this.showExtendWarranty) return
    this.$extendSDK.insert({}, this.insertExtendButtons)
  },

  methods: {
    insertExtendButtons() {
      this.$extendSDK.insertButtons({ referenceId: this.referenceId })
    },

    showExtendModal(callback) {
      this.$extendSDK.showModal({
        referenceId: this.referenceId,
        onClose(plan, product) {
          if (callback) callback(plan, product)
        }
      })
    },

    /**
     * Add to insertButtons callback to style Extend buttons
     */
    styleButtons() {
      const iframe = document
        .getElementById('extend-offer')
        .getElementsByTagName('iframe')[0]
      if (!iframe) return
      const style = document.createElement('style')
      style.textContent = '.price-currency-sym { font-weight: 900; }'
      iframe.contentDocument.head.appendChild(style)
    }
  }
}
