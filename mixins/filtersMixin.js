import { jewelry, settings } from '~~/utils/definitions/defaults'

export default {
  computed: {
    weddingBandsPlainFilters() {
      return [
        { name: 'MainFilterAreaSimple', filterName: 'metalTypes', icons: true },
        { name: 'MainFilterAreaSimple', filterName: 'styles' },
        {
          name: 'MainFilterAreaRange',
          filterName: 'bandWidthRange',
          label: this.$t('listingPage.filters.bandWidth'),
          step: 0.25
        }
      ]
    },

    weddingBandsFilters() {
      return [
        { name: 'MainFilterAreaSimple', filterName: 'metalTypes', icons: true },
        { name: 'MainFilterAreaSimple', filterName: 'stoneTypes' },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'stoneShapes',
          icons: true
        },
        { name: 'MainFilterAreaSimple', filterName: 'styles' },
        {
          name: 'MainFilterAreaRange',
          filterName: 'bandWidthRange',
          label: this.$t('listingPage.filters.bandWidth'),
          step: 0.25
        }
      ]
    },

    settingsFilters() {
      return [
        { name: 'MainFilterAreaSimple', filterName: 'metalTypes', icons: true },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'centerStoneShapes',
          icons: true
        },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'sideStoneShapes',
          icons: true
        },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'designStyles',
          icons: true
        }
      ]
    },

    jewelryFilters() {
      return [
        { name: 'MainFilterAreaSimple', filterName: 'metalTypes', icons: true },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'centerStoneShapes',
          icons: true
        },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'sideStoneShapes',
          icons: true
        },
        { name: 'MainFilterAreaSimple', filterName: 'stoneOrigins' },
        {
          name: 'MainFilterAreaPriceRange',
          filterName: 'priceRange',
          step: 0.5
        },
        { name: 'MainFilterAreaRange', filterName: 'caratRange', step: 0.5 },
        { name: 'MainFilterAreaSimple', filterName: 'treatments' },
        { name: 'MainFilterAreaSimple', filterName: 'styles', icons: true },
        { name: 'MainFilterAreaSimple', filterName: 'cuttingStyles' },
        { name: 'MainFilterAreaSimple', filterName: 'clarity' },
        { name: 'MainFilterAreaSimple', filterName: 'intensity' },
        { name: 'MainFilterAreaSimple', filterName: 'labColors' },
        { name: 'MainFilterAreaSimple', filterName: 'labTypes' }
      ]
    },

    stonesFilters() {
      return [
        {
          name: 'MainFilterAreaSimple',
          filterName: 'stoneShapes',
          icons: true
        },
        {
          name: 'MainFilterAreaSimple',
          filterName: 'stoneOrigins'
        },
        {
          name: 'MainFilterAreaPriceRange',
          filterName: 'priceRange',
          step: 0.5
        },
        { name: 'MainFilterAreaRange', filterName: 'caratRange', step: 0.5 },
        {
          name: 'MainFilterAreaDimensions',
          filterName: 'dimensions',
          children: [
            {
              filterName: 'lengthRange',
              label: this.$t('listingPage.filters.lengthRange'),
              step: 0.5
            },
            {
              filterName: 'widthRange',
              label: this.$t('listingPage.filters.widthRange'),
              step: 0.5
            }
          ]
        },
        { name: 'MainFilterAreaSimple', filterName: 'treatments' },
        { name: 'MainFilterAreaSimple', filterName: 'clarity' },
        { name: 'MainFilterAreaSimple', filterName: 'intensity' },
        { name: 'MainFilterAreaSimple', filterName: 'labColors' },
        { name: 'MainFilterAreaSimple', filterName: 'labTypes' }
      ]
    },

    filterComponent() {
      return this[`${this.itemsType}Filters`].find(
        (component) => this.filterName === component.filterName
      )
    },

    itemsType() {
      if (this.type === 'weddingBands') return 'weddingBands'
      if (this.type === 'weddingBandsPlain') return 'weddingBandsPlain'
      if (jewelry.includes(this.type)) return 'jewelry'
      if (settings.includes(this.type)) return 'settings'
      return 'stones'
    }
  }
}
