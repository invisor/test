import { mapState } from 'vuex'
import { htmlDecode } from '~~/utils/utils'
import { title } from '~~/utils/definitions/defaults'

export default {
  head() {
    const { link, script, meta } = this

    return {
      htmlAttrs: {
        lang: this.$i18n.locale
      },
      title: this.pageTitle,
      meta,
      __dangerouslyDisableSanitizers: ['script'],
      script,
      link
    }
  },

  computed: {
    ...mapState({
      page: (state) => state.wordpress.page
    }),

    isDev() {
      return process.env.isDev
    },

    meta() {
      return this.$createSeo(this.$route.name, this.page.metaData.meta)
    },

    script() {
      const defaultScripts = []
      if (!this.page.metaData) return defaultScripts
      return [...defaultScripts, ...this.page.metaData.script]
    },

    link() {
      const defaultLinks = []
      if (!this.page.metaData) return defaultLinks
      return [...defaultLinks, ...this.page.metaData.link]
    },

    pageTitle() {
      const yoastTitle = this.page.metaData?.meta?.find(
        (m) => m.property === 'og:title'
      )
      if (yoastTitle) return `${yoastTitle.content} | ${title}`

      if (!this.page.title) return title
      return `${htmlDecode(this.page.title.rendered)} | ${title}`
    }
  }
}
