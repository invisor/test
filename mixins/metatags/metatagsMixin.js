import { mapGetters } from 'vuex'
import { normRoute, normRouteName } from '~~/utils/utils'
import { host, title as postfix } from '~~/utils/definitions/defaults'

const defaultStructuredData = {
  '@context': 'http://schema.org',
  '@type': 'WebSite',
  name: postfix,
  url: host
}

const napStructuredData = {
  '@context': 'http://schema.org',
  '@type': 'LocalBusiness',
  address: {
    '@type': 'PostalAddress',
    streetAddress: '6 East 45th Street, 20th Floor',
    addressRegion: 'New York, NY 10017',
    addressCountry: 'United States'
  },
  description: 'Head Office, Showroom & Workshop',
  name: 'The Natural Ruby Company',
  priceRange: '$$',
  image: `${host}images/hi-res/logo@2x.jpg`,
  telephone: '+1-212-221-6136'
}

export default {
  head() {
    return {
      htmlAttrs: {
        lang: this.$i18n.locale
      },
      title: this.getTitle(),
      meta: this.getMetaTags(),
      __dangerouslyDisableSanitizers: ['script'],
      script: this.getScripts(),
      link: this.getLinks()
    }
  },

  computed: {
    ...mapGetters({
      metaTags: 'meta',
      pageTitle: 'pageTitle',
      locales: 'locale/locales',
      productDetails: 'productDetails/productDetails',
      customStone: 'customItem/customStone'
    }),

    isCustomItem() {
      return this.$options.pageType === 'review'
    },

    itemStatus() {
      if (this.isCustomItem) return this.customStone.itemStatus
      return this.productDetails.itemStatus
    },

    isSold() {
      return [2, 3].includes(this.itemStatus)
    },

    meta() {
      return this.metaTags
    },

    totalPagesCount() {
      if (!this.type) return 0
      return this.$store.state[this.type].totalPagesCount
    },

    isDev() {
      return process.env.isDev
    }
  },

  methods: {
    getTitle() {
      if (normRouteName(this.$route.name) === 'index') return this.pageTitle
      return [this.pageTitle, postfix].filter((t) => t).join(' | ')
    },

    getLinks() {
      const link = [
        { rel: 'canonical', href: this.getCanonicalPath(this.locale) }
      ]

      if (this.$options.pageType === 'listing') {
        const paginationLinks = this.getPaginationLinks()
        paginationLinks.forEach((l) => {
          link.push(l)
        })
      }

      this.locales.forEach((loc) => {
        link.push({
          rel: 'alternate',
          href: this.getAlternate(loc),
          hreflang: loc
        })
      })
      return link
    },

    getScripts() {
      return [
        {
          innerHTML: JSON.stringify(defaultStructuredData),
          type: 'application/ld+json'
        },
        {
          innerHTML: JSON.stringify(napStructuredData),
          type: 'application/ld+json'
        },
        {
          innerHTML: JSON.stringify(this.structuredData || {}),
          type: 'application/ld+json'
        }
      ]
    },

    getMetaTags() {
      const metaTags = this.$createSeo(this.$route.name, this.meta)
      if (this.isSold)
        metaTags.unshift({
          name: 'robots',
          content: 'noindex'
        })
      return metaTags
    },

    getAlternate(locale) {
      const route = normRoute(this.$route)
      return `https://${process.env.prodDomain}${this.localePath(
        route,
        locale
      )}`
    },

    getCanonicalPath() {
      return this.getPath(this.localePath(normRoute(this.$route)))
    },

    getPath(path) {
      return `https://${process.env.prodDomain}${path}`
    },

    getPaginationLinks() {
      const links = []
      const pageNumber = parseInt(this.$route.query.page) || 1
      const pageNumberNext = pageNumber + 1
      const pageNumberPrev = pageNumber - 1
      const route = normRoute(this.$route)

      if (pageNumberNext <= this.totalPagesCount) {
        route.query.page = pageNumberNext
        links.push({ rel: 'next', href: this.getPath(this.localePath(route)) })
      }

      if (pageNumberPrev > 0) {
        route.query.page = pageNumberPrev
        links.push({ rel: 'prev', href: this.getPath(this.localePath(route)) })
      }

      return links
    }
  }
}
