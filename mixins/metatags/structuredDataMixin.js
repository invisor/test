import { mapGetters } from 'vuex'
import isEmpty from 'lodash/isEmpty'
import { itemNameFormatter, priceValidDate } from '~~/utils/formatters'
import { companyName } from '~~/utils/definitions/defaults'

export default {
  // inject: ['pageType'],

  computed: {
    ...mapGetters({
      customStone: 'customItem/customStone',
      customItem: 'customItem/customItem',
      customSetting: 'customItem/customSetting',
      productDetails: 'productDetails/productDetails'
    }),

    isReview() {
      return this.$options.pageType === 'review'
    },

    description() {
      if (this.isReview) {
        return [
          this.customStone.productDescription,
          this.customSetting.productDescription
        ]
          .filter((d) => d)
          .join(' ')
      }
      return this.productDetails.productDescription
    },

    structuredData() {
      if (this.$options.pageType === 'details')
        return this.structuredDataDetails
      if (this.$options.pageType === 'review') return this.structuredDataReview
      return {}
    },

    structuredDataDetails() {
      if (isEmpty(this.productDetails)) return {}
      const thumbnails = this.productDetails.thumbnails
      const image = thumbnails ? thumbnails.map((image) => image.large) : []
      return {
        '@context': 'http://schema.org/',
        '@type': 'Product',
        name: itemNameFormatter.call(this, this.productDetails, true),
        image,
        description: this.description || this.pageDescription,
        sku: this.$h.getItemId(this.productDetails),
        mpn: this.$h.getItemId(this.productDetails),
        brand: {
          '@type': 'Thing',
          name: companyName[this.$site.name]
        },
        offers: {
          '@type': 'Offer',
          priceCurrency: 'USD',
          price: `${
            this.productDetails.discountPrice ||
            this.productDetails.price ||
            this.productDetails.totalPrice
          }`,
          priceValidUntil: priceValidDate(new Date(Date.now() + 12096e5)),
          itemCondition: 'http://schema.org/NewCondition',
          availability: 'http://schema.org/InStock',
          seller: {
            '@type': 'Organization',
            name: companyName[this.$site.name]
          },
          url: `${process.env.host}/${this.$h.getDetailsPath(
            this.productDetails
          )}`
        }
      }
    },

    structuredDataReview() {
      const thumbnails = this.customItem.preview?.thumbnails || []
      const image = thumbnails ? thumbnails.map((image) => image.large) : []

      return {
        '@context': 'http://schema.org/',
        '@type': 'Product',
        name: itemNameFormatter
          .call(this, this.customItem)
          .replace('<br />', ''),
        image,
        description: this.description || this.pageDescription,
        sku: this.customItem.id,
        mpn: this.customItem.id,
        brand: {
          '@type': 'Thing',
          name: companyName[this.$site.name]
        },
        offers: {
          '@type': 'Offer',
          priceCurrency: 'USD',
          price: `${this.customItem.finalPrice.totalPrice}`,
          priceValidUntil: priceValidDate(new Date(Date.now() + 12096e5)),
          itemCondition: 'http://schema.org/NewCondition',
          availability: 'http://schema.org/InStock',
          seller: {
            '@type': 'Organization',
            name: companyName[this.$site.name]
          },
          url: `${process.env.host}/${this.$h.getDetailsPath(this.customItem)}`
        }
      }
    }
  }
}
