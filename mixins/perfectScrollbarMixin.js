export default {
  data: () => ({
    ps: null
  }),

  beforeDestroy() {
    this.destroyPS()
  },

  methods: {
    destroyPS() {
      if (this.ps) {
        if (Array.isArray(this.ps)) {
          for (let i = 0; i < this.ps.length; i++) {
            this.ps[i].destroy()
          }
          this.ps = null
          return
        }
        this.ps.destroy()
        this.ps = null
      }
    },

    async makePS(el, options = {}, callback) {
      if (el && !this.ps && process.client) {
        if (Array.isArray(el)) {
          this.ps = []
          for (let i = 0; i < el.length; i++) {
            if (el[i])
              this.ps[i] = await this.$perfectScrollbar.insert(
                el[i],
                options,
                callback
              )
          }
          return
        }
        this.ps = await this.$perfectScrollbar.insert(el, options, callback)
      }
    }
  }
}
