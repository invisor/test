import { validationMixin } from 'vuelidate'
import required from 'vuelidate/lib/validators/required'

export default {
  mixins: [validationMixin],

  validations: {
    form: {
      'g-recaptcha-response': { required }
    }
  },

  data: () => ({
    form: {
      'g-recaptcha-response': ''
    }
  }),

  methods: {
    async mountReCaptcha() {
      // if re captcha script already inserted
      if (window.grecaptcha) {
        setTimeout(this.reCaptchaInit)
        return
      }
      window.onloadCallback = this.reCaptchaInit
      await this.$recaptcha.insert(this.reCaptchaInit)
    },

    resetRecaptcha() {
      if (window.grecaptcha) window.grecaptcha.reset()
    },

    reCaptchaInit() {
      if (window.grecaptcha?.render) {
        window.grecaptcha.render(this.$refs.submitForm, {
          sitekey: process.env.reCaptchaKey,
          size: 'invisible',
          callback: this.onFormSubmit,
          badge: 'bottomleft'
        })
      }
    }
  }
}
