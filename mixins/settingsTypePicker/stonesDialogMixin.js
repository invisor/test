import isEmpty from 'lodash/isEmpty'
import { mapGetters, mapActions } from 'vuex'
import { CustomNextPath } from '~~/utils/utils'
import { productListTypeByCategory } from '~~/utils/definitions/defaults'

export default {
  methods: {
    ...mapActions({
      enablePrefilter: 'filters/enablePrefilter',
      fetchDetails: 'productDetails/fetchDetails',
      setCustomStone: 'customItem/setCustomStone',
      resetCustomItem: 'customItem/resetCustomItem'
    }),

    onContinue(type) {},

    async onSettingButtonClick() {
      if (
        !this.showAddToCustomJewelryButton ||
        !this.canAddToJewelry ||
        this.isSoldOut
      )
        return
      this.$modal.hide('quick-view')
      const item = this.item || this.productDetails
      const type = productListTypeByCategory[item.category]
      if (this.isWishlist) {
        this.resetCustomItem()
        await this.fetchDetails({ id: item.id, type })
        this.setCustomStone(item)
      } else {
        this.setCustomStone(item)
      }

      if (
        isEmpty(this.customSetting) &&
        this.customStone.category === 'Stone'
      ) {
        this.$root.$emit('settings-selector')
        return
      }
      const route = new CustomNextPath({
        customStone: this.customStone,
        customSetting: this.customSetting,
        route: this.$route,
        query: this.$h.flushQuery(this.$route.query)
      })
      this.enablePrefilter()
      this.$router.push(this.localePath(route.get()))
    }
  },

  computed: {
    ...mapGetters({
      productDetails: 'productDetails/productDetails',
      customStone: 'customItem/customStone',
      customSetting: 'customItem/customSetting'
    }),

    isWishlist() {
      // details of custom items
      return /wishlist/.test(this.$route.name)
    }
  }
}
