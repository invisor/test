import { mapActions, mapState } from 'vuex'

export default {
  computed: {
    ...mapState({
      showroomForm: (state) => state.showroom.form
    }),

    ...(() => {
      const props = {}
      ;[
        'customerName',
        'phone',
        'email',
        'date',
        'time',
        // 'period',
        'salesPerson',
        'medicalMask',
        'itemIds',
        'message'
      ].forEach(
        (key) =>
          (props[key] = {
            get() {
              return this.showroomForm[key]
            },

            set(value) {
              this.formUpdate({ [key]: value })
            }
          })
      )
      return props
    })()
  },

  methods: {
    ...mapActions({
      formUpdate: 'showroom/formUpdate'
    })
  }
}
