import debounce from 'lodash/debounce'
import trim from 'lodash/trim'
import { mapActions, mapGetters, mapState } from 'vuex'
import md5 from '~~/utils/helpers/md5'

export default {
  watch: {
    totalOrderPrice() {
      this.calculateTax()
    }
  },

  beforeDestroy() {
    this.$root.$off('calc-tax', this.calculateTax)
  },

  mounted() {
    this.$root.$on('calc-tax', this.calculateTax)
    this.$root.$on('remove-tax', this.removeTax)
    this.initTaxCalculation()
  },

  computed: {
    ...mapState({
      fetched: (state) => state.fetched,
      taxHash: (state) => state.cart.taxHash,
      companyAddress: (state) => state.cart.companyAddress
    }),
    ...mapGetters({
      allCartItems: 'cart/allListItems',
      selectedUSState: 'cart/selectedUSState',
      totalOrderPrice: 'cart/getTotalWithoutTax',
      addressForm: 'cart/addressForm',
      metaData: 'cart/metaData',
      pickUpDeliverySelected: 'cart/pickUpDeliverySelected'
    }),

    canCalculateTax() {
      return (
        this.allCartItems.length &&
        (this.addressForm.shippingStreet1 ||
          this.addressForm.shippingStreet2) &&
        this.state?.tax &&
        this.addressForm.shippingZipCode &&
        this.addressForm.shippingCity &&
        this.shippingCountry.key === 'US'
      )
    },

    state() {
      const key = this.addressForm.shippingState
      return this.states.find((state) => state.key === key)
    },

    shippingCountry() {
      const id = this.addressForm.shippingCountryId || 'US'
      return this.metaData.countries.find((country) => country.key === id)
    },

    statesUS() {
      return this.metaData.states?.us || []
    },

    statesCAN() {
      return this.metaData.states?.can || []
    },

    states() {
      if (this.shippingCountry.key === 'US') return this.statesUS
      if (this.shippingCountry.key === 'CA') return this.statesCAN
      return []
    }
  },

  methods: {
    ...mapActions({
      calcTax: 'cart/calculateTax',
      setTaxHash: 'cart/setTaxHash',
      removeTax: 'cart/removeTax'
    }),

    initTaxCalculation() {
      // if store still not restores from local storage wait for store event
      if (!this.fetched) {
        this.$root.$once('store-restored', this.initTaxCalculation)
        return
      }

      // if selected state have not nexus
      if (!this.selectedUSState?.tax) {
        this.removeTax()
        return
      }
      // calc taxes
      if (!this.taxHash) this.setTaxHash(this.getTaxHash())
      this.calculateTax()
    },

    calculateTax: debounce(function () {
      if (!this.canCalculateTax || this.taxHash === this.getTaxHash()) return
      this.calcTax()
      this.setTaxHash(this.getTaxHash())
    }, 1000),

    /**
     * Calculate address hash to avoid multiple unnecessary TaxJar api calls
     */
    getTaxHash() {
      const addressString =
        trim(this.addressForm.shippingStreet1) +
        trim(this.addressForm.shippingStreet2) +
        trim(this.addressForm.shippingZipCode) +
        trim(this.addressForm.shippingCity) +
        trim(this.addressForm.shippingState) +
        trim(this.shippingCountry.key)
      return md5(addressString + this.totalOrderPrice)
    }
  }
}
