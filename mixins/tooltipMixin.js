// tooltips in items titles

export default {
  data: () => ({
    tooltipInstance: null
  }),

  beforeDestroy() {
    this.destroyTooltips()
  },

  mounted() {
    this.initTooltips()
  },

  methods: {
    async initTooltips() {
      this.tooltipInstance = await this.$tippy.insert('.tcw', {
        theme: 'light',
        content: 'Total Carat Weight'
      })
    },

    destroyTooltips() {
      if (this.tooltipInstance) {
        if (Array.isArray(this.tooltipInstance)) {
          this.tooltipInstance.forEach((instance) => {
            instance.destroy()
          })
          this.tooltipInstance = null
          return
        }
        this.tooltipInstance.destroy()
        this.tooltipInstance = null
      }
    }
  }
}
