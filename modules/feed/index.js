const { join, resolve } = require('path')
const { promisify } = require('util')
const { writeFileSync } = require('fs')
const AsyncCache = require('async-cache')
const rss2 = require('./rss2')

const defaults = {
  path: '/feed.xml',
  async create(feed) {},
  cacheTime: 1000 * 60 * 15
}

module.exports = async function(moduleOptions) {
  const options = [
    ...(await parseOptions(this.options.feed)),
    ...(await parseOptions(moduleOptions))
  ].map((o) => ({ ...defaults, ...o }))

  const feedCache = new AsyncCache({
    load(feedIndex, callback) {
      createFeed(options[feedIndex], callback)
    }
  })

  feedCache.get = promisify(feedCache.get)

  options.forEach((feedOptions, index) => {
    this.nuxt.hook('generate:done', async () => {
      if (index === 0) {
        console.log('Generating feeds')
      }

      const xmlGeneratePath = resolve(
        this.options.rootDir,
        join(this.options.generate.dir, feedOptions.path)
      )

      writeFileSync(xmlGeneratePath, await feedCache.get(index))

      console.log('Generated', feedOptions.path)
    })

    this.addServerMiddleware({
      path: feedOptions.path,
      async handler(req, res, next) {
        try {
          const xml = await feedCache.get(index)
          res.setHeader('Content-Type', resolveContentType(feedOptions.type))
          res.end(xml)
        } catch (err) {
          next(err)
        }
      }
    })
  })
}

async function parseOptions(options) {
  // Factory function
  if (typeof options === 'function') {
    options = await options()
  }

  // Factory object
  if (!Array.isArray(options)) {
    if (options.factory) {
      options = await options.factory(options.data)
    }
  }

  // Check if is empty
  if (Object.keys(options).length === 0) {
    return []
  }

  // Single feed
  if (!Array.isArray(options)) {
    options = [options]
  }

  return options
}

function resolveContentType(type) {
  const lookup = {
    rss2: 'application/rss+xml',
    atom1: 'application/atom+xml',
    json1: 'application/json'
  }
  return (
    (lookup.hasOwnProperty(type) ? lookup[type] : 'application/xml') +
    '; charset=UTF-8'
  )
}

async function createFeed(feedOptions, callback) {
  const feed = {
    items: [],
    categories: [],
    contributors: [],
    extensions: [],
    namespaces: []
  }

  try {
    await feedOptions.create.call(this, feed, feedOptions.data)
  } catch (err) {
    console.log('Error while executing feed creation function', err)

    return callback(null, '', feedOptions.cacheTime)
  }

  return callback(null, rss2(feed), feedOptions.cacheTime)
}

module.exports.meta = require('../../package.json')
