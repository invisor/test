const convert = require('xml-js')
// import * as convert from 'xml-js'

/**
 * Returns a RSS 2.0 feed
 */
module.exports = (params) => {
  const { options } = params
  let isAtom = false
  let isContent = false

  const base = {
    _declaration: { _attributes: { version: '1.0', encoding: 'utf-8' } },
    rss: {
      _attributes: { version: '2.0' },
      channel: {
        title: { _text: options.title },
        link: { _text: sanitize(options.link) },
        description: { _text: options.description },
        lastBuildDate: {
          _text: options.updated
            ? options.updated.toUTCString()
            : new Date().toUTCString()
        },
        docs: {
          _text: options.docs
            ? options.docs
            : 'https://validator.w3.org/feed/docs/rss2.html'
        }
      }
    }
  }

  /**
   * Channel language
   * https://validator.w3.org/feed/docs/rss2.html#ltlanguagegtSubelementOfLtchannelgt
   */
  if (options.language) {
    base.rss.channel.language = { _text: options.language }
  }

  /**
   * Channel ttl
   * https://validator.w3.org/feed/docs/rss2.html#ltttlgtSubelementOfLtchannelgt
   */
  if (options.ttl) {
    base.rss.channel.ttl = { _text: options.ttl }
  }

  /**
   * Channel Image
   * https://validator.w3.org/feed/docs/rss2.html#ltimagegtSubelementOfLtchannelgt
   */
  if (options.image) {
    base.rss.channel.image = {
      title: { _text: options.title },
      url: { _text: options.image },
      link: { _text: sanitize(options.link) }
    }
  }

  /**
   * Channel Copyright
   * https://validator.w3.org/feed/docs/rss2.html#optionalChannelElements
   */
  if (options.copyright) {
    base.rss.channel.copyright = { _text: options.copyright }
  }

  /**
   * Channel Categories
   * https://validator.w3.org/feed/docs/rss2.html#comments
   */
  params.categories.map((category) => {
    if (!base.rss.channel.category) {
      base.rss.channel.category = []
    }
    base.rss.channel.category.push({ _text: category })
  })

  /**
   * Feed URL
   * http://validator.w3.org/feed/docs/warning/MissingAtomSelfLink.html
   */
  const atomLink = options.feed || (options.feedLinks && options.feedLinks.rss)
  if (atomLink) {
    isAtom = true
    base.rss.channel['atom:link'] = [
      {
        _attributes: {
          href: sanitize(atomLink),
          rel: 'self',
          type: 'application/rss+xml'
        }
      }
    ]
  }

  /**
   * Hub for PubSubHubbub
   * https://code.google.com/p/pubsubhubbub/
   */
  if (options.hub) {
    isAtom = true
    if (!base.rss.channel['atom:link']) {
      base.rss.channel['atom:link'] = []
    }
    base.rss.channel['atom:link'] = {
      _attributes: {
        href: sanitize(options.hub),
        rel: 'hub'
      }
    }
  }

  /**
   * Channel Categories
   * https://validator.w3.org/feed/docs/rss2.html#hrelementsOfLtitemgt
   */
  base.rss.channel.item = []

  params.items.map((entry) => {
    const item = {}

    Object.keys(entry).forEach((param) => {
      item[param] = { _text: entry[param] }

      if (param === 'title') {
        item[param] = { _cdata: entry[param] }
      }

      if (param === 'link') {
        item[param] = { _text: sanitize(entry[param]) }
      }

      if (param === 'guid') {
        item[param] = { _text: entry[param] }
      } else if (entry.id) {
        item[param] = { _text: entry.id }
      } else if (entry.link) {
        item[param] = { _text: sanitize(entry.link) }
      }

      if (param === 'date') {
        item.pubDate = { _text: entry[param].toUTCString() }
      }

      if (param === 'published') {
        item.pubDate = { _text: entry[param].toUTCString() }
      }

      if (param === 'description') {
        item[param] = { _cdata: entry[param] }
      }

      if (param === 'content') {
        isContent = true
        item['content:encoded'] = { _cdata: entry[param] }
      }

      if (Array.isArray(entry[param])) {
        item[param] = []
        entry[param].map((value) => {
          item[param].push(value)
        })
      }
      /**
       * Item Author
       * https://validator.w3.org/feed/docs/rss2.html#ltauthorgtSubelementOfLtitemgt
       */
      if (param === 'author' && Array.isArray(entry[param])) {
        item[param] = []
        entry[param].map((author) => {
          if (author.email && author.name) {
            item[param].push({ _text: author.email + ' (' + author.name + ')' })
          }
        })
      }
      /**
       * Item Category
       * https://validator.w3.org/feed/docs/rss2.html#ltcategorygtSubelementOfLtitemgt
       */
      if (param === 'category' && Array.isArray(entry[param])) {
        item[param] = []
        entry[param].map((category) => {
          item[param].push(formatCategory(category))
        })
      }

      /**
       * Item Enclosure
       * https://validator.w3.org/feed/docs/rss2.html#ltenclosuregtSubelementOfLtitemgt
       */
      if (param === 'enclosure') {
        item[param] = formatEnclosure(entry[param])
      }

      if (param === 'image') {
        item.enclosure = formatEnclosure(entry[param], 'image')
      }

      if (param === 'audio') {
        item.enclosure = formatEnclosure(entry[param], 'audio')
      }

      if (param === 'video') {
        item.enclosure = formatEnclosure(entry[param], 'video')
      }
    })

    base.rss.channel.item.push(item)
  })

  if (isContent) {
    base.rss._attributes['xmlns:dc'] = 'http://purl.org/dc/elements/1.1/'
    base.rss._attributes['xmlns:content'] =
      'http://purl.org/rss/1.0/modules/content/'
  }

  if (options.namespaces.length) {
    options.namespaces.forEach((namespace) => {
      base.rss._attributes[namespace.name] = namespace.link
    })
  }

  if (isAtom) {
    base.rss._attributes['xmlns:atom'] = 'http://www.w3.org/2005/Atom'
  }
  return convert.js2xml(base, { compact: true, ignoreComment: true, spaces: 4 })
}

/**
 * Returns a formated enclosure
 * @param enclosure
 * @param mimeCategory
 */
const formatEnclosure = (enclosure, mimeCategory = 'image') => {
  if (typeof enclosure === 'string') {
    const type = new URL(enclosure).pathname.split('.').slice(-1)[0]
    return {
      _attributes: {
        url: enclosure,
        length: 0,
        type: `${mimeCategory}/${type}`
      }
    }
  }

  const type = new URL(enclosure.url).pathname.split('.').slice(-1)[0]
  return {
    _attributes: { length: 0, type: `${mimeCategory}/${type}`, ...enclosure }
  }
}

/**
 * Returns a formated category
 * @param category
 */
const formatCategory = (category) => {
  const { name, domain } = category
  return {
    _text: name,
    _attributes: {
      domain
    }
  }
}

export function sanitize(url) {
  if (typeof url === 'undefined') {
    return
  }
  return url.replace(/&/g, '&amp;')
}
