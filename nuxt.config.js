import { title, description } from './utils/definitions/defaults'

export const isDev = process.env.NODE_ENV !== 'production'
export const apiHost = isDev ? 'qa-api.47thst.com' : 'api.47thst.com'
export const protocol = isDev ? 'http' : 'https'
export const port = isDev ? 80 : 443

export const cacheTime = 1000 * 60 * 60 * 24

const script = []

// for production only
if (!isDev) {
  script.push({
    src: 'https://cdn.noibu.com/collect.js',
    async: true
  })
}

export default {
  server: {
    // port: 3000, // default: 3000
    // host: '192.168.1.68', // default: localhost
    timing: {
      total: true
    }
  },

  /*
   ** Headers of the page
   */
  head: {
    title,
    meta: [
      { charset: 'utf-8' },
      { name: 'p:domain_verify', content: 'a7c4a4085a03860ccd3222ab1738ee5c' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content: '#e2e2e2' }
    ],
    script,
    link: [
      {
        rel: 'preconnect',
        href: 'https://service.thenaturalsapphirecompany.com'
      },
      {
        rel: 'preload',
        as: 'font',
        crossOrigin: 'anonymous',
        href: '/styles/fonts/TrajanProRegular/TrajanPro-Regular.woff2'
      },
      {
        rel: 'preload',
        as: 'font',
        crossOrigin: 'anonymous',
        href: '/styles/fonts/Lora/woff2/Lora-Regular.woff2'
      },
      {
        rel: 'preload',
        as: 'font',
        crossOrigin: 'anonymous',
        href: '/styles/fonts/Lora/woff2/Lora-Italic.woff2'
      },
      {
        rel: 'preload',
        as: 'font',
        crossOrigin: 'anonymous',
        href: '/styles/fonts/Lato/woff2/Lato-Regular.woff2'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '../../components/Common/LoadingOverlay/LoadingOverlayNuxt',

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '../../plugins/store.js',
    '../../plugins/eventBus.js',
    '../../plugins/api',
    '../../plugins/axios',
    '../../plugins/analytics.client',
    '../../plugins/vue-observer',
    '../../plugins/events.client',
    '../../plugins/global-components',
    '../../plugins/fonts.client',
    '../../plugins/interpolationLinks.client',
    '../../plugins/helpers',
    '../../plugins/localStorage.client',
    '../../plugins/vue-js-modal',
    '../../plugins/metaTags'
  ],

  modern: process.env.NODE_ENV === 'production',
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '../../modules/integrations.js',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/device',
    '@nuxtjs/robots',
    'nuxt-i18n',
    '@nuxtjs/sitemap',
    '../../modules/feed',
    'cookie-universal-nuxt',
    'nuxt-compress'
  ],

  pwa: {
    meta: false // disables the meta module
  },

  'nuxt-compress': {
    gzip: {
      threshold: 8192
    },
    brotli: {
      threshold: 8192
    }
  },

  serverMiddleware: [
    { path: '/hc', handler: '../../server-middleware/healthcheck' },
    '../../server-middleware/headers'
  ],

  /*
   ** Global CSS
   */
  css: [
    { src: '@fortawesome/fontawesome-free/css/all.css' },
    { src: '~/assets/styles/main.sass' },
    { src: 'static/styles/fonts.css', lang: 'css' },
    { src: 'vue-js-modal/dist/styles.css', lang: 'css' }
  ],

  router: {
    trailingSlash: true,
    prefetchLinks: false
  },

  manifest: {
    name: title,
    short_name: 'NRC',
    display: 'standalone',
    background_color: '#fff',
    description
  },

  styleResources: {
    sass: ['./assets/styles/constants.sass', '../../assets/styles/_common.sass']
  },

  i18n: {
    defaultLocale: 'en',
    locales: [
      {
        code: 'en',
        file: 'en-US.js'
      },
      {
        code: 'zh',
        file: 'zh-CN.js'
      }
    ],
    lazy: true,
    langDir: 'locales/'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */

  axios: {
    host: apiHost,
    port,
    https: !isDev
  },

  render: {
    resourceHints: false, // disable prefetch routes,
    // static: {
    //   maxAge: 60 * 60 * 24 * 365 * 1000
    // }
    static: {
      maxAge: 10 * 1000
    }
  },

  /*
   ** Build configuration
   */
  build: {
    // analyze: isDev,

    optimization: {
      splitChunks: {
        minSize: 100000,
        maxSize: 500000
      }
    },

    loaders: {
      fontUrl: { limit: -1 },
      imgUrl: { limit: -1 },
      sass: { sourceMap: false }
    },

    filenames: {
      app: ({ isDev }) => (isDev ? '[name].js' : '[contenthash].js'),
      chunk: ({ isDev }) => (isDev ? '[name].js' : '[contenthash].js'),
      img: ({ isDev }) =>
        isDev ? '[path][name].[ext]' : 'img/[path][name].[ext]'
    },

    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    },

    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.devtool = ctx.isClient ? 'eval-source-map' : 'inline-source-map'
      config.node = {
        fs: 'empty'
      }
    }
  }
}
