export default ({ app }) => {
  app.router.afterEach((to, from) => {
    if (window && window.ga) window.ga('send', 'pageview', location.pathname)
  })
}
