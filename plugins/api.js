import repository from '../api'

export default async ({ $axios }, inject) => {
  const api = {}
  Object.keys(repository).forEach((key) => {
    api[key] = repository[key]($axios, process.env.isDev)
  })
  await inject('api', api)
}
