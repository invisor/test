import parser from 'ua-parser-js'
import get from 'lodash/get'
import isbot from 'isbot'
import { isTokenValid } from '~~/utils/utils'
import { APP } from '~~/store/modules/app'
import LIST from '~~/store/lists/cart/constants'
import addSecurityHeaders from '~~/utils/helpers/securityHeaders.js'

// add signature protect headers
export default async ({ app, $axios, store, req, $sentry, route }) => {
  if (process.server && req) {
    const ip =
      process.env.NODE_ENV === 'development'
        ? await $axios.$get('http://icanhazip.com')
        : req.headers['x-forwarded-for'] ||
          req.connection.remoteAddress ||
          req.socket.remoteAddress

    const agent = req.headers['user-agent'].toLowerCase()

    try {
      const ua = parser(agent)

      if (agent.match(/bot|spider|checker|lighthouse|insights/i)) return
      if (isbot(agent)) return

      if (ua.browser && ua.browser.name)
        await store.dispatch('getCountryCode', { ip, req, route })
    } catch (e) {
      if ($sentry) $sentry.captureException(e)
    }
  }

  $axios.onRequest((request) => {
    addSecurityHeaders(request)
    request.headers.common['Country-Code'] = store.getters.countryCode

    const token = store.state.auth.access_token
    const tokenExpiration = store.state.auth.tokenExpiration

    if (token && !isTokenValid(tokenExpiration)) {
      app.$auth.logout()
    }
    // Update token axios header
    if (token) {
      request.headers.common.Authorization = `Bearer ${token}`
    }
  })

  $axios.onError((error) => {
    const safeMessage =
      'Oops! Something went wrong! Please try again, or get in touch if you need help.'
    // handle expired token
    if (
      error.response?.status === 400 &&
      error.response.config.url.endsWith('account/auth')
    ) {
      store.commit(
        `app/${APP.ERROR}`,
        get(error, 'response.data', safeMessage) || safeMessage
      )
    } else if (error.response?.status === 401) {
      app.$auth.logout()
    } else if (error.response?.status === 429) {
      store.commit(`cart/${LIST.BACKEND_TAX_CALC_MODE}`, true)
    } else {
      store.commit(
        `app/${APP.ERROR}`,
        get(error, 'response.data', safeMessage) || safeMessage
      )
      return Promise.reject(error)
    }
  })
}
