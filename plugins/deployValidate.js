import { cookiesOpts } from '~~/utils/definitions/defaults.js'

export default ({ app }) => {
  // Every time the route changes (fired on initialization too) validate deploy version
  app.router.afterEach(async (to, from) => {
    if (to.name === from.name) return

    const v = await app.$api.app.fetchVersion()

    const deployVersion = app.$cookies.get('deploy-version')

    if (!deployVersion) {
      app.$cookies.set('deploy-version', v.deploy, cookiesOpts)
      return
    }

    if (Number(deployVersion) !== v.deploy) {
      app.$cookies.set('reload', true, cookiesOpts)
      app.$cookies.set('deploy-version', v.deploy, cookiesOpts)
    }

    if (process.client) {
      const reloadNeeded = app.$cookies.get('reload')

      if (reloadNeeded) {
        app.$cookies.remove('reload')
        if (location) location.reload()
      }
    }
  })
}
