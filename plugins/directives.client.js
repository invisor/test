import Vue from 'vue'

const loading = () => {
  const state = {}

  const applyLoader = (el, binding, vndoe) => {
    if (!state._el) return
    if (binding.value) {
      state._el.style.position = 'relative'
      state._loaderEl.classList.add('loading--trans-bg')
      state._el.prepend(state._loaderEl)
    } else {
      state._el.position = state._originalPosition
      const loader = state._el.querySelector('.loading')
      if (loader) loader.remove()
    }
  }

  return {
    bind(el, binding, vnode) {
      if (!el) return
      state._el = el
      state._originalPosition = state._el.style.position
      state._loaderEl = document.createElement('div')
      state._loaderEl.classList.add('loading')
      const _loaderElInner = document.createElement('div')
      _loaderElInner.classList.add('loading--inner')

      const fragment = document.createDocumentFragment()

      for (let i = 0; i < 3; i++) {
        const span = document.createElement('span')
        fragment.appendChild(span)
      }

      _loaderElInner.appendChild(fragment)

      state._loaderEl.appendChild(_loaderElInner)
    },

    inserted: applyLoader,

    update: applyLoader,

    unbind(el, binding, vnode) {
      delete state._el
      delete state._originalPosition
      delete state._loaderEl
    }
  }
}

Vue.directive('loading', loading())
