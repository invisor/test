export default ({ store }) => {
  document.fonts.ready.then(function() {
    const html = document.getElementsByTagName('html')[0]
    store.dispatch('app/setAllFontsLoaded')
    html.classList.add('fonts-loaded')
  })
}
