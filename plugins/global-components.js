import Vue from 'vue'
import Image from '~~/components/Common/Image'
import Icon from '~~/components/Common/Icon'
import Link from '~~/components/Common/Link'

Vue.component('v-image', Image)
Vue.component('v-link', Link)
Vue.component('icon', Icon)
