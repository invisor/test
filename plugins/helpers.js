import * as utils from '~~/utils/utils'

export default (context, inject) => {
  const helper = {
    ...utils
  }
  inject('h', helper)
  inject('site', {
    name: process.env.siteName
  })
}
