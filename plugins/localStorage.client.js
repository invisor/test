import createPersistedState from 'vuex-persistedstate'
import uniq from 'lodash/uniq'
import { productListTypeByCategory } from '~~/utils/definitions/defaults'

const wishlistHoursExpire = 48

export default ({ store, isHMR }) => {
  if (isHMR) return
  window.onNuxtReady((app) => {
    if (process.env.version !== localStorage.getItem('site-version')) {
      clearLocalStorage(store, app)
      deleteAllCookies()
      return
    }

    init(store)

    removeOldWishlistItems(store)

    store.dispatch('setFetched')
    setTimeout(() => {
      store.dispatch('favorites/markItemsAsNotSynced')
      store.dispatch('compareList/markItemsAsNotSynced')
      store.dispatch('cart/markItemsAsNotSynced')
      app.$root.$emit('store-restored')
    }, 10)

    store.dispatch('account/fetchLists')
  })
}

const MS_PER_HOUR = 1000 * 60 * 60

function clearLocalStorage(store, app) {
  localStorage.clear()
  localStorage.setItem('site-version', process.env.version)
  init(store)
  store.dispatch('setFetched')
  setTimeout(() => {
    app.$root.$emit('store-restored')
  })
}

function removeOldWishlistItems(store) {
  const listsNames = Object.values(productListTypeByCategory)
  listsNames.pop()
  const itemsKeys = uniq(listsNames)
  itemsKeys.forEach((key) => {
    store.getters[`favorites/${key}Items`].forEach((item) => {
      if (!item.addedAt) store.dispatch('favorites/removeFromList', item)
      if (
        dateDiffInDays(item.addedAt, new Date()) > wishlistHoursExpire &&
        !store.state.auth.loggedIn
      )
        store.dispatch('favorites/removeFromList', item)
    })
  })
}

function dateDiffInDays(a, b) {
  return Math.floor((Date.parse(b) - Date.parse(a)) / MS_PER_HOUR)
}

function init(store) {
  createPersistedState({
    key: 'rubies',
    paths: [
      'cart',
      'favorites',
      'account',
      'compareList',
      'navigation',
      'shareProduct'
    ]
  })(store)
}

function deleteAllCookies() {
  const cookies = document.cookie.split(';')

  for (let i = 0; i < cookies.length; i++) {
    const cookie = cookies[i]
    const eqPos = cookie.indexOf('=')
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'
  }
}
