import appModules from '~~/store'

export default async ({ store }) => {
  for (const module in appModules) {
    await store.registerModule(module, appModules[module])
    if (appModules[module]?.mutations?.bindNamespace)
      store.commit(`${module}/bindNamespace`, module)
  }
}
