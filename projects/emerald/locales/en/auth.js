export default {
  or: 'OR',
  signInForm: {
    signIn: 'Sign in',
    close: 'close',
    email: 'Email address',
    pass: 'Password',
    forgotPass: 'Forgot your password?',
    remember: 'Remember me',
    signUp: 'Sign up',
    cancel: 'Cancel',
    login: 'Login',
    rules: {
      email: {
        required: 'Please enter email address'
      },
      password: {
        required: 'Please enter your password'
      }
    }
  },
  signUpForm: {
    signUp: 'Sign up',
    close: 'close',
    firstName: 'First name',
    lastName: 'Last Name',
    email: 'Email address',
    pass: 'Password',
    rePass: 'Re-enter Password',
    signIn: 'Sign in',
    register: 'Register',
    rules: {
      email: {
        required: 'A valid email address is required',
        type: 'Please enter correct email address'
      },
      password: {
        required: 'Please enter your password',
        range: 'Your password must contain at least 6 characters'
      },
      recaptcha: {
        required: 'Please verify the captcha'
      }
    },
    checkPass1: 'Please enter the password again',
    checkPass2: 'Password does not match the confirm password'
  }
}
