export default {
  settings: {
    settingRing: 'Ring',
    settingEarring: 'Earrings',
    settingNecklace: 'Necklace',
    settingPendant: 'Pendant'
  },
  stone: 'Stone',
  pair: 'Pair',
  ring: 'Ring',
  mensRing: "Men's Ring",
  earring: 'Earrings',
  pendant: 'Pendant',
  necklace: 'Necklace',
  bracelet: 'Bracelet',
  weddingBand: 'Wedding Band',
  plainBand: 'Plain Band',
  settingRing: 'Setting_Ring',
  settingEarring: 'Setting_Earring',
  settingNecklace: 'Setting_Necklace',
  settingPendant: 'Setting_Pendant',
  custom: 'Custom',
  setting: 'Setting',
  wax: 'Wax'
}
