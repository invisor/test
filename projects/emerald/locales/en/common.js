export default {
  tcw: '<span>{0}</span>&nbsp;<span class="tooltip-light">Ct.Tw.<span class="tooltiptext">Total Carat Weight</span></span>',
  totalCaratWeight: 'Total Carat Weight',
  tcwPlain: '{0} Ct.Tw.',
  'tcw-t':
    '<span>{0}</span>&nbsp;<span class="tooltip-light tooltipless">Ct.Tw.</span>',
  unknown: 'Unknown',
  videoMessage:
    "This video is of the actual item, we do not use 'stock' photos. The media shown here is the actual item.",
  videoMessageInclusions:
    "This video is of the actual item, we do not use 'stock' photos. The item is filmed under high magnification. Note: Inclusions are far less visible at eye level in the phone video.",
  close: 'Close',
  priceConversionTitle: 'Currency Converted to {currency}',
  priceConversionMessage:
    "We detected that you are visiting our website from {country}. As a courtesy, we have made a close approximation of this item's price after converting from USD to {currency}. Please note that we process all charges in USD, and final prices may vary due to associated credit card fees and exchange rate fluctuations.",
  priceConversionOriginal: 'Price in USD: {price}',
  cookieMessage:
    "We use cookies to personalize content and ads, to provide social media features and to analyze our traffic. We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you've provided to them or that they've collected from your use of their services. You consent to our cookies if you continue to use our website.",
  cookieAccept: 'I accept',
  cookieMore: 'Learn more...',
  topBanner: {
    rubies: 'Rubies',
    emeralds: 'Emeralds',
    sapphires: 'Sapphires'
  },
  whyPhone: 'Why are we asking for a phone number?',
  whyPhoneContent:
    "Don't worry - we won't call you unless you specifically ask us to, or in the event that we have not heard back on a request via email. Also, please know that we respect your privacy. Your contact information is kept secure and and is never shared or sold."
}
