import shape from './constants/stoneShapes'
import origin from './constants/stonesOrigins'
import treatment from './constants/treatments'
import metalName from './constants/metalTypes'
import cut from './constants/stoneCut'
import clarity from './constants/stoneClarity'
import colorIntensity from './constants/stoneColorIntensity'
import color from './constants/stoneColors'
import prong from './constants/settingProngs'

export default {
  dreamCompare: 'Dream compare',
  dreamCompareDescription:
    "Our Dream Compare allows you to view any of our items that have a 360' video saved in your compare tabs.",
  customJewelry: 'Custom Jewelry',
  jewelry: 'Regular Jewelry',
  stones: 'Emeralds',
  pairs: 'Emerald Pairs',
  settings: 'Settings',
  weddingBands: 'Wedding Bands',
  dragAndDrop: 'Drag and Drop',
  stonesEmpty: 'Your emeralds compare list is empty!',
  jewelriesEmpty: 'Your jewelries compare list is empty!',
  pairsEmpty: 'Your stone pairs compare list is empty!',
  settingsEmpty: 'Your settings compare list is empty!',
  bandsEmpty: 'Your wedding bands compare list is empty!',
  customEmpty: 'Your custom jewelry compare list is empty!',
  lostSettingPrice: "Where's the Price",
  lostSettingPriceHeader: "Where's the Price?",
  lostSettingPriceText:
    'Each piece of custom jewelry is uniquely crafted to match the stone you have selected. As such your final price can only be calculated once you have chosen a compatible stone and all available options which may include: Metal Type, Ring Size, Side Stones and/or Pave Diamond Weight. Simply continue by selecting this setting below. We will calculate the price automatically for you during the next step.',
  compareTable: {
    addToFavorites: 'Add to favorites',
    alreadyIn: 'Already in',
    favorites: 'favorites',
    image: '',
    id: 'ID',
    designId: 'Design ID',
    weight: 'Carat',
    totalPrice: 'Total Price',
    totalCarat: 'Total Carat',
    metalWeight: 'Metal Weight',
    styleName: 'Style Name',
    stoneWeight: 'Carat',
    centralStoneWeight: 'Center Stone Weight',
    centerDimensions: 'Dimensions (mm)',
    productionTime: 'Production Time',
    productionTimeValue: '{from} to {to} Days',
    dimensions: 'Dimensions (mm)',
    color: 'Color',
    sideStonesCount: 'Total Side Stones',
    sideStonesWeight: 'Side Stone Weight',
    colorIntensity: 'Color Intensity',
    clarity: 'Clarity',
    labType: 'Lab type',
    labColor: 'Lab color',
    metalName: 'Metal Type',
    treatment: 'Treatment',
    shape: 'Shape',
    cut: 'Cut',
    origin: 'Origin',
    prong: 'Prongs',
    pricePerCt: 'Per Carat Price',
    price: 'Price',
    settingsPrice: 'Price'
  },
  color,
  shape,
  cut,
  clarity,
  colorIntensity,
  origin,
  treatment,
  metalName,
  prong
}
