export default {
  antique: 'Antique',
  classic: 'Classic',
  eternity: 'Eternity',
  mens: "Men's",
  modern: 'Modern'
}
