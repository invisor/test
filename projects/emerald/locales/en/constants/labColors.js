export default {
  nA: 'NA',
  deepGreen: 'Deep Green',
  green: 'Green',
  vividGreenToDeepGreen: 'Vivid Green to Deep Green',
  darkGreen: 'Dark Green',
  intenseGreen: 'Intense Green',
  intenseGreenToVividGreen: 'Intense Green to Vivid Green',
  vividGreen: 'Vivid Green',
  vividGreenImperialGreen: 'Vivid Green "Imperial Green"',
  vividGreenVibrant: 'Vivid Green "Vibrant"'
}
