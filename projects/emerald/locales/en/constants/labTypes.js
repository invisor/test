export default {
  gia: 'GIA',
  agta: 'AGTA',
  aigs: 'AIGS',
  other: 'Other',
  cDunaigre: 'C.Dunaigre',
  agl: 'AGL',
  grs: 'GRS',
  gal: 'GAL',
  gubelin: 'GUBELIN',
  igi: 'IGI',
  ssef: 'SSEF',
  tgl: 'TGL'
}
