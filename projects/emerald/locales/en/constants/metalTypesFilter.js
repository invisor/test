export default {
  palladium: 'Palladium',
  platinum950: 'Platinum 950',
  platinumRose: 'Platinum Rose',
  platinumYellow: 'Platinum Yellow',
  '14KRoseGold': '14K Rose Gold',
  '18KRoseGold': '18K Rose Gold',
  rosePlatinum: 'Rose Platinum',
  '14KRoseWhite': '14K Rose White',
  '18KRoseWhite': '18K Rose White',
  '14KRoseYellow': '14K Rose Yellow',
  silver: 'Silver',
  '14KWhiteGold': '14K White Gold',
  '18KWhiteGold': '18K White Gold',
  '22KWhiteGold': '22K White Gold',
  '14KWhiteRose': '14K White Rose',
  '18KWhiteRose': '18K White Rose',
  '14KWhiteYellow': '14K White Yellow',
  '18KWhiteYellow': '18K White Yellow',
  '14KYellowGold': '14K Yellow Gold',
  '18KYellowGold': '18K Yellow Gold',
  yellowPlatinum: 'Yellow Platinum',
  '22KYellowGold': '22K Yellow Gold',
  '14KYellowRose': '14K Yellow Rose',
  '18KYellowRose': '18K Yellow Rose',
  '14KYellowWhite': '14K Yellow White',
  '18KYellowWhite': '18K Yellow White',
  '18KRoseYellow': '18K Rose Yellow',
  platinum18KYellow: 'Platinum & 18K Yellow',
  none: 'none'
}
