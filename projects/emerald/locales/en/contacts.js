export default {
  pageTitle: 'Contact Us',
  pageMessage:
    'Have a question, inquiry, or simply want to get in touch? We’re here to help! Please complete the form below and a member of our sales staff will contact you shortly. You can also call us at <strong>1-212-204-8581</strong> or email us at <a class="email" href="mailto:info@emeralds.com">info@emeralds.com</a>.',
  collapseTitle1: 'Head Office, Showroom & Workshop',
  collapseTitle2: 'CAD and Production',
  dropdown: {
    close: 'Close',
    showroom: 'Showroom',
    viewingExperience:
      'For a unique viewing experience visit our private Penthouse Showroom in Manhattan NYC.',
    contactTime1: 'Available Mon - Fri',
    contactTime2: '10:30am - 5pm ET',
    contactTime3: 'Sun: 12pm - 4pm ET',
    appointment: 'Make Appointment',
    locations: 'Locations',
    locationsContent:
      '<span>Showroom & Workshop</span> 6 East 45th St, 20th Floor <br> New York, <br> NY 10017 <br> United States',
    miningProduction:
      '<span>CAD & Production</span> 73/1/3 Council Avenue <br>Demuwawatha, Ratnapura <br>Sri Lanka',
    phone: 'Phone',
    liveChat: 'Live Chat',
    hoursAvailable: 'Hours Available',
    email: 'Email',
    help: "<span>We're here to help!</span> We invite you to send us a message through our website at any time.",
    contactUs: 'Contact Us'
  },
  customerService: {
    title: 'Customer Service Hours',
    week1: 'Mon – Fri',
    week2: 'Thur - Fri',
    week3: 'Sat',
    week4: 'Sun',
    time1: '10:30am – 9:00pm <small>ET</small>',
    time2: '10:30am – 6:00pm <small>ET</small>',
    time3: '12:00pm – 4:00pm <small>ET</small>',
    time4: '12:00pm – 4:00pm <small>ET</small>',
    phone: 'Phone:',
    email: 'Email:'
  },
  headOffice: {
    title: 'Head Office, Showroom & Workshop',
    address1: '6 East 45th Street, 20th Floor',
    address2: 'New York, NY 10017',
    country: 'United States',
    map: 'View Map',
    directions: 'Get Directions',
    link: 'Make An Appointment'
  },
  mining: {
    title: 'CAD and Production',
    address1: '73/1/3 Council Avenue',
    address2: 'Demuwawatha, Ratnapura',
    country: 'Sri Lanka',
    map: 'View Map',
    directions: 'Get Directions'
  },
  formTitle: 'Send us a message!',
  form: {
    customerName: 'Name',
    phone: 'Phone',
    email: 'Email',
    city: 'City',
    zipCode: 'Zip Code',
    country: 'country',
    pleaseSelect: 'Please select',
    state: 'State',
    stateMax: 'STATE/PROVINCE/COUNTY',
    submit: 'Submit',
    address: 'Address'
  },
  swal: {
    title: 'Thank you for getting in touch!',
    message: 'Thanks for contacting us! We will be in touch with you shortly.',
    ok: 'Ok'
  },
  formErrors: {
    customerName: 'Please enter name',
    message: 'Please enter message content',
    captcha: 'Please verify the captcha',
    email: {
      required: 'Please enter email',
      valid: 'Please enter correct email address'
    },
    phone: {
      required: 'Please enter phone number'
    },
    date: 'Please enter date',
    time: 'Please enter time'
  }
}
