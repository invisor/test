export default {
  createYourJewelry:
    'Design your own Ring, Earrings, Pendant or Necklace in 3 simple steps...',
  startCreating: 'Start creating',
  stepOneTitle: 'Choose your emerald',
  stepOneText:
    'Begin the process by searching through our huge inventory of natural emeralds to find your perfect stone.',
  stepTwoTitle: 'Choose your setting',
  stepTwoText:
    'With your perfect emerald selected, peruse our expertly designed settings to find the exact one to perfectly showcase your emerald.',
  stepThreeTitle: 'Complete your jewelry',
  stepThreeText:
    'Preview your creation with your chosen emerald and setting before completing the purchase of your jewelry!'
}
