export default {
  title1: 'Custom Designs Created',
  title2: 'With This Setting',
  stone: 'With This Stone',
  pair: 'With This Pair',
  message: 'See what our customers have been creating with our {0} service.',
  dyo: 'Design Your Own',
  designDescription:
    'You’re looking at a custom designed piece of jewelry with a {0}, set in a {1} Setting. This item was created by one of our customers using our completely free, {2} service.',
  productionTime: 'Production Time',
  productionTimePeriod: 'from {0} to {1} days',
  price: 'Price Starting at',
  view: 'View & customize this design',
  loading: 'Loading data. Please wait...',
  empty: 'No custom designs found'
}
