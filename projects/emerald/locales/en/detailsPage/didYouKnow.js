export default {
  title: 'Did You Know?',
  message:
    'We provide two options to see your custom design before you purchase!',
  previewTitle: 'Free Image Preview',
  previewMessage:
    'We can send you a combined 3D image rendering of your emerald and setting for Free!',
  previewButton: 'Request free preview',
  waxTitle: 'Polymer Wax Model',
  waxMessage:
    'We can send you a 3D printed model of your combined setting design.',
  waxButton: 'Purchase a Polymer Model'
}
