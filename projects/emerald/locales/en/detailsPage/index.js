import designStylesFilter from '../constants/designStylesFilter'
import filtersOrigins from '../constants/filtersOrigins'
import metalTypesFilter from '../constants/metalTypesFilter'
import prongsNames from '../constants/prongsNames'
import stoneShapes from '../constants/stoneShapes'
import stoneTypes from '../constants/stoneTypes'
import treatments from '../constants/treatments'
import mainDetails from './mainDetails'
import productOrigin from './productOrigin'
import sizingGuide from './sizingGuide'
import requestPreview from './requestPreview'
import qualityValueHeart from './qualityValueHeart'
import question from './question'
import callToAction from './callToAction'
import ourProcess from './ourProcess'
import customDesigns from './customDesigns'
import didYouKnow from './didYouKnow'

export default {
  bankWirePrice: {
    title: 'Bank Wire Price',
    titleStone: 'Bank Wire Stone Price',
    tooltipTitle: 'Bank Wire Price',
    tooltipBody:
      'Save 2.5% off list price when you choose to pay via bank wire transfer. Simply place your order online and one of our friendly representatives will reach out to you with your final discounted price and payment instructions.',
    notification:
      'The 2.5% bank wire discount applied to the item in your shopping cart.'
  },
  lightSkin: 'Light Skin',
  darkSkin: 'Dark Skin',
  ringSizeConfirm: {
    text: 'Please reconfirm your ring size is a size {0} by clicking the confirm button below',
    confirm: 'Confirm',
    cancel: 'Cancel'
  },
  fullscreen: 'Open in fullscreen',
  controls: {
    question: 'Ask a question',
    share: 'Share',
    favorites: 'Add to favorites',
    compare: 'Add to compare list'
  },
  from: 'from',
  pairFrom: 'Pair from',
  setting: '{0} Setting',
  productToCart: 'Add {0} to cart',
  and: 'and',
  productToSetting: 'Choose this {0} setting',
  wireframe: 'Wireframe',
  reset: 'Reset',
  treatmentsName: {
    ...treatments
  },
  stoneShapesName: {
    ...stoneShapes
  },
  stoneTypeName: stoneTypes,
  originName: {
    ...filtersOrigins
  },
  metalTypesNames: {
    ...metalTypesFilter
  },
  prongsNames,
  sizingGuide,
  productTypes: {
    earring: 'Earring',
    bracelet: 'Bracelet',
    necklace: 'Necklace',
    pendant: 'Pendant',
    ring: 'Ring',
    weddingBand: 'Wedding band',
    plainBand: 'Plain band',
    settingRing: 'Setting ring',
    settingEarring: 'Setting earring',
    settingPendant: 'Setting pendant',
    settingNecklace: 'Setting necklace'
  },
  shareProduct: {
    form: {
      title: 'Send this product to a friend',
      subTitle:
        'To send your friend information about your selection, complete the information below and click the Send button',
      shareLink: 'Or just send this link',
      submit: 'Send',
      defaultMessage:
        'Hi! I wanted to share with you this awesome product I just found at Emeralds.com',
      form: {
        recipient1: "Friend's Email address",
        recipient2: "Second friend's email (optional)",
        senderName: 'Your name',
        senderEmail: 'Your email address',
        message: 'Your message',
        shipToBilling: 'Send me a copy of this email'
      },
      rules: {
        senderName: {
          required: 'Please enter your name'
        },
        senderEmail: {
          required: 'Please enter your email address',
          email: 'Email is not correct'
        },
        recipient1: {
          required: 'Please enter recipient email address',
          email: 'Email is not correct'
        },
        recipient2: {
          email: 'Email is not correct'
        }
      }
    },
    swal: {
      title: 'Your email has been sent!',
      subTitle: 'Your message and link to this page was sent to your friend',
      ok: 'Ok'
    }
  },
  mainDetails,
  productOrigin,
  requestPreview,
  qualityValueHeart,
  question,
  callToAction,
  designStylesFilter,
  ourProcess,
  customDesigns,
  didYouKnow
}
