import stoneShapes from '../constants/stoneShapes'
import stonesOrigins from '../constants/stonesOrigins'
import filtersOrigins from '../constants/filtersOrigins'
import treatments from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import stoneColors from '~/locales/en/constants/stoneColors'
import bandStyle from '~/locales/en/constants/bandStyle'
import bandFinish from '~/locales/en/constants/bandFinish'

export default {
  prongsNotice:
    'The item displayed above includes a stone set with {0} prongs. Please note the actual item you receive may look different depending on your prong selection.',
  availability: {
    0: 'Available',
    1: 'Delisted',
    2: 'Call for Status',
    3: 'Sold',
    4: 'On Hold'
  },
  confirmation: {
    ringSize:
      'Please reconfirm your ring size is a size {ringSize} by clicking the confirm button below',
    inCart:
      'A design using the same stone is currently in your shopping cart. By continuing, the item in your cart will be replaced by this new design.',
    confirm: 'Confirm',
    cancel: 'Cancel'
  },
  addedToCart: 'Added to {cart}',
  cart: 'cart',
  viewFullDetails: 'View full product details',
  totalStonesWeight: {
    diamond: 'Approximate total diamonds weight',
    emerald: 'Approximate total emeralds weight',
    sapphire: 'Approximate total sapphires weight'
  },
  chain: '18" Chain Included',
  soldSorry:
    "We're sorry but this item is no longer available, please contact us if you have further questions.",
  soldDelivery: ' - Order now for delivery by ',
  soldDeliveryTooltip:
    "But don't worry, we're working to bring this popular {category} back in stock as quickly as possible. Order now for delivery by {date}",
  thisIsMto: 'This Item is Made To Order',
  onHold: 'On Hold',
  soldOut: 'Sold',
  outOfStock: 'Out of Stock',
  ring: 'Ring',
  earring: 'Earrings',
  pendant: 'Pendant',
  necklace: 'Necklace',
  preview: 'All',
  stone: 'Stone',
  setting: 'Setting',
  reports: 'Reports',
  available: 'Available',
  freeShipping: 'Free shipping',
  noHasslePolicy: '14 Day No Hassle Return Policy',
  noHasslePolicyText:
    'We offer a 14 day return window to review our emeralds, jewelry or your one-of-a-kind custom emerald jewelry. Should you wish to return it we will refund you the full price paid, unless we specifically notify you before the start of production of any possible restocking fees. Our items very rarely have restocking fees. If you have concerns or questions please contact us.',
  stonePrice: 'Stone Price',
  pairPrice: 'Stones Price',
  settingPrice: 'Setting Price',
  designId: 'Design ID',
  itemId: 'Item ID',
  stoneItemId: 'Emerald Item ID',
  settingItemId: 'Setting Item ID',
  totalPrice: 'Total price',
  calculatedPrice: 'Price Calculated During Next Step',
  calculatedPriceTitle: "Where's the Price?",
  calculatedPriceText:
    'Each piece of custom jewelry is uniquely crafted to match the stone you have selected. As such your final price can only be calculated once you have chosen a compatible stone and all available options which may include: Metal Type, Ring Size, Side Stones and/or Pave Diamond Weight. Simply continue by selecting this setting below. We will calculate the price automatically for you during the next step.',
  price: 'Price',
  toCartStone: 'Add loose stone to cart',
  toCartPair: 'Add loose stones to cart',
  toSettingStone: 'Create jewelry with this stone',
  toCartPairs: 'Add to cart',
  toSettingPairs: 'Add to earrings',
  inCart: 'This item currently in your',
  inCartStone: 'This stone currently in your',
  shoppingCart: 'Shopping cart',
  videoShot: 'Shot on iPhone XS',
  metal: 'Metal',
  vedic: 'Vedic Setting Style',
  vedicText:
    "This setting can accommodate a 'Vedic' style stone placement. Vedic settings are usually chosen for their ability to set the gemstone low, so it specifically touches the skin of the wearer.",
  vedicTextCTA:
    'If you would like the gemstone to touch the skin when set please check this box to confirm.',
  chooseVedic: 'Place gemstone touching skin',
  vedicLearnMore: 'Learn more about Emeralds and Vedic astrology',
  vedicNotSelected: 'Not Selected',
  vedicSelected: 'Gemstone Touching Skin',
  prongs: 'Prongs',
  size: 'Ring size',
  finish: 'Finish',
  ringWidth: 'Band width (mm)',
  sideStones: 'Stone Size Selection',
  sizingGuide: 'Sizing guide',
  addEngraving: 'Add engraving',
  paveStone: 'Pave diamonds',
  totalPaveWeight: 'Total Estimated {0} Wt: {1}',
  optional: 'Optional',
  characters: '{0}/15 characters maximum, engraved on the inside of the band',
  engravingPlaceholder: 'Optional engraving',
  productionTime: 'Production Time',
  productionTimePeriod: 'from {0} to {1} Days',
  totalMetalWeight: 'Total metal weight',
  grade: 'Grade',
  stoneWeight: 'Weight',
  weight: 'Single Stone Weight',
  stoneTypeName: stoneTypes,
  paveStones: {
    sapphire: 'Sapphire',
    emerald: 'Emerald',
    diamond: 'Diamond',
    ruby: 'Ruby'
  },
  ringDetails: {
    ringSize: 'Select ring size',
    chooseRingSize: 'Choose ring size',
    ringWidth: 'Select ring width',
    selectSize: 'Please select a ring size'
  },
  settingTypePicker: {
    title: 'What would you like to create with your emerald?',
    continue: 'Continue',
    ringsList: 'Rings',
    earringsList: 'Earrings',
    pendantsList: 'Pendants',
    necklacesList: 'Necklaces',
    necklacesPendantsList: 'Necklaces & Pendants'
  },
  share: {
    facebook: 'Share via Facebook',
    twitter: 'Share via Twitter',
    googlePlus: 'Share via Google plus',
    pinterest: 'Share via Pinterest',
    email: 'Share via Email',
    copyLink: 'Copy link',
    linkCopied: 'Link copied to clipboard',
    linkCopyError: 'Something going wrong. Please, copy link manually'
  },
  help: {
    name: 'Need Help?',
    call: 'Call us',
    email: 'Email us',
    chat: "Let's chat!"
  },
  product: 'Product',
  description: 'description',
  details: 'Details',
  overview: 'Summary',
  stoneDetails: 'Stone details',
  stoneShapesName: {
    ...stoneShapes
  },
  originName: {
    ...filtersOrigins
  },
  stonesOrigins,
  treatmentsName: {
    ...treatments
  },
  bandFinish,
  bandStyle,
  stoneColors,
  itemOverview: {
    itemId: 'Item ID:',
    weight: 'Weight:',
    totalWeight: 'Total weight:',
    price: 'Price:',
    totalPrice: 'Total price:',
    settingPrice: 'Setting price:',
    perCaratPrice: 'Per carat price:',
    metalType: 'Metal type:',
    centerStones: 'Center stone(s):',
    sideStones: 'Side stone(s):',
    paveStones: 'Total estimated pave diamonds weight:'
  },
  stoneParams: {
    undefined: {
      tooltip: ''
    },
    perCaratPrice: {
      tooltip: 'Learn about Per Carat Price',
      title: 'Per Carat Price',
      text: `<p>The "per carat price" is a jewelry trade term that explains how gemstone prices are calculated. Fine quality gemstones are priced by multiplying their weight by the "per carat" price (ex: 2.50ct. x $1000 per/ct = $2500 total cost).</p>
      <p>The greater the per carat price, the higher the inherent quality of the 4 C’s (cut, clarity, carat size, and color) in the gemstone.</p>
      <p>For example, a 1.00 ct emerald with very poor clarity could sell for $10 per/ct while a 1.00 ct emerald with excellent clarity could sell for $100 per/ct. even though they are the same size. If another 1.00 ct. emerald is priced at $50 per/ct. then one can assume the clarity is slightly included.</p>`
    },
    color: {
      tooltip: 'Learn about Color',
      title: 'Color',
      text: '<p>All items are filmed with light meant to replicate intense, direct-over-head sunlight. This is the gemological standard for evaluating the color of all gemstone. We use 5000-6000 Kelvin LED light sources to replicate direct-overhead-sunlight.</p>'
    },
    shape: {
      tooltip: 'Learn about Emerald Shapes',
      title: 'Shape',
      text1:
        'The chart below features common gemstone shapes. The most popular shapes for colored gemstones are round, cushion, emerald, radiant, and princess. If you would like more information regarding the characteristics of each shape, please call us to speak with a gemologist who will be happy to help you select your emerald.',
      text2: `To view a PDF of millimeter dimensions for each shape, please click <a
      href="https://image.thenaturalsapphirecompany.com/site-files/nec_stone_dimensions.pdf"
      target="_blank">here</a>`
    },
    dimensions: {
      tooltip: 'Learn about Emerald Dimensions'
    },
    clarity: {
      tooltip: 'Learn about Emerald Clarity',
      title: 'Clarity',
      text: `<p></p><p><strong>Eye-Clean</strong>: The emerald is eye-clean at 10'' inches from the eye, but not necessarily at magnification or very close eye inspection.</p>
      <p><strong>Very Slightly Included</strong>: Inclusions or small internal crystals are very slightly visible 12'' from the eye.</p>
      <p><strong>Slightly Included</strong>: Inclusions are visible 12'' from the eye, but are not serious in plain view.</p>
      <p><strong>Included</strong>: Inclusions are visible, they hinder the color light reflection to varying extents.</p>`
    },
    cut: {
      tooltip: 'Learn about Emerald Cut',
      title: 'Cut',
      text: 'Below are common terms and characteristics used to evaluate and classify the cut of a emerald gemstone.'
    },
    colorIntensity: {
      tooltip: 'Learn about Emerald Color Intensity',
      title: 'Color Intensity Chart',
      text: `<p>Color is generally considered to be the number one feature that determines the quality of an emerald. The degree of lightness or darkness of green found in the emerald is described as the color intensity. Natural emeralds have a wide variety of green tones, and below we discuss these classifications.</p>
      <p><strong>Light</strong> - This classification is reserved for an emerald that shows a majority of greenish-white light reflection or an almost pastel color intensity.</p>
      <p><strong>Medium</strong> - A medium color intensity is given to an emerald that has a 50% color saturation.</p>
      <p><strong>Intense</strong> - A green hue that has very little yellow or green undertones, color would be described as a near-pure green with 50-80% saturation, a very desirable and attractive color reserved for fine quality transparent emeralds.</p>
      <p><strong>Vivid</strong> - The finest combination of hue (pure green), tone (deep green) and saturation (80-100%). Vivid color grade is given to the finest quality emeralds with excellent transparency and reflective light return to the eye.</p>
      <p><strong>Dark</strong> - Dark saturation is normally reserved for an emerald that has little light escaping from the stone. The color expresses little to no transparency, the green color is muted, often with a blackish green color tone.</p>`
    },
    origin: {
      tooltip: 'Learn about Emerald Origin',
      title: 'Origin',
      text: `<p><strong>Colombia</strong> - Two large mining districts produce all Columbian emeralds: Chivor and Muzo. These emeralds are famed for their legendary history as the most coveted emerald origin. They are known for their bright, pure green color that can sometimes dip to a bluish-green.</p>
      <p><strong>Brazil</strong> - Stories of Colombian emeralds led explorers to Brazil and the first discovery of emeralds in the 16th century. Recent deposits from the Itabira/Nova Era belt have been shown to have lovely color and very good transparency.</p>
      <p><strong>Zambia</strong> - The discovery of Zambian emeralds only extends to the early 20th century, but the country has recently established a presence as producing increasing quantities of lovely emeralds with a slightly darker, bluish tone and great clarity.</p>
      <p><strong>Russia</strong> - Over recent years, Russian emerald mining and production has been revolutionized and these “Ural Mountain” gemstones are known for their lighter green hues with slightly more inclusions. Russia has announced heavily increased mining endeavors through 2025, so supply should only build.</p>
      <p><strong>Zimbabwe</strong> - Most Zimbabwe emeralds are referred to as “Sandawana” emeralds in reference to the famed mining region. These emeralds are known for having intense color, smaller size and included. Their deep, balanced, vivid green color has become legendary in the industry.</p>
      <p><strong>Other Sources</strong> - Emeralds from other regions of the world have also been establishing a presence for quality and production including Afghanistan and Pakistan, Australia, and India. The gemstones from Afghanistan and Pakistan have a vibrant saturation and bright green hue. Those from Australia have ebbed and flowed in production, but have been of good quality, and emeralds from India have been around since antiquity, but are known for having a lower quality.</p>`
    },
    treatments: {
      tooltip: 'Learn about Emerald Treatments',
      title: 'Treatments',
      text: `<p><strong>Standard</strong> - The vast majority of emeralds today are enhanced to improve their clarity. There are many different substances used for this purpose. Emeralds.com gemstones are enhanced with only industry accepted materials (colorless oils, wax or polymers/resins).</p>
      <p><strong>None, No Enhancement</strong> - These emeralds have not been enhanced in any way; they have only been cut and polished from the mined rough crystal. Unenhanced emeralds, especially those in the finer quality, are exceptionally rare and desirable.</p>`
    }
  },
  stoneType: 'Stone type:',
  quantity: 'Quantity:',
  color: 'Color:',
  clarity: 'Clarity:',
  shape: 'Shape:',
  cut: 'Cut:',
  totalWeight: 'Total weight:',
  colorIntensity: 'Color intensity:',
  detailsDimensions: 'Dimensions (MM):',
  chainLength: 'Chain length (Inches):',
  braceletLength: 'Length (Inches):',
  origin: 'Origin:',
  treatment: 'Treatment:',
  detailsItemId: 'Item ID:',
  detailsWeight: 'Weight:',
  detailsColor: 'Color:',
  detailsClarity: 'Clarity:',
  detailsShape: 'Shape:',
  detailsPerCaratPrice: 'Per carat price:',
  detailsCut: 'Cut:',
  starAppearance: 'Star appearance:',
  detailsColorIntensity: 'Color intensity:',
  detailsOrigin: 'Origin:',
  detailsTreatments: 'Treatments:',
  length: 'Length: {0}',
  width: 'Width: {0}',
  height: 'Height: {0}',
  createJewelry: 'Create jewelry with stone',
  startFromStone: 'Create Jewelry With This Stone',
  startFromPair: 'Create Jewelry With These Stones',
  startFromSetting: 'Create Jewelry With This Setting',
  continueWithStone: 'Add stone to setting',
  continueWithPair: 'Add pair to setting',
  notCompatibleStone: 'This emerald is not compatible',
  notCompatiblePair: 'This emeralds pair is not compatible',
  notCompatibleRingSetting: 'This ring setting is not compatible',
  notCompatibleNecklaceSetting: 'This necklace setting is not compatible',
  notCompatibleEarringSetting: 'This earrings setting is not compatible',
  notCompatiblePendantSetting: 'This pendant setting is not compatible',
  takeMeBackStone: 'Take me back to compatible emeralds',
  takeMeBackPair: 'Take me back to compatible emerald pairs',
  takeMeBackRingSetting: 'Take me back to compatible ring settings',
  takeMeBackNecklaceSetting: 'Take me back to compatible necklace settings',
  takeMeBackEarringSetting: 'Take me back to compatible earrings settings',
  takeMeBackPendantSetting: 'Take me back to compatible pendant settings'
}
