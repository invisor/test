export default {
  designId: 'Design ID',
  haveQuestion: 'Have a Question About This Product?',
  subTitle:
    "No problem, We're here to help! Simply complete this brief form and someone will get back to you as soon as possible. If you'd rather speak with someone right away, feel free to give us a call at +1-212-204-8581",
  price: 'Price:',
  totalPrice: 'Total price:',
  warningText1: "We're No Pressure Company.",
  warningText2: 'Our customers service reps do not work on commission.',
  warningText3: "We won't sell your contact information or hard-sell",
  submit: 'Submit question',
  itemId: 'Item ID:',
  weight: 'Weight:',
  origin: 'Origin:',
  color: 'Color:',
  shape: 'Shape:',
  metalType: 'Metal type:',
  centerStone: 'Center stone:',
  centerStoneWeight: 'Center stone weight:',
  centerStoneOrigin: 'Center stone origin:',
  centerStoneShape: 'Center stone shape:',
  metalWeight: 'Metal weight:',
  styleName: 'Style name:',
  stonesWeight: 'Stones weight:',
  swal: {
    title: 'Your request has been submitted',
    text: 'Thank you for your request. We will contact you as soon as possible',
    confirmButtonText: 'Ok'
  },
  form: {
    customerName: 'Name',
    phone: 'Phone',
    email: 'Email',
    question: 'Your question'
  },
  formErrors: {
    customerName: 'Please enter name',
    phone: 'Please enter phone',
    email: {
      required: 'Please enter email',
      valid: 'Please enter correct email address'
    }
  }
}
