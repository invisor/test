export default {
  title: 'Request a 3D rendering',
  submit: 'Submit request',
  alreadySent: {
    text1:
      'Our records indicate that you have already requested a 3D rendering of this customized jewelry selection.',
    text2:
      'If you have lost or misplaced your customized image or simply feel this message is being displayed in error, please contact us at <strong>+1-212-204-8581</strong> and we will gladly assist.',
    text3: 'Thank you,<br>The Natural Emerald Company',
    limitText1:
      'It appears you have reached the daily limit of three (3) requests for this service. We appreciate your understanding as our custom 3D rendered images are highly labor intensive to create',
    limitText2:
      'If you feel this message is being displayed in error, or you would still like to receive a custom 3D rendered image for this jewelry creation, please contact us at <strong>+1-212-204-8581</strong> and we will gladly assist.',
    limitText3: 'Thank you,<br>The Natural Emerald Company'
  },
  swal: {
    title: 'Your request has been submitted',
    text: 'Please allow up to 24 hours to receive a combined 3D rendering of your customized jewelry creation via email',
    confirmButtonText: 'Ok'
  },
  form: {
    customerName: 'Name',
    phone: 'Phone',
    email: 'Email',
    reEmail: 'Re-enter email address',
    forWhom: 'For whom',
    instagramName: 'Instagram',
    country: 'Country',
    occasion: 'Occasion',
    timeframe: 'Timeframe',
    teamMember: 'Team member',
    deliveryDate: 'Delivery date',
    comments: 'Comments'
  },
  formErrors: {
    customerName: 'Please enter name',
    phone: 'Please enter phone',
    email: {
      required: 'Please enter email',
      valid: 'Please enter correct email address'
    },
    country: 'Please enter country',
    occasion: 'Please enter occasion',
    timeframe: 'Please enter timeframe',
    deliveryDate: 'Please enter delivery date'
  },
  placeholders: {
    country: 'Please select',
    occasion: 'Please select',
    timeframe: 'Please select',
    deliveryDate: 'Please pick a date',
    comments: 'Please enter comment'
  },
  occasionsList: {
    engagement: 'Engagement',
    anniversary: 'Anniversary',
    giftForLovedOne: 'Gift For Loved One',
    birthday: 'Birthday',
    graduation: 'Graduation',
    personalPurchase: 'Personal Purchase',
    vedicAstrology: 'Vedic Astrology',
    weddingNeedRingBand: 'Wedding Need/Ring Band',
    other: 'Other'
  },
  timeframesList: {
    asSoonAsPossible: 'As soon as possible',
    inTheNextFewWeeks: 'In the next few weeks',
    inTheNextFewMonths: 'In the next few months',
    sometimeInTheNext6Months: 'Sometime in the next 6 months',
    '6Months': '6+ months',
    justBrowsing: 'Just browsing'
  },
  text1:
    'Complete the form below and our team of CAD designers will create a realistic rendering of your choosen custom creation. Please allow up to 24 hours to receive this via email.',
  text2:
    "Don't worry, we're a no pressure company. We don't hard-sell or ever sell our customer's information.",
  text3: 'Our sales team does not work on commission.',
  occasion: 'What is the occasion of your purchase?',
  timeframe: 'When do you expect to make a decision?',
  teamMember: 'Already working with a member of our team?',
  deliveryDate: 'Do you have delivery date which you need met?',
  deliveryDateNotice:
    '<strong>Note</strong>: We cannot guarantee delivery dates until the order is placed and the date is confirmed with our production team',
  prongs: 'Selected prong style',
  questions: 'Do you have any specific questions or requests?',
  note1:
    'Note: Please limit your request for this complimentary service to three (3) total combinations.',
  note2:
    'We appreciate your consideration as our custom made images are highly labor intensive.'
}
