export default {
  term: 'Term: {0}.',
  remove: 'Remove Protection',
  years: '{0} Year Protection',
  lifetime: 'Lifetime Protection'
}
