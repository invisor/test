export default {
  askDesigner: {
    submit: 'Submit',
    label: {
      name: 'Name',
      email: 'Email',
      phone: 'Phone',
      message: 'What question would you like us to answer?'
    },
    error: {
      required: 'Field is required',
      email: 'Email is not correct',
      reEmailRequired: 'Please enter the email again',
      reEmail: 'Email does not match the confirm email'
    }
  },
  askAbout: {
    submit: 'Submit',
    label: {
      name: 'Name',
      email: 'Email',
      phone: 'Phone',
      message: 'Your question'
    },
    error: {
      required: 'Field is required',
      email: 'Email is not correct'
    }
  }
}
