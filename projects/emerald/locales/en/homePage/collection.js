export default {
  ourCollection1: 'Our Emerald',
  ourCollection2: 'COLLECTION',
  bracelets: {
    title: 'Bracelets',
    text: 'Discover stunning statement and classic tennis bracelets with emeralds of all shapes and sizes.',
    button: 'Shop now'
  },

  earrings: {
    title: 'Earrings',
    text: 'From timeless studs to ultra-luxe chandelier, discover our variety of dazzling emerald earrings.',
    button: 'Shop now'
  },

  necklaces: {
    title: 'Necklaces & Pendants',
    text: 'Browse elegant and simple necklaces and pendants, perfect for any occasion.',
    button: 'Shop now'
  },

  pairs: {
    title: 'Emerald Pairs',
    text: 'These extra-rare paired emeralds are ready to complete any special piece of jewelry.',
    button: 'Shop now'
  },

  rings: {
    title: 'Emerald Rings',
    text: 'Explore our one-of-a-kind, artistically created rings that you’ll treasure for generations to come.',
    button: 'Shop now'
  },

  settings: {
    title: 'Settings',
    text: 'Browse our distinctive selection of settings for the perfect showcase of your emerald.',
    button: 'Shop now'
  },

  stones: {
    title: 'Loose Emeralds',
    text: 'From deep forest greens to lighter minty shades, find the perfect natural emerald to match your taste.',
    button: 'Shop now'
  },

  weddingBands: {
    title: 'Wedding Bands',
    text: 'Vintage to modern, classic to unique, find your perfect symbol of everlasting love.',
    button: 'Shop now'
  }
}
