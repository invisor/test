export default {
  title: 'Education About Emeralds',
  text: 'To truly appreciate and understand the beauty and wonder of an emerald, visit our education section where we explore the vast and fascinating world of emeralds!',
  readMore: 'Read more'
}
