export default {
  title1: 'Famous',
  title2: 'Emeralds',
  slide1: {
    title: 'Empress Farah of Iran',
    text: "The Empress Tiara, worn on many occasions by Empress Farah of Iran. The tiara holds seven large emeralds across its top varying from 65 carats to 10 carats. It was designed by Harry Winston for the occasion of Farah's marriage to Reza Shah Pahlevi in 1958.",
    button: 'Read our story'
  },
  slide2: {
    title: 'The Royal Cambridges',
    text: 'The storied past of the Royal Cambridge emeralds begins with winning a charity lottery in the early 1800s and featured 30-40 stunning cabochon emeralds making their way into fabulous European jewelry throughout the years.',
    button: 'Read our story'
  },
  slide3: {
    title: 'Elizabeth<br>Taylor',
    text: 'An avid jewelry enthusiast, Elizabeth Taylor collected many emerald pieces, some of the most significant and magnificent being the pieces designed by renowned jewelry house Bulgari including an emerald and diamond necklace and earrings.',
    button: 'Read our story'
  }
}
