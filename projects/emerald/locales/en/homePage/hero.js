export default {
  create: 'Create Your Perfect',
  ring: 'Emerald ring',
  description:
    'We have thousands of exquisite, natural, untreated emeralds to choose from, matched perfectly with our beautiful, custom designed, hand crafted settings.',
  ready: 'Ready to create?',
  chooseSetting: 'Choose a setting',
  or: 'or',
  chooseStone: 'Choose a emerald',
  learnMore: 'Learn more'
}
