import hero from './hero'
import collection from './collection'
import process from './process'
import education from './education'
import quote from './quote'
import ourHistory from './ourHistory'
import gemologist from './gemologist'
import famous from './famous'
import showroom from './showroom'
import packaging from './packaging'
import reviews from './reviews'
import photoGallery from './photoGallery'
import mining from './mining'
export default {
  collection,
  process,
  hero,
  education,
  quote,
  ourHistory,
  gemologist,
  famous,
  showroom,
  packaging,
  reviews,
  photoGallery,
  mining
}
