export default {
  title1: 'Mining',
  title2: 'Locations',
  slide1: {
    title: 'Colombia',
    text: 'Reveal the famed history of the centuries-old mining endeavors in emerald-rich Colombia.',
    button: 'Read more'
  },
  slide2: {
    title: 'Brazil',
    text: "Learn about the early discovery of emeralds in Brazil by Portuguese explorers, and why production didn't boom until the 1970s.",
    button: 'Read more'
  },
  slide3: {
    title: 'Zambia',
    text: 'Discover why Zambia is second only to Colombia in the production of emeralds.',
    button: 'Read more'
  }
}
