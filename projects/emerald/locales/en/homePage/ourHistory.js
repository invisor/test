export default {
  title: '<i>Our</i> STORY & HERITAGE',
  slide1: {
    subTitle: 'Our history',
    title: 'Over 80 Years of History',
    text: "Since 1939 we have specialized in the finest natural sapphires. Our roots go back three generations, and now we're extending this legacy into natural emeralds.",
    button: 'Read more'
  },
  slide2: {
    subTitle: 'Our history',
    title: "How We're Different",
    text: 'Providing quality, rarity, and value will always be the cornerstones of our business, but discover what else sets us apart in delivering the best jewelry purchase experiences.',
    button: 'Read more'
  }
}
