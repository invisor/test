export default {
  slide1: {
    subTitle: 'Our Packaging',
    title: 'Beauty is in the details',
    text: 'Our packaging, reports and documentation is refined, concise and reputable.  Our reputation is our greatest asset, rest assured we don’t cut corners, please review our extensive reporting and specialty packaging for all our emerald items.',
    tab1name: 'The Box',
    tab1text:
      'Our hand made zinc alloy ring boxes are made to impress as much as our handcrafted emerald rings and emerald jewelry; they weigh almost one pound each!',
    tab2name: 'Our Quality Promise',
    tab2text:
      'Have confidence that if you choose The Natural Emerald Company the items you purchase for yourself or your loved ones will be impressive.'
  },
  slide2: {
    subTitle: 'Our Packaging',
    title: 'Beauty is in the detail',
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur lacus nec mauris pellentesque, non varius ex mattis. Donec mauris ipsum.',
    tab1name: 'The Box',
    tab1text:
      'Praesent vitae erat in ligula suscipit condimentum. Nullam suscipit ipsum sed nibh ultrices lobortis.',
    tab2name: 'Our Quality Promise',
    tab2text:
      'In ac lacinia urna. Cras blandit, ante ac feugiat dictum, nunc velit pulvinar ipsum, id tristique tortor lorem non sem.'
  }
}
