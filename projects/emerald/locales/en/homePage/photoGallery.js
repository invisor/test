export default {
  subTitle: 'Consectetur',
  title: 'Visit us on Instagram',
  text: 'Our Instagram is a emerald lover’s dream where we showcase our current and past emerald jewelry and loose emeralds before they were placed in lucky homes!',
  button: 'Follow us on instagram'
}
