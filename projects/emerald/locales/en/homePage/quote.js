export default {
  text: 'Love is an emerald. Its brilliant light wards off dragons on this treacherous path.',
  author: '- Rumi'
}
