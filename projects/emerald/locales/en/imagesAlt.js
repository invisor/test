export default {
  heroRing:
    "Pigeon's blood vivid emerald and diamond ring 3.26ct certified by GIA",
  collectionRings:
    'Emerald engagement rings with pave set diamonds set in platinum',
  collectionEarrings:
    'Emerald earrings with diamonds set in a halo setting design in platinum',
  collectionWeddingBands:
    'Emerald and diamond wedding bands set in white gold and platinum',
  collectionNecklaces:
    '8.88ct Burmese emerald set in platinum with pave set diamonds',
  collectionBracelets:
    'Mozambique emerald bracelet with invisible set diamonds in 18k white gold',
  collectionStones: 'Emeralds from Burma and Mozambique',
  collectionPairs: 'Pair of Burmese emeralds untreated and certified',
  collectionSettings: 'Custom made settings for emerald rings',
  educationVideo: 'Learn more about Colombian and Zambiane emeralds',
  ourStory: 'The Natural Emerald Company History',
  miningLocations: 'Emeralds Mining Locations',
  famousEmeralds: 'Famous Emeralds',
  famousQuality: 'Fine Quality Emeralds',
  famousElizabeth: 'Elizabeth Taylor Emerald Jewelry',
  packaging:
    'Emerald rings in The Natural Emerald Company custom made packaging'
}
