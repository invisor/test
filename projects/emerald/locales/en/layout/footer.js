export default {
  subscription: {
    title: 'Stay up to date with our news',
    text: 'Subscribe to our newsletter for updates and special offers.',
    placeholder: 'Email address',
    button: 'Sign up'
  },
  bottom: {
    terms: 'Terms & Conditions',
    privacy: 'Privacy policy'
  },
  menu: {
    aboutUs: {
      title: 'About Us',
      item1: "How We're Different",
      item2: 'Our Workshop',
      item3: 'Our History',
      item4: 'Our Showroom',
      item5: 'Our Staff',
      item6: 'Our Technology',
      item7: 'Gem Laboratory',
      item8: 'Contact Us'
    },
    faq: {
      title: 'FAQ',
      item1: 'General Questions',
      item2: 'Shipping & Insurance',
      item3: 'Payment',
      item4: 'Ring Sizing',
      item5: 'Return Policy',
      item6: 'Our Location',
      item7: 'Security and Fraud Policy',
      item8: 'Privacy Policy'
    },
    education: {
      title: 'Education',
      item1: 'The Nature Of Emeralds',
      item2: 'Synthetic Emeralds',
      item3: 'Source & Quality',
      item4: 'Brazil Emerald Mines',
      item5: 'Emeralds in Magic/New Age',
      item6: 'Inclusions in Emeralds',
      item7: 'Modern Emerald Jewelry',
      item8: 'Caring for Emerald Jewelry'
    }
  }
}
