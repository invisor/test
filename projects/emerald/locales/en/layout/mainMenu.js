export default {
  gemologistRecommended: 'Gemologist Recommended',
  taxes: 'Taxes',
  about: 'About Us',
  afghanistan: 'Afghanistan',
  andAbove: 'and above',
  antique: 'Antique',
  antiquity: 'Antiquity',
  appraisal: 'Appraisal Services',
  asscher: 'Asscher',
  astrology: 'Astrology & Birthstones',
  blog: 'Blog',
  bracelets: 'Bracelets',
  brazil: 'Brazil',
  britishCrown: 'The British Crown Jewels',
  buyingTips: 'Emerald Buying Tips',
  cantFind: "Can't find what you're looking for?",
  carat: 'carat',
  chat: 'Chat with us',
  chemistry: 'Beryl Crystals & Chemistry',
  colombiaEmeraldMines: 'Colombia Emerald Mines',
  colombia: 'Colombia',
  colombiaHeader: 'Colombia',
  colors: 'Colors & Varieties',
  columbia: 'Colombia',
  commonEmeralds: 'Common Emerald Treatments',
  contactUs: 'Contact Us',
  create: 'Design Your Own',
  createEarring: 'Design Earring',
  createNecklace: 'Design Necklace',
  createPendant: ' Design Pendant',
  createRing: 'Design Ring',
  cushion: 'Cushion',
  designJewelry: 'Design your own jewelry with our',
  designOwn: 'Design Your Own',
  dianaLegacy: 'Legacy of Princess Diana',
  different: 'Why Choose Us?',
  diversity: 'Diversity in Modern Emerald Jewelry',
  earlyHistory: 'Early History & Techniques',
  earrings: 'Earrings',
  education: 'Education',
  emeraldEducation: 'Emerald Education',
  emeraldCut: 'Emerald Cut',
  essentials: 'Emerald Essentials',
  ethiopia: 'Ethiopia',
  famous: 'Famous Emeralds',
  famousStone: 'Famous Emerald',
  famousStones: 'Famous Emeralds',
  faq: 'FAQ',
  fourC: 'The 4Cs of Quality',
  fracture: 'Fracture Filling in Emeralds',
  fractureFilling: 'Fracture Filling',
  freeService: 'free design service',
  funFacts: 'Fun Emerald Facts',
  gemLabs: 'Gem Laboratories & Certifications',
  general: 'General Questions',
  grading: 'Grading, Sizing and More',
  halo: 'Halo',
  heart: 'Heart',
  heirlooms: 'Emeralds as Heirlooms',
  herilooms: 'Emeralds in Heirlooms',
  historyAndUsege: 'History & Usage of Emeralds',
  inLiterature: 'Emeralds in Literature',
  inPress: 'In The Press',
  inclusions: 'Inclusions in Emeralds',
  jewelry: 'Jewelry',
  joinOurTeam: 'Join Our Team',
  judging: 'Judging Emerald Quality',
  learnMore: 'Learn more about our FREE design process',
  learnMoreVideo: 'Learn more',
  location: 'Our Location',
  looseEmeralds: 'Loose Emeralds',
  madagascar: 'Madagascar',
  middleAges: 'The Middle Ages',
  mining: 'Emerald Mining',
  miningLocations: 'Mining Locations',
  modernHistory: 'Modern History',
  montanaMines: 'Montana Emerald Mines',
  more: 'More',
  nature: 'The Nature of Emeralds',
  necklaces: 'Necklaces',
  necklacesAndPendants: 'Necklaces & Pendants',
  nsc: 'NSC Charity Foundation',
  or: 'OR',
  origin: 'Origin',
  colombianEmeralds: 'Colombian Emeralds',
  colombianEmeraldRings: 'Colombian Emerald Rings',
  zambianEmeralds: 'Zambian Emeralds',
  zambianEmeraldRings: 'Zambian Emerald Rings',
  ourHistory: 'Our History',
  gemLab: 'Gem Laboratory',
  ourShowroom: 'Our Showroom',
  ourStaff: 'Our Staff',
  ourTechnology: 'Our Technology',
  ourWorkshop: 'Our Workshop',
  oval: 'Oval',
  pairsOfEmeralds: 'Emerald Pairs',
  pairsStar: 'Star Emerald Pairs',
  pairsCabochon: 'Emerald Cabochon Pairs',
  pave: 'Pave',
  payment: 'Payment',
  pendants: 'Pendants',
  privacy: 'Privacy Policy',
  quality: 'The 4Cs of Quality',
  radiant: 'Radiant',
  readyToShip: 'Ready to Ship',
  return: 'Return Policy',
  ringSizing: 'Ring Sizing',
  rings: 'Rings',
  round: 'Round',
  emeralds: 'Emeralds',
  emeraldsStar: 'Star Emeralds',
  emeraldCabochon: 'Emerald Cabochons',
  emeraldsJewelry: 'Emeralds Jewelry',
  emerald101: 'Emerald 101',
  emeraldCare: 'Emerald Jewelry Care',
  emeraldEngagementRings: 'Emerald Engagement Rings',
  starEmeraldRings: 'Star Emerald Rings',
  emeraldRings: 'Emerald Rings',
  russia: 'Russia',
  security: 'Security and Fraud Policy',
  setting: 'Start with a Setting',
  settings: 'Settings',
  shipping: 'Shipping & Insurance',
  shopByCarat: 'Shop by Carat Range',
  shopByDesign: 'Start Designing',
  shopByDesignStyle: 'Shop by Design Style',
  shopByOrigin: 'Shop by Origin',
  shopByPrice: 'Shop by Price Range',
  shopByShape: 'Shop by Shape',
  showroom: 'Showroom',
  sideStone: 'Side Stone',
  accentStones: 'Accent Stones',
  stud: 'Stud',
  solitaire: 'Solitaire',
  source: 'Source & Quality',
  southAmerica: 'South American Emerald Legends',
  startWithStone: 'Start with a Emerald',
  testimonials: 'Testimonials',
  threeStone: 'Three Stone',
  weddingSet: 'Wedding Set',
  throughAges: 'Emerald Jewelry Through the Ages',
  tips: 'Buying Tips',
  vedic: 'Vedic Astrology',
  watchVideo: 'Watch the video',
  emeraldWeddingBands: 'Emerald Wedding Bands',
  weddingBands: 'Wedding Bands',
  weddingBandsPlain: 'Plain Bands',
  whyUs: 'Why Choose Us?',
  workshop: 'Our Workshop',
  zambia: 'Zambia',
  mozambique: 'Mozambique',
  burmaMyanmar: 'Burma (Myanmar)',
  thailandSiam: 'Thailand (Siam)',
  ceylon: 'Ceylon',
  montana: 'Montana',
  nigeria: 'Nigeria',
  vietnam: 'Vietnam',
  tajikistan: 'Tajikistan',
  thailand: 'Thailand',
  tanzania: 'Tanzania',
  sriLankaCeylon: 'Sri Lanka (Ceylon)',
  cambodia: 'Cambodia',
  australia: 'Australia',
  kashmir: 'Kashmir',
  zimbabwe: 'Zimbabwe',
  menSRings: "Men's Rings"
}
