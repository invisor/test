export default {
  us: 'USA: +1-212-204-8581',
  other: {
    line1: 'For international customers, please contact us by email.',
    line2: 'We will get back to you during our regular business hours.'
  }
}
