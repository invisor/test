export default {
  topImage: {
    subTitle: 'Use Our FREE Design Service to',
    title: 'Create Unique Jewelry',
    message:
      'Thousands of exquisite, natural, emeralds to choose from, matched to your unique specifications with our beautiful, custom-designed, hand-crafted settings.',
    button: 'Get Started'
  },
  firstBlock: {
    title: 'Your Tailor-Made Jewelry Starts With A {link}',
    settings: 'Setting',
    message:
      'Start by letting us know what type of jewelry you would like to create. Choose from thousands of unique hand-crafted settings in several different metals.'
  },
  secondBlock: {
    title: 'Next, Choose From Our 100% Natural {link}',
    stones: 'Emeralds',
    message:
      'Adorn your personalized jewelry with an emerald or emerald pair. With thousands of gemstones available, we’re sure you’ll find your perfect match.'
  },
  thirdBlock: {
    title: 'Preview Your Creation And Place Your Order',
    message:
      'Once you’re finished creating the perfect piece of jewelry, you’ll be able to request a free preview before placing your order. Our friendly team will work one-on-one with you to guarantee your creation comes to life beautifully.'
  },
  fourthBlock: {
    title: 'Exceptional Packaging For The Perfect Gift',
    message:
      'Each item is shipped in a beautiful custom jewelry box along with certifications and reports verifying the quality and authenticity.'
  },
  fifthBlock: {
    title: 'Ready To Design Your Own Beautiful Emerald Jewelry?',
    buttonStones: 'Choose a emerald',
    buttonPairs: 'Choose a pair of emeralds'
  }
}
