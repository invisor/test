import metalTypes from '../constants/metalTypes'
import stoneShapes from '../constants/stoneShapes'
import treatments from '../constants/treatments'
import origins from '../constants/filtersOrigins'
import stoneTypes from '../constants/stoneTypes'

export default {
  stones: {
    price: 'Price',
    itemId: 'Item ID',
    carat: 'Carat',
    type: 'Type'
  },
  table: {
    actualPhoto: 'Actual photo',
    shape: 'Shape',
    metalName: 'Metal name',
    treatment: 'Treatment',
    colorIntensity: 'Color Intensity',
    clarity: 'Clarity',
    carat: 'Carat',
    origin: 'Origin',
    cut: 'Cut',
    price: 'Price',
    remove: 'Remove',
    wishlist: 'Wishlist',
    details: 'Details'
  },
  item: {
    addToSetting: 'Add {0} to setting',
    addToCart: 'Add {0} to cart',
    cartLink: 'Shopping cart',
    inCart: 'This item currently in your {cartLink}'
  },
  metalTypes,
  stoneShapes,
  treatments,
  origins,
  stoneTypes,
  empty: 'Your compare list is empty!',
  dragAndDrop: 'Drag and drop to compare',
  totalCtWeight: 'Total weight:'
}
