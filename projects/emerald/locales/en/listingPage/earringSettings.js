import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No earring settings found | One earring setting found | {count} earring settings found'
  },
  prefilteredItems:
    'Displaying <strong>ONLY</strong> earrings settings that are compatible with the emeralds pair you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> earrings settings which means some may not be compatible with your emeralds pair.',
  showAllItems: 'No thanks, show me all earrings settings.',
  hideNonCompatibleItems:
    'Nevermind, take me back to compatible earrings settings.'
}
