import filtersShapes from '../constants/filtersShapes'
import stoneShapes from '../constants/stoneShapes'
import filtersOrigins from '../constants/filtersOrigins'
import metalTypesFilter from '../constants/metalTypesFilter'
import treatmentsFilter from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import stylesFilter from '../constants/designStylesFilter'
import cuttingStylesFilter from '../constants/cuttingStyles'
import labColorsFilter from '../constants/labColors'
import labTypesFilter from '../constants/labTypes'
import stoneClarity from '../constants/stoneClarity'
import intensityFilter from '../constants/stoneColorIntensity'

export default {
  previewSwitcher: {
    availability: 'Availability',
    all: 'All',
    ready: 'Ready to ship',
    notReady: '2-4 weeks'
  },
  cuttingStylesFilter,
  labColorsFilter,
  labTypesFilter,
  stoneTypesFilter: stoneTypes,
  clarityFilter: stoneClarity,
  jewelryStyles: stylesFilter,
  centerStoneShapes: 'Center Stone Shape',
  sideStoneShapes: 'Side Stone Shape',
  cuttingStyles: 'Cutting style',
  labColors: 'Lab color',
  labTypes: 'Lab type',
  caratRange: 'Carat',
  caratSum: 'Carat',
  clearAll: 'Clear all',
  close: 'Close',
  dimensions: 'Dimensions',
  lengthRange: 'Length (mm)',
  widthRange: 'Width (mm)',
  bandWidth: 'Width (mm)',
  bandWidthRange: 'Width',
  filter: 'Filter',
  sort: 'Sort',
  filters: 'Filters',
  filterBy: 'Filter by',
  lengthSum: 'Length',
  priceRange: 'Price',
  priceSum: 'Price',
  clarity: 'Clarity',
  intensity: 'Color intensity',
  stoneOrigins: 'Origin',
  stoneShapes: 'Stone shape',
  stonesShapes: 'Shapes',
  metalTypes: 'Metal type',
  designStyles: 'Design styles',
  widthSum: 'Width',
  selectCenterStoneShapes: 'Select Center Stone',
  selectSideStoneShapes: 'Select Side Stone',
  centerStone: 'Center stone',
  sideStone: 'Side stone',
  treatments: 'Treatment',
  stoneTypes: 'Stone types',
  styles: 'Design style',
  intensityFilter,
  designStylesFilter: stylesFilter,
  stylesFilter,
  bandsTypesFilter: stoneTypes,
  treatmentsFilter,
  metalTypesFilter,
  priceRangeFilter: {
    andAbove: ' and above'
  },
  stoneShapesFilter: {
    ...stoneShapes
  },
  centerStoneShapesFilter: {
    ...stoneShapes
  },
  sideStoneShapesFilter: {
    ...filtersShapes
  },
  stoneOriginsFilter: {
    ...filtersOrigins
  },
  summary: {
    andAbove: ' and above',
    caratSum: 'Carat',
    lengthSum: 'Length',
    widthSum: 'Width',
    clearAll: 'Clear all'
  },
  stoneTypeName: stoneTypes
}
