import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No necklace or pendant settings found | One necklace or pendant setting found | {count} necklace and pendant settings found'
  },
  prefilteredItems:
    'Displaying <strong>ONLY</strong> necklace settings that are compatible with the emerald you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> necklace settings which means some may not be compatible with your emerald.',
  showAllItems: 'No thanks, show me all necklace settings.',
  hideNonCompatibleItems:
    'Nevermind, take me back to compatible necklace settings.'
}
