import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No necklaces or pendants found | One piece of jewelry found | {count} necklaces and pendants found'
  }
}
