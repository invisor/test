import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No pendant settings found | One pendant setting found | {count} pendant settings found'
  },
  prefilteredItems:
    'Displaying <strong>ONLY</strong> pendant settings that are compatible with the emerald you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> pendant settings which means some may not be compatible with your emerald.',
  showAllItems: 'No thanks, show me all pendant settings.',
  hideNonCompatibleItems:
    'Nevermind, take me back to compatible pendant settings.'
}
