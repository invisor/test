import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No ring settings found | One ring setting found | {count} ring settings found'
  },
  ringSizePlaceholder: 'Select ring size',
  ringSizeError: 'Please select a ring size',
  prefilteredItems:
    'Displaying <strong>ONLY</strong> ring settings that are compatible with the emerald you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> ring settings which means some may not be compatible with your emerald.',
  showAllItems: 'No thanks, show me all ring settings.',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible ring settings.'
}
