import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    itemsFound:
      'No emerald pairs found | One emerald pair found | {count} emerald pairs found',
    itemsStarFound:
      'No star emerald pairs found | One star emerald pair found | {count} star emerald pairs found',
    itemsCabochonFound:
      'No emerald cabochon pairs found | One emerald cabochon pair found | {count} emerald cabochon pairs found'
  },
  pair: 'Pair',
  prefilteredItems:
    'Displaying <strong>ONLY</strong> emerald pairs that are compatible with the setting you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> emerald pairs which means some may not be compatible with your setting.',
  showAllItems: 'No thanks, show me all emerald pairs.',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible emerald pairs.'
}
