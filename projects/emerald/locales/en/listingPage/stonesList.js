import stonesOrigins from '../constants/stonesOrigins'
import metalTypes from '../constants/metalTypes'
import treatments from '../constants/treatments'
import stoneCut from '../constants/stoneCut'
import stoneClarity from '../constants/stoneClarity'
import stoneColorIntensity from '../constants/stoneColorIntensity'
import bandStyle from '../constants/bandStyle'
import stoneShapes from '../constants/stoneShapes'

export default {
  sort: {
    sortBy: 'Sort by',
    itemsFound:
      'No emeralds found | One emerald found | {count} emeralds found',
    itemsStarFound:
      'No star emeralds found | One star emerald found | {count} star emeralds found',
    itemsCabochonFound:
      'No cabochon emeralds found | One cabochon emerald found | {count} cabochon emeralds found',
    other: {
      featuredFirst: 'Featured First',
      newFirst: 'New First'
    },
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  localSort: {
    sortBy: 'Sort by',
    placeholder: 'Select',
    itemsFound: 'emeralds pairs found',
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  prefilteredItems:
    'Displaying <strong>ONLY</strong> emeralds that are compatible with the setting you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> emeralds which means some may not be compatible with your setting.',
  showAllItems: 'No thanks, show me all emeralds.',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible emeralds.',
  stoneShapes,
  stonesOrigins,
  metalTypes,
  treatments,
  stoneCut,
  stoneClarity,
  stoneColorIntensity,
  bandStyle
}
