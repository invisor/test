import metalTypes from '../constants/metalTypes'

export default {
  ringSizePlaceholder: 'Select ring size',
  ringSizeError: 'Please select a ring size',
  empty: "You currently don't have any favorites saved",
  stonesList: {
    addToCart: 'Add stone to cart',
    addToSettings: 'Add to setting'
  },
  stonePairsList: {
    addToCart: 'Add pair to cart',
    addToSettings: 'Add to setting'
  },
  lists: {
    stonesList: 'Stones',
    stonePairsList: 'Stones Pairs',
    braceletsList: 'Bracelets',
    earringsList: 'Earrings',
    necklacesPendantsList: 'Necklaces and Pendants',
    ringsList: 'Rings',
    weddingBands: 'Wedding bands',
    weddingBandsPlain: 'Plain wedding bands',
    ringSettings: 'Ring settings',
    earringSettings: 'Earring settings',
    pendantSettings: 'Pendant settings',
    necklaceSettings: 'Necklace settings',
    custom: 'Custom designs'
  },
  item: {
    itemId: 'Item ID',
    weight: '{0} Ct.',
    pair: 'Pair',
    centerStone: 'Center Stone(s)',
    totalWeight: '{0} Total Ct. Weight',
    itemToChart: 'Add {0} to cart',
    itemToJewelry: 'Add {0} to jewelry',
    stoneToChart: 'Add {0} to cart',
    createJewelry: 'Add to setting',
    cartLink: 'Shopping cart',
    inCart: 'This item currently in your {cartLink}',
    toReview: 'Review & Add to Cart',
    reviewBand: 'Review & Add to Cart',
    reviewSetting: 'Review & Add to Cart',
    addToSetting: 'Add {0} to setting',
    addToCart: 'Add {0} to cart',
    addToReview: 'Review & add to cart'
  },
  metalTypes
}
