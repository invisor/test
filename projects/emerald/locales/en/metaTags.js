export default {
  settings: {
    description: {
      default:
        'Custom made emerald {fullType} settings. We have hundreds of design styles from three stone {type}, pave diamonds, and solitaire to name a few. Our emerald {type} settings can be created in white and yellow gold, platinum, and palladium.',
      earringSettings:
        'From simple to elaborate earring settings, we offer it all! Choose among a wide variety of earring designs to fit your emerald pair.',
      necklacesSettings:
        "Choose one of our emerald necklace settings to accentuate any ensemble. With our simple or elaborate designs, you'll find everything you're looking for!",
      pendantsSettings:
        'Find a pendant setting that fits your emerald through our collection. Each of our pendant designs can be created in yellow, white, rose gold and platinum.',
      mensRingSettings:
        "With dozens of bold ring settings to choose from - specifically designed for the modern man - we'e confident you'll find the perfect men's emerald ring setting for any occasion."
    }
  }
}
