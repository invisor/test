import { companyName } from '~~/utils/definitions/defaults.js'

export default {
  index: {
    seo: {
      title: `${
        companyName[process.env.siteName]
      } - Emerald Rings & Jewelry Since 1939`,
      description:
        'We are the authority in emerald engagement rings, natural emeralds, and emerald jewelry, since 1939. We have the largest collection of emeralds found online.'
    },
    navPageTitle: 'Home'
  },
  'custom-design-emerald-rings-jewelry': {
    seo: {
      title: 'Design Custom Emerald Engagement Rings & Jewelry Online',
      description:
        'Looking for something special and unique? Let us help create the perfect custom Emerald Engagement Ring, Earrings, Necklace, or Pendant for you or a loved one.'
    },
    navPageTitle: 'Learn More'
  },
  'design-your-own-setting-mens-ring-settings': {
    seo: {
      title: "Custom Emerald Men's Ring Settings",
      description:
        "With dozens of bold ring settings to choose from - specifically designed for the modern man - we'e confident you'll find the perfect men's emerald ring setting for any occasion."
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-engagement-ring-settings': {
    seo: {
      title: 'Custom Emerald Engagement Ring Settings',
      description:
        'Custom made emerald engagement ring settings. We have hundreds of design styles from three stone ring, pave diamonds, and solitaire to name a few. Our emerald ring settings can be created in white and yellow gold, platinum, and palladium.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-earring-settings': {
    seo: {
      title: 'Custom Emerald Earring Settings',
      description:
        'From simple to elaborate earring settings, we offer it all! Choose among a wide variety of earring designs to fit your emerald pair.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-necklace-pendant-settings': {
    seo: {
      title: 'Custom Emerald Necklace & Pendant Settings',
      description:
        "Choose one of our emerald necklace or pendant settings to accentuate any ensemble. With our simple or elaborate designs, you'll find everything you're looking for!"
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-earring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-emerald-necklace-pendant-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-emerald-engagement-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-mens-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'emerald-jewelry-id': {
    seo: {
      title: 'Emerald {type} - {shape} {carat} Ct. - {metal} #{id}',
      description:
        'This beautiful {carat} Ct. {shape} Emerald {type} (#{id}) set in {metal} is the perfect piece of emerald jewelry for that someone special in your life.'
    },
    navPageTitle: 'Emerald {type} {metal}'
  },
  emeralds: {
    seo: {
      title: 'Loose Emeralds',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Loose Emerald Gemstones',
    navPageDescr:
      'Our emerald collection from around the world has taken generations to acquire. Each emerald is examined by our certified gemologists to guarantee authenticity, durability, and disclosure of any treatments.'
  },
  'emeralds-colombian-emeralds': {
    seo: {
      title: 'Colombian emeralds',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Colombian emeralds',
    navPageDescr:
      'Colombian emeralds are the most famous source of emeralds the world over. Emeralds from Colombia have a color like no other gemstone, the purity and vibrancy is fascinating to see in person.'
  },
  'emeralds-zambian-emeralds': {
    seo: {
      title: 'Zambian Emeralds',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Zambian Emeralds',
    navPageDescr:
      'Zambian emeralds have become one of the great gemological discoveries of modern times. No place on earth has ever been able to offer such consistent quality. Zambian emeralds are simply incredible.'
  },
  'emerald-jewelry-colombian-emerald-rings': {
    seo: {
      title: 'Colombian Emerald Rings',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Colombian Emerald Rings',
    navPageDescr:
      'Colombian emerald are the most famous source of emeralds the world over. Colombian emerald rings have a color like no other gemstone, the purity and vibrancy is fascinating to see in person.'
  },
  'emerald-jewelry-zambian-emerald-rings': {
    seo: {
      title: 'Zambian Emerald Rings',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Zambian Emerald Rings',
    navPageDescr:
      'Often described as the best quality deep-green color of any emerald on earth. Emeralds from Zambia are in a class of their own and continue to grow in popularity the world over.  Presenting Zambian emerald rings.'
  },
  'emeralds-emerald-cabochon': {
    seo: {
      title: 'Cabochon Emeralds',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Cabochon Emeralds',
    navPageDescr:
      'The cabochon emerald is one of the oldest and most coveted gemstone used since ancient times. From the Egyptians to the royal jewels of kings and queens this simplistic cut of a cabochon emerald remains a beloved cut of the rare emerald gemstone. Learn more <a href="/education/all-about-emerald-cabochons/">here</a>.'
  },
  'emeralds-gemologist-recommended': {
    seo: {
      title: 'Gemologist Recommended',
      description:
        'We offer a large variety of beautiful, natural emeralds. Between different hues and tones, and all type of shapes you will be sure to find your perfect emerald.'
    },
    navPageTitle: 'Gemologist Recommended',
    navPageDescr:
      'Our staff gemologists see thousands of emeralds each year and can become very picky in what emeralds speak to them. In this section we’ve asked them to suggest some of their personal favorites.'
  },
  'emeralds-id': {
    seo: {
      title: 'Loose Emerald - {shape} {carat} Ct. - #{id}',
      description:
        'Choose this Natural {carat} Ct. {shape} Emerald from {origin} (#{id}) ready to be set to create the perfect piece of jewelry for you or a loved one.'
    },
    navPageTitle: 'Emerald Gemstone'
  },
  'emeralds-matched-pairs-of-emeralds': {
    seo: {
      title: 'Emerald Pairs',
      description:
        'We offer a large variety of beautiful emerald pairs. Each emerald pair has been well-matched in shape, size, hue, and tone by our GIA certified gemologists.'
    },
    navPageTitle: 'Loose Emerald Pairs',
    navPageDescr:
      'Matching colored stones can be exceedingly difficult with the way their color varies. Despite only being one color, emeralds can range from a yellowish green associated with Colombian emeralds to a bluish green.'
  },
  'emeralds-matched-pairs-of-emeralds-cabochon': {
    seo: {
      title: 'Emerald Cabochon Pairs',
      description:
        'Pairs of cabochon emeralds show the pure green color of the emerald crystal.  Emerald cabochon pairs are not easy to match in clarity and tone of color, our pairs are carefully selected to become partners in many beautiful earrings and necklaces.  To Learn more about emerald cabochon pairs click <a href="/education/all-about-emerald-cabochons/">here</a>.'
    },
    navPageTitle: 'Emerald Cabochon Pairs',
    navPageDescr:
      'Pairs of cabochon emeralds are unusual and difficult to match, below is a selection of options showcasing this beautiful cut seen in a natural emerald. Learn more about cabochon emeralds <a href="/education/all-about-emerald-cabochons/">here</a>.'
  },
  'emeralds-matched-pairs-of-emeralds-id': {
    seo: {
      title: 'Emerald Pair - {shape} {carat} Ct. - #{id}',
      description:
        'Choose this Natural {carat} Ct. {shape} Emerald Pair from {origin} (#{id}), ready to be set to create the perfect piece of jewelry for you or a loved one.'
    },
    navPageTitle: 'Emerald Pair'
  },
  'emerald-jewelry-engagement-emerald-rings': {
    seo: {
      title: 'Emerald Engagement Rings',
      description:
        'Find the perfect engagement ring for your loved one. We offer expertly crafted, handmade emerald engagement rings, or design your own unique emerald ring!'
    },
    navPageTitle: 'Emerald Engagement Rings',
    navPageDescr:
      'We offer fine quality emeralds from Colombia, Zambia, Afghanistan, Brazil, Ethiopia and Madagascar each special and unique. We specialize in custom order emerald engagement rings for the discerning individuals.'
  },
  'emerald-jewelry-emerald-rings': {
    seo: {
      title: 'Emerald Rings',
      description:
        'Find the perfect emerald ring for your loved one. We offer expertly crafted, handmade emerald rings, or design your own unique emerald ring!'
    },
    navPageTitle: 'Emerald Rings',
    navPageDescr:
      'We offer emeralds from Colombia, Zambia, Afghanistan, Brazil, Ethiopia and Madagascar each special and unique. Whether it is for an engagement ring, wedding ring, or any other occasion, we specialize in emeralds.'
  },
  'emerald-jewelry-mens-emerald-rings': {
    seo: {
      title: "Men's Emerald Rings",
      description:
        'Find the perfect emerald ring for your loved one. We offer expertly crafted, handmade emerald rings, or design your own unique emerald ring!'
    },
    navPageTitle: "Men's Emerald Rings",
    navPageDescr:
      "Browse hundreds of men's emerald rings using emeralds from Colombia, Zambia and Afghanistan. We specialize in emerald rings for Vedic astrology."
  },
  'emerald-wedding-rings-bands': {
    seo: {
      title: 'Emerald Wedding Bands',
      description:
        'Explore our expertly designed and handmade emerald and diamond wedding bands and stackable wedding rings with emeralds and diamonds in all precious metal types.'
    },
    navPageTitle: 'Emerald and Diamond Wedding Bands',
    navPageDescr:
      'We offer emerald wedding bands set with diamonds in different shapes and sizes in a range of settings styles. This includes emerald eternity bands and stackable in a selection of precious metals such as platinum.'
  },
  'emerald-wedding-rings-bands-id': {
    seo: {
      title: '{metal} Emerald Band #{id}{metalTypeCode}',
      description:
        'Make this exquisite {metal} Emerald Band (#{id}{metalTypeCode}) into the perfect wedding band to match your one-of-a-kind emerald engagement ring.'
    },
    navPageTitle: '{metal} Emerald Band'
  },
  'wedding-bands-without-gemstone': {
    seo: {
      title: 'Plain Wedding Bands',
      description:
        'Plain wedding bands and rings are classic designs and anything but simple. From textured or two tone metal, to polished or matte design options, we have it all!'
    },
    navPageTitle: 'Plain Wedding Bands',
    navPageDescr:
      'We offer a large selection of handmade wedding bands from classic and simple; to textured and two tone metal designs. We offer our plain metal bands in all precious metal types.'
  },
  'wedding-bands-without-gemstone-id': {
    seo: {
      title: '{metal} Plain Band #{id}{metalTypeCode}',
      description:
        'Make this exquisite {metal} Plain Band (#{id}{metalTypeCode}) into the perfect wedding band to match your one-of-a-kind emerald engagement ring.'
    },
    navPageTitle: '{metal} Plain Band'
  },
  'design-your-own': {
    seo: {
      title: 'Create Your Custom Emerald Ring in Our Simple Three-Step Process',
      description:
        'Get started on creating your custom emerald ring in our simple three-step process, designed to build you your perfect emerald ring in the exact right setting.'
    },
    navPageTitle: 'Design Your Own Ring'
  },
  'design-your-own-stones-single': {
    seo: {
      title:
        'Choose An Emerald to Create Your Perfect Piece of Emerald Jewelry',
      description:
        "With the world's largest collection of natural emeralds, you will surely be able to find the perfect emerald to add to you or your loved one's collection."
    },
    navPageTitle: 'Loose Emeralds'
  },
  'design-your-own-stones-pair': {
    seo: {
      title: 'Choose An Emerald Pair to Create Your Perfect Emerald Jewelry',
      description:
        "Search through our handpicked emerald pairs to find the perfect jewelry set for you. You'll see a wide variety in hue, tone, color and shape. Let's get started!"
    },
    navPageTitle: 'Emerald Pairs'
  },
  'design-your-own-stones-single-filterName-filterValue': {
    seo: {
      title:
        'Choose An Emerald to Create Your Perfect Piece of Emerald Jewelry',
      description:
        "With the world's largest collection of natural emeralds, you will surely be able to find the perfect emerald to add to you or your loved one's collection."
    },
    navPageTitle: 'Loose Emeralds'
  },
  'design-your-own-stones-pair-filterName-filterValue': {
    seo: {
      title: 'Emerald Pairs',
      description:
        "Search through our handpicked emerald pairs to find the perfect jewelry set for you. You'll see a wide variety in hue, tone, color and shape. Let's get started!"
    },
    navPageTitle: 'Emerald Pairs'
  },
  'design-your-own-review-id': {
    seo: {
      title: '{stoneType} {category} {weight} {metalName}',
      description:
        'Looking for something special? Let us help you to create the perfect custom Emerald {fullType} as you finalize your emerald gemstone and setting options.'
    },
    navPageTitle: 'Finalize Your {type}'
  },
  'emerald-jewelry-emerald-earrings': {
    seo: {
      title: 'Emerald Earrings',
      description:
        'With hundreds of designs in stock, find simple emerald studs to red-carpet-ready chandelier emerald earrings. Browse today, and find the perfect fit for you!'
    },
    navPageTitle: 'Emerald Earrings',
    navPageDescr:
      'Emerald earrings are vivacious and eye-catching. We have styles including emerald studs, diamond halos, drop earrings, and more. Our emerald earring pairs are expertly matched from color to cut and clarity.'
  },
  'emerald-jewelry-emerald-necklaces-pendants': {
    seo: {
      title: 'Emerald Necklaces & Pendants',
      description:
        'Search our wide variety of emerald necklaces & pendants including classic styles & modern designs. We offer one-of-a-kind necklaces for everyone, including you!'
    },
    navPageTitle: 'Emerald Necklaces and Pendants',
    navPageDescr:
      'We offer emerald necklaces and pendants in many design styles from simple, bezel-set pendants to a halo of diamonds in a wide selection of precious metals.'
  },
  'emerald-jewelry-emerald-bracelets': {
    seo: {
      title: 'Emerald Bracelets',
      description:
        'Find a timeless look of elegance with our emerald bracelets. With countless designs and color combinations, you are sure to find the bracelet of your dreams.'
    },
    navPageTitle: 'Emerald Bracelets',
    navPageDescr:
      'We offer perfectly matched emeralds in our emerald bracelets set in a variety of precious metals including platinum, white gold, yellow gold, and even rose gold. Emerald and diamond bracelets are our specialty.'
  },
  cart: {
    seo: {
      title: 'Cart',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-index': {
    seo: {
      title: 'Cart',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-order-summary': {
    seo: {
      title: 'Order Summary',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Order summary'
  },
  'cart-index-secure-payment': {
    seo: {
      title: 'Secure Payment',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-index-payment-order': {
    seo: {
      title: 'Payment Order',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  account: {
    seo: {
      title: 'Account',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'My Account'
  },
  'account-notifications': {
    seo: {
      title: 'Email Notifications',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Notifications'
  },
  'account-recommendations': {
    seo: {
      title: 'Recommended for You',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Recommended for You'
  },
  'account-personal-details': {
    seo: {
      title: 'Personal & Login Details',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Personal & Login Details'
  },
  'account-preview-request-submissions': {
    seo: {
      title: 'Preview Request Submissions',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Preview Request Submissions'
  },
  'account-address-book': {
    seo: {
      title: 'Address book',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Address book'
  },
  'account-orders': {
    seo: {
      title: 'Review Your Order History',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Orders'
  },
  'account-change-password': {
    seo: {
      title: 'Change Your Password',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Change Your Password'
  },
  'account-reset-password': {
    seo: {
      title: 'Reset Your Password',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Reset Password'
  },
  'account-reset-password-token': {
    seo: {
      title: 'Reset Your Password',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Reset Password'
  },
  wishlist: {
    seo: {
      title: 'Favorite Emeralds',
      description:
        'Keep track of your favorite loose emeralds, settings, emerald pairs, and finished emerald jewelry pieces in one place. Compare them all before choosing the one!'
    },
    navPageTitle: 'Favorites'
  },
  'compare-list': {
    seo: {
      title: 'Compare list',
      description:
        'The authority in emerald engagement rings, natural emeralds and emerald jewelry since 1939. We have the largest collection of emeralds you will find online.'
    },
    navPageTitle: 'Compare list'
  },
  'contact-us': {
    seo: {
      title: 'Contacts',
      description:
        'Our team of emerald and jewelry experts is available via live chat, phone, and email. Contact us now for personal assistance.'
    },
    navPageTitle: 'Contact The Natural Emerald Company'
  },
  showroom: {
    seo: {
      title: 'Visit the Natural Emerald Company showroom',
      description:
        'Visit the Natural Emerald Company showroom at our Manhattan office in New York, NY to find your perfect emerald or emerald jewelry.'
    },
    navPageTitle: 'Visit the Natural Emerald Company showroom'
  },
  'showroom-preview': {
    seo: {
      title: 'Visit the Natural Emerald Company showroom',
      description:
        'Visit the Natural Emerald Company showroom at our Manhattan office in New York, NY to find your perfect emerald or emerald jewelry.'
    },
    navPageTitle: 'Visit the Natural Emerald Company showroom'
  },
  'showroom-preview-success': {
    seo: {
      title: 'Visit the Natural Emerald Company showroom',
      description:
        'Visit the Natural Emerald Company showroom at our Manhattan office in New York, NY to find your perfect emerald or emerald jewelry.'
    },
    navPageTitle: 'Visit the Natural Emerald Company showroom'
  },
  'our-staff': {
    seo: {
      title: 'Meet our the Natural Emerald Company teams',
      description:
        'Learn more about the talented professionals that make everything happen at The Natural Emerald Company.'
    },
    navPageTitle: 'Our Staff'
  },
  'michael-arnstein': {
    seo: {
      title: 'Michael Arnstein',
      description:
        'Learn more about the talented professionals that make everything happen at The Natural Emerald Company.'
    },
    navPageTitle: 'Michael Arnstein',
    navPageDescr: 'President - The Natural Emerald Company'
  },
  auth: {
    seo: {
      title: 'Create Account',
      description:
        'Create an account to track your order and save items of interest to your favorites for later purchase at emeralds.com'
    },
    navPageTitle: 'The Natural Emerald Company - Create Account'
  },
  'education-index-category-slug': {
    seo: {
      title: 'Education',
      description: ''
    },
    navPageTitle: 'The Natural Emerald Company'
  }
}
