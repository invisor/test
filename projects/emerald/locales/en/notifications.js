export default {
  cartItemAdded: '{0} was successfully added to your Cart',
  cartItemUpdated: '{0}{1} was updated in your Cart',
  cartItemRemoved: '{0}{1} has removed from your Cart',
  wishlistItemUpdated: '{0} was updated in your Favorites',
  wishlistItemAdded: '{0} was added to your Favorites',
  wishlistItemRemoved: '{0} was removed from your Favorites',
  compareListItemUpdated: '{0}{1} was updated in your Compare list',
  compareListItemAdded: '{0} was added to your Compare list',
  compareListRemoved: '{0} was removed from your Compare list',
  accountUpdated: 'Your account successfully updated',
  shippingAddressUpdated: 'Your shipping address successfully updated',
  billingAddressUpdated: 'Your billing address successfully updated',
  loggedOut: 'You are now logged out',
  loggedIn: 'You are logged in',
  tokenExpired: 'Your session has expired, please login',
  taxAdded: '{0} state tax has been added to your order',
  taxRemoved: 'State tax has been removed from your order',
  backendCalc: 'The final tax amount will be calculated after order submit',
  wrongAddress: 'Wrong shipping address'
}
