import metalTypes from './constants/metalTypesFilter'
import stoneShapes from './constants/stoneShapes'

export default {
  pair: 'pair',
  itemId: 'Item ID',
  centerStone: 'Center Stone(s)',
  stone: 'Emerald',
  weight: '{0} Ct.',
  Setting_Ring: 'Ring',
  Setting_Earring: 'Earring',
  Setting_Pendant: 'Pendant',
  Setting_Necklace: 'Necklace',
  metalTypes,
  stoneShapes
}
