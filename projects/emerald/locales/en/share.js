export default {
  facebook: 'Share via Facebook',
  twitter: 'Share via Twitter',
  googlePlus: 'Share via Google plus',
  pinterest: 'Share via Pinterest',
  email: 'Share via Email',
  copyLink: 'Copy link',
  linkCopied: 'Link copied to clipboard',
  linkCopyError: 'Something going wrong. Please, copy link manually'
}
