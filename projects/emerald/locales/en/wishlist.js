import metalTypes from './constants/metalTypesFilter'
import stoneShapes from './constants/stoneShapes'
import treatments from './constants/treatments'
import origins from './constants/filtersOrigins'
import stoneTypes from './constants/stoneTypes'

export default {
  metalTypes,
  stoneShapes,
  treatments,
  origins,
  stoneTypes,
  hero: {
    title: 'My Favorites',
    message: 'To save your favorites, create an account or log in',
    loginButton: 'Log in / Sign up'
  },
  lists: {
    stonesList: 'Stones',
    stonePairsList: 'Stones Pairs',
    braceletsList: 'Bracelets',
    earringsList: 'Earrings',
    necklacesPendantsList: 'Necklaces and Pendants',
    ringsList: 'Rings',
    weddingBands: 'Wedding bands',
    weddingBandsPlain: 'Plain wedding bands',
    ringSettings: 'Ring settings',
    earringSettings: 'Earring settings',
    pendantSettings: 'Pendant settings',
    necklaceSettings: 'Necklace settings',
    custom: 'Custom designs'
  },
  item: {
    itemId: 'Item ID',
    designId: 'Design ID',
    weight: '{0} Ct.',
    pair: 'Pair',
    centerStone: 'Center Stone(s)',
    totalWeight: '{0} Total Ct. Weight',
    itemToChart: 'Add {0} to cart',
    itemToJewelry: 'Add {0} to jewelry',
    stoneToChart: 'Add {0} to cart',
    createJewelry: 'Add to setting',
    cartLink: 'Shopping cart',
    inCart: 'This item currently in your {cartLink}',
    viewItem: 'View Item',
    toReview: 'Review & Add to Cart',
    reviewBand: 'Review & Add to Cart',
    reviewSetting: 'Review & Add to Cart'
  },
  continueShopping: 'Continue Shopping',
  empty: "You currently don't have any favorites saved"
}
