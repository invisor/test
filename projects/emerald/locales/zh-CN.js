import layout from './zh/layout'
import homePage from './zh/homePage'
import metadata from './zh/metadata'
import breadcrumbs from './zh/breadcrumbs'
import listingPage from './zh/listingPage'
import price from './zh/price'
import lists from './zh/lists'
import categories from './zh/categories'
import detailsPage from './zh/detailsPage'
import common from './zh/common'
import customItem from './zh/customItem'
import notifications from './zh/notifications'
import form from './zh/form'
import cart from './zh/cart'
import share from './zh/share'
import order from './zh/order'
import auth from './zh/auth'
import account from './zh/account'
import wishlist from './zh/wishlist'
import quickSearch from './zh/quickSearch'
import ourStaff from './zh/ourStaff'
import contacts from './zh/contacts'
import phones from './zh/phones'
import showroom from './zh/showroom'
import newsletters from './zh/newsletters'
import learnMore from './zh/learnMore'
import designYourOwn from './zh/designYourOwn'
import compareList from './zh/compareList'
import imagesAlt from './zh/imagesAlt'
import extendWarranty from './zh/extendWarranty'
import metaTags from './zh/metaTags'

export default {
  extendWarranty,
  layout,
  homePage,
  metadata,
  breadcrumbs,
  listingPage,
  price,
  lists,
  categories,
  detailsPage,
  common,
  customItem,
  notifications,
  form,
  cart,
  share,
  order,
  auth,
  account,
  wishlist,
  quickSearch,
  ourStaff,
  contacts,
  phones,
  showroom,
  newsletters,
  learnMore,
  designYourOwn,
  compareList,
  imagesAlt,
  metaTags
}
