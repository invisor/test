import divisionNames from './constants/divisionNames.js'

export default {
  hi: '您好！ {name}',
  signIn: '登录',
  recommendations: {
    title: '为您推荐',
    messageWithRecommendations:
      "Recommended For You is a personalized service option where one of our sales representatives lists suggestions for you to review based on your specific interests. This section of your user account will only be updated after you've already initiated communication with one of our client service representatives.",
    messageWithoutRecommendations:
      '在 Natural Emerald Company，我们密切关注我们的客户以及他们的好恶， 以下是我们的设计团队特别为您选择的一些经过仔细考虑的建议列表。',
    itemID: '商品编号: {0}',
    viewItem: '查看商品',
    favorites: '添加到收藏夹',
    compare: '加入对比列表',
    removeFavorites: '从收藏夹中移除',
    removeCompare: '从对比列表中移除',
    questions: '对这个设计有疑问？',
    help: '设计帮助',
    delete: '删除此推荐商品'
  },
  orders: {
    title: '订单',
    invoice: '发票',
    statusDetails: '查看状态详情',
    productionStatus: '生产状态详情',
    viewDetails: '查看详情',
    designId: '设计编号: {0}',
    stoneId: '宝石编号: {0}',
    settingId: '镶嵌编号: {0}',
    date: '创建订单时间: {0}',
    orderId: '订单 #: {0}',
    track: '单号 #: {0}',
    itemPrice: '价格:',
    totalPrice: '总价:',
    itemCount: '(1件商品)',
    itemsCount: '1件商品 | {count} 件商品',
    status: {
      0: '订单已提交',
      1: '订单处理中',
      2: '已发货'
    },
    paymentStatus: {
      0: '- 等待付款中',
      1: '- 付全款'
    }
  },
  personal: {
    title: '个人及登录详细信息',
    edit: '编辑',
    personalDetails: '个人信息',
    loginDetails: '登录详情',
    firstName: '名字',
    lastName: '姓',
    birthday: '生日',
    relationship: '关系',
    emailAddress: '邮箱',
    password: '密码',
    editPersonal: '填写个人信息',
    editLogin: '填写登录信息',
    selectDay: '选择日期',
    selectMonth: '选择月份',
    selectStatus: '选择一种关系状态',
    relationshipStatus: '关系状态',
    save: '保存个人信息',
    saveLogin: '保存登录信息',
    email: '邮箱',
    currentPassword: '当前密码',
    newPassword: '新密码',
    confirmPassword: '确认密码',
    statuses: {
      0: '单身',
      1: '恋爱中',
      2: '订婚',
      3: '结婚',
      4: '民事结合',
      5: '处于国内的合作伙伴关系中',
      6: '比较复杂',
      7: '在开放的关系中',
      8: '丧偶',
      9: '分手',
      10: '离婚',
      11: '保密'
    },
    months: {
      1: '一月',
      2: '二月',
      3: '三月',
      4: '四月',
      5: '五月',
      6: '六月',
      7: '七月',
      8: '八月',
      9: '九月',
      10: '十月',
      11: '十一月',
      12: '十二月'
    }
  },
  preview: {
    title: '预览请求提交',
    itemImage: '产品图片',
    itemName: '产品名称',
    itemDetails: '产品详情',
    dateRequested: '指定日期',
    status: '状态',
    awaitingPreview: '等待预览',
    readyToView: '准备查看',
    itemID: '商品编号:',
    color: '颜色:',
    carat: '克拉:',
    shape: '形状:',
    metal: '材质:',
    prong: '尖头:',
    productTime: '制作时间:',
    range: '{0} 到 {1} 天'
  },
  addressBook: {
    divisionNames,
    title: '地址簿',
    addAddress: '添加地址',
    edit: '编辑',
    remove: '移除',
    defaultBilling: '默认订单地址',
    defaultShipping: '默认收货地址',
    editAddressTitle: '编辑地址',
    addAddressTitle: '添加地址',
    addressNickname: '地址名称',
    firstName: '名字',
    lastName: '姓',
    address1: '地址1',
    address2: '地址栏2',
    address3: '地址栏3',
    city: '城市',
    state: '州',
    zipCode: '邮编',
    country: '国家',
    phone: '电话',
    cancel: '取消',
    submit: '保存',
    error: {
      required: '必填字段'
    }
  },
  notifications: {
    title: '通知',
    0: {
      title: '个人及登录详情',
      descr: '查看和更新您的电子邮件和登录详细信息'
    },
    1: {
      title: '地址簿',
      descr: '查看并更新您的收货地址'
    },
    2: {
      title: '订单',
      descr: '查看您当前的订单状态和历史订单'
    },
    3: {
      title: '通知',
      descr: '接收有关新商品、您的订单状态、保存的搜索和推荐的电子邮件'
    },
    4: {
      title: '预览请求提交',
      descr: '查看您的定制设计图片预览请求'
    },
    5: {
      title: '高级搜索',
      descr: '保存商品搜索，并在这些商品有货或将有货时收到通知'
    },
    6: {
      title: '为您推荐',
      descr: '我们的设计团队认为您会喜欢的物品的精选清单'
    },
    7: {
      title: '收藏夹',
      descr: '查看收藏夹中的商品'
    },
    8: {
      title: '对比列表',
      descr: '查看和比较商品详情'
    }
  },
  dashboard: {
    title: '帐户信息中心',
    titleMobile: '我的账号',
    signOut: '退出',
    change: '更改',
    generalInformation: '一般信息',
    generalInformationText:
      '您的帐户信息用于登录该网站，请将密码保存在安全的地方。',
    addressBook: '地址簿',
    addressBookText:
      '在下面查看您的帐单和送货地址，您可以通过点击“更改”来修改地址簿'
  },
  menu: {
    myAccount: '我的账号',
    logIn: '登录',
    changePassword: '更改密码',
    orderHistory: '历史订单',
    wishList: '收藏夹',
    signOut: '退出',
    noAccount: '还没有账户？',
    createAccount: '创建一个账号',
    signIn: '登入你的账号',
    email: '邮箱地址',
    pass: '密码',
    forgot: '忘记密码？',
    sign: '登录',
    rules: {
      email: {
        required: '需要一个有效的邮箱地址',
        type: '请输入正确的邮箱地址'
      },
      password: {
        required: '请输入您的密码'
      }
    }
  },
  generalInformation: {
    firstName: '名字',
    lastName: '姓氏',
    email: '邮箱地址',
    phone: '电话',
    password: '密码',
    emailPreferences: '邮箱首选项',
    mailingListTrue: '向我发送新闻更新和优惠',
    mailingListFalse: '不用向我发送新闻更新和优惠'
  },
  addressForm: {
    firstName: '名字',
    lastName: '姓氏',
    street1: '街道名称/区名称',
    street2: '房间号/门牌号',
    city: '配送城市',
    zipCode: '邮编',
    country: '国家',
    state: '省',
    stateMax: '州/省/县',
    pleaseSelect: '请选择'
  },
  billingAddress: {
    title: '我的主要帐单地址',
    saveButton: '更新账户',
    validate: {
      firstName: '请输入您的帐单名字',
      lastName: '请输入您的帐单的姓氏',
      street1: '请输入一个有效的账单地址',
      city: '请输入您的账单城市',
      state: '请输入一个有效的账单的州',
      countryId: '请选择账单国家',
      zipCode: '请输入有效的帐单邮递区号'
    }
  },
  shippingAddress: {
    title: '我的主要的邮寄地址',
    saveButton: '更新账号',
    validate: {
      firstName: '请输入您的收货人名字',
      lastName: '请输入您的收货人姓氏',
      street1: '请输入一个有效的配送地址',
      city: '请输入您的配送城市',
      state: '请输入一个有效的配送州',
      countryId: '请选择配送国家',
      zipCode: '请输入有效的邮递区号'
    }
  },
  profile: {
    form: {
      firstName: '名字',
      lastName: '姓氏',
      email: '邮箱地址',
      phone: '电话',
      gender: '性别',
      select: '选择',
      mailingList: '向我发送新闻更新和优惠',
      updatePassword: '更新密码',
      saveButton: '更新账号',
      mailingListItem: '可以发送电子邮件',
      yes: '是的',
      no: '不',
      mailingListMessage:
        '(我们尊重您的隐私，只会通过我们的每月时事通讯向您偶尔发送特价)'
    },
    validation: {
      firstName: {
        empty: '名字为必填项'
      },
      lastName: {
        empty: '姓氏为必填项'
      },
      phone: {
        empty: '电话是必填项'
      },
      email: {
        empty: '需要一个有效的电子邮箱地址',
        invalid: '请输入一个有效的邮箱地址'
      }
    },
    genders: {
      male: '男',
      female: '女'
    }
  },
  resetPassword: {
    title: '忘记密码',
    titleMobile: '忘记密码',
    assistance: '账号帮助',
    email: '邮箱地址',
    submit: '提交',
    enterEmail: '输入您以前用来创建帐户的电子邮件地址',
    notify1: '- 您将很快收到一封电子邮件，可以重置密码',
    notify2: '- 如果您没有收到电子邮件, 请{createAccount}',
    notify3: '- 如果您没有收到电子邮件, 请{contactUs} 获得帮助',
    createAccount: '创建新的账号',
    contactUs: '联系我们',
    validation: {
      email: {
        empty: '需要一个有效的邮箱地址',
        invalid: '请输入一个有效的邮箱地址'
      }
    },
    swal: '您将很快收到一封电子邮件，可以重置密码',
    ok: '好的'
  },
  changePassword: {
    title: '更改密码',
    titleMobile: '更改密码',
    assistance: '密码帮助',
    create1: '创建一个新的密码',
    create2: '登录账号时将使用此密码',
    submit: '保存更改',
    passwordConditions: '密码区分大小写，并且必须至少包含6个字符',
    form: {
      currentPassword: '当前密码',
      newPassword: '新密码',
      confirmPassword: '确认密码',
      showPasswords: '显示密码',
      saveButton: '保存密码',
      rules: {
        currentPassword: {
          required: '请输入您的当前密码'
        },
        password: {
          required: '请输入您的新密码',
          rangeMin: '您的密码必须至少包含6个字符',
          rangeMax: '您的密码必须至少包含6个字符',
          notSame: '您的新密码和旧密码不能相同'
        },
        confirmPassword: {
          required: '请再次输入新密码',
          sameAsPassword: '密码与确认密码不符'
        }
      }
    },
    validation: {
      currentPassword: {
        empty: '请输入您的当前密码',
        length: '您的密码必须至少包含6个字符',
        invalid: '密码必须包含至少一个数字以及至少一个大小写字母'
      },
      newPassword: {
        empty: '请输入您的新密码',
        invalid: '密码必须包含至少一个数字以及至少一个大小写字母'
      },
      confirmPassword: {
        empty: '请确认您的新密码',
        match: '新密码和确认密码不匹配'
      }
    },
    alert: {
      confirmButtonText: '好的',
      titleFail: '无法更改密码',
      titleSuccess: '密码修改成功',
      titleSuccess2: '重置密码',
      textSuccess: '您的密码已被更改',
      textSuccess2: '您的密码已重置<br>\n您可以使用新密码登录帐户',
      textFail2: '新密码和确认密码不匹配',
      textFail3: '您输入的当前密码不正确',
      textFail4:
        '重置密码失败,确保您的新密码至少包含6个字符，包括大写/小写和数字',
      textFail5:
        '重置密码失败,确保您的新密码至少包含6个字符，包括大写/小写和数字',
      textFail6: '抱歉，您的重置链接不再有效，请在“忘记密码”页面上请求新的链接',
      textFailDefault: '未知错误'
    }
  }
}
