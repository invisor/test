export default {
  or: '或者',
  signInForm: {
    signIn: '登录',
    close: '关闭',
    email: '邮箱地址',
    pass: '密码',
    forgotPass: '忘记密码？',
    remember: '记住我',
    signUp: '注册',
    cancel: '取消',
    login: '登录',
    rules: {
      email: {
        required: '请输入邮箱地址'
      },
      password: {
        required: '请输入您的密码'
      }
    }
  },
  signUpForm: {
    signUp: '注册',
    close: '关闭',
    firstName: '名字',
    lastName: '姓氏',
    email: '邮箱地址',
    pass: '密码',
    rePass: '重新输入密码',
    signIn: '登录',
    register: '注册',
    rules: {
      email: {
        required: '需要一个有效的邮箱地址',
        type: '请输入正确的邮箱地址'
      },
      password: {
        required: '请输入您的密码',
        range: '您的密码必须至少包含6个字符'
      },
      recaptcha: {
        required: '请进行验证验证码'
      }
    },
    checkPass1: '请再次输入密码',
    checkPass2: '密码与确认密码不符'
  }
}
