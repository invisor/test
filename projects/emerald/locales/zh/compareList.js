import shape from './constants/stoneShapes'
import origin from './constants/stonesOrigins'
import treatment from './constants/treatments'
import metalName from './constants/metalTypes'
import cut from './constants/stoneCut'
import clarity from './constants/stoneClarity'
import colorIntensity from './constants/stoneColorIntensity'
import color from './constants/stoneColors'
import prong from './constants/settingProngs'

export default {
  dreamCompare: '统一对比',
  dreamCompareDescription:
    '我们的统一对比可让您查看比较列表中保存有360°视频的任何项目。',
  customJewelry: '自定义珠宝',
  jewelry: '珠宝',
  stones: '绿宝石',
  pairs: '绿宝石对',
  settings: '镶嵌',
  weddingBands: '结婚戒指',
  dragAndDrop: '拖放',
  stonesEmpty: '您的绿宝石对比列表是空的！',
  jewelriesEmpty: '您的珠宝对比列表是空的！',
  pairsEmpty: '您的宝石对的对比列表是空的！',
  settingsEmpty: '您的镶嵌对比列表是空的！',
  bandsEmpty: '您的结婚戒指对比列表是空的！',
  customEmpty: '您的自定义珠宝的对比列表是空的！',
  lostSettingPrice: '价格在哪里？',
  lostSettingPriceHeader: '价格在哪里？',
  lostSettingPriceText:
    '每件定制珠宝都经过独特制作，以与您选择的宝石相匹配。因此，只有在您选择了匹配的石材和所有可用选项后，才能计算最终价格，其中包括：金属类型、戒指大小、副石和/或铺石钻石的重量，只需在下面选择此镶嵌即可继续，下一步，我们将自动为您计算价格。',
  compareTable: {
    addToFavorites: '添加到收藏夹',
    alreadyIn: '已经在',
    favorites: '收藏夹',
    image: '',
    id: '编号',
    designId: '设计编号',
    weight: '克拉',
    totalPrice: '总价格',
    totalCarat: '总克拉',
    metalWeight: '金属重量',
    styleName: '样式名称',
    stoneWeight: '克拉',
    centralStoneWeight: '主石重量',
    centerDimensions: '尺寸(mm)',
    productionTime: '生产时间',
    productionTimeValue: '{from}到{to}天',
    dimensions: '尺寸(mm)',
    color: '颜色',
    sideStonesCount: '总副石',
    sideStonesWeight: '副石重量',
    colorIntensity: '颜色饱和度',
    clarity: '净度',
    labType: 'Lab type',
    labColor: 'Lab color',
    metalName: '金属类型',
    treatment: '处理',
    shape: '形状',
    cut: '切工',
    origin: '产地',
    prong: '镶嵌齿',
    pricePerCt: '每克拉价格',
    price: '价格',
    settingsPrice: '价格'
  },
  color,
  shape,
  cut,
  clarity,
  colorIntensity,
  origin,
  treatment,
  metalName,
  prong
}
