export default {
  nA: '无',
  deepGreen: '深绿色',
  green: '绿色',
  vividGreenToDeepGreen: '浅绿到深绿',
  darkGreen: '暗绿色',
  intenseGreen: '深绿色',
  intenseGreenToVividGreen: '深绿色到鲜绿色',
  vividGreen: '鲜绿色',
  vividGreenImperialGreen: '鲜艳的红-“帝王绿”',
  vividGreenVibrant: '鲜艳的绿色“充满活力”'
}
