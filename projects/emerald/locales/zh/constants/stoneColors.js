export default {
  blue: '蓝色',
  bluishPurple: '蓝紫色',
  bluishGreen: '蓝绿色',
  bluishGray: '蓝灰色',
  black: '黑色',
  biColor: '双色',
  multiColor: '多色',
  colorChange: '颜色变化',
  green: '绿色',
  greenishBlue: '绿蓝色',
  greenishBrown: '棕绿色',
  greenishYellow: '青黄色',
  gray: '灰色',
  grayishBlue: '灰蓝色',
  orange: '橙色',
  orangishYellow: '橘黄色',
  orangishBrown: '棕褐色',
  orangishRed: '橘红色',
  padparadschaPinkishOrangeOrangishPink: '帕德玛 (粉红橙色/ 橙粉色)',
  peachYellowOrange: '桃色(黄橙色)',
  pink: '粉红色',
  pinkishPurple: '粉紫色',
  pinkishRedReddishPink: '品红/红粉红色',
  purple: '紫色',
  purplishBlue: '紫蓝色',
  purplishPink: '紫粉色',
  violet: '紫色',
  yellow: '黄色',
  yellowishBrown: '黄棕色',
  yellowishGreen: '黄绿色',
  yellowishOrange: '黄橙色',
  white: '白色',
  red: '红色',
  pinkishRed: '粉红色',
  purplishRed: '紫红色',
  brownishRed: '棕红色',
  yellowishRed: '黄红',
  brown: '棕色'
}
