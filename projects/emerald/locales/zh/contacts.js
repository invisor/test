export default {
  pageTitle: '联系我们',
  pageMessage:
    '有任何疑问，疑问或只是获取联系？我们在这里会提供帮助！请填写下面的表格，我们的销售人员会尽快与您联系。您也可以致电 <strong>1-212-204-8581</strong> 或给我们发送邮件 <a class="email" href="mailto:info@emeralds.com">info@emeralds.com</a>.',
  collapseTitle1: '总公司，陈列室和办公室',
  collapseTitle2: 'CAD和产品部门',
  dropdown: {
    close: '关闭',
    showroom: '陈列室',
    viewingExperience:
      '要获得独特的观看体验，请访问我们位于纽约曼哈顿的私人阁楼陈列室。',
    contactTime1: '预约时间 周一- 周五',
    contactTime2: '10:30am - 5pm ET',
    contactTime3: '周天: 12pm - 4pm ET',
    appointment: '预约',
    locations: '地址',
    locationsContent:
      '<span>陈列室 & 工作室</span> 6 East 45th St, 20th Floor <br> New York, <br> NY 10017 <br> United States',
    miningProduction:
      '<span>CAD & 产品部门</span> 73/1/3 Council Avenue <br>Demuwawatha, Ratnapura <br>Sri Lanka',
    phone: '电话',
    liveChat: '在线沟通',
    hoursAvailable: '预约时长',
    email: '邮箱',
    help: '<span>我们在这里提供帮助！</span>我们邀请您随时通过我们的网站向我们发送消息。',
    contactUs: '联系我们'
  },
  customerService: {
    title: '客户服务时间',
    week1: '周一 – 周五',
    week2: '周四 - 周五',
    week3: '周六',
    week4: '周天',
    time1: '10:30am – 9:00pm <small>ET</small>',
    time2: '10:30am – 6:00pm <small>ET</small>',
    time3: '12:00pm – 4:00pm <small>ET</small>',
    time4: '12:00pm – 4:00pm <small>ET</small>',
    phone: '电话：',
    email: '邮箱：'
  },
  headOffice: {
    title: '总公司, 陈列室 & 工作室',
    address1: '6 East 45th Street, 20th Floor',
    address2: 'New York, NY 10017',
    country: 'United States',
    map: '查看地图',
    directions: '获取路线',
    link: '预约'
  },
  mining: {
    title: 'CAD和产品部门',
    address1: '73/1/3 Council Avenue',
    address2: 'Demuwawatha, Ratnapura',
    country: 'Sri Lanka',
    map: '查看地图',
    directions: '获取路线'
  },
  formTitle: '给我们发信息！',
  form: {
    customerName: '姓名',
    phone: '电话',
    email: '邮箱',
    city: '城市',
    zipCode: '邮政编码',
    country: '国家',
    pleaseSelect: '请选择',
    state: '州',
    stateMax: '州/省/县',
    submit: '提交',
    address: '地址'
  },
  swal: {
    title: '感谢您的联系！',
    message: '谢谢您联络我们！我们会尽快与您联系。',
    ok: '好的'
  },
  formErrors: {
    customerName: '请输入姓名',
    message: '请输入留言内容',
    captcha: '请进行验证码验证',
    email: {
      required: '请输入邮箱',
      valid: '请输入正确的邮箱地址'
    },
    phone: {
      required: 'Please enter phone number'
    },
    date: '请输入日期',
    time: '请输入时间'
  }
}
