import categories from '~/locales/zh/categories'

export default {
  noMatch: {
    detailsTitle: '完成您的定制',
    detailsAlert: '很抱歉，您选择的绿宝石和镶嵌可能不兼容',
    detailsProposal:
      '有兴趣将这两个商品结合起来吗？ 我们也许可以提供帮助！ 通过我们的免费定制设计服务，我们将与您一起，帮助打造您理想中的定制珠宝。',
    detailsInquire: '咨询此定制设计',
    detailsOr: '或',
    detailsContinue: '希望使用不同的绿宝石或镶嵌继续您的设计？',
    detailsStone: '选择新的绿宝石',
    detailsSetting: '选择新的镶嵌',
    form: {
      title: '定制设计疑问',
      message:
        '通过免费的定制设计服务，我们相信我们可以将您的想法成真，只需填写这份简短的表格，就会有工作人员尽快回复您， 如果您想立即与公司人员取得联系，请随时致电 +1-212-204-8581 与我们联系。',
      alert1: '我们是一家没有压力的公司',
      alert2:
        '我们的客服不会收取任何佣金，我们也不会泄露您的联系信息或者硬进行推销。',
      designRequest: '设计要求',
      yourName: '姓名:',
      emailAddress: '邮箱:',
      phoneNumber: '电话:',
      comments: '备注:',
      submit: '提交查询',
      rules: {
        email: {
          required: '需要一个有效的邮箱',
          template: '请输入一个有效的邮箱'
        },
        name: {
          required: '请输入您的姓名：'
        }
      }
    }
  },
  categories: {
    ...categories,
    stone: '绿宝石'
  },
  Jewelry: '珠宝',
  Ring: '戒指',
  Earring: '耳环',
  Pendant: '吊坠',
  Necklace: '项链',
  finalizeSetting_Ring: '完成您的戒指',
  finalizeSetting_Earring: '完成您的耳环',
  finalizeSetting_Pendant: '完成您的吊坠',
  finalizeSetting_Necklace: '完成您的项链',
  emerald: '绿宝石',
  emeralds: '绿宝石',
  Setting_Ring: '戒指',
  Setting_Earring: '耳环',
  Setting_Pendant: '吊坠',
  Setting_Necklace: '项链',
  itemId: '货品编号',
  completedPrice: '总价格：',
  productionTime: '生产时间：',
  productionTimePeriod: '{0} 到 {1} 天',
  pushService: '若有特殊情况，可提供加急服务。',
  freePreview: '申请免费预览',
  checkout: '安全支付',
  shoppingCart: '购物车',
  itemInCart: '这个货品目前在您的',
  waxInCart: '这个模型已经添加到您的',
  stoneInCart: '这个宝石目前在您的',
  pairInCart: '这个匹配对当前在您的 ',
  help: '帮助',
  designerHelp: '设计师帮助',
  designerHelpSuccessTitle: '谢谢您，和您的问题',
  designerHelpSuccessFooter: '已发送给我们的专家，我们会尽快与您联系。',
  designerHelpBack: '返回到问题',
  askHelp: '询问设计师',
  helpMessage:
    '我们一直有专业的设计师为您提供方便和愉悦的服务。无论是有关您的宝石的问题，您的镶嵌的选择，或是珠宝制作的任何问题，给Erika发送您的问题，她会在24小时内给您回复。',
  availableDesigns: {
    title: '使用此镶嵌的可用设计',
    subTitle: '在下面选择一个设计以查看该设计的更多详细信息',
    message:
      '您正在看的是定制设计的{0}，放在{1}中的珠宝。此珠宝是由我们的一位客户使用我们完全免费的“定制设计服务”创建的。',
    production: '生产时间：',
    fromTo: '{0} 到 {1} 天',
    price: '价格如图所示：',
    customize: '定制并购买此设计'
  },
  steps: {
    createYourJewelry: '制作您的 {0}',
    chooseYourStone: '选择绿宝石',
    chooseYourStones: '选择绿宝石',
    chooseYourSetting: '选择镶嵌',
    reviewYourJewelry: '查看您的{0}',
    yourDesign: '您的设计',
    addToCart: '添加到购物车',
    startOver: '重新开始',
    jewelry: '珠宝',
    start: '开始',
    stone: '绿宝石',
    setting: '镶嵌',
    review: '查看',
    change: '更改',
    remove: '删除'
  },
  removeModal: {
    title: '我们可以重新开始！ 但首先，你可以选择以下操作...',
    saveDesignFavorites: '保存设计到收藏夹',
    saveDesignCompare: '保存设计到对比列表',
    savedDesignFavorites: '已保存至收藏夹',
    savedDesignCompare: '已保存至对比列表',
    findStone: '使用绿宝石开始',
    findPair: '使用绿宝石对开始',
    findSetting: '使用镶嵌开始',
    messageSetting:
      '如果删除了这个选定的镶嵌，将会丢失您的设计，如果要保留设计，请务必先将其保存到收藏夹中！',
    messageStone:
      '如果删除了这个选定的绿宝石，将会丢失您的设计，如果要保留设计，请务必先将其保存到收藏夹中！',
    saveButton: '将设计保存到收藏夹，并选择新的镶嵌',
    saveSettingButton: '将设计保存到收藏夹，并选择新的镶嵌',
    saveStoneButton: '将设计保存到收藏夹，并选择新的绿宝石',
    saveButtonInWishList: '这个物品已经在您的收藏夹中',
    notSaveButton: '不用保存，选择一个新的镶嵌',
    notSaveSettingButton: '不用保存，选择一个新的镶嵌',
    notSaveStoneButton: '不用保存，选择一个新的绿宝石',
    cancelButton: '继续设计'
  },
  polymer: {
    title: '3D 打印模型',
    text: '我们会向您发送您的设计的 3D 打印模型！',
    price: '$95',
    button: '加入购物车'
  }
}
