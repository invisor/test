export default {
  createYourJewelry: '仅需3步，设计属于您自己的戒指、耳环、吊坠或者项链……',
  startCreating: '开始制作',
  stepOneTitle: '选择您的绿宝石',
  stepOneText: '通过搜索我们庞大的天然绿宝石库存存，来开始寻找您完美的宝石。',
  stepTwoTitle: '选择您的镶嵌',
  stepTwoText:
    '选择好满意的绿宝石后，请仔细研究我们经过专业设计的镶嵌，可以找到一个恰当的镶嵌来完美的展示您的绿宝石。',
  stepThreeTitle: '完成了您的珠宝',
  stepThreeText: '在完成购买珠宝之前，请使用所选的绿宝石和镶嵌预览您的作品！'
}
