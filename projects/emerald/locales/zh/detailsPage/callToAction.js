export default {
  headerTitle: '谢谢!您的请求已经提交。',
  headerBody:
    '请允许24小时内，通过电子邮件接收到您定制的珠宝创作的组合3D渲染。',
  headerDismiss: '忽略这个消息',
  bodyTitle: '您下一步想做什么?',
  bodyAction: '请选择下面的一个选项。',
  favoritesText: '将此组合添加到收藏夹',
  favoritesTextComplete: '添加到收藏夹',
  compareText: '添加这个组合到对比列表',
  compareTextComplete: '添加到对比列表',
  shopStone: '选择与镶嵌{0}匹配的其他绿宝石',
  shopSetting: '选择不同的 {0} 镶嵌匹配 {1} {2}',
  homePage: '继续浏览天然绿宝石公司主页',
  addFavorites: '添加到收藏夹',
  addCompare: '添加到对比列表',
  added: '已添加',
  earring: '耳环',
  pendant: '吊坠',
  necklace: '项链',
  ring: '戒指'
}
