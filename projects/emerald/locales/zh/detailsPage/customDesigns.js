export default {
  title1: '创建定制设计',
  title2: '使用这个镶嵌',
  stone: '使用这个宝石',
  pair: '使用这个宝石对',
  message: '浏览一下我们的顾客通过我们的 {0} 服务创作了哪些首饰',
  dyo: '设计定制',
  designDescription:
    '您正在浏览的首饰是使用宝石{0}和镶嵌{1}设计而来， 此项目是由我们的一位客户使用我们完全免费的{2}服务创建的',
  productionTime: '生产时间',
  productionTimePeriod: '从 {0} 到 {1} 天',
  price: '最低价格',
  view: '查看 & 自定义此设计',
  loading: '加载数据中，请稍等...',
  empty: '暂无设计定制首饰'
}
