export default {
  title: '您知道吗？',
  message: '我们提供两种选择，让您在购买前查看您的定制设计！',
  previewTitle: '免费的图片预览',
  previewMessage: '我们可以免费向您发送您的绿宝石和镶嵌组合的 3D 图像渲染！',
  previewButton: '请求免费预览',
  waxTitle: '聚合物蜡模型',
  waxMessage: '我们可以向您发送您的组合设计的3D打印模型。',
  waxButton: '购买聚合物蜡模型'
}
