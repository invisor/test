import designStylesFilter from '../constants/designStylesFilter'
import filtersOrigins from '../constants/filtersOrigins'
import metalTypesFilter from '../constants/metalTypesFilter'
import prongsNames from '../constants/prongsNames'
import stoneShapes from '../constants/stoneShapes'
import stoneTypes from '../constants/stoneTypes'
import treatments from '../constants/treatments'
import mainDetails from './mainDetails'
import productOrigin from './productOrigin'
import sizingGuide from './sizingGuide'
import requestPreview from './requestPreview'
import qualityValueHeart from './qualityValueHeart'
import question from './question'
import callToAction from './callToAction'
import ourProcess from './ourProcess'
import customDesigns from './customDesigns'
import didYouKnow from './didYouKnow'

export default {
  bankWirePrice: {
    title: '银行电汇价格',
    titleStone: '银行电汇宝石价格',
    tooltipTitle: '银行电汇价格',
    tooltipBody:
      '当您选择通过银行电汇付款时，可享受标价 2.5% 的折扣。 只需在线下订单，我们的销售代表将与您联系，并为您提供最终折扣价和付款说明。',
    notification: '2.5% 的银行电汇折扣适用于您购物车中的商品。'
  },
  lightSkin: '浅色皮肤',
  darkSkin: '黑色皮肤',
  ringSizeConfirm: {
    text: '请点击确认按钮，再次确认您的戒指尺寸是{0}',
    confirm: '确认',
    cancel: '取消'
  },
  fullscreen: '全屏显示',
  controls: {
    question: '咨询问题',
    share: '分享',
    favorites: '加入收藏夹',
    compare: '加入对比列表'
  },
  from: '来自',
  pairFrom: '成对的来自于',
  setting: '{0} 镶嵌',
  productToCart: '添加 {0} 至购物车',
  and: '和',
  productToSetting: '选择这个 {0} 镶嵌',
  wireframe: '线框',
  reset: '重置',
  treatmentsName: {
    ...treatments
  },
  stoneShapesName: {
    ...stoneShapes
  },
  stoneTypeName: stoneTypes,
  originName: {
    ...filtersOrigins
  },
  metalTypesNames: {
    ...metalTypesFilter
  },
  prongsNames,
  sizingGuide,
  productTypes: {
    earring: '耳环',
    bracelet: '手链',
    necklace: '项链',
    pendant: '吊坠',
    ring: '戒指',
    weddingBand: '结婚戒指',
    plainBand: '素圈戒指',
    settingRing: '镶嵌戒指',
    settingEarring: '镶嵌耳环',
    settingPendant: '镶嵌吊坠',
    settingNecklace: '镶嵌项链'
  },
  shareProduct: {
    form: {
      title: '将此产品发送给朋友',
      subTitle:
        '给您的朋友发送您所选择产品的信息, 请完成下面的信息并点击发送按钮',
      shareLink: '或者只发送此链接',
      submit: '发送',
      defaultMessage:
        '嗨！ 我想与您分享我在Emeralds.com上找到的这款很棒的产品！',
      form: {
        recipient1: '朋友的邮箱',
        recipient2: '第二位朋友的邮箱 (可选)',
        senderName: '您的姓名',
        senderEmail: '您的邮箱',
        message: '您的留言',
        shipToBilling: '给我也发送一份同样的邮件'
      },
      rules: {
        senderName: {
          required: '请输入您的姓名'
        },
        senderEmail: {
          required: '请输入您的邮箱地址',
          email: '邮箱不正确'
        },
        recipient1: {
          required: '请输入收件人的邮箱地址',
          email: '邮箱不正确'
        },
        recipient2: {
          email: '邮箱不正确'
        }
      }
    },
    swal: {
      title: '您的邮件已经发送！',
      subTitle: '您的留言和这个页面的链接已经发送给您的朋友',
      ok: '好的'
    }
  },
  mainDetails,
  productOrigin,
  requestPreview,
  qualityValueHeart,
  question,
  callToAction,
  designStylesFilter,
  ourProcess,
  customDesigns,
  didYouKnow
}
