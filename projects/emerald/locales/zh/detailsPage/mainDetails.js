import stoneShapes from '../constants/stoneShapes'
import stonesOrigins from '../constants/stonesOrigins'
import filtersOrigins from '../constants/filtersOrigins'
import treatments from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import stoneColors from '../constants/stoneColors'
import bandStyle from '../constants/bandStyle'
import bandFinish from '../constants/bandFinish'

export default {
  prongsNotice:
    'The item displayed above includes a stone set with {0} prongs. Please note the actual item you receive may look different depending on your prong selection.',
  availability: {
    0: 'Available',
    1: 'Delisted',
    2: 'Call for Status',
    3: 'Sold',
    4: 'On Hold'
  },
  confirmation: {
    ringSize:
      'Please reconfirm your ring size is a size {ringSize} by clicking the confirm button below',
    inCart:
      'A design using the same stone is currently in your shopping cart. By continuing, the item in your cart will be replaced by this new design.',
    confirm: 'Confirm',
    cancel: 'Cancel'
  },
  addedToCart: 'Added to {cart}',
  cart: 'cart',
  viewFullDetails: 'View full product details',
  totalStonesWeight: {
    diamond: '钻石的预估总重量',
    emerald: '绿宝石的预估总重量',
    sapphire: '蓝宝石的预估总重量'
  },
  chain: '18"包含链子',
  soldSorry: '很抱歉，此商品已售罄，如果您有其他问题，请联系我们。',
  soldDelivery: ' - Order now for delivery by ',
  soldDeliveryTooltip:
    "But don't worry, we're working to bring this popular {category} back in stock as quickly as possible. Order now for delivery by {date}",
  thisIsMto: '此商品可正常购买',
  onHold: '暂时不能购买',
  soldOut: '已出售',
  outOfStock: '缺货',
  ring: '戒指',
  earring: '耳环',
  pendant: '吊坠',
  necklace: '项链',
  preview: 'All',
  stone: '宝石',
  setting: '镶嵌',
  reports: '报告',
  available: '可购买',
  freeShipping: '免邮费',
  noHasslePolicy: '14天无忧退货政策',
  noHasslePolicyText:
    '我们提供14天的退货窗口，以查看我们的绿宝石、珠宝或您独一无二的定制的绿宝石珠宝。如果您希望退货，我们将全额退款给您，除非我们在开始生产前特别通知您任何可能的进货费用。我们的商品很少有补货费用。如果您有疑问，请与我们联系。',
  stonePrice: '宝石对价格',
  pairPrice: '宝石对价格',
  settingPrice: '镶嵌价格',
  designId: '设计编号',
  itemId: '货品编号',
  stoneItemId: 'Emerald Item ID',
  settingItemId: 'Setting Item ID',
  totalPrice: '总价',
  calculatedPrice: '在下一步计算价格',
  calculatedPriceTitle: '价格在哪里？',
  calculatedPriceText:
    '每一件定制珠宝都是独一无二的，以匹配您所选择的宝石。因此，最终价格只能在您选择了一个兼容的宝石和所有可用的选项之后进行计算，其中可能包括:金属类型，戒指大小，副石和/或铺钻石的重量。只需继续选择下面的这个镶嵌。我们会在下一步为您自动计算价格',
  price: '价格',
  toCartStone: '将裸石加入购物车',
  toCartPair: 'Add loose stones to cart',
  toSettingStone: '使用这个宝石来创作首饰',
  toCartPairs: '加入购物车',
  toSettingPairs: '添加到耳环上',
  inCart: '这个货品目前已经在您的',
  inCartStone: '这个宝石目前已经在您的',
  shoppingCart: '购物车',
  videoShot: '使用iPhone XS拍摄',
  metal: '金属',
  vedic: 'Vedic Setting Style',
  vedicText:
    "This setting can accommodate a 'Vedic' style stone placement. Vedic settings are usually chosen for their ability to set the gemstone low, so it specifically touches the skin of the wearer.",
  vedicTextCTA:
    'If you would like the gemstone to touch the skin when set please check this box to confirm.',
  chooseVedic: 'Place gemstone touching skin',
  vedicLearnMore: 'Learn more about Emeralds and Vedic astrology',
  vedicNotSelected: 'Not Selected',
  vedicSelected: 'Gemstone Touching Skin',
  prongs: '镶嵌齿',
  size: '戒指尺寸',
  finish: '完成',
  ringWidth: '婚戒宽度 (mm)',
  sideStones: '选择宝石大小',
  sizingGuide: '尺寸参考',
  addEngraving: '添加雕刻',
  paveStone: '密镶钻石',
  totalPaveWeight: '总估计 {0} 重量: {1}',
  optional: '可选',
  characters: '{0}/15最多字符，刻在戒指内侧',
  engravingPlaceholder: '雕刻（可选）',
  productionTime: '生产时间',
  productionTimePeriod: '{0}到{1}天',
  totalMetalWeight: '金属总重量',
  grade: '等级',
  stoneWeight: '重量',
  weight: '单石重量',
  stoneTypeName: stoneTypes,
  paveStones: {
    sapphire: '蓝宝石',
    emerald: '绿宝石',
    diamond: '钻石',
    ruby: '红宝石'
  },
  ringDetails: {
    ringSize: '选择戒指尺寸',
    chooseRingSize: '选择戒指尺寸',
    ringWidth: '选择戒指宽度',
    selectSize: '请选择一个戒指尺寸'
  },
  settingTypePicker: {
    title: '您想用绿宝石创作什么首饰呢？',
    continue: '继续',
    ringsList: '戒指',
    earringsList: '耳环',
    pendantsList: '吊坠',
    necklacesList: '项链',
    necklacesPendantsList: 'Necklaces & Pendants'
  },
  share: {
    facebook: '通过Facebook分享',
    twitter: '通过Twitter分享',
    googlePlus: '通过Google plus分享',
    pinterest: '通过Pinterest分享',
    email: '使用邮件分享',
    copyLink: '复制链接',
    linkCopied: '链接已复制到粘贴板',
    linkCopyError: '出错了，请手动复制链接'
  },
  help: {
    name: '需要帮助？',
    call: '电话咨询',
    email: '邮件联系',
    chat: '在线沟通！'
  },
  product: '产品',
  description: '说明',
  details: '详情',
  overview: '概述',
  stoneDetails: '宝石详情',
  stoneShapesName: {
    ...stoneShapes
  },
  originName: {
    ...filtersOrigins
  },
  stonesOrigins,
  treatmentsName: {
    ...treatments
  },
  bandFinish,
  bandStyle,
  stoneColors,
  itemOverview: {
    itemId: '货品编号:',
    weight: '重量:',
    totalWeight: '总重量:',
    price: '价格:',
    totalPrice: '总价:',
    settingPrice: '镶嵌价格:',
    perCaratPrice: '每克拉价格:',
    metalType: '金属类型:',
    centerStones: '主石:',
    sideStones: '副石:',
    paveStones: '预估计密镶钻石总重量：'
  },
  stoneParams: {
    undefined: {
      tooltip: ''
    },
    perCaratPrice: {
      tooltip: '了解每克拉价格',
      title: '每克拉价格',
      text: `<p> "每克拉价格" 是一个珠宝贸易术语，解释了如何计算宝石价格。 优质宝石的定价是将它们的重量乘以“每克拉”价格（例如：2.50ct.x $ 1000 per / ct = $ 2500总成本）。</p>
      <p>每克拉价格越高，宝石中4C（切割，净度，克拉尺寸和颜色）的固有质量就越高。</p>
      <p>例如，净度较差的1.00克拉绿宝石，单价为10美元/克拉；而净度极佳的1.00克拉绿宝石，单价为100美元/克拉，即使它们的大小相同。普通净度的1.00克拉绿宝石，单价约为50美元/克拉。</p>`
    },
    color: {
      tooltip: '了解颜色',
      title: '颜色',
      text: '<p>所有物品都用灯光拍摄，旨在复制强烈的直射阳光，这是评估所有宝石颜色的宝石学标准，我们使用5000-6000开尔文LED 光源来复制头顶直射阳光。</p>'
    },
    shape: {
      tooltip: '了解绿宝石的形状',
      title: '形状',
      text1:
        '下图展示了常见的宝石形状。最受欢迎的彩色宝石形状有圆形、垫型、祖母绿形、雷德恩型和公主方型。如果您想了解有关每种形状特征的更多信息，请致电我们与宝石专家交谈，他们将很乐意帮助您选择您的绿宝石。',
      text2: `在PDF文件中查看不同形状的尺寸，请点击<a
      href="https://image.thenaturalsapphirecompany.com/site-files/nec_stone_dimensions.pdf"
      target="_blank">这里</a>`
    },
    dimensions: {
      tooltip: '了解绿宝石的尺寸'
    },
    clarity: {
      tooltip: '了解绿宝石的净度',
      title: '净度',
      text: `<p></p><p><strong>肉眼可见的清晰</strong>:不仔细看的话，绿宝石没有肉眼可见的杂质。</p>
      <p><strong>非常轻微杂质</strong>:肉眼观察，夹杂物会略微可见，但不会阻碍观察者从正面观看宝石的光彩。</p>
      <p><strong>轻微杂质</strong>: 肉眼可以见轻微的杂质，略微影响宝石的色泽。</p>
      <p><strong>包含杂质</strong>: 有明显的杂质，严重或完全阻碍宝石内的反射光彩。</p>`
    },
    cut: {
      tooltip: '了解祖母绿的切面',
      title: '切面',
      text: '以下是用于评估和分类祖母绿宝石切割的常用术语和特征。'
    },
    colorIntensity: {
      tooltip: '了解关于绿宝石的颜色饱和度',
      title: '颜色饱和度表',
      text: `<p>颜色通常被认为是决定祖母绿质量的第一要素。 祖母绿中绿色的明暗程度被描述为颜色强度。 天然祖母绿有多种绿色色调，下面我们将讨论这些分类。</p>
      <p><strong>浅色</strong> - 此分类保留给显示大部分绿白色光反射或几乎柔和的颜色强度的祖母绿。</p>
      <p><strong>中色</strong> - 颜色饱和度为 50% 的祖母绿具有中等颜色强度。</p>
      <p><strong>强烈的</strong> - 一种几乎没有黄色或绿色底色的绿色色调，颜色被描述为具有 50-80% 饱和度的近乎纯绿色，这是一种非常理想且有吸引力的颜色，专为优质透明祖母绿保留。</p>
      <p><strong>明亮的</strong> - 色调（纯绿色）、色调（深绿色）和饱和度（80-100%）的最佳组合。 最优质的祖母绿具有鲜艳的颜色等级，具有出色的透明度和反射光返回眼睛。</p>
      <p><strong>深色</strong> - 深色饱和度通常保留给几乎没有光线从宝石中逸出的祖母绿。 颜色几乎没有透明度，绿色是柔和的，通常带有黑绿色调。</p>`
    },
    origin: {
      tooltip: '了解关于绿宝石的产地',
      title: '产地',
      text: `<p><strong>哥伦比亚</strong> - 两个大型矿区生产所有哥伦比亚祖母绿：奇沃尔和穆佐。 这些祖母绿以其传奇的历史而闻名，是最令人梦寐以求的祖母绿原产地。 它们以明亮、纯绿色而闻名，有时会变成蓝绿色。</p>
      <p><strong>巴西</strong> - 哥伦比亚祖母绿的故事将探险家带到了巴西，并在 16 世纪首次发现了祖母绿。 Itabira/Nova Era 带的近期沉积物已被证明具有可爱的颜色和非常好的透明度。</p>
      <p><strong>赞比亚</strong> - 赞比亚祖母绿的发现仅持续到 20 世纪初，但该国最近已经确立了生产越来越多可爱的祖母绿的地位，这些祖母绿的颜色略深、偏蓝，透明度很高。</p>
      <p><strong>俄罗斯</strong> - 近年来，俄罗斯祖母绿的开采和生产发生了革命性变化，这些“乌拉尔山”宝石以其较浅的绿色和略多的内含物而闻名。 俄罗斯已宣布到 2025 年大幅增加采矿工作，因此供应只会增加。</p>
      <p><strong>津巴布韦</strong> - 大多数津巴布韦祖母绿被称为“Sandawana”祖母绿，指的是著名的矿区。 这些祖母绿以颜色浓烈、尺寸较小且内含而著称。 它们深沉、平衡、鲜艳的绿色已成为业内的传奇。</p>
      <p><strong>其他来源</strong> - 来自世界其他地区的祖母绿也在质量和生产方面建立了影响力，包括阿富汗和巴基斯坦、澳大利亚和印度。 来自阿富汗和巴基斯坦的宝石具有充满活力的饱和度和明亮的绿色色调。 来自澳大利亚的祖母绿在生产中起起落落，但质量一直很好，而来自印度的祖母绿自古就有，但以质量较低而闻名。</p>`
    },
    treatments: {
      tooltip: '了解绿宝石的优化处理',
      title: '优化处理',
      text: `<p><strong>标准处理</strong> - 如今，绝大多数祖母绿都经过改良以提高其净度。 有许多不同的物质用于此目的。 Emeralds.com 宝石仅使用行业认可的材料（无色油、蜡或聚合物/树脂）进行增强。</p>
      <p><strong>未处理</strong> - 这些祖母绿并没有以任何方式增强，它们只是从开采的原石晶体中切割和抛光而成。未经强化的祖母绿，尤其是那些质量较好的祖母绿，极为稀有和令人向往。</p>`
    }
  },
  stoneType: '宝石类型：',
  quantity: '数量:',
  color: '颜色：',
  clarity: '净度：',
  shape: '形状：',
  cut: '切工:',
  totalWeight: '总重量:',
  colorIntensity: '颜色饱和度:',
  detailsDimensions: '尺寸 (MM):',
  chainLength: '链长 (Inches):',
  braceletLength: '长度 (Inches):',
  origin: '产地：',
  treatment: '处理:',
  detailsItemId: '货品编号:',
  detailsWeight: '重量:',
  detailsColor: '颜色:',
  detailsClarity: '净度：',
  detailsShape: '形状：',
  detailsPerCaratPrice: '价格/每克拉：',
  detailsCut: '切工：',
  starAppearance: '星相:',
  detailsColorIntensity: '颜色饱和度：',
  detailsOrigin: '产地：',
  detailsTreatments: '处理：',
  length: '长度: {0}',
  width: '宽度: {0}',
  height: '高度: {0}',
  createJewelry: '使用宝石制作首饰',
  startFromStone: 'Create Jewelry With This Stone',
  startFromPair: 'Create Jewelry With These Stones',
  startFromSetting: 'Create Jewelry With This Setting',
  continueWithStone: '将宝石添加到镶嵌',
  continueWithPair: '将这对宝石添加到镶嵌',
  notCompatibleStone: '这个绿宝石不匹配',
  notCompatiblePair: '这对绿宝石不匹配',
  notCompatibleRingSetting: '这个戒指镶嵌不匹配',
  notCompatibleNecklaceSetting: '这个项链镶嵌不匹配',
  notCompatibleEarringSetting: '这个耳环镶嵌不匹配',
  notCompatiblePendantSetting: '这个吊坠镶嵌不匹配',
  takeMeBackStone: '返回到匹配的绿宝石',
  takeMeBackPair: '返回到匹配的绿宝石对',
  takeMeBackRingSetting: '返回到匹配的戒指镶嵌',
  takeMeBackNecklaceSetting: '返回到匹配的项链镶嵌',
  takeMeBackEarringSetting: '返回到匹配的耳环镶嵌',
  takeMeBackPendantSetting: '返回到匹配的吊坠镶嵌'
}
