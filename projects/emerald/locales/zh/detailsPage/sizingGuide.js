export default {
  label: '尺寸参考',
  ringSizeTitle: '不了解您的戒指尺寸？',
  ringSizeMessage: `我们有一个方便的PDF，您可以将其打印出来，以帮助确定您的确切戒指尺寸。 此图表基于美国
  戒指尺寸系统。 重要的是要注意，我们不能保证100％的准确性。 但是，当完整打印
  尺寸并正确使用，我们发现此图表非常准确。`,
  ringSizeDownload: '下载并打印我们的',
  ringSizingChart: '戒指尺寸表格',
  pdf: '(PDF)',
  converterTitle: '环戒指尺寸转换表',
  converterMessage:
    '我们使用的是美国标准的戒指尺寸表。 以下是不同国际标准的戒指尺寸对比图。',
  usaSize: '美国尺寸',
  diamInch: '直径 INCH',
  diamMm: '直径 MM',
  circumMm: '周长 MM',
  british: '英国',
  french: '法国',
  german: '德国',
  japanese: '日本',
  swiss: '瑞士'
}
