export default {
  term: '条款: {0}.',
  remove: 'Remove Protection',
  years: '{0} Year Protection',
  lifetime: '终身延保'
}
