export default {
  smallText: '绿宝石基础',
  title: '关于绿宝石的知识',
  text: '想要真正欣赏和了解祖母绿的美丽和奇妙，请访问我们的知识部分，在这里探索广阔而迷人的祖母绿世界！',
  readMore: '阅读更多'
}
