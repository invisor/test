export default {
  create: '创造属于您的完美饰品',
  ring: '绿宝石戒指',
  description:
    '我们拥有数以千计的精致、天然、未经处理的绿宝石可供选择，与我们美丽、定制设计、手工制作的底座完美镶嵌。',
  ready: '准备创作？',
  chooseSetting: '选择镶嵌',
  or: '或者',
  chooseStone: '选择一个绿宝石',
  learnMore: '了解更多'
}
