export default {
  title1: '矿业',
  title2: '地点',
  slide1: {
    title: '哥伦比亚',
    text: '展示在祖母绿丰富的哥伦比亚进行数百年采矿活动的著名历史。',
    button: '阅读更多'
  },
  slide2: {
    title: '巴西',
    text: '了解葡萄牙探险家在巴西早期发现的祖母绿，以及为什么直到 1970 年代祖母绿的生产才蓬勃发展',
    button: '阅读更多'
  },
  slide3: {
    title: '赞比亚',
    text: '了解为什么赞比亚的祖母绿产量仅次于哥伦比亚。',
    button: '阅读更多'
  }
}
