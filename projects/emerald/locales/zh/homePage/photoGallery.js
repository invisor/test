export default {
  subTitle: '奉献',
  title: '在 Instagram上访问我们',
  text: '我们的Instagram是绿宝石爱好者的梦想，在那里我们展示了有史以来所有的绿宝石珠宝，以及还没有出售的裸绿宝石！',
  button: '关注我们Instagram'
}
