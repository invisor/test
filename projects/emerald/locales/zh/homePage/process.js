export default {
  ourProcess1: '我们的 ',
  ourProcess2: ' 流程',
  button: '详细了解我们的免费设计流程',
  step1: '选择您的绿宝石',
  step2: '选择您的镶嵌',
  step3: '检查 & 完成设计',
  step4: '申请免费预览',
  step5: '申请您设计产品的3D模型',
  step5notice: '*3D模型是可选的，仅售$95'
}
