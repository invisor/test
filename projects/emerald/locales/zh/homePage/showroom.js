export default {
  title1: '访问我们的 ',
  title2: '陈列室',
  text:
    '享受独特的珠宝购买体验，在充满自然光线，舒适静谧的环境中充分享受观赏珠宝的乐趣。',
  button: '预约'
}
