export default {
  heroRing: '3.26克拉的鸽血绿绿宝石和钻石戒指，获得了GIA认证',
  collectionRings: '镶嵌有铂金密镶的绿宝石订婚戒指',
  collectionEarrings: '铂金光环镶托设计中，镶有钻石的绿宝石耳环',
  collectionWeddingBands: '绿宝石和钻石结婚戒指镶入白金和铂金中',
  collectionNecklaces: '8.88ct铂金镶嵌哥伦比亚绿宝石，密钉镶钻石',
  collectionBracelets: '赞比亚绿宝石手链，配隐形18k白色黄金镶托钻石',
  collectionStones: '来自关哥伦比亚和赞比亚的绿宝石',
  collectionPairs: '一对未经处理和认证的哥伦比亚绿宝石',
  collectionSettings: '定制的绿宝石戒指镶嵌',
  educationVideo: '了解有关哥伦比亚和赞比亚绿宝石的更多信息',
  ourStory: '天然绿宝石公司的历史',
  miningLocations: '绿宝石开采地点',
  famousEmeralds: '著名的绿宝石',
  famousQuality: '优质绿宝石',
  famousElizabeth: '伊丽莎白·泰勒绿宝石珠宝',
  packaging: '天然绿宝石公司定制包装中的绿宝石戒指'
}
