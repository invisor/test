export default {
  subscription: {
    title: '及时了解我们的更新',
    text: '订阅我们的时事邮件，了解更新和特别的优惠',
    placeholder: '邮箱地址',
    button: '注册'
  },
  bottom: {
    terms: '条款 & 条件',
    privacy: '隐私政策'
  },
  menu: {
    aboutUs: {
      title: '关于我们',
      item1: '不同之处',
      item2: '工作室',
      item3: '历史',
      item4: '陈列室',
      item5: '团队',
      item6: '技术',
      item7: '宝石实验室',
      item8: '联系我们'
    },
    faq: {
      title: '常见问题',
      item1: '常见问题',
      item2: '运输 & 保险',
      item3: '支付',
      item4: '戒指尺寸',
      item5: '退货政策',
      item6: '地址',
      item7: '安全和防诈骗政策',
      item8: '隐私政策'
    },
    education: {
      title: '相关内容',
      item1: '天然绿宝石',
      item2: '合成绿宝石',
      item3: '来源 & 质量',
      item4: '巴西绿宝石矿产',
      item5: '绿宝石的魅力/新时代',
      item6: '绿宝石的夹杂物',
      item7: '现代绿宝石首饰',
      item8: '绿宝石的保养'
    }
  }
}
