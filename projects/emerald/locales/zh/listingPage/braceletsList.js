import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound: '没有找到手链 | 找到1款手链 | {count} 款手链被找到了'
  }
}
