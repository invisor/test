import stoneTypes from '../constants/stoneTypes'

const stonesList = {
  photo: '实拍',
  weight: '克拉',
  length: '长度(MM)',
  width: '宽度 (MM)',
  shape: '形状',
  origin: '产地',
  price: '价格',
  compare: '对比',
  wishlist: '收藏'
}

const ringSettings = {
  photo: '实拍',
  metal: '金属名称',
  compare: '对比',
  wishlist: '收藏'
}

const jewelryList = {
  photo: '实拍',
  weight: '克拉',
  metal: '金属名称',
  treatment: '处理方式',
  shape: 'Shape',
  origin: '产地',
  price: '价格',
  compare: '对比',
  wishlist: '收藏'
}

const bandsList = {
  photo: '实拍',
  metalWeight: '金属重量',
  totalCarat: '总克拉数',
  metal: '金属名称',
  price: '价格',
  compare: '对比',
  wishlist: '收藏',
  style: '样式'
}

export default {
  and: '和',
  startingFrom: '开始于',
  more: '更多',
  less: '较少',
  quickView: '快速浏览',
  moreDetails: 'View Details',
  itemId: '货品编号',
  details: '详情',
  stoneTypes,
  prev: '上一页',
  next: '下一页',
  tableHeader: {
    id: '编号',
    remove: '删除',
    stonesList,
    stonePairsList: stonesList,
    ringSettings,
    earringSettings: ringSettings,
    pendantSettings: ringSettings,
    necklaceSettings: ringSettings,
    braceletsList: jewelryList,
    earringsList: jewelryList,
    necklacesPendantsList: jewelryList,
    ringsList: jewelryList,
    weddingBands: bandsList,
    weddingBandsPlain: bandsList
  }
}
