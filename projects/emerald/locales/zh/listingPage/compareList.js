import metalTypes from '../constants/metalTypes'
import stoneShapes from '../constants/stoneShapes'
import treatments from '../constants/treatments'
import origins from '../constants/filtersOrigins'
import stoneTypes from '../constants/stoneTypes'

export default {
  stones: {
    price: '价格',
    itemId: '货品编号',
    carat: '克拉',
    type: '类型'
  },
  table: {
    actualPhoto: '实拍照',
    shape: '形状',
    metalName: '金属名称',
    treatment: '处理方式',
    colorIntensity: '颜色饱和度',
    clarity: '净度',
    carat: '克拉',
    origin: '产地',
    cut: '切割',
    price: '价格',
    remove: '移除',
    wishlist: '收藏夹',
    details: '详情'
  },
  item: {
    addToSetting: '添加{0}至镶嵌',
    addToCart: '添加{0}到购物车',
    cartLink: '购物车',
    inCart: '这个货品目前已经在您的 {cartLink}'
  },
  metalTypes,
  stoneShapes,
  treatments,
  origins,
  stoneTypes,
  empty: '您的对比列表是空的！',
  dragAndDrop: '拖放以进行比较',
  totalCtWeight: '总重量：'
}
