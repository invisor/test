import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound: '没有找到耳环 | 找到1款耳环 | {count} 款耳环被找到了'
  }
}
