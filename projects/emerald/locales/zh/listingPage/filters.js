import stylesFilter from '../constants/designStylesFilter'
import filtersShapes from '../constants/filtersShapes'
import stoneShapes from '../constants/stoneShapes'
import filtersOrigins from '../constants/filtersOrigins'
import metalTypesFilter from '../constants/metalTypesFilter'
import treatmentsFilter from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import cuttingStylesFilter from '../constants/cuttingStyles'
import labColorsFilter from '../constants/labColors'
import labTypesFilter from '../constants/labTypes'
import stoneClarity from '../constants/stoneClarity'
import intensityFilter from '../constants/stoneColorIntensity'

export default {
  previewSwitcher: {
    availability: '获取',
    all: '全部',
    ready: '现货产品',
    notReady: '2-4周发货'
  },
  cuttingStylesFilter,
  labColorsFilter,
  labTypesFilter,
  stoneTypesFilter: stoneTypes,
  clarityFilter: stoneClarity,
  jewelryStyles: stylesFilter,
  centerStoneShapes: 'Center Stone Shape',
  sideStoneShapes: 'Side Stone Shape',
  cuttingStyles: '切割方式',
  labColors: 'Lab 颜色',
  labTypes: 'Lab 类型',
  caratRange: '克拉',
  caratSum: '克拉',
  clearAll: '清除所有',
  close: '关闭',
  dimensions: '尺寸',
  lengthRange: '长度 (mm)',
  widthRange: '宽度 (mm)',
  bandWidth: '宽度 (mm)',
  bandWidthRange: '宽度',
  filter: '筛选',
  sort: '排序',
  filters: '筛选条件',
  filterBy: '筛选',
  lengthSum: '长度',
  priceRange: '价格',
  priceSum: '价格',
  clarity: '净度',
  intensity: '颜色强度',
  stoneOrigins: '产地',
  stoneShapes: '宝石形状',
  stonesShapes: '形状',
  metalTypes: '金属类型',
  designStyles: '设计样式',
  widthSum: '宽度',
  selectCenterStoneShapes: '选择主石形状',
  selectSideStoneShapes: '选择副石形状',
  centerStone: '主石',
  sideStone: '副石',
  treatments: 'Treatment',
  stoneTypes: '宝石类型',
  styles: '设计风格',
  intensityFilter,
  designStylesFilter: stylesFilter,
  stylesFilter,
  bandsTypesFilter: stoneTypes,
  treatmentsFilter,
  metalTypesFilter,
  priceRangeFilter: {
    andAbove: '以上所有'
  },
  stoneShapesFilter: {
    ...stoneShapes
  },
  centerStoneShapesFilter: {
    ...stoneShapes
  },
  sideStoneShapesFilter: {
    ...filtersShapes
  },
  stoneOriginsFilter: {
    ...filtersOrigins
  },
  summary: {
    andAbove: ' 以上所有',
    caratSum: '克拉',
    lengthSum: '长度',
    widthSum: '宽度',
    clearAll: '清除所有'
  },
  stoneTypeName: stoneTypes
}
