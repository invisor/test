import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    itemsFound:
      '没有找到绿宝石对 | 找到1款绿宝石对 | {count} 款绿宝石对被找到了',
    itemsStarFound:
      '没有找到星光绿宝石对 | 找到1款星光绿宝石对 | {count} 款星光绿宝石对被找到了',
    itemsCabochonFound:
      '没有找到天然圆形绿宝石对 | 找到1款天然圆形绿宝石对 | {count} 款天然圆形绿宝石对被找到了'
  },
  pair: '对',
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的镶嵌的绿宝石对。',
  notPrefilteredItems:
    '显示 <strong>全部</strong> 绿宝石对，意味着有些绿宝石对可能和您选择的镶嵌不匹配。',
  showAllItems: '请显示全部的绿宝石对。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的绿宝石对'
}
