import stonesOrigins from '../constants/stonesOrigins'
import metalTypes from '../constants/metalTypes'
import treatments from '../constants/treatments'
import stoneCut from '../constants/stoneCut'
import stoneClarity from '../constants/stoneClarity'
import stoneColorIntensity from '../constants/stoneColorIntensity'
import bandStyle from '../constants/bandStyle'
import stoneShapes from '../constants/stoneShapes'

export default {
  sort: {
    sortBy: '排序方式',
    itemsFound: '没有找到绿宝石 | 找到1款绿宝石 | {count} 款绿宝石被找到了',
    itemsStarFound:
      '没有找到星光绿宝石 | 找到1款星光绿宝石 | {count} 款星光绿宝石被找到了',
    itemsCabochonFound:
      '没有找到天然圆形绿宝石 | 找到1款天然圆形绿宝石 | {count} 款天然圆形绿宝石被找到了',
    other: {
      featuredFirst: '精选优先',
      newFirst: '最新优先'
    },
    price: {
      lowToHigh: '价格 (由低到高)',
      highToLow: '价格 (由高到低)'
    },
    carat: {
      lowToHigh: '克拉 (由低到高)',
      highToLow: '克拉 (由高到低)'
    }
  },
  localSort: {
    sortBy: '排序方式',
    placeholder: '选择',
    itemsFound: '款绿宝石对被找到了',
    price: {
      lowToHigh: '价格 (由低到高)',
      highToLow: '价格 (由高到低)'
    },
    carat: {
      lowToHigh: '克拉 (由低到高)',
      highToLow: '克拉 (由高到低)'
    }
  },
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的镶嵌的绿宝石。',
  notPrefilteredItems:
    '显示 <strong>全部</strong>绿宝石，意味着有些绿宝石可能和您选择的镶嵌不匹配。',
  showAllItems: '请显示全部的绿宝石。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的绿宝石',
  stoneShapes,
  stonesOrigins,
  metalTypes,
  treatments,
  stoneCut,
  stoneClarity,
  stoneColorIntensity,
  bandStyle
}
