import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound:
      '没有找到素圈戒指 | 找到1款素圈戒指 | {count} 款素圈戒指被找到了'
  },
  ringSizePlaceholder: '选择戒指尺寸',
  ringSizeError: '请选择一个戒指尺寸',
  engravingPlaceholder: '免费雕刻（可选）'
}
