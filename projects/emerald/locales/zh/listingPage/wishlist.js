import metalTypes from '../constants/metalTypes'

export default {
  ringSizePlaceholder: '选择戒指尺寸',
  ringSizeError: '请选择一个戒指尺寸',
  empty: '您的收藏夹是空的！',
  stonesList: {
    addToCart: '将宝石添加到购物车',
    addToSettings: '添加到镶嵌'
  },
  stonePairsList: {
    addToCart: '添加宝石对到购物车',
    addToSettings: '添加至镶嵌'
  },
  lists: {
    stonesList: '宝石',
    stonePairsList: '宝石对',
    braceletsList: '手链',
    earringsList: '耳环',
    necklacesPendantsList: '项链和吊坠',
    ringsList: '戒指',
    weddingBands: '结婚戒指',
    weddingBandsPlain: '素圈结婚戒指',
    ringSettings: '戒指镶嵌',
    earringSettings: '耳环镶嵌',
    pendantSettings: '吊坠镶嵌',
    necklaceSettings: '项链镶嵌',
    custom: '完成设计'
  },
  item: {
    itemId: '货品编号',
    weight: '{0}克拉',
    pair: '宝石对',
    centerStone: '主宝石',
    totalWeight: '{0}总重量',
    itemToChart: '添加{0}到购物车',
    itemToJewelry: '添加{0}到首饰',
    stoneToChart: '添加{0}到购物车',
    createJewelry: '添加到镶嵌',
    cartLink: '购物车',
    inCart: '这个货品目前在您的 {cartLink}',
    toReview: '查看&添加到购物车',
    reviewBand: '查看&添加到购物车',
    reviewSetting: '查看&添加到购物车',
    addToSetting: '添加{0}到购物车',
    addToCart: '添加{0}到购物车',
    addToReview: '查看&添加到购物车'
  },
  metalTypes
}
