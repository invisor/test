import { companyName } from '~~/utils/definitions/defaults.js'

export default {
  index: {
    seo: {
      title: `${
        companyName[process.env.siteName]
      } - Emerald Rings & Jewelry Since 1939`,
      description:
        '自1939年以来,我们就是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '首页'
  },
  'custom-design-emerald-rings-jewelry': {
    seo: {
      title: '在线设计定制绿宝石订婚戒指和珠宝',
      description:
        '想要与众不同吗？让我们帮助您或亲人打造完美的定制绿宝石订婚戒指、耳环、项链或吊坠。'
    },
    navPageTitle: '了解更多'
  },
  'design-your-own-setting-mens-ring-settings': {
    seo: {
      title: "Custom Emerald Men's Ring Settings",
      description:
        "With dozens of bold ring settings to choose from - specifically designed for the modern man - we'e confident you'll find the perfect men's emerald ring setting for any occasion."
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-engagement-ring-settings': {
    seo: {
      title: 'Custom Emerald Engagement Ring Settings',
      description:
        'Custom made emerald engagement ring settings. We have hundreds of design styles from three stone ring, pave diamonds, and solitaire to name a few. Our emerald ring settings can be created in white and yellow gold, platinum, and palladium.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-earring-settings': {
    seo: {
      title: 'Custom Emerald Earring Settings',
      description:
        'From simple to elaborate earring settings, we offer it all! Choose among a wide variety of earring designs to fit your emerald pair.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-necklace-pendant-settings': {
    seo: {
      title: 'Custom Emerald Necklace & Pendant Settings',
      description:
        "Choose one of our emerald necklace or pendant settings to accentuate any ensemble. With our simple or elaborate designs, you'll find everything you're looking for!"
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for emerald rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-emerald-earring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-emerald-necklace-pendant-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-emerald-engagement-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-mens-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom emerald {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'emerald-jewelry-id': {
    seo: {
      title: 'Emerald {type} - {shape} {carat} Ct. - {metal} #{id}',
      description:
        '这个精美的 {carat} 克拉、{shape} 绿宝石 {type} (#{id}) 镶嵌在 {metal}中，将会是您生命中特别的人的完美绿宝石首饰。'
    },
    navPageTitle: '绿宝石 {type} {metal}'
  },
  emeralds: {
    seo: {
      title: '绿宝石裸石',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '绿宝石裸石',
    navPageDescr:
      '历经几代人的努力我们才获得来自世界各地的祖母绿系列,每颗祖母绿都经过我们认证的宝石学家的检验，以保证其真实性、耐用性和任何处理方法的披露。'
  },
  'emeralds-colombian-emeralds': {
    seo: {
      title: '哥伦比亚绿宝石',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '哥伦比亚绿宝石',
    navPageDescr:
      '哥伦比亚祖母绿是世界上最著名的祖母绿产地。 来自哥伦比亚的祖母绿具有与其他宝石不同的颜色，其纯度和活力令人着迷。'
  },
  'emeralds-zambian-emeralds': {
    seo: {
      title: '赞比亚绿宝石',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '赞比亚绿宝石',
    navPageDescr:
      '赞比亚祖母绿已成为现代伟大的宝石学发现之一。 地球上没有任何地方能够提供如此稳定的质量和数量的宝石级祖母绿。赞比亚祖母绿简直令人难以置信。'
  },
  'emerald-jewelry-colombian-emerald-rings': {
    seo: {
      title: '哥伦比亚绿宝石戒指',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '哥伦比亚绿宝石戒指',
    navPageDescr:
      '哥伦比亚祖母绿是世界上最著名的祖母绿产地。 哥伦比亚的祖母绿戒指具有与其他宝石不同的颜色，其纯度和活力令人着迷。'
  },
  'emerald-jewelry-zambian-emerald-rings': {
    seo: {
      title: '赞比亚绿宝石戒指',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '赞比亚绿宝石戒指',
    navPageDescr:
      '赞比亚的祖母绿通常被描述为地球上任何祖母绿中质量最好的深绿色，属于自己的一类，并在世界各地继续流行。赠送赞比亚祖母绿戒指。'
  },
  'emeralds-emerald-cabochon': {
    seo: {
      title: '哥伦比亚绿宝石',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '哥伦比亚绿宝石',
    navPageDescr:
      '凸圆形祖母绿是自古以来使用的最古老、最令人垂涎的宝石之一。从埃及人到国王和王后的皇家珠宝，这款简单的凸圆形祖母绿切工至今仍是稀有祖母绿宝石中备受喜爱的切工。点击<a href="/education/all-about-emerald-cabochons/">这里</a>了解更多信息.'
  },
  'emeralds-gemologist-recommended': {
    seo: {
      title: '宝石专家推荐',
      description:
        '我们提供种类繁多的美丽天然祖母绿，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的祖母绿。'
    },
    navPageTitle: '宝石专家推荐',
    navPageDescr:
      '我们的宝石专家每年都会查看数千颗绿宝石，他们对宝石的信息非常挑剔。在这部分，我们的宝石专家会在清单中推荐一些他们个人的最爱。'
  },
  'emeralds-id': {
    seo: {
      title: '绿宝石裸石 - {shape} {carat} Ct. - #{id}',
      description:
        '选择这个天然的{carat}克拉{shape}来自{origin} (#{id})的绿宝石，为您或所爱的人打造完美的珠宝。'
    },
    navPageTitle: '绿宝石'
  },
  'emeralds-matched-pairs-of-emeralds': {
    seo: {
      title: '绿宝石对',
      description:
        '我们提供多种多样的美丽天然绿宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的绿宝石。'
    },
    navPageTitle: '绿宝石对',
    navPageDescr:
      '由于颜色变化的方式，匹配有色宝石可能非常困难。 尽管只有一种颜色，但祖母绿的范围从与哥伦比亚祖母绿相关的黄绿色到与赞比亚祖母绿相关的蓝绿色。'
  },
  'emeralds-matched-pairs-of-emeralds-cabochon': {
    seo: {
      title: '凸圆形绿宝石对',
      description:
        '成对的凸圆形祖母绿呈现出祖母绿晶体的纯绿色，凸圆形祖母绿宝石对在净度和色调上不易搭配，我们的一对经过精心挑选，成为许多美丽耳环和项链的搭档，要了解有关祖母绿凸圆形宝石对的更多信息，请单击<a href="/education/all-about-emerald-cabochons/">此处</a>。'
    },
    navPageTitle: '凸圆形绿宝石对',
    navPageDescr:
      '一对凸圆形祖母绿不寻常且难以匹配的，以下是展示天然祖母绿中这种美丽切工的一系列选项，点击<a href="/education/all-about-emerald-cabochons/">这里</a>了解更多关于凸圆形祖母绿的信息。'
  },
  'emeralds-matched-pairs-of-emeralds-id': {
    seo: {
      title: '绿宝石对 - {shape} {carat} Ct. - #{id}',
      description:
        '选择这颗天然的{carat} 克拉、 来自{origin} (#{id})的{shape}绿宝石对，为您或您所爱的人打造完美的珠宝。'
    },
    navPageTitle: '绿宝石对'
  },
  'emerald-jewelry-engagement-emerald-rings': {
    seo: {
      title: '绿宝石订婚戒指',
      description:
        '为您所爱的人寻找完美的订婚戒指，我们提供精心制作的手工绿宝石订婚戒指，或设计您自己独特的绿宝石戒指！'
    },
    navPageTitle: '绿宝石订婚戒指',
    navPageDescr:
      '我们提供来自哥伦比亚、赞比亚、阿富汗、巴西、埃塞俄比亚和马达加斯加的优质祖母绿，每一颗都是独一无二的。 我们专门为有眼光的人定制定制祖母绿订婚戒指。'
  },
  'emerald-jewelry-emerald-rings': {
    seo: {
      title: '绿宝石戒指',
      description:
        '为您所爱的人寻找完美的绿宝石戒指。 我们提供专业制作的手工绿宝石戒指，或设计您自己独特的绿宝石戒指！'
    },
    navPageTitle: '绿宝石戒指',
    navPageDescr:
      '我们提供来自哥伦比亚、赞比亚、阿富汗、巴西、埃塞俄比亚和马达加斯加的祖母绿，每一种都是独一无二的。 无论是订婚戒指、结婚戒指还是任何其他场合，我们都专注于祖母绿。'
  },
  'emerald-jewelry-mens-emerald-rings': {
    seo: {
      title: "Men's Emerald Rings",
      description:
        'Find the perfect emerald ring for your loved one. We offer expertly crafted, handmade emerald rings, or design your own unique emerald ring!'
    },
    navPageTitle: "Men's Emerald Rings",
    navPageDescr:
      "Browse hundreds of men's emerald rings using emeralds from Colombia, Zambia and Afghanistan. We specialize in emerald rings for Vedic astrology."
  },
  'emerald-wedding-rings-bands': {
    seo: {
      title: '绿宝石结婚戒指',
      description:
        '探索我们专业设计和手工制作的祖母绿和钻石结婚戒指，以及镶嵌所有贵金属类型的祖母绿和钻石的可叠放结婚戒指。'
    },
    navPageTitle: '绿宝石结婚戒指',
    navPageDescr:
      '我们提供镶嵌不同形状和尺寸钻石的祖母绿结婚戒指，款式多样。 这包括祖母绿永恒带和可堆叠在铂等贵金属的选择中。'
  },
  'emerald-wedding-rings-bands-id': {
    seo: {
      title: '{metal}绿宝石戒指#{id}{metalTypeCode}',
      description:
        '将这款精美的 {metal} 绿宝石戒指 (#{id}{metalTypeCode}) 制作成完美的结婚戒指，制作成您独一无二的绿宝石订婚戒指。'
    },
    navPageTitle: '{metal}绿宝石婚戒'
  },
  'wedding-bands-without-gemstone': {
    seo: {
      title: '素圈婚戒',
      description:
        '素圈的结婚戒指和带宝石的戒指都是经典设计，但又不简单，从纹理金属或双色调金属，到抛光或哑光设计选项，我们应有尽有！'
    },
    navPageTitle: '素圈婚戒',
    navPageDescr:
      '我们有一系列的手工戒指，有古典的、现代简约的，可设计成不同的纹理，两种不同的金属颜色，还有各种贵金属的材质。'
  },
  'wedding-bands-without-gemstone-id': {
    seo: {
      title: '{metal} 素圈戒指 #{id}{metalTypeCode}',
      description:
        '将这款精美的 {metal}素圈戒指(#{id}{metalTypeCode}) 制成完美的结婚戒指，以匹配您独一无二的绿宝石订婚戒指。'
    },
    navPageTitle: '{metal}素圈戒指'
  },
  'design-your-own': {
    seo: {
      title: '经过简单的三个步骤，创建您的定制绿宝石戒指',
      description:
        '开始简单的三个步骤，创建您的定制绿宝石戒指,可以为您打造完美的绿宝石戒指。'
    },
    navPageTitle: '设计您自己的戒指'
  },
  'design-your-own-stones-single': {
    seo: {
      title: '选择一颗绿宝石来定制您的完美绿宝石首饰',
      description:
        '在拥有世界上最大的天然绿宝石收藏中,一定能为您或您的爱人找到完美的绿宝石'
    },
    navPageTitle: '绿宝石裸石'
  },
  'design-your-own-stones-pair': {
    seo: {
      title: '选择一颗绿宝石来打造您的完美绿宝石首饰 ',
      description:
        '搜索我们精心挑选的绿宝石对,为您找到完美的珠宝套装。你会看到各种各样的色调、色调、颜色和形状，让我们开始吧!'
    },
    navPageTitle: '绿宝石对'
  },
  'design-your-own-stones-single-filterName-filterValue': {
    seo: {
      title: '选择一颗绿宝石来打造您的完美绿宝石首饰',
      description:
        '在拥有世界上最大的天然绿宝石收藏中,一定能为您或您的爱人找到完美的绿宝石'
    },
    navPageTitle: '绿宝石裸石'
  },
  'design-your-own-stones-pair-filterName-filterValue': {
    seo: {
      title: '绿宝石对',
      description:
        '搜索我们精心挑选的绿宝石对,为您找到完美的珠宝套装。你会看到各种各样的色调、色调、颜色和形状，让我们开始吧!'
    },
    navPageTitle: '绿宝石对'
  },
  'design-your-own-review-id': {
    seo: {
      title: '{stoneType} {category} {weight} {metalName}',
      description:
        '想要与众不同吗? 当您最终确定您的绿宝石宝石和镶嵌选项时,让我们帮助您创建完美的定制绿宝石{fullType}。'
    },
    navPageTitle: '完成的{type}'
  },
  'emerald-jewelry-emerald-earrings': {
    seo: {
      title: '绿宝石耳环',
      description:
        '库存有数百种设计，找到简单的祖母绿耳钉到绿镶嵌上形成祖母绿耳环，立即浏览，找到最适合您的！'
    },
    navPageTitle: '绿宝石耳环',
    navPageDescr:
      '祖母绿耳环活泼夺目，我们的款式包括祖母绿耳钉、钻石光环、耳坠等，我们的祖母绿耳环从颜色到净度都经过专业匹配，并放置在令人眼花缭乱的环境中。'
  },
  'emerald-jewelry-emerald-necklaces-pendants': {
    seo: {
      title: '绿宝石项链 & 吊坠',
      description:
        '搜索我们多种多样的绿宝石项链和吊坠，包括经典款式和现代设计。我们为包括您在内的所有顾客提供独一无二的项链！'
    },
    navPageTitle: '绿宝石项链 & 吊坠',
    navPageDescr:
      '我们提供多种设计风格的祖母绿项链和吊坠，从简单的包边镶嵌吊坠到由多种贵金属制成的钻石光环。'
  },
  'emerald-jewelry-emerald-bracelets': {
    seo: {
      title: '绿宝石手链',
      description:
        '用我们的祖母绿手链寻找永恒的优雅外观，多种设计和颜色组合，你一定会找到你梦想中的手链。'
    },
    navPageTitle: '绿宝石手链',
    navPageDescr:
      '我们在祖母绿手链中提供完美匹配的祖母绿，镶嵌在各种贵金属中，包括铂金、白金、黄金，甚至玫瑰金。 祖母绿和钻石手链是我们的专长。'
  },
  cart: {
    seo: {
      title: '购物车',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-index': {
    seo: {
      title: '购物车',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-order-summary': {
    seo: {
      title: '订单详情',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '订单详情'
  },
  'cart-index-secure-payment': {
    seo: {
      title: '安全支付',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-index-payment-order': {
    seo: {
      title: '支付订单',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  account: {
    seo: {
      title: '账号',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '我的账号'
  },
  'account-notifications': {
    seo: {
      title: '邮件通知',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '通知'
  },
  'account-recommendations': {
    seo: {
      title: '为您推荐',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '为您推荐'
  },
  'account-personal-details': {
    seo: {
      title: '个人 & 登录详情',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '个人 & 登录详情'
  },
  'account-preview-request-submissions': {
    seo: {
      title: '预览请求提交',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '预览请求提交'
  },
  'account-address-book': {
    seo: {
      title: '地址簿',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '地址簿'
  },
  'account-orders': {
    seo: {
      title: '查看历史订单',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '订单'
  },
  'account-change-password': {
    seo: {
      title: '修改密码',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '修改密码'
  },
  'account-reset-password': {
    seo: {
      title: '重置密码',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '重置密码'
  },
  'account-reset-password-token': {
    seo: {
      title: '重置密码',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '重置密码'
  },
  wishlist: {
    seo: {
      title: '最喜欢的绿宝石',
      description:
        '将您最喜爱的绿宝石裸石、镶嵌、绿宝石对和成品绿宝石首饰放在一起。 在选择之前可以将它们进行比较！'
    },
    navPageTitle: '收藏夹'
  },
  'compare-list': {
    seo: {
      title: '对比列表',
      description:
        '自 1939 年以来,我们一直是绿宝石订婚戒指、天然绿宝石和绿宝石首饰的权威,我们拥有最多的在线绿宝石收藏。'
    },
    navPageTitle: '对比列表'
  },
  'contact-us': {
    seo: {
      title: '聊天',
      description:
        '我们的绿宝石和珠宝专家团队可通过实时聊天、电话和电子邮件联系，立即联系我们获取帮助。'
    },
    navPageTitle: '联系天然绿宝石公司'
  },
  showroom: {
    seo: {
      title: '参观天然绿宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的天然绿宝石公司陈列室,寻找您心仪的绿宝石或绿宝石首饰。'
    },
    navPageTitle: '参观天然绿宝石公司的陈列室'
  },
  'showroom-preview': {
    seo: {
      title: '参观天然绿宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的天然绿宝石公司陈列室,寻找您心仪的绿宝石或绿宝石首饰。'
    },
    navPageTitle: '参观天然绿宝石公司的陈列室'
  },
  'showroom-preview-success': {
    seo: {
      title: '参观天然绿宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的天然绿宝石公司陈列室,寻找您心仪的绿宝石或绿宝石首饰。'
    },
    navPageTitle: '参观天然绿宝石公司的陈列室'
  },
  'our-staff': {
    seo: {
      title: '认识我们的团队',
      description: '详细了解在绿宝石公司成就一切的才华横溢的专业人士。'
    },
    navPageTitle: '我们的团队'
  },
  'michael-arnstein': {
    seo: {
      title: 'Michael Arnstein',
      description: '详细了解在绿宝石公司成就一切的才华横溢的专业人士。'
    },
    navPageTitle: 'Michael Arnstein',
    navPageDescr: '天然绿宝石公司董事长'
  },
  auth: {
    seo: {
      title: '注册账号',
      description:
        '注册一个帐户,可以跟踪您的订单并将感兴趣的物品保存到您的收藏夹中,以便以后在绿宝石网站上下单'
    },
    navPageTitle: '天然绿宝石公司 - 注册账号'
  },
  'education-index-category-slug': {
    seo: {
      title: '文化',
      description: ''
    },
    navPageTitle: '天然绿宝石公司'
  }
}
