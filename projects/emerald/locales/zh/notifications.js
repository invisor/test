export default {
  cartItemAdded: '{0}已成功添加到您的购物车',
  cartItemUpdated: '{0}{1}已在您的购物车中更新',
  cartItemRemoved: '{0}{1}已从您的购物车中删除',
  wishlistItemUpdated: '{0}已在您的收藏夹中更新',
  wishlistItemAdded: '{0}已成功添加到您的收藏夹',
  wishlistItemRemoved: '{0}已从您的收藏夹中删除',
  compareListItemUpdated: '{0}{1}已在您的对比列表中更新',
  compareListItemAdded: '{0}已成功添加到您的对比列表',
  compareListRemoved: '{0}已从您的对比列表中删除',
  accountUpdated: '您的帐号已成功更新',
  shippingAddressUpdated: '您的邮寄地址已成功更新',
  billingAddressUpdated: '您的账单地址已成功更新',
  loggedOut: '您现在已退出',
  loggedIn: '您已登录',
  tokenExpired: '您的会话已过期，请登录',
  taxAdded: '{0} 州的税已添加到您的订单中',
  taxRemoved: '州税已从您的订单中删除',
  backendCalc: '最终税额将在订单提交后计算',
  wrongAddress: '错误的收货地址'
}
