export default {
  facebook: '通过Facebook分享',
  twitter: '通过Twitter分享',
  googlePlus: '通过Google plus分享',
  pinterest: '通过Pinterest分享',
  email: '通过邮箱分享',
  copyLink: '复制链接',
  linkCopied: '链接复制到粘贴板',
  linkCopyError: '出错了，请手动复制链接'
}
