export default {
  header: {
    visitTitle: '参观天然绿宝石公司',
    messageTitle: '纽约陈列室',
    messageText:
      '我们在曼哈顿办公室提供特殊的购买体验，我们的陈列室位于市中心办公楼的顶层，为客人打造了一个舒适私密的环境，让大家能够观赏我们丰富的绿宝石、 参观我们的陈列室和办公室，客户可以亲眼目睹我们如何将最先进的技术与古老的手工制作和设计珠宝艺术相结合。',
    city: '美国，纽约',
    security: '安全须知',
    review: '请所有顾客查看我们的{link} ',
    galleryButton: '展开陈列室画廊'
  },
  sectionOne: {
    addressTitle: '陈列室开放时间及地址',
    detailsTitle: '您的详细信息',
    companyName: '天然绿宝石公司',
    address:
      'Head Office, Showroom & Workshop<br>6 East 45th Street, 20th Floor<br>New York, NY 10017<br>United States',
    viewMap: '查看地图',
    getDirections: '获取路线',
    whenTitle: '您想什么时候参观我们的陈列室？',
    clockLabel: '输入您想参观的时间'
  },
  sectionTwo: {
    sectionTitle: '你具体想看哪些的商品？',
    message:
      '在下方选择或输入商品编号，您可以在商品页面详情页中找到商品号，或者从您的 {收藏夹} 或 {对比列表} 列表中添加一个列表',
    itemsAdded: '{0}/{1} 已添加',
    fromFavorites: '收藏夹',
    fromCompare: '对比列表',
    selectItemsPlaceholder: '选择商品'
  },
  sectionThree: {
    sectionTitle: '您是否有其他问题或特殊需求？'
  },
  sectionFour: {
    sectionTitle: '您是否注册了账户？',
    sectionTitleAuth: '您是否已经填写完您的需求？',
    reviewButton: '查看我填写的内容',
    submitButton: '提交展厅预定',
    loginTitle: '登录以完成您的提交',
    authMessage:
      '请检查您是否对上述详细信息感到满意，如果是，请继续查看您的请求。',
    loginMessage:
      '登录后，您可以查看您安排的其他展厅参观，还允许您更改或取消任何展厅参观请求或已经确认的到访。',
    guestTitle: '以访客的身份继续',
    guestMessage:
      '如果您在我们网站没有注册账户，我们建议您{signUp}，您可以更加方便的进行预约，查看并管理您的陈列室参观，或者，您也可以以访客的身份继续提交请求。',
    signUp: '注册账户',
    or: '或者'
  },
  preview: {
    previewTitle: '请检查您的陈列室参观请求',
    previewMessage: '在提交之前，请确保您输入的详细信息正确无误',
    details: '您的请求详细信息',
    contactDetails: '您的请求详细信息',
    date: '日期：',
    time: '时间：',
    salesPerson: '指定销售员：',
    back: '返回并修改我的请求'
  },
  success: {
    successTitle: '您的请求已经被提交！',
    successMessage: '我们的销售人员将尽快与您联系，确认您的陈列室参观',
    details: '您的请求详细信息',
    contactDetails: '',
    back: '请求取消我的访问'
  },
  securityDialog: {
    title: '陈列室，办公室和工作室的安全要求',
    intro: `<p>感谢您有兴趣参观我们的陈列室！我们特别渴望您能亲自来参观我们的天然绿宝石珠宝，当您访问我们的展厅时，您可以近距离地了解我们团队的日常工作流程。您会看到我们的工作人员在处理绿宝石、镶嵌和成品珠宝，为每位顾客提供完美的戒指、耳环、项链或吊坠。</p>
    <p>我们希望见到您，但是，出于安全原因，每位前来拜访的客人都需要提前预约。如果您计划访问我们的办公室，请查看下面的安全措施。</p>`,
    title1: '我们重视您的隐私！',
    message1:
      '<p>您的个人信息不会与任何第三方共享，您的个人信息将被严格保密以确保给您带来安全而愉快的购买体验。</p>',
    title2: '陈列室开放时间: {0}',
    message2: `<ol><li>提醒：请不要擅自进入。 如果您在附近请电话联系我们。</li>
    <li>每位访客都必须出示有效的政府证件 <strong>带有照片和编号</strong> 例如驾驶证或者护照。</li>
    <li>您的照片编号将被扫描进我们的安全记录。</li>
    <li>参观者通过金属探测器进行扫描，并对袋子进行检查。</li></ol>`,
    message21:
      '<p><strong>再次返场的游客？</strong> 我们很抱歉您仍然需要再次进行上述的安检才能进入陈列室</p>',
    title3: '关于我们的安全实践的注意事项',
    message3:
      '<p>我们与第三方合作，始终确保我们的办公室和展厅安全可靠。 我们的24/7武装警卫安全和监视服务包括但不限于:</p>',
    message31: `<ul>
    <li>16个带音频的高分辨率摄像机。</li>
    <li>10个运动传感器。.</li>
    <li>18个门窗传感器</li>
    <li>6个特殊位置的振动传感器。</li>
    <li>10个无声呼叫按钮 - 快速响应警察和第三方武装警卫</li>
    <li>3个具有保护功能的双安全门空间</li>
    <li>1个带2英寸厚防弹玻璃的进气窗</li>
    <li>钢筋墙和门的布置</li>
    <li>高安全性办公楼</li>
    <li>全职武装保安人员。 一周7天，每天24小时。</li>
    </ul>`,
    message32:
      '<p>如果有需要，我们的24/7安全监控公司会立即响应。 远程访问允许即时监控和报告，提供细致的安全细节。</p>',
    message33:
      '<p><strong>注意</strong>: 出于安全原因，在展厅时间内，随时都有武装警卫在场。</p>'
  },
  form: {
    customerName: '姓名',
    phone: '电话',
    email: '邮箱',
    date: '日期',
    time: '时间',
    itemIds: '产品编号',
    message: '留言',
    submit: '提交',
    salesPerson: '选择销售人员',
    medicalMask: '您希望销售人员佩戴口罩吗？',
    medicalMaskYes: '我希望销售人员佩戴着口罩',
    medicalMaskNo: '不佩戴口罩也可以',
    itemIdPlaceholder: '请开始输入产品编号'
  },
  swal: {
    title: '成功',
    text: '您的请求已经被提交',
    ok: '好的'
  },
  formErrors: {
    customerName: '请输入姓名',
    phone: '请输入电话',
    email: {
      required: '请输入邮箱',
      valid: '请输入正确的邮箱地址'
    },
    date: '请输入日期',
    time: '请输入时间',
    itemIds: '请选择商品'
  },
  images: {
    image1:
      '继续探索现场进行的技术和工艺，我们与专业人员一起访问，使NEC成为行业的领导者。',
    image2:
      '抵达后，您将进入我们明亮而愉快的顶层沙龙展厅，糅合令人印象深刻的新古典主义建筑和设计元素。',
    image3:
      '在您的私人宝石专家陪同下，您将被护送到私人房间，充分享受个人空间。',
    image4:
      '在我们的陈列室中，您将享受轻松的氛围，我们的工作人员将帮助您轻松探索珠宝的奥秘。',
    image5: '我们的展厅有充足的空间和设施，并且充分考虑游客的隐私。',
    image6: '您可以将宝石带到我们的阳台上，在自然阳光下欣赏它的所有荣耀。',
    image7: '您可以将宝石带到我们的阳台上，在自然阳光下欣赏它的所有荣耀。'
  }
}
