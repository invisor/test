import metalTypes from './constants/metalTypesFilter'
import stoneShapes from './constants/stoneShapes'
import treatments from './constants/treatments'
import origins from './constants/filtersOrigins'
import stoneTypes from './constants/stoneTypes'

export default {
  metalTypes,
  stoneShapes,
  treatments,
  origins,
  stoneTypes,
  hero: {
    title: '收藏夹',
    message: '请创建一个帐号或登录，以保存货品到您的收藏夹',
    loginButton: '登录 / 注册'
  },
  lists: {
    stonesList: '绿宝石',
    stonePairsList: '绿宝石对',
    braceletsList: '手链',
    earringsList: '耳环',
    necklacesPendantsList: '项链和吊坠',
    ringsList: '戒指',
    weddingBands: '结婚戒指',
    weddingBandsPlain: '素圈戒指',
    ringSettings: '戒指镶嵌',
    earringSettings: '耳环镶嵌',
    pendantSettings: '吊坠镶嵌',
    necklaceSettings: '项链镶嵌',
    custom: '完成设计'
  },
  item: {
    itemId: '货品编号',
    designId: '设计编号:',
    weight: '{0} 克拉',
    pair: '绿宝石对',
    centerStone: '主石',
    totalWeight: '{0} 总重量',
    itemToChart: '添加 {0} 到购物车',
    itemToJewelry: '添加 {0} 到珠宝',
    stoneToChart: '添加 {0} 到购物车',
    createJewelry: '添加到镶嵌',
    cartLink: '购物车',
    inCart: '这个产品目前在您的 {cartLink}',
    viewItem: '查看产品',
    toReview: '查看 & 添加到购物车',
    reviewBand: '查看 & 添加到购物车',
    reviewSetting: '查看 & 添加到购物车'
  },
  continueShopping: '继续购物',
  empty: '您的收藏夹是空的！'
}
