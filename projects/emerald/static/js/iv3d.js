glMatrixArrayType =
  typeof Float32Array != 'undefined'
    ? Float32Array
    : typeof WebGLFloatArray != 'undefined'
    ? WebGLFloatArray
    : Array
var vec3 = {}
vec3.create = function(a) {
  var b = new glMatrixArrayType(3)
  if (a) {
    b[0] = a[0]
    b[1] = a[1]
    b[2] = a[2]
  }
  return b
}
vec3.set = function(a, b) {
  b[0] = a[0]
  b[1] = a[1]
  b[2] = a[2]
  return b
}
vec3.add = function(a, b, c) {
  if (!c || a == c) {
    a[0] += b[0]
    a[1] += b[1]
    a[2] += b[2]
    return a
  }
  c[0] = a[0] + b[0]
  c[1] = a[1] + b[1]
  c[2] = a[2] + b[2]
  return c
}
vec3.subtract = function(a, b, c) {
  if (!c || a == c) {
    a[0] -= b[0]
    a[1] -= b[1]
    a[2] -= b[2]
    return a
  }
  c[0] = a[0] - b[0]
  c[1] = a[1] - b[1]
  c[2] = a[2] - b[2]
  return c
}
vec3.scale = function(a, b, c) {
  if (!c || a == c) {
    a[0] *= b
    a[1] *= b
    a[2] *= b
    return a
  }
  c[0] = a[0] * b
  c[1] = a[1] * b
  c[2] = a[2] * b
  return c
}
vec3.normalize = function(a, b) {
  b || (b = a)
  var c = a[0],
    d = a[1],
    e = a[2],
    g = Math.sqrt(c * c + d * d + e * e)
  if (g) {
    if (g == 1) {
      b[0] = c
      b[1] = d
      b[2] = e
      return b
    }
  } else {
    b[0] = 0
    b[1] = 0
    b[2] = 0
    return b
  }
  g = 1 / g
  b[0] = c * g
  b[1] = d * g
  b[2] = e * g
  return b
}
vec3.cross = function(a, b, c) {
  c || (c = a)
  var d = a[0],
    e = a[1]
  a = a[2]
  var g = b[0],
    f = b[1]
  b = b[2]
  c[0] = e * b - a * f
  c[1] = a * g - d * b
  c[2] = d * f - e * g
  return c
}
vec3.length = function(a) {
  var b = a[0],
    c = a[1]
  a = a[2]
  return Math.sqrt(b * b + c * c + a * a)
}
vec3.dot = function(a, b) {
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]
}
var mat3 = {}
mat3.create = function(a) {
  var b = new glMatrixArrayType(9)
  if (a) {
    b[0] = a[0]
    b[1] = a[1]
    b[2] = a[2]
    b[3] = a[3]
    b[4] = a[4]
    b[5] = a[5]
    b[6] = a[6]
    b[7] = a[7]
    b[8] = a[8]
    b[9] = a[9]
  }
  return b
}
mat3.identity = function(a) {
  a[0] = 1
  a[1] = 0
  a[2] = 0
  a[3] = 0
  a[4] = 1
  a[5] = 0
  a[6] = 0
  a[7] = 0
  a[8] = 1
  return a
}
var mat4 = {}
mat4.create = function(a) {
  var b = new glMatrixArrayType(16)
  if (a) {
    b[0] = a[0]
    b[1] = a[1]
    b[2] = a[2]
    b[3] = a[3]
    b[4] = a[4]
    b[5] = a[5]
    b[6] = a[6]
    b[7] = a[7]
    b[8] = a[8]
    b[9] = a[9]
    b[10] = a[10]
    b[11] = a[11]
    b[12] = a[12]
    b[13] = a[13]
    b[14] = a[14]
    b[15] = a[15]
  }
  return b
}
mat4.identity = function(a) {
  a[0] = 1
  a[1] = 0
  a[2] = 0
  a[3] = 0
  a[4] = 0
  a[5] = 1
  a[6] = 0
  a[7] = 0
  a[8] = 0
  a[9] = 0
  a[10] = 1
  a[11] = 0
  a[12] = 0
  a[13] = 0
  a[14] = 0
  a[15] = 1
  return a
}
mat4.m = function(b, a, c) {
  c || (c = b)
  var d = a[0],
    e = a[1],
    g = a[2],
    f = a[3],
    h = a[4],
    i = a[5],
    j = a[6],
    k = a[7],
    l = a[8],
    o = a[9],
    m = a[10],
    n = a[11],
    p = a[12],
    r = a[13],
    s = a[14]
  a = a[15]
  var A = b[0],
    B = b[1],
    t = b[2],
    u = b[3],
    v = b[4],
    w = b[5],
    x = b[6],
    y = b[7],
    z = b[8],
    C = b[9],
    D = b[10],
    E = b[11],
    q = b[12],
    F = b[13],
    G = b[14]
  b = b[15]
  c[0] = A * d + B * h + t * l + u * p
  c[1] = A * e + B * i + t * o + u * r
  c[2] = A * g + B * j + t * m + u * s
  c[3] = A * f + B * k + t * n + u * a
  c[4] = v * d + w * h + x * l + y * p
  c[5] = v * e + w * i + x * o + y * r
  c[6] = v * g + w * j + x * m + y * s
  c[7] = v * f + w * k + x * n + y * a
  c[8] = z * d + C * h + D * l + E * p
  c[9] = z * e + C * i + D * o + E * r
  c[10] = z * g + C * j + D * m + E * s
  c[11] = z * f + C * k + D * n + E * a
  c[12] = q * d + F * h + G * l + b * p
  c[13] = q * e + F * i + G * o + b * r
  c[14] = q * g + F * j + G * m + b * s
  c[15] = q * f + F * k + G * n + b * a
  return c
}
mat4.mulPoint = function(a, b, c) {
  c || (c = b)
  var d = b[0],
    e = b[1]
  b = b[2]
  c[0] = a[0] * d + a[4] * e + a[8] * b + a[12]
  c[1] = a[1] * d + a[5] * e + a[9] * b + a[13]
  c[2] = a[2] * d + a[6] * e + a[10] * b + a[14]
  return c
}
mat4.mulVector = function(a, b, c) {
  c || (c = b)
  var d = b[0],
    e = b[1]
  b = b[2]
  c[0] = a[0] * d + a[4] * e + a[8] * b
  c[1] = a[1] * d + a[5] * e + a[9] * b
  c[2] = a[2] * d + a[6] * e + a[10] * b
  return c
}
mat4.rotate = function(a, b, c, d) {
  var e = c[0],
    g = c[1]
  c = c[2]
  var f = Math.sqrt(e * e + g * g + c * c)
  if (!f) {
    return null
  }
  if (f != 1) {
    f = 1 / f
    e *= f
    g *= f
    c *= f
  }
  var h = Math.sin(b),
    i = Math.cos(b),
    j = 1 - i
  b = a[0]
  f = a[1]
  var k = a[2],
    l = a[3],
    o = a[4],
    m = a[5],
    n = a[6],
    p = a[7],
    r = a[8],
    s = a[9],
    A = a[10],
    B = a[11],
    t = e * e * j + i,
    u = g * e * j + c * h,
    v = c * e * j - g * h,
    w = e * g * j - c * h,
    x = g * g * j + i,
    y = c * g * j + e * h,
    z = e * c * j + g * h
  e = g * c * j - e * h
  g = c * c * j + i
  if (d) {
    if (a != d) {
      d[12] = a[12]
      d[13] = a[13]
      d[14] = a[14]
      d[15] = a[15]
    }
  } else {
    d = a
  }
  d[0] = b * t + o * u + r * v
  d[1] = f * t + m * u + s * v
  d[2] = k * t + n * u + A * v
  d[3] = l * t + p * u + B * v
  d[4] = b * w + o * x + r * y
  d[5] = f * w + m * x + s * y
  d[6] = k * w + n * x + A * y
  d[7] = l * w + p * x + B * y
  d[8] = b * z + o * e + r * g
  d[9] = f * z + m * e + s * g
  d[10] = k * z + n * e + A * g
  d[11] = l * z + p * e + B * g
  return d
}
mat4.frustum = function(a, b, c, d, e, g, f) {
  f || (f = mat4.create())
  var h = b - a,
    i = d - c,
    j = g - e
  f[0] = (e * 2) / h
  f[1] = 0
  f[2] = 0
  f[3] = 0
  f[4] = 0
  f[5] = (e * 2) / i
  f[6] = 0
  f[7] = 0
  f[8] = (b + a) / h
  f[9] = (d + c) / i
  f[10] = -(g + e) / j
  f[11] = -1
  f[12] = 0
  f[13] = 0
  f[14] = -(g * e * 2) / j
  f[15] = 0
  return f
}
mat4.perspective = function(a, b, c, d, e) {
  a = c * Math.tan((a * Math.PI) / 360)
  b = a * b
  return mat4.frustum(-b, b, -a, a, c, d, e)
}
mat4.ortho = function(a, b, c, d, e, g, f) {
  f || (f = mat4.create())
  var h = b - a,
    i = d - c,
    j = g - e
  f[0] = 2 / h
  f[1] = 0
  f[2] = 0
  f[3] = 0
  f[4] = 0
  f[5] = 2 / i
  f[6] = 0
  f[7] = 0
  f[8] = 0
  f[9] = 0
  f[10] = -2 / j
  f[11] = 0
  f[12] = -(a + b) / h
  f[13] = -(d + c) / i
  f[14] = -(g + e) / j
  f[15] = 1
  return f
}
mat4.lookAt = function(a, b, c, d) {
  d || (d = mat4.create())
  var e = a[0],
    g = a[1]
  a = a[2]
  var f = c[0],
    h = c[1],
    i = c[2]
  c = b[1]
  var j = b[2]
  if (e == b[0] && g == c && a == j) {
    return mat4.identity(d)
  }
  var k, l, o, m
  c = e - b[0]
  j = g - b[1]
  b = a - b[2]
  m = 1 / Math.sqrt(c * c + j * j + b * b)
  c *= m
  j *= m
  b *= m
  k = h * b - i * j
  i = i * c - f * b
  f = f * j - h * c
  if ((m = Math.sqrt(k * k + i * i + f * f))) {
    m = 1 / m
    k *= m
    i *= m
    f *= m
  } else {
    f = i = k = 0
  }
  h = j * f - b * i
  l = b * k - c * f
  o = c * i - j * k
  if ((m = Math.sqrt(h * h + l * l + o * o))) {
    m = 1 / m
    h *= m
    l *= m
    o *= m
  } else {
    o = l = h = 0
  }
  d[0] = k
  d[1] = h
  d[2] = c
  d[3] = 0
  d[4] = i
  d[5] = l
  d[6] = j
  d[7] = 0
  d[8] = f
  d[9] = o
  d[10] = b
  d[11] = 0
  d[12] = -(k * e + i * g + f * a)
  d[13] = -(h * e + l * g + o * a)
  d[14] = -(c * e + j * g + b * a)
  d[15] = 1
  return d
}
vec3.sub_ip = function(a, b) {
  a[0] -= b[0]
  a[1] -= b[1]
  a[2] -= b[2]
}
vec3.sub_r = function(a, b) {
  var d = [a[0] - b[0], a[1] - b[1], a[2] - b[2]]
  return d
}
vec3.add_ip = function(a, b) {
  a[0] += b[0]
  a[1] += b[1]
  a[2] += b[2]
}
vec3.add_r = function(a, b) {
  var d = [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
  return d
}
vec3.scale_ip = function(a, b) {
  a[0] *= b
  a[1] *= b
  a[2] *= b
}
vec3.scale_r = function(a, b) {
  var d = [a[0] * b, a[1] * b, a[2] * b]
  return d
}
vec3.cross_r = function(a, b) {
  var d = a[0],
    e = a[1]
  a = a[2]
  var g = b[0],
    f = b[1]
  b = b[2]
  var c = [e * b - a * f, a * g - d * b, d * f - e * g]
  return c
}
vec3.cross_rn = function(a, b) {
  var c = vec3.cross_r(a, b)
  vec3.normalize(c)
  return c
}
vec3.lerp_r = function(a, b, c) {
  var d = [
    a[0] + c * (b[0] - a[0]),
    a[1] + c * (b[1] - a[1]),
    a[2] + c * (b[2] - a[2])
  ]
  return d
}
mat4.offset = function(tm, offset) {
  tm[12] += offset[0]
  tm[13] += offset[1]
  tm[14] += offset[2]
}
mat4.copy = function(src, dst) {
  for (var i = 0; i < 16; i++) {
    dst[i] = src[i]
  }
}
mat4.rotateAxisOrg = function(tm, org, axis, angle) {
  var _org = [-org[0], -org[1], -org[2]]
  mat4.offset(tm, _org)
  var tmR = mat4.create()
  var tm2 = mat4.create()
  mat4.identity(tmR)
  mat4.rotate(tmR, angle, axis)
  mat4.m(tm, tmR, tm2)
  mat4.copy(tm2, tm)
  mat4.offset(tm, org)
}
mat4.invert = function(out, a) {
  var a00 = a[0],
    a01 = a[1],
    a02 = a[2],
    a03 = a[3],
    a10 = a[4],
    a11 = a[5],
    a12 = a[6],
    a13 = a[7],
    a20 = a[8],
    a21 = a[9],
    a22 = a[10],
    a23 = a[11],
    a30 = a[12],
    a31 = a[13],
    a32 = a[14],
    a33 = a[15],
    b00 = a00 * a11 - a01 * a10,
    b01 = a00 * a12 - a02 * a10,
    b02 = a00 * a13 - a03 * a10,
    b03 = a01 * a12 - a02 * a11,
    b04 = a01 * a13 - a03 * a11,
    b05 = a02 * a13 - a03 * a12,
    b06 = a20 * a31 - a21 * a30,
    b07 = a20 * a32 - a22 * a30,
    b08 = a20 * a33 - a23 * a30,
    b09 = a21 * a32 - a22 * a31,
    b10 = a21 * a33 - a23 * a31,
    b11 = a22 * a33 - a23 * a32,
    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06
  if (!det) {
    return null
  }
  det = 1 / det
  out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det
  out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det
  out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det
  out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det
  out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det
  out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det
  out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det
  out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det
  out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det
  out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det
  out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det
  out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det
  out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det
  out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det
  out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det
  out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det
  return out
}
mat4.M3_M1xM2_1 = function(m3, m1, m2) {
  mat4.invert(m1, m2)
  mat4.m(m3, m1, m1)
}
mat4.setRow = function(tm, index, p) {
  index *= 4
  tm[index] = p[0]
  tm[index + 1] = p[1]
  tm[index + 2] = p[2]
}
var mouseCancelPopup = false
var g_mouseDownWindow = null
function ivSetEvent(d, e, f) {
  if (d.attachEvent) {
    d.attachEvent('on' + e, f)
  } else {
    if (d.addEventListener) {
      d.addEventListener(e, f)
    }
  }
}
function ivwindow3d(canvas, file, color, path, txtPath) {
  this.m_canvas = canvas
  canvas.ivwindow3d = this
  this.mvMatrix = mat4.create()
  this.viewFrom = [0, 0, 6]
  this.viewTo = [0, 0, 0]
  this.viewUp = [0, 1, 0]
  this.viewFar = 100
  this.viewNear = 0.1
  this.fov = 90
  this.LX = null
  this.LY = null
  this.lastTouchDistance = -1
  this.mouseMoved = false
  this.m_color = color != undefined ? color : 8355711
  this.m_bOk = this.initHardware()
  this.vpVersion = 0
  this.timer = false
  this.m_cameramode = 0
  if (this.m_bOk) {
    if (file) {
      this.loadSpace(file, path, txtPath)
    } else {
      this.space = 0
    }
    this.gl.enable(this.gl.DEPTH_TEST)
    ivSetEvent(canvas, 'touchstart', handleTouchStart)
    ivSetEvent(document, 'touchmove', handleTouchMove)
    ivSetEvent(document, 'touchend', handleTouchEnd)
    ivSetEvent(document, 'touchcancel', handleTouchCancel)
    var mousewheelevt = /Firefox/i.test(navigator.userAgent)
      ? 'DOMMouseScroll'
      : 'mousewheel'
    ivSetEvent(canvas, mousewheelevt, handleMouseWheel)
    canvas.oncontextmenu = handleContextMenu
    canvas.onmousedown = handleMouseDown
    canvas.onmousemove = handleMouseMove2
    document.onmouseup = handleMouseUp
    document.onmousemove = handleMouseMove
    document.onselectstart = handleSelectStart
    this.invalidate()
  }
}
ivwindow3d.prototype.getWindow = function() {
  return this
}
ivwindow3d.prototype.initHardware = function() {
  var n = ['webgl', 'experimental-webgl', 'webkit-3d', 'moz-webgl']
  for (var i = 0; i < n.length; i++) {
    try {
      this.gl = this.m_canvas.getContext(n[i], { alpha: false })
    } catch (e) {}
    if (this.gl) {
      this.gl.viewportWidth = this.m_canvas.width
      this.gl.viewportHeight = this.m_canvas.height
      this.m_bOk = true
      break
    }
  }
  if (!this.gl) {
    alert('Could not initialise WebGL')
  }
  return this.gl != null
}
ivwindow3d.prototype.setViewImp = function(v) {
  if (v) {
    this.viewFrom = v.org.slice()
    this.viewTo = v.target.slice()
    this.viewUp = v.up.slice()
    if (v.fov) {
      this.fov = v.fov
    } else {
      this.fov = 90
    }
    if (v.far) {
      this.viewFar = v.far
    }
    if (v.near) {
      this.viewNear = v.near
    }
    if (this.space) {
      this.invalidate(IV.INV_VERSION)
    }
  }
}
ivwindow3d.prototype.setDefView = function() {
  this.viewFar = 0.1
  this.viewNear = 100
  this.setViewImp(this.space.view)
}
ivwindow3d.prototype.loadSpace = function(file, path, txtPath) {
  this.space = new space3d(this, this.gl)
  if (path != undefined) {
    this.space.path = path
  }
  if (txtPath != undefined) {
    this.space.txtPath = txtPath
  }
  var request = CreateRequest(file, path)
  request.ivspace = this.space
  request.ivwnd = this
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      this.ivspace.load(JSON.parse(this.responseText))
      this.ivwnd.setDefView()
    }
  }
  request.send()
}
ivwindow3d.prototype.getDoubleSided = function() {
  return this.space.cfgDbl
}
ivwindow3d.prototype.setDoubleSided = function(b) {
  if (this.space.cfgDbl != b) {
    var s = this.space
    s.cfgDbl = b
    s.invalidate(IV.INV_MTLS)
  }
}
ivwindow3d.prototype.getMaterials = function() {
  return this.space.cfgDefMtl == null
}
ivwindow3d.prototype.setMaterials = function(b) {
  var s = this.space
  if (b) {
    if (s.cfgDefMtl) {
      s.cfgDefMtl = null
      this.invalidate()
    }
  } else {
    if (!s.cfgDefMtl) {
      s.cfgDefMtl = {
        diffuse: [0.8, 0.8, 0.8],
        specular: [0.5, 0.5, 0.5],
        ambient: [0.02, 0.02, 0.02],
        phong: 25.6
      }
      this.invalidate()
    }
  }
}
ivwindow3d.prototype.getTextures = function() {
  return this.space.cfgTextures
}
ivwindow3d.prototype.setTextures = function(b) {
  if (this.space.cfgTextures != b) {
    this.space.cfgTextures = b
    this.invalidate()
  }
}
ivwindow3d.prototype.setLights = function(l) {
  var s = this.space
  s.lights = l
  s.invalidate(IV.INV_MTLS)
}
function handleTouchStart(event) {
  var view = event.currentTarget.ivwindow3d
  if (view) {
    view.OnMouseDown(event, true)
    g_mouseDownWindow = view
    event.preventDefault()
  }
  mouseCancelPopup = false
}
function handleTouchMove(event) {
  if (!g_mouseDownWindow) {
    return
  }
  g_mouseDownWindow.OnMouseMove(event, true)
  event.preventDefault()
  return false
}
function handleTouchEnd(event) {
  if (g_mouseDownWindow) {
    g_mouseDownWindow.OnMouseUp(event, true)
    event.preventDefault()
  }
  g_mouseDownWindow = null
}
function handleTouchCancel(event) {
  if (g_mouseDownWindow) {
    g_mouseDownWindow.OnMouseUp(event, true)
    if (event.cancelable) {
      event.preventDefault()
    }
  }
  g_mouseDownWindow = null
}
function handleMouseDown(event) {
  var view = event.currentTarget.ivwindow3d
  if (view) {
    view.OnMouseDown(event, false)
    g_mouseDownWindow = view
  }
  mouseCancelPopup = false
}
function handleMouseUp(event) {
  if (g_mouseDownWindow) {
    g_mouseDownWindow.OnMouseUp(event, false)
  }
  g_mouseDownWindow = null
}
ivwindow3d.prototype.OnMouseUp = function(event, bTouch) {}
function GetTouchDistance(e) {
  var dx = e.touches[0].clientX - e.touches[1].clientX
  var dy = e.touches[0].clientY - e.touches[1].clientY
  return Math.sqrt(dx * dx + dy * dy)
}
ivwindow3d.prototype.OnMouseDown = function(event, bTouch) {
  var r = this.m_canvas.getBoundingClientRect()
  var e = event
  this.lastTouchDistance = -1
  if (bTouch) {
    e = event.touches[0]
    if (event.touches.length == 2) {
      this.lastTouchDistance = GetTouchDistance(event)
    }
  }
  this.LX = e.clientX - r.left
  this.LY = e.clientY - r.top
  this.mouseMoved = false
  var b = DecodeButtons(event, bTouch)
  if (b & 4) {
    event.preventDefault()
  }
}
ivwindow3d.prototype.OnMouseMove = function(event, bTouch) {
  var r = this.m_canvas.getBoundingClientRect()
  var e = event
  if (bTouch) {
    e = event.touches[0]
    if (event.touches.length == 2) {
      var d = GetTouchDistance(event)
      if (this.lastTouchDistance != d) {
        if (this.lastTouchDistance > 0) {
          var _d = this.lastTouchDistance - d
          this.handleVPFOV(_d, _d)
          this.invalidate(IV.INV_VERSION)
        }
        this.lastTouchDistance = d
        this.mouseMoved = true
        this.LX = e.clientX - r.left
        this.LY = e.clientY - r.top
      } else {
        this.lastTouchDistance - 1
      }
      return
    }
  }
  var newX = e.clientX - r.left
  var newY = e.clientY - r.top
  var dX = newX - this.LX
  var dY = newY - this.LY
  if (Math.abs(dX) > 1 || Math.abs(dY) || this.mouseMoved) {
    var b = DecodeButtons(event, bTouch)
    var invF = 0
    if (this.m_cameramode && b == 1) {
      if (this.m_cameramode == 1) {
        b = 2
      } else {
        if (this.m_cameramode == 2) {
          b = 4
        }
      }
    }
    if (b & 4) {
      this.handleVPPan(dX, dY)
      invF = IV.INV_VERSION
    } else {
      if (b & 1) {
        this.handleVPRotate(dX, dY)
        invF = IV.INV_VERSION
      } else {
        if (b & 2) {
          if (!this.handleVPFOV(dX, dY)) {
            return
          }
          invF = IV.INV_VERSION
          mouseCancelPopup = true
        }
      }
    }
    this.invalidate()
    this.LX = newX
    this.LY = newY
    this.mouseMoved = true
  }
}
function handleMouseWheel(event) {
  var view = event.currentTarget.ivwindow3d
  if (view) {
    var d
    if (event.wheelDelta != undefined) {
      d = event.wheelDelta / -10
    } else {
      if (event.detail != undefined) {
        d = event.detail
        if (d > 10) {
          d = 10
        } else {
          if (d < -10) {
            d = -10
          }
        }
        d *= 4
      }
    }
    view.handleVPDolly(0, d)
    view.invalidate(IV.INV_VERSION)
    event.preventDefault()
  }
}
ivwindow3d.prototype.handleVPPan = function(dX, dY) {
  var gl = this.gl
  var x0 = gl.viewportWidth / 2
  var y0 = gl.viewportHeight / 2
  var r0 = this.GetRay(x0, y0)
  var r1 = this.GetRay(x0 - dX, y0 - dY)
  var d = [r1[3] - r0[3], r1[4] - r0[4], r1[5] - r0[5]]
  vec3.add_ip(this.viewFrom, d)
  vec3.add_ip(this.viewUp, d)
  vec3.add_ip(this.viewTo, d)
}
ivwindow3d.prototype.handleVPRotate = function(dX, dY) {
  var vf = this.viewFrom,
    t = this.viewTo.slice(),
    tm = [],
    _u = this.getUpVector(),
    u = this.viewUp
  var o = vf.slice()
  vec3.normalize(_u)
  if (dX) {
    mat4.identity(tm)
    mat4.rotateAxisOrg(tm, t, _u, -dX / 200)
    mat4.mulPoint(tm, vf)
    mat4.mulPoint(tm, u)
  }
  if (dY) {
    var _d = vec3.sub_r(t, o)
    vec3.normalize(_d)
    var _axis = vec3.cross(_d, _u, _axis)
    mat4.identity(tm)
    mat4.rotateAxisOrg(tm, t, _axis, -dY / 200)
    mat4.mulPoint(tm, vf)
    mat4.mulPoint(tm, u)
  }
}
ivwindow3d.prototype.handleVPDolly = function(dX, dY) {
  var dir = vec3.sub_r(this.viewFrom, this.viewTo)
  var l = vec3.length(dir)
  var _l = l + (l * dY) / 100
  if (_l < 0.000001) {
    return
  }
  vec3.normalize(dir)
  vec3.scale_ip(dir, _l)
  var _new = vec3.add_r(this.viewTo, dir)
  var delta = vec3.sub_r(_new, this.viewFrom)
  vec3.add_ip(this.viewFrom, delta)
  vec3.add_ip(this.viewUp, delta)
}
ivwindow3d.prototype.handleVPFOV = function(dX, dY) {
  var fov = this.fov + dY / 8
  if (fov >= 175) {
    fov = 175
  } else {
    if (fov <= 1) {
      fov = 1
    }
  }
  if (fov != this.fov) {
    this.fov = fov
    return true
  }
  return false
}
function handleContextMenu(event) {
  if (mouseCancelPopup) {
    mouseCancelPopup = false
    return false
  }
  return true
}
function handleSelectStart(event) {
  if (g_mouseDownWindow) {
    return false
  }
  return true
}
ivwindow3d.prototype.getUpVector = function() {
  var _up = [
    this.viewUp[0] - this.viewFrom[0],
    this.viewUp[1] - this.viewFrom[1],
    this.viewUp[2] - this.viewFrom[2]
  ]
  return _up
}
ivwindow3d.prototype.GetRay = function(x, y, ray) {
  var gl = this.gl
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight)
  var p1 = this.viewFrom
  var p2 = this.viewTo
  var dir = vec3.sub_r(this.viewTo, this.viewFrom)
  var dirLen = vec3.length(dir)
  var up = this.getUpVector()
  var k = Math.tan((Math.PI * this.fov) / 360)
  var h2 = gl.viewportHeight / 2
  var w2 = gl.viewportWidth / 2
  var _k = (h2 - y) / h2
  var _kx = (x - w2) / w2
  vec3.normalize(up)
  var xaxis = vec3.cross_rn(dir, up)
  var _up = vec3.scale_r(up, k * dirLen * _k)
  var _x = vec3.scale_r(
    xaxis,
    (k * dirLen * _kx * gl.viewportWidth) / gl.viewportHeight
  )
  var ray = [
    p1[0],
    p1[1],
    p1[2],
    p2[0] + _up[0] + _x[0],
    p2[1] + _up[1] + _x[1],
    p2[2] + _up[2] + _x[2]
  ]
  return ray
}
function DecodeButtons(event, bt) {
  var buttons = 0
  if (bt && event.touches != undefined) {
    if (event.touches.length >= 3) {
      return 4
    }
    return 1
  }
  if (event.buttons == undefined) {
    if (event.which == 1) {
      buttons = 1
    } else {
      if (event.which == 2) {
        buttons = 4
      } else {
        if (event.which == 3) {
          buttons = 2
        } else {
          buttons = 1
        }
      }
    }
  } else {
    buttons = event.buttons
  }
  return buttons
}
function handleMouseMove2(event) {
  var w = event.currentTarget.ivwindow3d
  if (w) {
    if (!g_mouseDownWindow) {
      if (w.OnMouseHover) {
        w.OnMouseHover(event)
      }
    } else {
      if (w == g_mouseDownWindow) {
        w.OnMouseMove(event, false)
      } else {
        return false
      }
    }
    event.stopPropagation()
  }
  return false
}
function handleMouseMove(event) {
  if (!g_mouseDownWindow) {
    return
  }
  g_mouseDownWindow.OnMouseMove(event, false)
  return false
}
ivwindow3d.prototype.updateMVTM = function() {
  mat4.lookAt(this.viewFrom, this.viewTo, this.getUpVector(), this.mvMatrix)
}
ivwindow3d.prototype.drawScene = function() {
  var gl = this.gl
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight)
  if (this.space.bk == undefined || !this.space.drawBk()) {
    var bk = this.m_color
    var t = typeof bk
    if (t === 'string') {
      bk = parseInt(bk.substr(1, 6), 16)
      this.m_color = bk
    }
    var r = ((bk >> 16) & 255) / 255
    var g = ((bk >> 8) & 255) / 255
    var b = (bk & 255) / 255
    gl.clearColor(r, g, b, 1)
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
  }
  this.updateMVTM()
  this.space.render(this.mvMatrix)
  this.timer = false
}
ivwindow3d.prototype.invalidate = function(flags) {
  if (this.timer) {
    return
  }
  this.timer = true
  if (flags !== undefined) {
    if (flags & IV.INV_VERSION) {
      this.vpVersion++
    }
  }
  setTimeout(this.drawScene.bind(this), 1)
}
var IV = { INV_MTLS: 2, INV_VERSION: 4 }
function space3d(view, gl) {
  this.cfgTextures = true
  this.gl = gl
  this.window = view
  this.root = null
  this.view = null
  this.materials = []
  this.projectionTM = mat4.create()
  this.modelviewTM = mat4.create()
  this.cfgDbl = true
  this.m_cfgKeepMeshData = 3
  this.cfgDefMtl = null
  this.textures = []
  this.lights = 0
  this.activeShader = null
  this.pre = []
  this.post = []
  this.clrSelection = [1, 0, 0]
  this.rmode = 0
  this.meshesInQueue = 0
  this.rmodes = [
    { f: true, n: true, e: false, mtl: null },
    { f: false, n: false, e: true, mtl: null }
  ]
  if (gl) {
    this.e_ans =
      gl.getExtension('EXT_texture_filter_anisotropic') ||
      gl.getExtension('MOZ_EXT_texture_filter_anisotropic') ||
      gl.getExtension('WEBKIT_EXT_texture_filter_anisotropic')
    if (this.e_ans) {
      this.e_ansMax = gl.getParameter(this.e_ans.MAX_TEXTURE_MAX_ANISOTROPY_EXT)
    }
  }
}
function CreateRequest(f, p) {
  if (f == undefined) {
    return null
  }
  var r = new XMLHttpRequest()
  if (p) {
    r.open('GET', p + f)
  } else {
    r.open('GET', f)
  }
  return r
}
space3d.prototype.getWindow = function() {
  return this.window
}
space3d.prototype.onMeshLoaded = function(m) {
  this.meshesInQueue--
  if (!this.meshesInQueue) {
    var w = this.window
    if (w && w.onMeshesReady) {
      w.onMeshesReady(w, this)
    }
  }
  this.invalidate()
}
space3d.prototype.updateShadeArgs = function(a) {
  var gl = this.gl,
    i
  var p = this.activeShader
  var ca = p ? p.attrs.length : 0,
    na = a ? a.attrs.length : 0
  if (na > ca) {
    for (i = ca; i < na; i++) {
      gl.enableVertexAttribArray(i)
    }
  } else {
    if (na < ca) {
      for (i = na; i < ca; i++) {
        gl.disableVertexAttribArray(i)
      }
    }
  }
  ca = p ? p.textures.length : 0
  for (i = 0; i < ca; i++) {
    gl.activeTexture(gl.TEXTURE0 + i)
    var txt = p.textures[i]
    var type = txt.txt.ivtype
    gl.bindTexture(type === undefined ? gl.TEXTURE_2D : type, null)
  }
}
space3d.prototype.activateShader = function(m, s, tm, flags) {
  if (s != this.activeShader) {
    this.updateShadeArgs(s)
  }
  if (s) {
    s.activate(this, m, tm, flags, s == this.activeShader)
  } else {
    this.gl.useProgram(null)
  }
  this.activeShader = s
}
space3d.prototype.activateMaterial = function(m, tm, flags) {
  var s = m ? m.getShader(flags) : 0
  if (s && !s.bValid) {
    if (this.activeShader) {
      this.activateShader(null, null)
    }
    s.update(m)
  }
  this.activateShader(m, s, tm, flags)
  return s
}
function bk3d(space, txt) {
  var gl = space.gl
  this.uv = new Float32Array([0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1])
  this.uvBuffer = ivBufferF(gl, this.uv, 2)
  this.vertexBuffer = ivBufferF(
    gl,
    new Float32Array([
      -1,
      -1,
      0,
      1,
      -1,
      0,
      -1,
      1,
      0,
      -1,
      1,
      0,
      1,
      -1,
      0,
      1,
      1,
      0
    ]),
    3
  )
  var mtl = new material3d(space)
  var c = mtl.newChannel('emissive')
  mtl.newTexture(c, txt)
  c.wrapS = gl.CLAMP_TO_EDGE
  c.wrapT = gl.CLAMP_TO_EDGE
  this.mtl = mtl
  this.texture = c.texture
}
space3d.prototype.drawBk = function() {
  if (this.bk && this.bk.texture.ivready) {
    var gl = this.gl
    if (gl.viewportHeight && gl.viewportWidth) {
      gl.clear(gl.DEPTH_BUFFER_BIT)
      var bk = this.bk
      var s = this.activateMaterial(bk.mtl, null, 2)
      for (var i = 0; i < s.attrs.length; i++) {
        var v = s.attrs[i]
        switch (v.id) {
          case 'v':
            gl.bindBuffer(gl.ARRAY_BUFFER, bk.vertexBuffer)
            gl.vertexAttribPointer(
              v.slot,
              bk.vertexBuffer.itemSize,
              gl.FLOAT,
              false,
              0,
              0
            )
            break
          case 'uv':
            gl.bindBuffer(gl.ARRAY_BUFFER, bk.uvBuffer)
            var img = bk.texture.image
            var kx = gl.viewportWidth / img.naturalWidth,
              ky = gl.viewportHeight / img.naturalHeight
            var x = 0,
              y = 0
            if (kx > ky) {
              y = (1 - ky / kx) / 2
            } else {
              if (kx < ky) {
                x = (1 - kx / ky) / 2
              }
            }
            var uv = bk.uv
            if (
              Math.abs(uv[0] - x) > 0.00001 ||
              Math.abs(uv[1] - y) > 0.00001
            ) {
              uv[0] = x
              uv[1] = y
              uv[2] = 1 - x
              uv[3] = y
              uv[4] = x
              uv[5] = 1 - y
              uv[6] = x
              uv[7] = 1 - y
              uv[8] = 1 - x
              uv[9] = y
              uv[10] = 1 - x
              uv[11] = 1 - y
              gl.bufferData(gl.ARRAY_BUFFER, uv, gl.STATIC_DRAW)
            }
            gl.vertexAttribPointer(
              v.slot,
              bk.uvBuffer.itemSize,
              gl.FLOAT,
              false,
              0,
              0
            )
            break
        }
      }
      gl.disable(gl.DEPTH_TEST)
      gl.depthMask(false)
      gl.drawArrays(gl.TRIANGLES, 0, 6)
      gl.enable(gl.DEPTH_TEST)
      gl.depthMask(true)
      return true
    }
  }
  return false
}
space3d.prototype.invalidate = function(flags) {
  if (flags !== undefined) {
    if (flags & IV.INV_MTLS && this.materials) {
      for (var i = 0; i < this.materials; i++) {
        this.materials[i].invalidate()
      }
    }
  } else {
    flags = 0
  }
  this.window.invalidate(flags)
}
function isPOW2(v) {
  return (v & (v - 1)) == 0
}
function handleLoadedTexture(texture) {
  if (texture.image.naturalWidth > 0 && texture.image.naturalHeight > 0) {
    var type = texture.ivtype
    var gl = texture.ivspace.gl
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true)
    gl.bindTexture(type, texture)
    gl.texImage2D(type, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image)
    var pot =
      isPOW2(texture.image.naturalWidth) && isPOW2(texture.image.naturalHeight)
    gl.texParameteri(type, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
    gl.texParameteri(
      type,
      gl.TEXTURE_MIN_FILTER,
      pot ? gl.LINEAR_MIPMAP_NEAREST : gl.LINEAR
    )
    if (pot) {
      gl.generateMipmap(type)
    }
    gl.bindTexture(type, null)
    texture.ivready = true
    texture.ivpot = pot
    texture.ivspace.invalidate()
  }
  delete texture.image.ivtexture
  delete texture.ivspace
}
function handleLoadedCubeTexture(image) {
  var texture = image.ivtexture
  var gl = texture.ivspace.gl
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture)
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false)
  gl.texImage2D(image.ivface, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image)
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
  gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
  gl.bindTexture(gl.TEXTURE_CUBE_MAP, null)
  texture.ivnumfaces++
  if (texture.ivnumfaces == 6) {
    texture.ivready = true
    texture.ivspace.invalidate()
    delete texture.ivspace
  }
  delete image.ivtexture
}
space3d.prototype.getTexture = function(str, type) {
  var t
  for (var i = 0; i < this.textures.length; i++) {
    var t = this.textures[i]
    if (t.ivfile == str && t.ivtype == type) {
      t.ivrefcount++
      return t
    }
  }
  var gl = this.gl
  t = this.gl.createTexture()
  t.ivspace = this
  t.ivready = false
  t.ivfile = str
  t.ivtype = type
  t.ivrefcount = 1
  if (type == gl.TEXTURE_CUBE_MAP) {
    var faces = [
      ['posx', gl.TEXTURE_CUBE_MAP_POSITIVE_X],
      ['negx', gl.TEXTURE_CUBE_MAP_NEGATIVE_X],
      ['posy', gl.TEXTURE_CUBE_MAP_POSITIVE_Y],
      ['negy', gl.TEXTURE_CUBE_MAP_NEGATIVE_Y],
      ['posz', gl.TEXTURE_CUBE_MAP_POSITIVE_Z],
      ['negz', gl.TEXTURE_CUBE_MAP_NEGATIVE_Z]
    ]
    t.ivnumfaces = 0
    var _str = str.split('.')
    var path = this.txtPath ? this.txtPath : this.path
    if (path) {
      _str[0] = path + _str[0]
    }
    for (var i = 0; i < 6; i++) {
      var filename = _str[0] + faces[i][0] + '.' + _str[1]
      var image = new Image()
      image.ivtexture = t
      image.ivface = faces[i][1]
      image.onload = function() {
        handleLoadedCubeTexture(this)
      }
      image.src = filename
    }
  } else {
    t.image = new Image()
    t.image.ivtexture = t
    t.image.onload = function() {
      handleLoadedTexture(this.ivtexture)
    }
    t.image.src = path ? path + str : str
  }
  this.textures.push(t)
  return t
}
space3d.prototype.load = function(data) {
  if (data) {
    if (data.space) {
      var s = data.space,
        m = s.meshes,
        i
      var d = { objects: [], materials: [], space: this }
      if (s.materials) {
        for (i = 0; i < s.materials.length; i++) {
          var mtl = new material3d(this)
          mtl.load(s.materials[i])
          this.materials.push(mtl)
          d.materials.push(mtl)
        }
      }
      if (m) {
        for (i = 0; i < m.length; i++) {
          var obj = new mesh3d(this.gl)
          if (this.path) {
            obj.url = this.path + m[i].ref
          } else {
            obj.url = m[i].ref
          }
          if (obj.url) obj.url = obj.url.toLowerCase() // Hack for Shawn
          d.objects.push(obj)
        }
      }
      if (s.root) {
        if (!this.root) {
          this.root = new node3d()
        }
        this.root.load(s.root, d)
      }
      if (s.view) {
        this.view = s.view
      }
      if (s.lights) {
        this.lights = s.lights
        for (i = 0; i < this.lights.length; i++) {
          var l = this.lights[i]
          if (l.dir) {
            vec3.normalize(l.dir)
          }
        }
      }
      if (s.views) {
        this.views = s.views
      }
      if (data.space.bk != undefined) {
        this.bk = new bk3d(this, data.space.bk)
      }
      var w = this.window
      if (w && w.onDataReady) {
        w.onDataReady(this)
      }
    }
  }
}
space3d.prototype.renderQueue = function(items) {
  var c = items.length
  var a
  var gl = this.gl
  for (var i = 0; i < c; i++) {
    var item = items[i]
    var d = this.cfgDbl || (item.state & 32) != 0
    if (d != a) {
      if (d) {
        gl.disable(gl.CULL_FACE)
      } else {
        gl.enable(gl.CULL_FACE)
      }
      a = d
    }
    item.object.render(item.tm, this, item.mtl, item.state)
  }
}
space3d.prototype.updatePrjTM = function(tm) {
  var w = this.window
  var gl = this.gl
  var v = [0, 0, 0]
  var bOk = false
  var far = 0,
    near = 0,
    z
  var tm = mat4.create()
  for (var iPass = 0; iPass < 2; iPass++) {
    var items = iPass ? this.post : this.pre
    if (!items) {
      continue
    }
    var c = items.length
    for (var iO = 0; iO < c; iO++) {
      var item = items[iO]
      tm = mat4.m(item.tm, this.modelviewTM, tm)
      var _min = item.object.boxMin
      var _max = item.object.boxMax
      for (var i = 0; i < 8; i++) {
        v[0] = i & 1 ? _max[0] : _min[0]
        v[1] = i & 2 ? _max[1] : _min[1]
        v[2] = i & 4 ? _max[2] : _min[2]
        mat4.mulPoint(tm, v)
        z = -v[2]
        if (bOk) {
          if (z < near) {
            near = z
          } else {
            if (z > far) {
              far = z
            }
          }
        } else {
          far = near = z
          bOk = true
        }
      }
    }
  }
  if (bOk) {
    var d = far - near
    d /= 100
    far += d
    near -= d
    d = far / 1000
    if (near < d) {
      near = d
    }
  } else {
    ;(near = w.viewNear), (far = w.viewFar)
  }
  mat4.perspective(
    w.fov,
    gl.viewportWidth / gl.viewportHeight,
    near,
    far,
    this.projectionTM
  )
}
space3d.prototype.render = function(tm) {
  if (this.root) {
    var gl = this.gl
    gl.cullFace(gl.BACK)
    var tmWorld = mat4.create()
    mat4.identity(tmWorld)
    this.root.traverse(tmWorld, nodeRender, this, this.rmode << 8)
    mat4.copy(tm, this.modelviewTM)
    this.updatePrjTM(tm)
    this.renderQueue(this.pre)
    this.pre = []
    if (this.post.length) {
      gl.enable(gl.BLEND)
      gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
      this.renderQueue(this.post)
      gl.disable(gl.BLEND)
      this.post = []
    }
    this.activateMaterial(null)
  }
}
space3d.prototype.toRenderQueue = function(atm, node, state) {
  var mtl = this.cfgDefMtl ? this.cfgDefMtl : node.material
  var rmode = this.rmodes[(state & 65280) >> 8]
  if (rmode.mtl) {
    mtl = rmode.mtl
  }
  var item = {
    tm: atm,
    object: node.object,
    mtl: mtl,
    state: state | (node.state & (16 | 32 | 196608))
  }
  var l = mtl.opacity != undefined ? this.post : this.pre
  l.push(item)
}
space3d.prototype.getMaterial = function(name) {
  var it = this.materials
  for (var i = 0; i < it.length; i++) {
    var m = it[i]
    if (m.name !== undefined && m.name == name) {
      return m
    }
  }
  return null
}
function channel3d() {
  this.mode = 0
  this.color = null
  this.texture = null
}
function mtlvar3d(id) {
  this.id = id
  this.slot = null
}
function shader3d(f) {
  this.flags = f
  this.bValid = false
  this.attrs = []
  this.vars = []
  this.textures = []
  this.program = null
  this.vShader = null
  this.fShader = null
  this.loadedtextures = 0
  this.numLights = 0
}
function material3d(space) {
  this.space = space
  this.gl = space.gl
  this.type = 'standard'
  this.shaders = []
  this.phong = 18
}
material3d.prototype.invalidate = function() {
  var s = this.shader
  if (s) {
    for (var i = 0; i < s.length; i++) {
      s[i].detach(this.mtl)
    }
    this.shader = []
  }
}
material3d.prototype.reset = function() {
  if (this.diffuse) {
    delete this.diffuse
  }
  if (this.specular) {
    delete this.specular
  }
  if (this.emissive) {
    delete this.emissive
  }
  if (this.reflection) {
    delete this.reflection
  }
  if (this.bump) {
    delete this.bump
  }
  if (this.opacity) {
    delete this.opacity
  }
  this.invalidate()
}
material3d.prototype.isChannel = function(c) {
  if (c === undefined || c === null) {
    return false
  }
  if (c.length === 0) {
    return false
  }
  for (var i = 0; i < c.length; i++) {
    var item = c[i]
    if (item.texture != null || item.color != null || item.amount != null) {
      return true
    }
  }
  return false
}
material3d.prototype.newChannel = function(type, ch) {
  if (!ch) {
    ch = new channel3d()
  }
  if (!(type in this)) {
    this[type] = []
  }
  this[type].push(ch)
  this.bValid = false
  return ch
}
material3d.prototype.getChannel = function(type) {
  if (!(type in this)) {
    return null
  }
  var items = this[type]
  return items[0]
}
material3d.prototype.newTexture = function(c, name, type) {
  var gl = this.gl
  if (type === undefined) {
    type = gl.TEXTURE_2D
  }
  c.texture = this.space.getTexture(name, type)
  if (type == gl.TEXTURE_CUBE_MAP) {
    c.wrapS = gl.CLAMP_TO_EDGE
    c.wrapT = gl.CLAMP_TO_EDGE
  } else {
    c.wrapS = gl.REPEAT
    c.wrapT = gl.REPEAT
  }
}
function cnvTtxMatrix(a) {
  var tm = mat3.create()
  var index = 0
  for (var i = 0; i < 3; i++) {
    for (var j = 0; j < 2; j++) {
      tm[i * 3 + j] = a[index]
      index++
    }
  }
  tm[2] = 0
  tm[5] = 0
  tm[8] = 1
  return tm
}
material3d.prototype.loadChannel = function(d, name) {
  var v = d[name]
  var type = typeof v
  if (type === 'object') {
    var c = this.newChannel(name)
    if (v instanceof Array && v.length == 3) {
      c.color = v
    } else {
      if (v.color !== undefined) {
        c.color = v.color
      }
      if (v.amount !== undefined) {
        c.amount = v.amount
      }
      if (v.texture !== undefined) {
        var type = undefined
        if ('type' in v && v.type == 'cube') {
          type = this.gl.TEXTURE_CUBE_MAP
        }
        this.newTexture(c, v.texture, type)
        if (v.tm !== undefined) {
          c.tm = cnvTtxMatrix(v.tm)
        }
        if (v.cmp) {
          c.cmp = v.cmp
        }
      }
    }
  }
}
material3d.prototype.load = function(d) {
  for (var v in d) {
    switch (v) {
      case 'diffuse':
      case 'specular':
      case 'emissive':
      case 'reflection':
      case 'opacity':
      case 'bump':
        this.loadChannel(d, v)
        break
      case 'name':
        this.name = d[v]
        break
      case 'phong':
        this.phong = d[v]
        break
    }
  }
  return true
}
material3d.prototype.getShader = function(flags) {
  flags &= ~256
  if (!this.space.cfgTextures) {
    flags &= ~2
  }
  for (var i = 0; i < this.shaders.length; i++) {
    var s = this.shaders[i]
    if (s.flags == flags) {
      if (s.loadedtextures != s.textures.length && s.bValid) {
        var c = s.readyTextures(false)
        if (c != s.loadedtextures) {
          s.bValid = false
        }
      }
      if (s.numLights != this.space.lights.length) {
        s.bValid = false
      }
      return s
    }
  }
  var s = new shader3d(flags)
  this.shaders.push(s)
  return s
}
shader3d.prototype.addVar = function(id) {
  var v = new mtlvar3d(id)
  this.vars.push(v)
  return v
}
shader3d.prototype.addAttr = function(id, shName, gl) {
  var attr = {}
  attr.id = id
  attr.slot = gl.getAttribLocation(this.program, shName)
  gl.enableVertexAttribArray(attr.slot)
  this.attrs.push(attr)
}
shader3d.prototype.addLightVar = function(id, name, light) {
  var v = this.addVar(id)
  v.name = name
  v.light = light
  return v
}
shader3d.prototype.addChVar = function(id, name, ch) {
  var v = this.addVar(id)
  v.name = name
  v.channel = ch
  return v
}
function compareTM3(a, b) {
  if (a === undefined && b === undefined) {
    return true
  }
  if (a === undefined || b === undefined) {
    return false
  }
  for (var i = 0; i < 9; i++) {
    if (Math.abs(a[i] - b[i]) > 0.0001) {
      return false
    }
  }
  return true
}
shader3d.prototype.getTexture = function(c) {
  var items = this.textures
  for (var j = 0; j < items.length; j++) {
    var t = items[j]
    if (
      t.txt === c.texture &&
      t.wrapS == c.wrapS &&
      t.wrapT == c.wrapT &&
      compareTM3(t.tm, c.tm)
    ) {
      return t
    }
  }
  return null
}
shader3d.prototype.preChannel = function(ch) {
  var text = ''
  for (var i = 0; i < ch.length; i++) {
    var c = ch[i]
    c._id = this.channelId
    this.channelId++
    if (c.color != null) {
      var name = 'ch' + c._id + 'clr'
      this.addChVar('color', name, c)
      text += 'uniform vec3 ' + name + ';\r\n'
    }
    if ('amount' in c) {
      var name = 'ch' + c._id + 'amount'
      this.addChVar('amount', name, c)
      text += 'uniform float ' + name + ';\r\n'
    }
  }
  return text
}
shader3d.prototype.handleChannel = function(gl, ch, cmp) {
  var text = ''
  for (var i = 0; i < ch.length; i++) {
    var c = ch[i]
    var cname = null
    var tname = null
    var aname = null
    if (c.color != null) {
      cname = 'ch' + c._id + 'clr'
    }
    if (c.texture != null && c.texture.ivready) {
      var t = this.getTexture(c)
      if (t) {
        if (c.texture.ivtype == gl.TEXTURE_CUBE_MAP) {
          text +=
            'vec3 lup = reflect(eyeDirection,normal);lup.y*=-1.0;vec4 refColor=' +
            'textureCube(txtUnit' +
            t.slot +
            ',lup);'
          tname = 'refColor'
        } else {
          tname = 'txtColor' + t.slot
        }
      }
    }
    if (tname && c.amount != null) {
      aname = 'ch' + c._id + 'amount'
    }
    if (cname || tname) {
      if (cmp) {
        text += cmp + '*='
      } else {
        text += 'color+='
      }
      if (aname && tname) {
        text += 'vec3(' + aname + ')*vec3(' + tname + ')'
      } else {
        if (cname && tname) {
          text += cname + '*vec3(' + tname + ')'
        } else {
          if (cname) {
            text += cname
          } else {
            text += 'vec3(' + tname + ')'
          }
        }
      }
      text += ';\r\n'
    }
  }
  if (cmp) {
    text += 'color+=' + cmp + ';\r\n'
  }
  return text
}
shader3d.prototype.handleAlphaChannel = function(gl, ch) {
  if (ch && ch.length) {
    if (ch.length == 1) {
      var c = ch[0]
      var tname = null
      var aname = null
      if (c.texture != null && c.texture.ivready) {
        var t = this.getTexture(c)
        if (t) {
          tname = 'txtColor' + t.slot
        }
      }
      if (c.amount != null) {
        aname = 'ch' + c._id + 'amount'
      }
      if (tname) {
        var t
        if (aname) {
          t = aname + '*'
        } else {
          t = ''
        }
        if (c.cmp && c.cmp == 'a') {
          t += tname + '.a'
        } else {
          t += '(' + tname + '.x+' + tname + '.y+' + tname + '.z)/3.0'
        }
        return t
      } else {
        if (aname) {
          return aname
        }
      }
    }
  }
  return 1
}
shader3d.prototype.handleBumpChannel = function(gl, ch) {
  if (ch && ch.length) {
    var c = ch[0]
    if (c.texture != null && c.texture.ivready) {
      var t = this.getTexture(c)
      if (t) {
        if (c.texture) {
          var tname = 'txtColor' + t.slot
          var text = '\r\nvec3 _n=vec3(' + tname + ');'
          text += '_n-=vec3(0.5,0.5,0);_n*=vec3(2.0,2.0,1.0);'
          if (c.amount != null) {
            var aname = 'ch' + c._id + 'amount'
            text += '_n*=vec3(' + aname + ',' + aname + ',1.0);'
          }
          text += '_n=normalize(_n);'
          return text
        }
      }
    }
  }
  return null
}
shader3d.prototype.collectTextures = function(ch) {
  var rez = false
  if (ch != undefined) {
    for (var i = 0; i < ch.length; i++) {
      var c = ch[i]
      if (c.texture) {
        if (!this.getTexture(c)) {
          var t = { txt: c.texture, slot: 0, wrapS: c.wrapS, wrapT: c.wrapT }
          if (c.tm) {
            t.tm = c.tm
            t.ch = c
          }
          this.textures.push(t)
        }
        if (c.texture.ivready) {
          rez = true
        }
      }
    }
  }
  return rez
}
shader3d.prototype.readyTextures = function(bSet) {
  var c = 0
  for (var i = 0; i < this.textures.length; i++) {
    var t = this.textures[i]
    if (t.txt.ivready) {
      if (bSet) {
        t.slot = c
      }
      c++
    }
  }
  return c
}
shader3d.prototype.update = function(mtl) {
  if (this.program) {
    this.detach(mtl.gl)
  }
  this.numLights = mtl.space.lights.length
  this.channelId = 0
  var gl = mtl.gl,
    i
  var _lights = null
  var vText =
    this.flags & 8
      ? 'uniform mat4 tmWorld;uniform mat4 tmModelView; uniform mat4 tmPrj;'
      : ''
  if (this.flags & 196608) {
    vText += 'uniform float zOffset;'
  }
  var bNormals = (this.flags & 1) != 0,
    bSpecular = false,
    bDiffuse = false,
    bLights = false,
    bReflection = false,
    bBump = false
  var bEmissive = mtl.isChannel(mtl.emissive)
  var bOpacity = mtl.isChannel(mtl.opacity)
  var lights = mtl.space.lights
  vText += 'attribute vec3 inV;'
  vText += 'varying vec4 wPosition;'
  if (this.flags & 4) {
    vText += 'varying vec3 vC;attribute vec3 inC;'
  }
  var bUV = false
  if (bNormals) {
    vText += 'varying vec3 wNormal;attribute vec3 inN;'
    if (lights.length) {
      if (mtl.isChannel(mtl.diffuse)) {
        bDiffuse = true
      }
      if (mtl.isChannel(mtl.specular)) {
        bSpecular = true
      }
      bLights = bDiffuse || bSpecular
    }
    if (mtl.space.cfgTextures) {
      bReflection = this.collectTextures(mtl.reflection)
    }
  }
  if (this.flags & 2) {
    if (bDiffuse) {
      bUV |= this.collectTextures(mtl.diffuse)
    }
    if (bSpecular) {
      bUV |= this.collectTextures(mtl.specular)
    }
    if (bEmissive) {
      bUV |= this.collectTextures(mtl.emissive)
    }
    if (bOpacity) {
      bUV |= this.collectTextures(mtl.opacity)
    }
    if (bNormals && bLights) {
      bUV |= bBump = this.collectTextures(mtl.bump)
    }
  }
  if (bUV) {
    vText += 'varying vec2 vUV;attribute vec2 inUV;'
  }
  if (bBump) {
    vText += 'varying vec3 vBN,vBT;attribute vec3 inBN,inBT;'
  }
  this.loadedtextures = this.readyTextures(true)
  vText += '\r\nvoid main(void){\r\n'
  if (this.flags & 8) {
    vText +=
      'wPosition= tmWorld*vec4(inV,1.0);vec4 vPosition = tmModelView* wPosition; gl_Position = tmPrj* vPosition; '
    this.addVar('tmWorld')
    this.addVar('tmModelView')
    this.addVar('tmPrj')
  } else {
    vText += 'gl_Position = vec4(inV,1.0);'
  }
  if (this.flags & 196608) {
    this.addVar('zOffset')
    vText += 'gl_Position.z+=zOffset;'
  }
  if (bNormals) {
    vText += '\r\n wNormal = normalize(vec3(tmWorld* vec4(inN,0.0)));'
  }
  if (bBump) {
    vText +=
      '\r\n vBN = normalize(vec3(tmWorld* vec4(inBN,0.0)));vBT = normalize(vec3(tmWorld* vec4(inBT,0.0)));'
  }
  if (bUV) {
    vText += 'vUV = inUV;'
  }
  if (this.flags & 4) {
    vText += 'vC = inC;'
  }
  vText += '}'
  var fText = 'precision mediump float;'
  if (bNormals) {
    fText += 'varying vec4 wPosition;'
  }
  if (this.flags & 4) {
    fText += 'varying vec3 vC;'
  }
  if (bUV) {
    fText += 'varying vec2 vUV;'
  }
  if (bBump) {
    fText += 'varying vec3 vBN,vBT;'
  }
  if (bDiffuse) {
    fText += this.preChannel(mtl.diffuse)
  }
  if (bSpecular) {
    fText += this.preChannel(mtl.specular)
  }
  if (bEmissive) {
    fText += this.preChannel(mtl.emissive)
  }
  if (bReflection) {
    fText += this.preChannel(mtl.reflection)
  }
  if (bOpacity) {
    fText += this.preChannel(mtl.opacity)
  }
  if (bBump) {
    fText += this.preChannel(mtl.bump)
  }
  for (i = 0; i < this.textures.length; i++) {
    var t = this.textures[i]
    if (t.txt.ivready) {
      fText += 'uniform '
      if (t.txt.ivtype == gl.TEXTURE_CUBE_MAP) {
        fText += 'samplerCube'
      } else {
        fText += 'sampler2D'
      }
      fText += ' txtUnit' + t.slot + ';'
      if (t.tm) {
        var v = this.addVar('tm')
        v.channel = t.ch
        fText += 'uniform mat3 ch' + t.ch._id + 'tm;'
      }
    }
  }
  if (bNormals) {
    fText += 'uniform vec3 eye;'
    this.addVar('eye')
    if (bSpecular) {
      fText += 'uniform float mtlPhong;'
      this.addVar('mtlPhong')
    }
    fText += 'varying vec3 wNormal;'
    fText += 'float k;'
    if (bLights) {
      fText += 'vec3 diffuse,specular,lightDir;'
      _lights = []
      for (i = 0; i < lights.length; i++) {
        var ls = lights[i]
        var l = {}
        l.light = ls
        var colorname = 'light' + i + 'Clr'
        fText += '\r\n uniform vec3 ' + colorname + ';'
        l.colorname = colorname
        this.addLightVar('lightColor', colorname, ls)
        if (ls.dir) {
          var dirname = 'light' + i + 'Dir'
          l.dirname = dirname
          fText += 'uniform vec3 ' + dirname + ';'
          this.addLightVar('lightDir', dirname, ls)
        }
        if (ls.org) {
          var orgname = 'light' + i + 'Org'
          l.orgname = orgname
          fText += 'uniform vec3 ' + orgname + ';'
          this.addLightVar('lightOrg', orgname, ls)
        }
        _lights.push(l)
      }
    }
  }
  fText += '\nvoid main(void) {\r\n'
  for (i = 0; i < this.textures.length; i++) {
    var t = this.textures[i]
    if (t.txt.ivready && t.txt.ivtype == gl.TEXTURE_2D) {
      if (t.tm) {
        fText += 'vec2 _uv=vec2(ch' + t.ch._id + 'tm*vec3(vUV,1.0));\r\n'
      }
      fText +=
        'vec4 txtColor' +
        t.slot +
        '= texture2D(txtUnit' +
        t.slot +
        ',' +
        (t.tm ? '_uv' : 'vUV') +
        ');'
    }
  }
  if (bNormals) {
    fText += 'vec3 normal = normalize(wNormal);'
    if (bBump) {
      var txt = this.handleBumpChannel(gl, mtl.bump)
      if (txt) {
        fText += txt
        fText += 'mat3 tsM = mat3(normalize(vBN), normalize(vBT), normal);'
        fText += 'normal =  normalize(tsM*_n);'
      }
    }
    if (mtl.space.cfgDbl) {
      fText += 'if(!gl_FrontFacing)normal=-normal;'
    }
    fText += 'vec3 eyeDirection = normalize(wPosition.xyz-eye);vec3 reflDir;'
    if (_lights) {
      for (i = 0; i < _lights.length; i++) {
        var l = _lights[i]
        var dirName
        if (l.orgname) {
          fText += 'lightDir = normalize( wPosition.xyz-' + l.orgname + ');'
          dirName = 'lightDir'
        } else {
          dirName = l.dirname
        }
        if (bSpecular) {
          fText += '\nreflDir = reflect(-' + dirName + ', normal);'
          fText += 'k= pow(max(dot(reflDir, eyeDirection), 0.0), mtlPhong);'
          if (i) {
            fText += 'specular+='
          } else {
            fText += 'specular='
          }
          fText += 'k*' + l.colorname + ';'
        }
        if (bDiffuse) {
          fText += 'k = max(dot(normal, -' + dirName + '), 0.0);'
          if (i) {
            fText += 'diffuse+='
          } else {
            fText += 'diffuse='
          }
          fText += 'k*' + l.colorname + ';'
        }
      }
    }
  }
  fText += 'vec3 color= vec3(0.0,0.0,0.0);\r\n'
  if (bReflection) {
    fText += this.handleChannel(gl, mtl.reflection, null)
  }
  if (this.flags & 4) {
    fText += bDiffuse ? 'diffuse=diffuse*vC;' : 'color+=vC;'
  }
  if (bDiffuse) {
    fText += this.handleChannel(gl, mtl.diffuse, 'diffuse')
  }
  if (bSpecular) {
    fText += this.handleChannel(gl, mtl.specular, 'specular')
  }
  if (bEmissive) {
    fText += this.handleChannel(gl, mtl.emissive, null)
  }
  if (bOpacity) {
    var n = this.handleAlphaChannel(gl, mtl.opacity)
    if (bReflection) {
      fText += 'float alpha = ' + n + ';'
      fText += 'gl_FragColor = vec4(color,alpha);'
    } else {
      fText += 'gl_FragColor = vec4(color,' + n + ');'
    }
  } else {
    fText += 'gl_FragColor = vec4(color,1);'
  }
  fText += '}'
  this.vShader = ivCompileShader(gl, vText, gl.VERTEX_SHADER)
  this.fShader = ivCompileShader(gl, fText, gl.FRAGMENT_SHADER)
  var shPrg = gl.createProgram()
  this.program = shPrg
  gl.attachShader(shPrg, this.vShader)
  gl.attachShader(shPrg, this.fShader)
  gl.linkProgram(shPrg)
  if (!gl.getProgramParameter(shPrg, gl.LINK_STATUS)) {
    alert('Could not initialise shaders')
  }
  gl.useProgram(shPrg)
  this.addAttr('v', 'inV', gl)
  if (bNormals) {
    this.addAttr('n', 'inN', gl)
  }
  if (bUV) {
    this.addAttr('uv', 'inUV', gl)
  }
  if (bBump) {
    this.addAttr('bn', 'inBN', gl)
    this.addAttr('bt', 'inBT', gl)
  }
  if (this.flags & 4) {
    this.addAttr('clr', 'inC', gl)
  }
  for (i = 0; i < this.textures.length; i++) {
    var t = this.textures[i]
    if (t.txt.ivready) {
      t.uniform = gl.getUniformLocation(shPrg, 'txtUnit' + t.slot)
    }
  }
  for (i = 0; i < this.vars.length; i++) {
    var v = this.vars[i]
    switch (v.id) {
      case 'tm':
        v.slot = gl.getUniformLocation(shPrg, 'ch' + v.channel._id + 'tm')
        break
      case 'color':
        v.slot = gl.getUniformLocation(shPrg, 'ch' + v.channel._id + 'clr')
        break
      case 'amount':
        v.slot = gl.getUniformLocation(shPrg, 'ch' + v.channel._id + 'amount')
        break
      case 'lightColor':
      case 'lightDir':
      case 'lightOrg':
        v.slot = gl.getUniformLocation(shPrg, v.name)
        break
      default:
        v.slot = gl.getUniformLocation(shPrg, v.id)
    }
  }
  this.bValid = true
  return true
}
shader3d.prototype.detach = function(gl) {
  if (this.program !== null) {
    gl.detachShader(this.program, this.vShader)
    gl.detachShader(this.program, this.fShader)
    gl.deleteProgram(this.program)
    gl.deleteShader(this.vShader)
    gl.deleteShader(this.fShader)
    this.program = null
    this.fShader = null
    this.vShader = null
  }
  this.attrs = []
  this.vars = []
  this.textures = []
  this.loadedtextures = 0
}
shader3d.prototype.activate = function(space, mtl, tm, flags, newObj) {
  var gl = mtl.gl,
    i
  if (!newObj) {
    gl.useProgram(this.program)
    for (i = 0; i < this.textures.length; i++) {
      var t = this.textures[i]
      if (t.txt.ivready) {
        gl.activeTexture(gl.TEXTURE0 + t.slot)
        var type = t.txt.ivtype
        gl.bindTexture(type, t.txt)
        if (type == gl.TEXTURE_2D && space.e_ans) {
          gl.texParameterf(
            type,
            space.e_ans.TEXTURE_MAX_ANISOTROPY_EXT,
            space.e_ansMax
          )
        }
        gl.texParameteri(type, gl.TEXTURE_WRAP_S, t.wrapS)
        gl.texParameteri(type, gl.TEXTURE_WRAP_T, t.wrapT)
        gl.uniform1i(t.uniform, t.slot)
      }
    }
  }
  if (flags & 256) {
    var sel = space.clrSelection
  }
  for (i = 0; i < this.vars.length; i++) {
    var a = this.vars[i],
      s = a.slot
    switch (a.id) {
      case 'tmWorld':
        gl.uniformMatrix4fv(s, false, tm)
        break
      case 'tmModelView':
        gl.uniformMatrix4fv(s, false, space.modelviewTM)
        break
      case 'tmPrj':
        gl.uniformMatrix4fv(s, false, space.projectionTM)
        break
      case 'mtlPhong':
        gl.uniform1f(s, mtl.phong)
        break
      case 'color':
        if (sel) {
          var c = a.channel.color
          gl.uniform3f(s, c[0] * sel[0], c[1] * sel[1], c[2] * sel[2])
        } else {
          gl.uniform3fv(s, a.channel.color)
        }
        break
      case 'tm':
        gl.uniformMatrix3fv(s, false, a.channel.tm)
        break
      case 'amount':
        gl.uniform1f(s, a.channel.amount)
        break
      case 'zOffset':
        gl.uniform1f(s, flags & 65536 ? 0.00002 : -0.02)
        break
      case 'lightColor':
        gl.uniform3fv(s, a.light.color)
        break
      case 'lightOrg':
        gl.uniform3fv(s, a.light.org)
        break
      case 'lightDir':
        gl.uniform3fv(s, a.light.dir)
        break
      case 'eye':
        gl.uniform3fv(s, space.window.viewFrom)
        break
      default:
    }
  }
}
function ivCompileShader(gl, str, type) {
  var shader = gl.createShader(type)
  gl.shaderSource(shader, str)
  gl.compileShader(shader)
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert(str + '\r\n' + gl.getShaderInfoLog(shader))
    return null
  }
  return shader
}
function mesh3d(gl) {
  this.gl = gl
  this.vertexBuffer = null
  this.normalBuffer = null
  this.uvBuffer = null
  this.facesBuffer = null
  this.lineMode = false
  this.url = ''
  this.ref = 0
}
mesh3d.prototype.addRef = function() {
  this.ref++
}
mesh3d.prototype.release = function() {
  this.ref--
  if (this.ref < 1) {
    this.clear()
  }
}
function addEdge(e, v1, v2) {
  if (v2 > v1) {
    var _v = v2
    v2 = v1
    v1 = _v
  }
  if (e[v1] == undefined) {
    e[v1] = v2
  } else {
    if (typeof e[v1] === 'number') {
      e[v1] = [e[v1], v2]
    } else {
      e[v1].push(v2)
    }
  }
}
mesh3d.prototype.updateEdges = function() {
  if (!this.edgeBuffer) {
    var e = []
    var f = this.faces
    var nf = f.length / 3
    var j = 0
    var i
    for (i = 0; i < nf; i++) {
      addEdge(e, f[j], f[j + 1])
      addEdge(e, f[j + 1], f[j + 2])
      addEdge(e, f[j + 2], f[j])
      j += 3
    }
    var ne = e.length
    var num = 0
    for (i = 0; i < ne; i++) {
      var v = e[i]
      if (v != undefined) {
        if (typeof v === 'number') {
          num++
        } else {
          num += v.length
        }
      }
    }
    var edges = new Uint16Array(num * 2)
    var j = 0
    for (i = 0; i < ne; i++) {
      var v = e[i]
      if (v != undefined) {
        if (typeof v === 'number') {
          edges[j] = i
          edges[j + 1] = v
          j += 2
        } else {
          for (var i1 = 0; i1 < v.length; i1++) {
            edges[j] = i
            edges[j + 1] = v[i1]
            j += 2
          }
        }
      }
    }
    this.edgeBuffer = ivBufferI(this.gl, edges)
  }
}
function b_normalize_array(v) {
  var sz = v.length / 3
  for (i = 0; i < 4; i++) {
    var j = i * 3
    var a = v[j],
      b = v[j + 1],
      c = v[j + 2]
    var l = Math.sqrt(a * a + b * b + c * c)
    if (l) {
      v[j] = a / l
      v[j + 1] = b / l
      v[j + 2] = c / l
    }
  }
}
function b_getv(a, i, v) {
  v[0] = a[i * 3]
  v[1] = a[i * 3 + 1]
  v[2] = a[i * 3 + 2]
}
function b_gett(a, i, t) {
  t[0] = a[i * 2]
  t[1] = a[i * 2 + 1]
}
function b_sub(a, b, l) {
  l[0] = a[0] - b[0]
  l[1] = a[1] - b[1]
  l[2] = a[2] - b[2]
  vec3.normalize(l)
}
function b_setv(a, i, v) {
  a[i * 3] += v[0]
  a[i * 3 + 1] += v[1]
  a[i * 3 + 2] += v[2]
}
mesh3d.prototype.updateBumpInfo = function(gl, f, v, n, uv) {
  if (f && v && n && uv) {
    var wtm = mat4.create(),
      ttm = mat4.create(),
      ittm = mat4.create()
    mat4.identity(wtm)
    mat4.identity(ttm)
    var sz = v.length,
      tc = f.length
    var a = new Float32Array(sz)
    var b = new Float32Array(sz)
    var i, j
    var v0 = [0, 0, 0],
      v1 = [0, 0, 0],
      v2 = [0, 0, 0]
    var t0 = [0, 0],
      t1 = [0, 0],
      t2 = [0, 0]
    var line0 = [0, 0, 0],
      line1 = [0, 0, 0]
    var r = [0, 0, 0],
      U = [0, 0, 0],
      V = [0, 0, 0]
    var vone = [0, 0, 1]
    var vzero = [0, 0, 0]
    for (i = 0; i < tc; i++) {
      var i0 = f[i * 3],
        i1 = f[i * 3 + 1],
        i2 = f[i * 3 + 2]
      b_getv(v, i0, v0)
      b_getv(v, i1, v1)
      b_getv(v, i2, v2)
      b_gett(uv, i0, t0)
      b_gett(uv, i1, t1)
      b_gett(uv, i2, t2)
      b_sub(v0, v1, line0)
      b_sub(v1, v2, line1)
      var normal = vec3.cross_rn(line0, line1)
      for (j = 0; j < 2; j++) {
        var vj = j == 0 ? v1 : v2
        var tj = j == 0 ? t1 : t2
        r[0] = tj[0] - t0[0]
        r[1] = tj[1] - t0[1]
        vec3.normalize(r)
        mat4.setRow(ttm, j, r)
        r[0] = vj[0] - v0[0]
        r[1] = vj[1] - v0[1]
        r[2] = vj[2] - v0[2]
        vec3.normalize(r)
        mat4.setRow(wtm, j, r)
      }
      mat4.setRow(ttm, 2, vone)
      mat4.setRow(wtm, 2, normal)
      mat4.setRow(wtm, 3, vzero)
      mat4.setRow(ttm, 3, vzero)
      mat4.invert(ittm, ttm)
      mat4.m(wtm, ittm, ttm)
      U = mat4.mulVector(ttm, [1, 0, 0], U)
      V = mat4.mulVector(ttm, [0, 1, 0], V)
      b_setv(a, i0, U)
      b_setv(a, i1, U)
      b_setv(a, i2, U)
      b_setv(b, i0, V)
      b_setv(b, i1, V)
      b_setv(b, i2, V)
    }
    b_normalize_array(a)
    b_normalize_array(b)
    this.bnBuffer = ivBufferF(gl, a, 3)
    this.btBuffer = ivBufferF(gl, b, 3)
  }
}
function meshSetFInfo(gl, b, v) {
  gl.bindBuffer(gl.ARRAY_BUFFER, b)
  gl.vertexAttribPointer(v.slot, b.itemSize, gl.FLOAT, false, 0, 0)
}
mesh3d.prototype.render = function(tm, space, material, state) {
  var fb = this.facesBuffer
  if (fb && this.vertexBuffer) {
    var gl = space.gl
    if (state & 16) {
      gl.depthMask(false)
    }
    var shFlags = 8
    var rmode = space.rmodes[(state & 65280) >> 8]
    var bEdges = rmode.e
    if (bEdges) {
      this.updateEdges(gl)
    } else {
      if (this.normalBuffer) {
        shFlags |= 1
      }
      if (this.uvBuffer) {
        shFlags |= 2
      }
      if (this.colorBuffer) {
        shFlags |= 4
      }
      if (this.bnBuffer) {
        shFlags |= 16
      }
      if (state & 4) {
        shFlags |= 256
      }
    }
    shFlags |= state & 196608
    var s = space.activateMaterial(material, tm, shFlags)
    for (var i = 0; i < s.attrs.length; i++) {
      var v = s.attrs[i]
      switch (v.id) {
        case 'v':
          meshSetFInfo(gl, this.vertexBuffer, v)
          break
        case 'n':
          meshSetFInfo(gl, this.normalBuffer, v)
          break
        case 'uv':
          meshSetFInfo(gl, this.uvBuffer, v)
          break
        case 'bn':
          meshSetFInfo(gl, this.bnBuffer, v)
          break
        case 'bt':
          meshSetFInfo(gl, this.btBuffer, v)
          break
        case 'clr':
          gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer)
          gl.vertexAttribPointer(
            v.slot,
            this.colorBuffer.itemSize,
            gl.UNSIGNED_BYTE,
            true,
            0,
            0
          )
          break
      }
    }
    if (bEdges) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.edgeBuffer)
      gl.drawElements(gl.LINES, this.edgeBuffer.numItems, gl.UNSIGNED_SHORT, 0)
    } else {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, fb)
      var o = fb.offset
      gl.drawElements(
        this.lineMode ? gl.LINES : gl.TRIANGLES,
        fb.numItems,
        gl.UNSIGNED_SHORT,
        o ? o : 0
      )
    }
    if (state & 16) {
      gl.depthMask(true)
    }
  } else {
  }
}
function ivBufferF(gl, v, cmp) {
  var b = gl.createBuffer()
  gl.bindBuffer(gl.ARRAY_BUFFER, b)
  gl.bufferData(gl.ARRAY_BUFFER, v, gl.STATIC_DRAW)
  b.itemSize = cmp
  b.numItems = v.length / cmp
  return b
}
function ivBufferI(gl, v) {
  var b = gl.createBuffer()
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, b)
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, v, gl.STATIC_DRAW)
  b.itemSize = 1
  b.numItems = v.length
  return b
}
mesh3d.prototype.clear = function() {
  var gl = this.gl
  if (this.facesBuffer) {
    gl.deleteBuffer(this.facesBuffer)
    delete this.facesBuffer
  }
  if (this.uvBuffer) {
    gl.deleteBuffer(this.uvBuffer)
    delete this.uvBuffer
  }
  if (this.vertexBuffer) {
    gl.deleteBuffer(this.vertexBuffer)
    delete this.vertexBuffer
  }
  if (this.normalBuffer) {
    gl.deleteBuffer(this.normalBuffer)
    delete this.normalBuffer
  }
  if (this.edgeBuffer) {
    gl.deleteBuffer(this.edgeBuffer)
    delete this.edgeBuffer
  }
}
function node3d() {
  this.object = null
  this.tm = null
  this.name = ''
  this.material = null
  this.state = 3
  this.ref = 0
}
node3d.prototype.addRef = function() {
  this.ref++
}
node3d.prototype.release = function() {
  this.ref--
  if (this.ref < 1) {
    this.clear()
  }
}
node3d.prototype.newNode = function() {
  var n = new node3d()
  this.insert(n)
  return n
}
node3d.prototype.insert = function(n) {
  n.ref++
  if (this.lastChild) {
    this.lastChild.next = n
  } else {
    this.firstChild = n
  }
  this.lastChild = n
  n.parent = this
}
node3d.prototype.clear = function() {
  while (this.firstChild) {
    this.remove(this.firstChild)
  }
  this.setObject(null)
}
node3d.prototype.remove = function(n) {
  if (n.parent != this) {
    return false
  }
  var _n = null
  if (this.firstChild == n) {
    this.firstChild = n.next
  } else {
    _n = this.firstChild
    while (_n) {
      if (_n.next == n) {
        _n.next = n.next
        break
      }
      _n = _n.next
    }
  }
  if (this.lastChild == n) {
    this.lastChild = _n
  }
  n.parent = null
  n.next = null
  n.release()
  return true
}
node3d.prototype.setState = function(s, mask) {
  var _state = (this.state & ~mask) | (mask & s)
  if (_state != this.state) {
    this.state = _state
    return true
  }
  return false
}
node3d.prototype.traverse = function(ptm, proc, param, astate) {
  astate |= this.state & 4
  if (this.state & 65280) {
    astate &= ~65280
    astate |= this.state & 65280
  }
  var v = 3
  v = this.state & 3
  if (!v) {
    return
  }
  var newtm
  if (this.tm) {
    if (ptm) {
      newtm = mat4.create()
      mat4.m(this.tm, ptm, newtm)
    } else {
      newtm = this.tm
    }
  } else {
    newtm = ptm
  }
  if (v & 1) {
    if (!proc(this, newtm, param, astate)) {
      return
    }
  }
  if (v & 2) {
    var child = this.firstChild
    while (child) {
      child.traverse(newtm, proc, param, astate)
      child = child.next
    }
  }
}
node3d.prototype.setObject = function(obj) {
  if (this.object != obj) {
    if (this.object) {
      this.object.release()
    }
    this.object = obj
    if (obj) {
      obj.ref++
    }
  }
}
node3d.prototype.load = function(d, info) {
  var i, j
  if (d.name !== undefined) {
    this.name = d.name
  }
  if (d.meta !== undefined) {
    this.meta = d.meta
  }
  if (d.object != undefined) {
    this.setObject(info.objects[d.object])
  }
  if (d.mtl != undefined) {
    this.material = info.materials[d.mtl]
    if (this.material && this.material.bump && this.object) {
      this.object.bump = true
    }
  }
  if (d.s != undefined) {
    this.state = d.s
  }
  if (d.t != undefined) {
    this.type = d.t
  }
  if (d.tm) {
    this.tm = mat4.create()
    mat4.identity(this.tm)
    var index = 0
    for (i = 0; i < 4; i++) {
      for (j = 0; j < 3; j++) {
        this.tm[i * 4 + j] = d.tm[index]
        index++
      }
    }
  }
  if (d.anim) {
    this.anim = d.anim
  }
  if (d.i) {
    var n = d.i
    for (i = 0; i < n.length; i++) {
      var node = this.newNode()
      node.load(n[i], info)
    }
  }
}
function nodeRender(node, tm, space, state) {
  var o = node.object
  if (o) {
    if (o.url) {
      var r = CreateRequest(o.url)
      if (r) {
        space.meshesInQueue++
        r.ivobject = o
        r.ivspace = space
        loadMesh(r)
        r.send()
      }
      delete o.url
    } else {
      if (o.boxMin) {
        space.toRenderQueue(tm, node, state)
      }
    }
  }
  return true
}
node3d.prototype.GetWTM = function() {
  var tm = null
  var n = this
  while (n) {
    if (n.tm) {
      if (tm) {
        mat4.m(tm, n.tm)
      } else {
        tm = mat4.create(n.tm)
      }
    }
    n = n.parent
  }
  return tm
}
function loadMesh(request) {
  request.responseType = 'arraybuffer'
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var arrayBuffer = this.response
      this.ivobject.onMeshReady(this.ivspace, arrayBuffer)
      this.ivspace.onMeshLoaded(this.ivobject)
    }
  }
}
function ivBitStream(dv, dvpos) {
  this.st = dv
  this.stpos = dvpos
  this.m_b = 0
  this.m_pos = 0
}
ivBitStream.prototype.Read = function(b) {
  var r = 0
  while (b) {
    if (!this.m_pos) {
      this.m_b = this.st.getUint8(this.stpos++)
      this.m_pos = 8
    }
    var t = b
    if (t > this.m_pos) {
      t = this.m_pos
    }
    r <<= t
    r |= (this.m_b & 255) >> (8 - t)
    this.m_b <<= t
    b -= t
    this.m_pos -= t
  }
  return r
}
function NumBits(index) {
  var i = 0
  while (index) {
    index >>= 1
    i++
  }
  return i
}
function ivdcmpf(cmp, v, c) {
  var index = 0,
    k = 0,
    code = 0
  while (k < c) {
    code = cmp.Read(2)
    if (code == 0) {
      v[k] = index
      index++
    } else {
      code <<= 1
      code |= cmp.Read(1)
      if (code == 4) {
        v[k] = v[k - 1] + 1
      } else {
        if (code == 5) {
          v[k] = v[k - cmp.Read(4) - 2]
        } else {
          if (code == 6) {
            v[k] = cmp.Read(NumBits(index))
          } else {
            if (code == 7) {
              var sign = cmp.Read(1)
              var delta = cmp.Read(5)
              if (sign) {
                delta *= -1
              }
              v[k] = v[k - 1] + delta
            } else {
              var j = k - (cmp.Read(code == 2 ? 4 : 13) + 1)
              var _v1, _v2
              if (j % 3 || j >= k - 2) {
                _v1 = j
                _v2 = j - 1
              } else {
                _v2 = j + 2
                _v1 = j
              }
              v[k] = v[_v1]
              v[k + 1] = v[_v2]
              k++
            }
          }
        }
      }
    }
    k++
  }
}
function dcdnrml(d, i, n, j) {
  var nx, ny, nz, k, l
  var a = 0.00009587526218325454 * d.getUint16(i, true)
  var b = 0.00004793763109162727 * d.getUint16(i + 2, true)
  k = Math.sin(b)
  nx = Math.cos(a) * k
  ny = Math.sin(a) * k
  nz = Math.cos(b)
  l = Math.sqrt(nx * nx + ny * ny + nz * nz)
  nx /= l
  ny /= l
  nz /= l
  n[j] = nx
  j++
  n[j] = ny
  j++
  n[j] = nz
}
function copyn(v, _n, i, j) {
  i *= 3
  j *= 3
  v[i] = _n[j]
  v[i + 1] = _n[j + 1]
  v[i + 2] = _n[j + 2]
}
function copyn_i(v, _n, i, j) {
  i *= 3
  j *= 3
  v[i] = -_n[j]
  v[i + 1] = -_n[j + 1]
  v[i + 2] = -_n[j + 2]
}
mesh3d.prototype.onMeshReady = function(space, buffer) {
  var gl = space.gl
  var data = new DataView(buffer)
  var numPoints = data.getUint16(0, true)
  var numFaces = data.getUint16(2, true)
  var flags = data.getUint16(4, true)
  var offset = 6
  var n3 = numPoints * 3
  var nF = numFaces
  var vminx, vminy, vminz, vmdx, vmdy, vmdz
  if (flags & 8) {
    this.lineMode = true
    nF *= 2
  } else {
    nF *= 3
  }
  var v = new Float32Array(n3)
  var voffset = offset
  offset += 24
  var index = 0,
    i
  offset += numPoints * 6
  var f = new Uint16Array(nF)
  if (flags & 256) {
    var bs = new ivBitStream(data, offset)
    ivdcmpf(bs, f, nF)
    offset = bs.stpos
  } else {
    if (flags & 4) {
      for (i = 0; i < nF; i++) {
        f[i] = data.getUint8(offset++)
      }
    } else {
      for (i = 0; i < nF; i++) {
        f[i] = data.getUint16(offset, true)
        offset += 2
      }
    }
  }
  this.facesBuffer = ivBufferI(gl, f)
  if (flags & 16) {
    var cs = data.getUint16(offset, true)
    offset += 2
    var n
    if (this.bump) {
      n = new Float32Array(n3)
    } else {
      n = v
    }
    if (cs) {
      var _n = new Float32Array(cs * 3)
      for (i = 0; i < cs; i++) {
        dcdnrml(data, offset, _n, i * 3)
        offset += 4
      }
      var bs = new ivBitStream(data, offset)
      i = 0
      var j = 0,
        ibits = 0
      while (i < numPoints) {
        var cd = bs.Read(1)
        if (cd) {
          cd = bs.Read(1)
          if (ibits) {
            index = bs.Read(ibits)
          } else {
            index = 0
          }
          if (cd) {
            copyn(n, _n, i, index)
          } else {
            copyn_i(n, _n, i, index)
          }
        } else {
          ibits = NumBits(j)
          copyn(n, _n, i, j)
          j++
        }
        i++
      }
      offset = bs.stpos
    } else {
      for (i = 0; i < numPoints; i++) {
        dcdnrml(data, offset, n, i * 3)
        offset += 4
      }
    }
    this.normalBuffer = ivBufferF(gl, n, 3)
  }
  if (flags & 32) {
    var uv = new Float32Array(numPoints * 2)
    vminx = data.getFloat32(offset, true)
    vminy = data.getFloat32(offset + 4, true)
    vmdx = data.getFloat32(offset + 8, true)
    vmdy = data.getFloat32(offset + 12, true)
    offset += 16
    index = 0
    for (i = 0; i < numPoints; i++) {
      uv[index] = vmdx * data.getUint16(offset, true) + vminx
      offset += 2
      uv[index + 1] = vmdy * data.getUint16(offset, true) + vminy
      offset += 2
      index += 2
    }
    this.uvBuffer = ivBufferF(gl, uv, 2)
  }
  if (flags & 64) {
    var colors = new Uint8Array(n3)
    for (i = 0; i < n3; i++) {
      colors[i] = data.getUint8(offset++)
    }
    this.colorBuffer = ivBufferF(gl, colors, 3)
  }
  offset = voffset
  vminx = data.getFloat32(offset, true)
  vminy = data.getFloat32(offset + 4, true)
  vminz = data.getFloat32(offset + 8, true)
  vmdx = data.getFloat32(offset + 12, true)
  vmdy = data.getFloat32(offset + 16, true)
  vmdz = data.getFloat32(offset + 20, true)
  offset += 24
  this.boxMin = [vminx, vminy, vminz]
  this.boxMax = [
    vmdx * 65535 + vminx,
    vmdy * 65535 + vminy,
    vmdz * 65535 + vminz
  ]
  index = 0
  for (i = 0; i < numPoints; i++) {
    v[index] = vmdx * data.getUint16(offset, true) + vminx
    offset += 2
    v[index + 1] = vmdy * data.getUint16(offset, true) + vminy
    offset += 2
    v[index + 2] = vmdz * data.getUint16(offset, true) + vminz
    offset += 2
    index += 3
  }
  this.vertexBuffer = ivBufferF(gl, v, 3)
  if (this.bump) {
    this.updateBumpInfo(gl, f, v, n, uv)
    delete this.bump
  }
  if (space.m_cfgKeepMeshData & 1) {
    this.faces = f
  }
  if (space.m_cfgKeepMeshData & 2) {
    this.points = v
  }
}
node3d.prototype.getNodeById = function(id) {
  if (this.name == id) {
    return this
  }
  var n = this.firstChild
  while (n) {
    var _n = n.getNodeById(id)
    if (_n) {
      return _n
    }
    n = n.next
  }
}
ivwindow3d.prototype.getNodeById = function(id) {
  if (this.space && this.space.root) {
    return this.space.root.getNodeById(id)
  }
  return null
}
ivwindow3d.prototype.showNodeById = function(id, bShow, bUpdate) {
  var n = this.getNodeById(id)
  if (n) {
    if (n) {
      var old = 3
      if (n.state != undefined) {
        old = n.state & 3
      }
      if (bShow) {
        n.state |= 3
      } else {
        n.state &= ~3
      }
      if (bUpdate != undefined && bUpdate && (old != n.state) & 3) {
        this.invalidate()
      }
    }
  }
}
ivwindow3d.prototype.setView = function(index) {
  if (
    this.space &&
    this.space.views &&
    index >= 0 &&
    index < this.space.views.length
  ) {
    var v = this.space.views[index]
    if (v) {
      this.setViewImp(v)
    }
  }
}
var view3d
