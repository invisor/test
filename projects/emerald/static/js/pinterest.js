!(function(e) {
  if (!window.pintrk) {
    window.pintrk = function() {
      window.pintrk.queue.push(Array.prototype.slice.call(arguments))
    }
    const n = window.pintrk
    ;(n.queue = []), (n.version = '3.0')
    const t = document.createElement('script')
    ;(t.async = !0), (t.src = e)
    const r = document.getElementsByTagName('script')[0]
    r.parentNode.insertBefore(t, r)
  }
})('https://s.pinimg.com/ct/core.js')
