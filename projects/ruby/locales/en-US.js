import layout from './en/layout'
import homePage from './en/homePage'
import metadata from './en/metadata'
import breadcrumbs from './en/breadcrumbs'
import listingPage from './en/listingPage'
import price from './en/price'
import lists from './en/lists'
import categories from './en/categories'
import detailsPage from './en/detailsPage'
import common from './en/common'
import customItem from './en/customItem'
import notifications from './en/notifications'
import form from './en/form'
import cart from './en/cart'
import share from './en/share'
import order from './en/order'
import auth from './en/auth'
import account from './en/account'
import wishlist from './en/wishlist'
import quickSearch from './en/quickSearch'
import ourStaff from './en/ourStaff'
import contacts from './en/contacts'
import phones from './en/phones'
import showroom from './en/showroom'
import newsletters from './en/newsletters'
import learnMore from './en/learnMore'
import designYourOwn from './en/designYourOwn'
import compareList from './en/compareList'
import imagesAlt from './en/imagesAlt'
import extendWarranty from './en/extendWarranty'
import metaTags from './en/metaTags'

export default {
  extendWarranty,
  layout,
  homePage,
  metadata,
  breadcrumbs,
  listingPage,
  price,
  lists,
  categories,
  detailsPage,
  common,
  customItem,
  notifications,
  form,
  cart,
  share,
  order,
  auth,
  account,
  wishlist,
  quickSearch,
  ourStaff,
  contacts,
  phones,
  showroom,
  newsletters,
  learnMore,
  designYourOwn,
  compareList,
  imagesAlt,
  metaTags
}
