import divisionNames from '~/locales/en/constants/divisionNames.js'

export default {
  hi: 'Hi, {name}',
  signIn: 'Sign in',
  recommendations: {
    title: 'Recommended for You',
    messageWithRecommendations:
      "Recommended For You is a personalized service option where one of our sales representatives lists suggestions for you to review based on your specific interests. This section of your user account will only be updated after you've already initiated communication with one of our client service representatives.",
    messageWithoutRecommendations:
      'At Natural Ruby Company, we pay close attention to our customers and their likes and dislikes. We offer no-fee customized shopping experience. If you would like a service representative to suggest items for you (which would then show on this page for you to review), please fill out the form below and we will be in touch with you soon with suggestions.',
    itemID: 'Item ID: {0}',
    viewItem: 'View Item',
    favorites: 'Add to favorites',
    compare: 'Add to compare',
    removeFavorites: 'Remove from favorites',
    removeCompare: 'Remove from compare',
    questions: 'Have questions about design?',
    help: 'Designer Help',
    delete: 'Delete this recommendation'
  },
  orders: {
    title: 'Orders',
    invoice: 'Invoice',
    statusDetails: 'View status details',
    productionStatus: 'Production status details',
    viewDetails: 'View Details',
    designId: 'Design ID: {0}',
    stoneId: 'Stone ID: {0}',
    settingId: 'Setting ID: {0}',
    date: 'Order placed: {0}',
    orderId: 'Order #: {0}',
    track: 'Tracking #: {0}',
    itemPrice: 'Price:',
    totalPrice: 'Total price:',
    itemCount: '(1 item)',
    itemsCount: 'one item | {count} items',
    status: {
      0: 'Ordered',
      1: 'Order processing',
      2: 'Shipped'
    },
    paymentStatus: {
      0: '- Payment pending',
      1: '- Paid in full'
    }
  },
  personal: {
    title: 'Personal & Login details',
    edit: 'Edit',
    personalDetails: 'Personal details',
    loginDetails: 'Login details',
    firstName: 'First Name',
    lastName: 'Last Name',
    birthday: 'Birthday',
    relationship: 'Relationship',
    emailAddress: 'Email Address',
    password: 'Password',
    editPersonal: 'Edit personal details',
    editLogin: 'Edit login details',
    selectDay: 'Select day',
    selectMonth: 'Select month',
    selectStatus: 'Select relationship status',
    relationshipStatus: 'Relationship status',
    save: 'Save Personal Details',
    saveLogin: 'Save Login Details',
    email: 'Email address',
    currentPassword: 'Current password',
    newPassword: 'New password',
    confirmPassword: 'Confirm password',
    statuses: {
      0: 'Single',
      1: 'In a Relationship',
      2: 'Engaged',
      3: 'Married',
      4: 'In a Civil Union',
      5: 'In a Domestic Partnership',
      6: "It's Complicated",
      7: 'In an Open Relationship',
      8: 'Widowed',
      9: 'Separated',
      10: 'Divorced',
      11: 'None of your business'
    },
    months: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'May',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Oct',
      11: 'Nov',
      12: 'Dec'
    }
  },
  preview: {
    title: 'Preview Request Submissions',
    itemImage: 'Item Image',
    itemName: 'Item Name',
    itemDetails: 'Item Details',
    dateRequested: 'Date Requested',
    status: 'Status',
    awaitingPreview: 'Awaiting preview',
    readyToView: 'Ready to view',
    itemID: 'Item ID:',
    color: 'Color:',
    carat: 'Carat:',
    shape: 'Shape:',
    metal: 'Metal:',
    prong: 'Prong:',
    productTime: 'Product Time:',
    range: '{0} to {1} Days'
  },
  addressBook: {
    divisionNames,
    title: 'Address book',
    addAddress: 'Add Address',
    edit: 'Edit',
    remove: 'Remove',
    defaultBilling: 'Default billing address',
    defaultShipping: 'Default shipping address',
    editAddressTitle: 'Edit address',
    addAddressTitle: 'Add address',
    addressNickname: 'Address nickname',
    firstName: 'First Name',
    lastName: 'Last Name',
    address1: 'Address 1',
    address2: 'Address line 2',
    address3: 'Address line 3',
    city: 'City/Town',
    state: 'State',
    zipCode: 'Zip / Post code',
    country: 'Country',
    phone: 'Phone number',
    cancel: 'Cancel',
    submit: 'Save',
    error: {
      required: 'Field is required'
    }
  },
  notifications: {
    title: 'Notifications',
    0: {
      title: 'Personal & Login details',
      descr: 'View and update your email and login details'
    },
    1: {
      title: 'Address Book',
      descr: 'View and update your shipping addresses'
    },
    2: {
      title: 'Orders',
      descr: 'View your current order status and order history'
    },
    3: {
      title: 'Notifications',
      descr:
        'Receive emails about new items, your order status, saved searches and recommendations'
    },
    4: {
      title: 'Preview Request Submissions',
      descr: 'View your custom design image preview requests'
    },
    5: {
      title: 'Advanced Search',
      descr:
        'Save item searches and be notified when these items are or will be in stock'
    },
    6: {
      title: 'Recommended for You',
      descr: 'A curated list of items that our design team thinks you’ll love'
    },
    7: {
      title: 'Favorites',
      descr: 'View your saved favorite items'
    },
    8: {
      title: 'Compare',
      descr: 'View and compare item listing details'
    }
  },
  dashboard: {
    title: 'Account dashboard',
    titleMobile: 'My Account',
    signOut: 'Sign Out',
    change: 'Change',
    generalInformation: 'General Information',
    generalInformationText:
      'Your account information is used to login to the site. Please save your password in safe place.',
    addressBook: 'Address Book',
    addressBookText:
      'View your billing and shipping addresses below. You can modify your address book by clicking "Change"'
  },
  menu: {
    myAccount: 'My Account',
    logIn: 'Log in',
    changePassword: 'Change Password',
    orderHistory: 'Order History',
    wishList: 'Favorites',
    signOut: 'Sign Out',
    noAccount: "Don't have an account?",
    createAccount: 'create an account',
    signIn: 'Sign in to your account',
    email: 'Email address',
    pass: 'Password',
    forgot: 'Forgot your password?',
    sign: 'sign in',
    rules: {
      email: {
        required: 'A valid email address is required',
        type: 'Please enter correct email address'
      },
      password: {
        required: 'Please enter your password'
      }
    }
  },
  generalInformation: {
    firstName: 'First name',
    lastName: 'Last Name',
    email: 'Email address',
    phone: 'Phone',
    password: 'Password',
    emailPreferences: 'Email preferences',
    mailingListTrue: 'Send me news updates and offers',
    mailingListFalse: "Don't send me news updates and offers"
  },
  addressForm: {
    firstName: 'First name',
    lastName: 'Last name',
    street1: 'Address line 1',
    street2: 'Address line 2',
    city: 'Shipping City',
    zipCode: 'Zip / Postal Code',
    country: 'Country',
    state: 'State',
    stateMax: 'STATE/PROVINCE/COUNTY',
    pleaseSelect: 'Please select'
  },
  billingAddress: {
    title: 'My Primary Billing Address',
    saveButton: 'Update account',
    validate: {
      firstName: 'Please enter your billing first name',
      lastName: 'Please enter your billing last name',
      street1: 'Please enter a valid billing address',
      city: 'Please enter your billing city',
      state: 'Please enter a valid billing state',
      countryId: 'Please select billing country',
      zipCode: 'Please enter a valid billing zip code'
    }
  },
  shippingAddress: {
    title: 'My Primary Shipping Address',
    saveButton: 'Update account',
    validate: {
      firstName: 'Please enter your shipping first name',
      lastName: 'Please enter your shipping last name',
      street1: 'Please enter a valid shipping address',
      city: 'Please enter your shipping city',
      state: 'Please enter a valid shipping state',
      countryId: 'Please select shipping country',
      zipCode: 'Please enter a valid shipping zip code'
    }
  },
  profile: {
    form: {
      firstName: 'First name',
      lastName: 'Last Name',
      email: 'Email address',
      phone: 'Phone',
      gender: 'Gender',
      select: 'Select',
      mailingList: 'Send me news updates and offers',
      updatePassword: 'Update password',
      saveButton: 'Update account',
      mailingListItem: 'OK to email',
      yes: 'Yes',
      no: 'No',
      mailingListMessage:
        '(We respect your privacy and will only send you occasional specials via our Monthly Newsletter)'
    },
    validation: {
      firstName: {
        empty: 'First name is required'
      },
      lastName: {
        empty: 'Last name is required'
      },
      phone: {
        empty: 'A phone number is required'
      },
      email: {
        empty: 'A valid email address is required',
        invalid: 'Please enter a valid email address'
      }
    },
    genders: {
      male: 'Male',
      female: 'Female'
    }
  },
  resetPassword: {
    title: 'Forgotten password',
    titleMobile: 'Forgotten password',
    assistance: 'Account Assistance',
    email: 'Email address',
    submit: 'Submit',
    enterEmail: 'Enter the email address you used to create your account',
    notify1: '- You will receive an email shortly to reset your password',
    notify2: '- If you do not receive an email, please {createAccount}',
    notify3:
      '- If you do not receive an email, please {contactUs} for assistance',
    createAccount: 'create new account',
    contactUs: 'contact us',
    validation: {
      email: {
        empty: 'A valid email address is required',
        invalid: 'Please enter a valid email address'
      }
    },
    swal: 'You will receive an email shortly to reset your password',
    ok: 'Ok'
  },
  changePassword: {
    title: 'Change password',
    titleMobile: 'Change password',
    assistance: 'Password Assistance',
    create1: 'Create a new password',
    create2: 'This password will be used when you login to your Account',
    submit: 'Save changes',
    passwordConditions:
      'Passwords are case sensitive and must be at least 6 characters long',
    form: {
      currentPassword: 'Current password',
      newPassword: 'New password',
      confirmPassword: 'Confirm password',
      showPasswords: 'Show passwords',
      saveButton: 'Save password',
      rules: {
        currentPassword: {
          required: 'Please enter your current password'
        },
        password: {
          required: 'Please enter your new password',
          rangeMin: 'Your password must contain at least 6 characters',
          rangeMax: 'Your password must contain less than 20 characters',
          notSame: 'Your new password and old password cannot be the same'
        },
        confirmPassword: {
          required: 'Please enter the new password again',
          sameAsPassword: 'Password does not match the confirm password'
        }
      }
    },
    validation: {
      currentPassword: {
        empty: 'Please enter your current password',
        length: 'The password must be at least 6 characters',
        invalid:
          'Password must contain at least one number and at least one lower and upper case letter'
      },
      newPassword: {
        empty: 'Please enter your new password',
        invalid:
          'Password must contain at least one number and at least one lower and upper case letter'
      },
      confirmPassword: {
        empty: 'Please confirm your new password',
        match: "The new password and confirmation password don't match"
      }
    },
    alert: {
      confirmButtonText: 'Ok',
      titleFail: 'Unable to change password',
      titleSuccess: 'Password changed',
      titleSuccess2: 'Password reset',
      textSuccess: 'Your password has been changed',
      textSuccess2:
        'Your password has been reset<br>\nYou can Sign In to your account using your new password',
      textFail2: "The new password and confirmation password don't match",
      textFail3: 'The current password you entered is incorrect',
      textFail4:
        'Failed to reset your password. Make sure your new password at least 6 characters including UPPER/lower and numbers',
      textFail5:
        'Failed to reset the password. Make sure your new password at least 6 characters including UPPER/lower and numbers',
      textFail6:
        'Sorry, your reset link is no longer valid, please request a new link from "Forgot Password" page',
      textFailDefault: 'Unknown error'
    }
  }
}
