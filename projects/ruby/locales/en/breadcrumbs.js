export default {
  home: 'Home',
  contactUs: 'Contact us',
  showroom: 'Our Showroom',
  staff: 'Our Staff',
  michael: 'Michael Arnstein',
  account: {
    dashboard: 'Account dashboard',
    personal: 'Personal & Login details',
    addressBook: 'Address Book',
    orders: 'Orders',
    notifications: 'Notifications',
    preview: 'Preview Request Submissions',
    search: 'Advanced Search',
    recommended: 'Recommended for You'
  },
  listings: {
    necklaceAndPendantSettings: 'Necklace & Pendant Settings',
    mensRings: "Men's Ruby Rings",
    mensRingSettings: "Men's Ring Settings",
    gemologistRecommended: 'Gemologist Recommended',
    burmeseRubies: 'Burmese Rubies',
    mozambiqueRubies: 'Mozambique Rubies',
    burmeseRubyRings: 'Burmese Ruby Rings',
    mozambiqueRubyRings: 'Mozambique Ruby Rings',
    earrings: 'Ruby Earrings',
    nAndP: 'Ruby Necklaces & Pendants',
    bracelets: 'Ruby Bracelets',
    rings: 'Ruby Rings',
    weddingBands: 'Ruby Wedding Bands',
    plainBands: 'Plain Wedding Bands',
    rubyRings: 'Ruby Rings',
    engagementRings: 'Ruby Engagement Rings',
    starRubyRings: 'Star Ruby Rings',
    pendants: 'Ruby Pendants',
    necklaces: 'Ruby Necklaces',
    stone: 'Loose Rubies',
    stoneCabochon: 'Cabochon Rubies',
    stoneStar: 'Star Rubies',
    pair: 'Ruby Pairs',
    pairCabochon: 'Ruby Cabochon Pairs',
    pairStar: 'Star Ruby Pairs',
    ringSettings: 'Ring Settings',
    earringSettings: 'Earring Settings',
    pendantSettings: 'Pendant Settings',
    necklaceSettings: 'Necklace Settings'
  },
  details: {
    stone: 'Ruby Gemstone',
    stoneCabochon: 'Cabochon Ruby Gemstone',
    stoneStar: 'Star Ruby Gemstone',
    pair: 'Ruby Pair',
    pairCabochon: 'Ruby Cabochon Pair',
    pairStar: 'Star Ruby Pair',
    ring: 'Ruby Ring',
    earring: 'Ruby Earrings',
    pendant: 'Ruby Pendant',
    bracelet: 'Ruby Bracelet',
    necklace: 'Ruby Necklace',
    weddingBand: 'Ruby Band',
    plainBand: 'Plain Band',
    settingRing: 'Ring Setting',
    settingEarring: 'Earring Setting',
    settingPendant: 'Pendant Setting',
    settingNecklace: 'Necklace Setting'
  },
  review: {
    stone: 'Loose Rubies',
    pair: 'Ruby Pairs',
    ring: 'Engagement Ring',
    earring: 'Earring',
    pendant: 'Pendant',
    necklace: 'Necklace',
    necklaceAndPendant: 'Necklace & Pendant'
  }
}
