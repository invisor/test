import stoneShapes from './constants/stoneShapes'
import metalTypesFilter from './constants/metalTypesFilter'
import stoneTypes from './constants/stoneTypes'
import stoneCut from './constants/stoneCut'
import stoneClarity from './constants/stoneClarity'
import stoneColorIntensity from './constants/stoneColorIntensity'
import stoneColors from './constants/stoneColors'
import prongTypes from './constants/prongsNames'
import stonesOrigins from './constants/stonesOrigins'
import treatments from './constants/treatments'
import bandStyles from './constants/bandStyle'
import designStyles from './constants/designStylesFilter'
import divisionNames from './constants/divisionNames'

export default {
  viewItem: 'View item',
  editItem: 'Edit item',
  removeItem: 'Remove item',
  outOfStock: 'Out of stock:',
  outOfStockText:
    'The item has been added to your cart but please note it is currently out of stock and someone from our service team will be in touch soon with a more accurate estimated delivery date.',
  onHold: 'On hold:',
  onHoldText:
    'This item is currently on hold for another customer. Continue your order to be placed on a wait-list, and someone will contact you within 24 - 48 hours with an update on the status of this item. You will not be charged at this time.',
  countryPopup: {
    US1: 'Please Note: Extend Warrant Protection is NOT available for non-USA based customers',
    US2: 'Please Note: Extend Warranty Protection is available for USA based customers. You may add a warranty to any applicable item in your <a class="is-underline" href="/cart/">shopping cart</a>.',
    BE: `Belgium has a few import prohibitions - to that which includes loose stones and jewelry.</br>
        You can read more on the government website <a class="is-underline" href="https://www.trade.gov/country-commercial-guides/belgium-customs-regulations" target="_blank">here</a>.<br>
        To view the import prohibitions and restrictions with shipping to Belgium, click <a class="is-underline" href="https://crossborder.fedex.com/us/assets/prohibited-restricted/belgium/index.shtml" target="_blank">here</a>.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`,
    IN: `India has a few import prohibitions- to that which includes loose stones and jewelry.</br>
        You can read more on the government website <a class="is-underline" href="https://commerce.gov.in/about-us/divisions/export-products-division/gems-and-jewellery/" target="_blank">here</a>.<br>
        To view the import prohibitions and restrictions with shipping to India, click <a class="is-underline" href="https://crossborder.fedex.com/us/assets/prohibited-restricted/india/index.shtml" target="_blank">here</a>.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`,
    IR: `Iran is unfortunately not a country that we are permitted to ship to.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`
  },
  backendTaxCalculation: 'Will be calculated after order submit',
  vatNotice: {
    note: '{note} Import Customs, Duty/VAT/GST may apply. {link} for more information on what you may expect when importing into your country.',
    strong: 'Please Note:',
    link: 'Review our FAQ',
    accept:
      'I understand possible additional taxes I may need to pay to my government.'
  },
  paymentMethod: {
    1: 'Credit Card',
    3: 'Bank Wire Transfer',
    5: 'Check / ACH'
  },
  docs: {
    title: 'Reports and Certification Documents',
    message:
      'Our gemstone and jewelry reports detail all relevant attributes of your item and are accepted by most insurance providers. Please choose a method below by which you would like to receive these reports.',
    scan: 'Gemstone Scan Report',
    scan3d: '3D Scan Report',
    appraisal: 'Gemstone Appraisal',
    option1:
      '<strong>Printed documents.</strong> I’d like printed copies sent to my shipping address.',
    option2:
      '<strong>Eco-friendly documents.</strong> I’d like to help save the planet. Please send me a PDF copy by email.',
    option3:
      '<strong>No documents.</strong> I don’t wish to receive any documents.*',
    notice:
      '*Should you decide in future you would like your documents, just email us your request and we will send it to you.'
  },
  expandedCart: {
    close: 'Close',
    itemId: 'Item ID: ',
    total: 'Total:',
    totalIn: 'Total in {0}:',
    seeCart: 'See my cart',
    checkout: 'Checkout',
    pay: 'Pay securely with',
    slideSwitcherText1: 'See the {0} other added item',
    slideSwitcherText2: 'See the next added item',
    wax: {
      ring: 'Ring Wax Model',
      earring: 'Earring Wax Model',
      pendant: 'Pendant Wax Model',
      necklace: 'Necklace Wax Model',
      default: 'Wax Model'
    }
  },
  item: {
    itemId: 'Item ID:',
    designId: 'Design ID:',
    vedicSetting: 'Vedic Style:',
    vedicSettingSelected: 'Stone Touching Skin',
    prongs: 'Prongs:',
    metal: 'Metal:',
    shape: 'Shape:',
    weight: 'Weight:',
    color: 'Color:',
    cut: 'Cut:',
    clarity: 'Clarity:',
    ringWidth: 'Width:',
    stoneType: 'Stone type:',
    quantity: 'Quantity:',
    widthMm: '{0} mm',
    size: 'Size:',
    paveStones: 'Pave diamonds:',
    centerStone: 'Center stone:',
    jewelrySideStones: 'Side stones:',
    centerStones: '{0} x {1} {2} {3}(s)',
    colorIntensity: 'Color intensity:',
    origin: 'Origin:',
    totalPerCt: '{0} Total ({1} per carat)',
    totalMetalWeight: 'Total metal weight:',
    totalStonesWeight: 'Total stones weight:',
    paveStonesWeight: 'Pave stones weight:',
    treatment: 'Treatment:',
    dimensions: 'Dimensions (MM):',
    length: 'Length: {0}',
    height: 'Height: {0}',
    width: 'Width: {0}',
    seeMore: 'See more details',
    seeLess: 'See less details',
    starAppearance: 'Star appearance:',
    style: 'Style:',
    sideStone: 'Side stones:',
    sideStones: '{0} x {1} {2} {3}(s)',
    stoneColor: 'Stone color:',
    stoneShape: 'Stone shape:',
    centStCt: 'Cent. stone wt.:',
    sideStCt: 'Side stone wt.:',
    prodTime: 'Prod. time:',
    prodTimeDays: '{0} to {1} days',
    finish: 'Finish style:',
    engraving: 'Engraving:',
    sidestoneSelections: 'Sidestone Selections:',
    totalWeight: 'Total weight:',
    stoneId: 'Stone ID:',
    settingId: 'Setting ID:',
    grade: 'Grade: {0}',
    eachStoneWeight: 'Each stone weight:',
    total: 'Total',
    perCarat: 'per carat',
    stoneShapes: {
      ...stoneShapes
    },
    metalTypes: {
      ...metalTypesFilter
    },
    stoneTypeName: stoneTypes,
    stoneCut,
    stoneClarity,
    stoneColorIntensity,
    stoneColors,
    prongTypes,
    stonesOrigins,
    treatments,
    bandStyles,
    designStyles
  },
  waxTitles: {
    ringWaxModel: 'Ring 3D Printed Model',
    earringWaxModel: 'Earring 3D Printed Model',
    pendantWaxModel: 'Pendant 3D Printed Model',
    necklaceWaxModel: 'Necklace 3D Printed Model',
    waxModel: '3D Printed Model'
  },
  band: 'Band',
  plainBand: 'Plain Band',
  pair: 'Pair',
  addressForm: {
    divisionNames,
    salesTeam: 'Already working with a member of our team?',
    customerInformation: 'Customer information',
    deliveryAddress: 'Shipping address',
    shippingAddressLabel:
      'Select a shipping address from your address book or enter new address',
    shippingAddressPlaceholder: 'Select shipping address',
    email: 'Email address',
    firstNameLabel: 'First name',
    lastNameLabel: 'Last name',
    addressLine1: 'Address line 1',
    addressLine2: 'Address line 2',
    shippingCity: 'shipping City',
    state: 'State',
    billingCity: 'city',
    regions: 'STATE/PROVINCE/COUNTY',
    pleaseSelect: 'Please select',
    zip: 'Zip / Postal Code',
    country: 'Country',
    phoneNumber: 'Phone number',
    billingAddress: 'Billing address',
    asShipping: 'Same as my shipping address ',
    billingAddressLabel:
      'Select a billing address from your address book or enter new address',
    billingAddressPlaceholder: 'Select billing address',
    deliveryOptions: 'Delivery options',
    comments: 'Comments or questions',
    commentsTitle:
      'If you have any questions or details you would like to add to your order please do so here.',
    payment: 'Proceed to payment',
    errorTitle: 'Sorry, but an error has been made',
    fixErrors: 'Please correct any error indicated below and try again:',
    ok: 'Ok',
    rules: {
      email: {
        required: 'A valid email address is required',
        template: 'Please enter a valid email address'
      },
      firstName: {
        required: 'Please enter your first name'
      },
      billingFirstName: {
        required: 'Please enter your billing first name'
      },
      lastName: {
        required: 'Please enter your last name'
      },
      billingLastName: {
        required: 'Please enter a billing first name'
      },
      shippingStreet1: {
        required: 'Please enter a valid shipping address'
      },
      billingStreet1: {
        required: 'Please enter a valid billing address'
      },
      shippingCity: {
        required: 'Please enter your shipping city'
      },
      billingCity: {
        required: 'Please enter a billing city'
      },
      shippingState: {
        required: 'Please enter a valid shipping state'
      },
      billingState: {
        required: 'Please enter a valid billing state'
      },
      shippingCountryId: {
        required: 'Please select shipping country',
        notProhibitedCountry: 'Not a country permitted to ship to'
      },
      billingCountryId: {
        required: 'Please select billing country'
      },
      shippingZipCode: {
        required: 'Please enter a valid shipping zip code'
      },
      billingZipCode: {
        required: 'Please enter a valid billing zip code'
      },
      phone: {
        required:
          'Please supply a phone number so we can call if there are any problems with your order'
      },
      delivery: {
        required: 'Please select delivery method'
      },
      acceptVAT: {
        required: 'Please apply these rules'
      }
    }
  },
  cartControls: {
    backToCart: 'Back to Shopping Cart',
    backToAddress: 'Back to Address & Shipping',
    checkout: 'Checkout securely',
    signIn: 'Sign in',
    signInAs: 'Signed in as',
    asGuest: 'Continue as guest',
    guest: 'Guest',
    signOut: 'Sign out',
    continue: 'Continue browsing',
    itemsCount: 'Shopping cart ({count} items)',
    shoppingCart: 'Shopping cart',
    items: '{0} items',
    edit: 'Edit cart',
    hold: 'Put on hold'
  },
  cartTotal: {
    pay: 'Pay securely with:',
    subtotal: 'Subtotal:',
    shipping: 'Shipping:',
    subtotalIn: 'Subtotal in {0}:',
    shippingIn: 'Shipping in {0}:'
  },
  screens: {
    shoppingCart: 'Shopping Cart',
    securePayment: 'Secure checkout',
    orderSummary: 'Order Summary',
    addressShipping: 'Address & Shipping',
    paymentOrder: 'Payment & Order'
  },
  orderSummary: {
    thankYou: 'Thank you for shopping with us',
    emailSent: "We've sent you a confirmation email with your receipt.",
    order: 'Your order reference: ',
    thankYouOrder: 'Thank you for your order',
    orderNumber: 'Order number is:',
    receiveEmail: 'You will receive an email confirmation shortly at',
    ok: 'Ok'
  },
  paymentForm: {
    paymentsOptions: 'Payments options',
    paymentsMessage: `<p>We will contact you shortly after your order is placed to arrange payment via <b>Wire Transfer</b> or <b>Certified Funds</b>. </p>
    <p><b>Please Note:</b> We accept wire transfers in U.S Dollars Only for both domestic and international orders.</p>`,
    cardDetails: 'Card details',
    cardholder: 'Cardholder Name (as shown on a card)',
    cardNumber: 'Card Number',
    expirationDate: 'Expiration Date',
    ccv: 'CVV Code',
    ccvDescription:
      'CVV number is the 3-digit number found on the back of your credit card next to the signature panel.',
    terms:
      'By ticking this box, I agree to the Natural Ruby Company’s <a href="https://thenaturalrubycompany.com/education/frequently-asked-questions/#privacy-policy" target="_blank">Terms</a> & <a href="https://thenaturalrubycompany.com/education/frequently-asked-questions/#security" target="_blank">Fraud Deterrent Practices</a>',
    proceed: 'Complete My Order',
    ccvExpError: 'The credit card you have used for your order has expired',
    rules: {
      paymentOptionId: {
        required: 'Please select a valid payment type'
      },
      billingName: {
        required: 'Please enter the name exactly as it appears on your card',
        range: 'Length should be 5 to 100'
      },
      cardNumber: {
        required: 'A valid credit card number is required',
        type: 'Please enter a valid credit card number',
        range: 'It appears you have entered an invalid credit card number'
      },
      ccExpMonth: {
        required: 'A valid credit card expiration month is required',
        between: 'Must be between {0} and {1}'
      },
      ccExpYear: {
        required: 'A valid credit card expiration year is required',
        between: 'Must be between {0} and {1}'
      },
      cvvCode: {
        requiredAmex: 'Please enter 4 digit number on the back of your card',
        required: 'Please enter 3 digit number on the back of your card',
        type: 'The security code for this card is not valid',
        range: 'Please enter 3-4 digit number on the back of your card'
      }
    },
    swal: {
      title: 'Sorry, but an error has been made',
      message: 'Please correct any error indicated below and try again:',
      ok: 'Ok'
    }
  },
  shoppingCart: {
    item: 'item',
    itemPrice: 'item price',
    remove: 'remove'
  },
  cartSidebar: {
    in: ' in {0}',
    orderSummary: 'Order summary',
    totalInfo:
      '*Please Note: The price shown above is a close approximation of your order total after converting from USD to {0}. Please note that we process all charges in USD, and final charge may vary due to associated credit card fees and exchange rate fluctuations.',
    subtotal: 'Subtotal',
    tax: 'Tax ({0})',
    shipping: 'Shipping',
    orderTotal: 'Total',
    orderTotalLocal: 'Total estimated conversion in {0}',
    description:
      '<strong>Currency conversion:</strong> Prices shown in {0} are an approximation after converting from USD to {0}. Please note that we process all charges in USD, and final charge may vary due to associated credit card fees and exchange rate fluctuations.',
    paySecurely: 'Pay securely with',
    deliveryInfo:
      'If you require Saturday delivery, please call us at</br><b>1-212-221-6136</b> or email Customer Service at <b>info@thenaturalrubycompany.com</b> to confirm. Sunday and holiday delivery are not available.',
    infoBlock: {
      whatNext: {
        title: 'What Happens Next?',
        description:
          'After you place your order, our sales team will review all of the details. If further information is required, we will contact you as soon as possible. Have a question? Read our FAQ or please feel free to Contact Us.'
      },
      returnPolicy: {
        title: 'Our Return Policy',
        description:
          'We offer a 14 day inspection period for you to review your ruby or finished jewelry item. If you are not completely satisfied with your item, you can return it for a full refund. For more information on these policies and for all "Custom Jewelry Orders", please review our full Return Policy.'
      },
      shippingInformation: {
        title: 'Shipping Information',
        description:
          'Your order will be uniquely packaged and shipped fully insured. Once your order has shipped, we will send tracking information to the email address you provide. Please note that a signature will be required upon delivery. For more information please review our full Shipping Policy.'
      },
      privacySecurity: {
        title: 'Your Privacy & Security',
        description:
          'All transactions on our website are executed through SSL security technology. The security and privacy of your credit card, personal contact data, and all other electronic information is never at risk of being stolen or misused.'
      },
      labReports: {
        title: 'Third Party Lab Reports',
        description:
          'We send all of rubies and jewelry with our in-house reports. If your item also has a third party lab certification (from the GIA or another institution), this document will be sent separately after the purchase has been confirmed. Third party lab reports cannot be replaced, so we send them after we hear from our customers that they will be keeping their item.'
      }
    }
  },
  empty: 'Your shopping cart is empty.',
  continueShopping: 'Continue shopping and come here later for checkout!'
}
