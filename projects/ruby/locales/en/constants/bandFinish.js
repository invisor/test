export default {
  crossSatin: 'Cross Satin',
  hammer: 'Hammer',
  highPolish: 'High Polish',
  sandBlast: 'Sand Blast',
  satinFinish: 'Satin Finish',
  stoneFinish: 'Stone Finish',
  wireMatte: 'Wire Matte'
}
