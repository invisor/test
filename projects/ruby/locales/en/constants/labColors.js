export default {
  nA: 'NA',
  deepRed: 'Deep Red',
  red: 'Red',
  vividRedToDeepRed: 'Vivid Red to Deep Red',
  darkRed: 'Dark Red',
  intenseRed: 'Intense Red',
  intenseRedToVividRed: 'Intense Red to Vivid Red',
  vividPinkRed: 'Vivid Pink-Red',
  vividRed: 'Vivid Red',
  vividRedPigeonsBlood: 'Vivid Red "Pigeon’s Blood"',
  vividRedImperialRed: 'Vivid Red "Imperial Red"',
  purplishRed: 'Purplish Red',
  vividRedVibrant: 'Vivid Red "Vibrant"'
}
