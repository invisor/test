export default {
  ballProng: 'Ball Prong',
  doubleBall: 'Double Ball',
  doubleClaw: 'Double Claw',
  doubleRound: 'Double Round',
  flatTab: 'Flat Tab',
  singleClaw: 'Single Claw',
  vProng: 'V Prong',
  vProngs: 'V Prong'
}
