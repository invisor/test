export default {
  ballProng: 'Ball Prong',
  doubleClaw: 'Double Claw',
  flatTab: 'Flat Tab',
  doubleRound: 'Double Round',
  singleClaw: 'Single Claw',
  vProng: 'V Prong',
  none: ''
}
