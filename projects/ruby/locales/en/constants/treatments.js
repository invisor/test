export default {
  fractureFilling: 'Fracture Filling',
  noEnhancement: 'No Enhancement',
  insignificantClarityEnhancement: 'Insignificant Clarity Enhancement',
  minorClarityEnhancement: 'Minor Clarity Enhancement',
  moderateClarityEnhancement: 'Moderate Clarity Enhancement',
  significantClarityEnhancement: 'Significant Clarity Enhancement',
  dyed: 'Dyed',
  enhancedWithUnidentifiedMethods: 'Enhanced with unidentified methods',
  heatTreated: 'Heat Treated',
  none: 'none'
}
