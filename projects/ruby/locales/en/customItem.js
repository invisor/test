import categories from '~/locales/en/categories'

export default {
  noMatch: {
    detailsTitle: 'Finalize Your Design',
    detailsAlert:
      "We're sorry, but the items you've selected are likely incompatible.",
    detailsProposal:
      "Interested in combining these two items? We may be able to help! With our FREE custom design services, we'll work with you to help create the custom jewelry piece of your dreams.",
    detailsInquire: 'Inquire About This Custom Design',
    detailsOr: 'OR',
    detailsContinue:
      'Wish to continue your design with a different Ruby or Setting?',
    detailsStone: 'Choose New Ruby',
    detailsSetting: 'Choose New Setting',
    form: {
      title: 'Custom Design Inquiry',
      message:
        "With FREE custom design services, we're confident we can bring your ideas to life. Simply complete this brief form and someone will get back to you as soon as possible. If you'd rather speak with someone right away, feel free to give us a call at (212) 869-1165.",
      alert1: "We're a No Pressure Company",
      alert2:
        "Our customer service reps do not work on commission. We won't sell your contact information or hard-sell.",
      designRequest: 'Design Request',
      yourName: 'Your Name:',
      emailAddress: 'Email Address:',
      phoneNumber: 'Phone Number:',
      comments: 'Comments:',
      submit: 'Submit Inquiry',
      rules: {
        email: {
          required: 'A valid email address is required',
          template: 'Please enter a valid email address'
        },
        name: {
          required: 'Please enter your name'
        }
      }
    }
  },
  categories: {
    ...categories,
    stone: 'Ruby'
  },
  Jewelry: 'jewelry',
  Ring: 'ring',
  Earring: 'earring',
  Pendant: 'pendant',
  Necklace: 'necklace',
  finalizeSetting_Ring: 'Finalize your ring',
  finalizeSetting_Earring: 'Finalize your earring',
  finalizeSetting_Pendant: 'Finalize your pendant',
  finalizeSetting_Necklace: 'Finalize your necklace',
  ruby: 'Ruby',
  rubies: 'Rubies',
  Setting_Ring: 'Ring',
  Setting_Earring: 'Earring',
  Setting_Pendant: 'Pendant',
  Setting_Necklace: 'Necklace',
  itemId: 'Item ID',
  completedPrice: 'Total Price:',
  productionTime: 'Production Time',
  productionTimePeriod: 'from {0} to {1} Days',
  pushService: 'Rush service may be available upon request.',
  freePreview: 'Request a free preview',
  checkout: 'Proceed to secure checkout',
  shoppingCart: 'Shopping cart',
  itemInCart: 'This item currently in your ',
  waxInCart: 'The polymer model has been added to your',
  stoneInCart: 'This stone currently in your ',
  pairInCart: 'This matching pair currently in your ',
  help: 'Help',
  designerHelp: 'Designer Help',
  designerHelpSuccessTitle: 'Thank you, your question',
  designerHelpSuccessFooter:
    'has been sent to one of our experts and we will get back to you shortly.',
  designerHelpBack: 'Go back to question',
  askHelp: 'Ask a designer',
  helpMessage:
    "We always have our expert designers available to make it as easy and enjoyable for you as possible. Whether it's a question about your gemstone, your choice of setting or anything at all to do with creating your own jewelry, send Erika your questions and she will get back to you within 24 hours",
  availableDesigns: {
    title: 'Available Designs Using This Setting',
    subTitle: 'Select a design below to see more details the design',
    message:
      "You're looking at a custom designed piece of jewelry with a {0}, set in a {1}. This item was created by one of our customers using our completely free, Design Your Own service.",
    production: 'Production Time:',
    fromTo: '{0} to {1} days',
    price: 'Price as Shown:',
    customize: 'Customize & Purchase this design'
  },
  steps: {
    createYourJewelry: 'Create your {0}',
    chooseYourStone: 'Choose your ruby',
    chooseYourStones: 'Choose your rubies',
    chooseYourSetting: 'Choose your setting',
    reviewYourJewelry: 'Review your {0}',
    yourDesign: 'Review Your design',
    addToCart: 'Add to cart',
    startOver: 'Start over',
    jewelry: 'Jewelry',
    start: 'Start',
    stone: 'Ruby',
    setting: 'Setting',
    review: 'Review',
    change: 'Change',
    remove: 'Remove'
  },
  removeModal: {
    title: "Let's start over! But first, do you want to...",
    saveDesignFavorites: 'Save this design to favorites',
    saveDesignCompare: 'Save this design to compare list',
    savedDesignFavorites: 'Saved to favorites',
    savedDesignCompare: 'Saved to compare list',
    findStone: 'Start with a ruby',
    findPair: 'Start with a pair of rubies',
    findSetting: 'Start with a setting',
    messageSetting:
      'If you remove this selected setting, you will lose your design. If you want to keep your design, be sure to save it to your favorites first!',
    messageStone:
      'If you remove this selected stone, you will lose your design. If you want to keep your design, be sure to save it to your favorites first!',
    saveButton: 'Save design to favorites and choose a new setting',
    saveSettingButton: 'Save design to favorites and choose a new setting',
    saveStoneButton: 'Save design to favorites and choose a new stone',
    saveButtonInWishList: 'This item is already in your favorites',
    notSaveButton: 'Choose a new setting without saving',
    notSaveSettingButton: 'Choose a new setting without saving',
    notSaveStoneButton: 'Choose a new stone without saving',
    cancelButton: 'Continue with design'
  },
  polymer: {
    title: '3D Printed Model',
    text: 'We can send you a 3D Printed Model of your Combined Setting Design!',
    price: '$95',
    button: 'Add to shopping cart'
  }
}
