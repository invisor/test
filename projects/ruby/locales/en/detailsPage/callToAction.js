export default {
  headerTitle: 'Thanks! Your request has been submitted.',
  headerBody:
    'Please allow 24 hours to receive a combined 3D rendering of your customized jewelry creation via email.',
  headerDismiss: 'Dismiss this message.',
  bodyTitle: 'What would you like to do next?',
  bodyAction: 'Please select an option below.',
  favoritesText: 'Add this combination to Favorites',
  favoritesTextComplete: 'Added to Favorites',
  compareText: 'Add this combination to Compare',
  compareTextComplete: 'Added to Compare',
  shopStone: 'Shop for other ruby options that will go with setting {0}',
  shopSetting: 'Shop for different {0} settings to go with {1} {2}',
  homePage: 'Continue to browse at The Natural Ruby Company homepage',
  addFavorites: 'Add to favorites',
  addCompare: 'Add to compare',
  added: 'Added',
  earring: 'earrings',
  pendant: 'pendant',
  necklace: 'necklace',
  ring: 'ring'
}
