import stoneShapes from '../constants/stoneShapes'
import stonesOrigins from '../constants/stonesOrigins'
import filtersOrigins from '../constants/filtersOrigins'
import treatments from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import stoneColors from '~/locales/en/constants/stoneColors'
import bandStyle from '~/locales/en/constants/bandStyle'
import bandFinish from '~/locales/en/constants/bandFinish'

export default {
  prongsNotice:
    'The item displayed above includes a stone set with {0} prongs. Please note the actual item you receive may look different depending on your prong selection.',
  availability: {
    0: 'Available',
    1: 'Delisted',
    2: 'Call for Status',
    3: 'Sold',
    4: 'On Hold'
  },
  confirmation: {
    ringSize:
      'Please reconfirm your ring size is a size {ringSize} by clicking the confirm button below',
    inCart:
      'A design using the same stone is currently in your shopping cart. By continuing, the item in your cart will be replaced by this new design.',
    confirm: 'Confirm',
    cancel: 'Cancel'
  },
  addedToCart: 'Added to {cart}',
  cart: 'cart',
  viewFullDetails: 'View full product details',
  totalStonesWeight: {
    diamond: 'Approximate total diamonds weight',
    ruby: 'Approximate total rubies weight'
  },
  chain: '18" Chain Included',
  soldSorry:
    "We're sorry but this item is no longer available, please contact us if you have further questions.",
  soldDelivery: ' - Order now for delivery by ',
  soldDeliveryTooltip:
    "But don't worry, we're working to bring this popular {category} back in stock as quickly as possible. Order now for delivery by {date}",
  thisIsMto: 'This Item is Made To Order',
  onHold: 'On Hold',
  soldOut: 'Sold',
  outOfStock: 'Out of Stock',
  ring: 'Ring',
  earring: 'Earrings',
  pendant: 'Pendant',
  necklace: 'Necklace',
  preview: 'All',
  stone: 'Stone',
  setting: 'Setting',
  reports: 'Reports',
  available: 'Available',
  freeShipping: 'Free shipping',
  noHasslePolicy: '14 Day No Hassle Return Policy',
  noHasslePolicyText:
    'We offer a 14 day return window to review our rubies, jewelry or your one-of-a-kind custom ruby jewelry. Should you wish to return it we will refund you the full price paid, unless we specifically notify you before the start of production of any possible restocking fees. Our items very rarely have restocking fees. If you have concerns or questions please contact us.',
  stonePrice: 'Stone Price',
  pairPrice: 'Stones Price',
  settingPrice: 'Setting Price',
  designId: 'Design ID',
  itemId: 'Item ID',
  stoneItemId: 'Ruby Item ID',
  settingItemId: 'Setting Item ID',
  totalPrice: 'Total price',
  calculatedPrice: 'Price Calculated During Next Step',
  calculatedPriceTitle: "Where's the Price?",
  calculatedPriceText:
    'Each piece of custom jewelry is uniquely crafted to match the stone you have selected. As such your final price can only be calculated once you have chosen a compatible stone and all available options which may include: Metal Type, Ring Size, Side Stones and/or Pave Diamond Weight. Simply continue by selecting this setting below. We will calculate the price automatically for you during the next step.',
  price: 'Price',
  toCartStone: 'Add loose stone to cart',
  toCartPair: 'Add loose stones to cart',
  toSettingStone: 'Create jewelry with this stone',
  toCartPairs: 'Add to cart',
  toSettingPairs: 'Add to earrings',
  inCart: 'This item currently in your',
  inCartStone: 'This stone currently in your',
  shoppingCart: 'Shopping cart',
  videoShot: 'Shot on iPhone XS',
  metal: 'Metal',
  vedic: 'Vedic Setting Style',
  vedicText:
    "This setting can accommodate a 'Vedic' style stone placement. Vedic settings are usually chosen for their ability to set the gemstone low, so it specifically touches the skin of the wearer.",
  vedicTextCTA:
    'If you would like the gemstone to touch the skin when set please check this box to confirm.',
  chooseVedic: 'Place gemstone touching skin',
  vedicLearnMore: 'Learn more about Rubies and Vedic astrology',
  vedicNotSelected: 'Not Selected',
  vedicSelected: 'Gemstone Touching Skin',
  prongs: 'Prongs',
  size: 'Ring size',
  finish: 'Finish',
  ringWidth: 'Band width (mm)',
  sideStones: 'Stone Size Selection',
  sizingGuide: 'Sizing guide',
  addEngraving: 'Add engraving',
  paveStone: 'Pave diamonds',
  totalPaveWeight: 'Total Estimated {0} Wt: {1}',
  optional: 'Optional',
  characters: '{0}/15 characters maximum, engraved on the inside of the band',
  engravingPlaceholder: 'Optional engraving',
  productionTime: 'Production Time',
  productionTimePeriod: 'from {0} to {1} Days',
  totalMetalWeight: 'Total metal weight',
  grade: 'Grade',
  stoneWeight: 'Weight',
  weight: 'Single Stone Weight',
  stoneTypeName: stoneTypes,
  paveStones: {
    sapphire: 'Sapphire',
    ruby: 'Ruby',
    diamond: 'Diamond',
    emerald: 'Emerald'
  },
  ringDetails: {
    ringSize: 'Select ring size',
    chooseRingSize: 'Choose ring size',
    ringWidth: 'Select ring width',
    selectSize: 'Please select a ring size'
  },
  settingTypePicker: {
    title: 'What would you like to create with your ruby?',
    continue: 'Continue',
    ringsList: 'Rings',
    earringsList: 'Earrings',
    pendantsList: 'Pendants',
    necklacesList: 'Necklaces',
    necklacesPendantsList: 'Necklaces & Pendants'
  },
  share: {
    facebook: 'Share via Facebook',
    twitter: 'Share via Twitter',
    googlePlus: 'Share via Google plus',
    pinterest: 'Share via Pinterest',
    email: 'Share via Email',
    copyLink: 'Copy link',
    linkCopied: 'Link copied to clipboard',
    linkCopyError: 'Something going wrong. Please, copy link manually'
  },
  help: {
    name: 'Need Help?',
    call: 'Call us',
    email: 'Email us',
    chat: "Let's chat!"
  },
  product: 'Product',
  description: 'description',
  details: 'Details',
  overview: 'Summary',
  stoneDetails: 'Stone details',
  stoneShapesName: {
    ...stoneShapes
  },
  originName: {
    ...filtersOrigins
  },
  stonesOrigins,
  treatmentsName: {
    ...treatments
  },
  bandFinish,
  bandStyle,
  stoneColors,
  itemOverview: {
    itemId: 'Item ID:',
    weight: 'Weight:',
    totalWeight: 'Total weight:',
    price: 'Price:',
    totalPrice: 'Total price:',
    settingPrice: 'Setting price:',
    perCaratPrice: 'Per carat price:',
    metalType: 'Metal type:',
    centerStones: 'Center stone(s):',
    sideStones: 'Side stone(s):',
    paveStones: 'Total estimated pave diamonds weight:'
  },
  stoneParams: {
    undefined: {
      tooltip: ''
    },
    perCaratPrice: {
      tooltip: 'Learn about Per Carat Price',
      title: 'Per Carat Price',
      text: `<p>The "per carat price" is a jewelry trade term that explains how gemstone prices are calculated. Fine quality gemstones are priced by multiplying their weight by the "per carat" price (ex: 2.50ct. x $1000 per/ct = $2500 total cost).</p>
      <p>The greater the per carat price, the higher the inherent quality of the 4 C’s (cut, clarity, carat size, and color) in the gemstone.</p>
      <p>For example, a 1.00 ct ruby with very poor clarity could sell for $10 per/ct while a 1.00 ct ruby with excellent clarity could sell for $100 per/ct. even though they are the same size. If another 1.00 ct. ruby is priced at $50 per/ct. then one can assume the clarity is slightly included.</p>`
    },
    color: {
      tooltip: 'Learn about Color',
      title: 'Color',
      text: '<p>All items are filmed with light meant to replicate intense, direct-over-head sunlight. This is the gemological standard for evaluating the color of all gemstone. We use 5000-6000 Kelvin LED light sources to replicate direct-overhead-sunlight.</p>'
    },
    shape: {
      tooltip: 'Learn about Ruby Shapes',
      title: 'Shape',
      text1:
        'The chart below features common gemstone shapes. The most popular shapes for colored gemstones are round, cushion, emerald, radiant, and princess. If you would like more information regarding the characteristics of each shape, please call us to speak with a gemologist who will be happy to help you select your ruby.',
      text2: `To view a PDF of millimeter dimensions for each shape, please click <a
      href="https://image.thenaturalsapphirecompany.com/site-files/nrc_stone_dimensions.pdf"
      target="_blank">here</a>`
    },
    dimensions: {
      tooltip: 'Learn about Ruby Dimensions'
    },
    clarity: {
      tooltip: 'Learn about Ruby Clarity',
      title: 'Clarity',
      text: `<p></p><p><strong>Eye-Clean</strong>: The ruby is completely clean to the eye, but not necessarily at 10x magnification.</p>
      <p><strong>Very Slightly Included</strong>: Inclusions or small internal crystals are very slightly visible on close inspection, but are not serious in plain view.</p>
      <p><strong>Slightly Included</strong>: Inclusions or small internal crystals are very slightly visible on close inspection, but are not serious in plain view.</p>
      <p><strong>Included</strong>: Inclusions are visible, they hinder the color light reflection to varying extents.</p>`
    },
    cut: {
      tooltip: 'Learn about Emerald Cut',
      title: 'Cut',
      text: 'Below are common terms and characteristics used to evaluate and classify the cut of a ruby gemstone.'
    },
    colorIntensity: {
      tooltip: 'Learn about Ruby Color Intensity',
      title: 'Color Intensity Chart',
      text: `<p>Color Intensity:</p>
      <p><strong>Vivid:</strong> A color classification for the rubies that exhibit a pure-red color, with full brilliance and light return throughout the gemstone.</p>
      <p><strong>Pigeon's Blood:</strong> A trade term commonly used to define one of the most highly desired colors of ruby. It is a deep, blood red color.</p>
      <p><strong>Intense:</strong> A shade less than Vivid. This color intensity is also called 'open color' in the trade.</p>
      <p><strong>Medium:</strong> A ruby that has a pinkish red or purplish red color tone.</p>
      <p><strong>Dark:</strong> A saturation is normally reserved for a ruby that has very little light escaping from the gemstone, the color  is near or close to opaque.</p>`
    },
    origin: {
      tooltip: 'Learn about Ruby Origin',
      title: 'Origin',
      text: `<p><strong>Mozambique</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/mozambique-ruby-mines/">here</a>.</p>
      <p><strong>Burma (Myanmar)</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/myanmar-ruby-mines/">here</a>.</p>
      <p><strong>Madagascar</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/madagascar-ruby-mines/">here</a>.</p>
      <p><strong>Thailand (Siam)</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/thailand-ruby-mines/">here</a>.</p>
      <p><strong>Vietnam</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/vietnam-ruby-mines/">here</a>.</p>
      <p><strong>Tanzania</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/tanzania-ruby-mines/">here</a>.</p>
      <p><strong>Kenya</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/kenya-ruby-mines/">here</a>.</p>
      <p><strong>Cambodia (Palin)</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/cambodia-ruby-mines/">here</a>.</p>
      <p><strong>Afghanistan</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/afghanistan-ruby-mines/">here</a>.</p>
      <p><strong>Pakistan</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/pakistan-ruby-mines/">here</a>.</p>
      <p><strong>Sri Lanka (Ceylon)</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/sri-lanka-ruby-mines/">here</a>.</p>
      <p><strong>Malawi</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/malawi-ruby-mines/">here</a>.</p>
      <p><strong>Laos</strong> - Learn more <a class="has-border" target="_blank" href="/education/mining-locations-rubies/laos-ruby-mines/">here</a>.</p>`
    },
    treatments: {
      tooltip: 'Learn about Ruby Treatments',
      title: 'Treatments',
      text: `<p>Enhancement for Rubies:</p>
      <p><strong>No Enhancement:</strong> The ruby has had no treatment or heating whatsoever. The color and clarity was created entirely by nature in the earth by natural geological process.</p>
      <p><strong>Heat Treated:</strong> Heat treatments are used to improve the color and clarity of rubies that came from the earth with a less pure red color or inclusions that inhibit light reflection. Heated rubies are heated without any additives up to a temperature of about 1500-1700°C, this heat exposure generally improves clarity and color.</p>
      <p><strong>Cavity Filling:</strong> Designed to improve a gemstone's clarity and durability by fixing fractures and cavities within the stone. These treatments can make low value, poor quality rubies more marketable. A cavity filled ruby should clearly be disclosed if the cavity was filled with glass, these glass-filled rubies are much much less expensive.</p>
      <p><strong>Diffusion or Glass Filled:</strong> Diffusion is a color enhancing treatment where light colored rubies are exposed to elements in the heating process to turn the crystal color red. These are very inexpensive gemstones and must be disclosed when treated in this way. Glass filled rubies are also considered very low quality gemstones and must be disclosed if treated in this way.</p>`
    }
  },
  stoneType: 'Stone type:',
  quantity: 'Quantity:',
  color: 'Color:',
  clarity: 'Clarity:',
  shape: 'Shape:',
  cut: 'Cut:',
  totalWeight: 'Total weight:',
  colorIntensity: 'Color intensity:',
  detailsDimensions: 'Dimensions (MM):',
  chainLength: 'Chain length (Inches):',
  braceletLength: 'Length (Inches):',
  origin: 'Origin:',
  treatment: 'Treatment:',
  detailsItemId: 'Item ID:',
  detailsWeight: 'Weight:',
  detailsColor: 'Color:',
  detailsClarity: 'Clarity:',
  detailsShape: 'Shape:',
  detailsPerCaratPrice: 'Per carat price:',
  detailsCut: 'Cut:',
  starAppearance: 'Star appearance:',
  detailsColorIntensity: 'Color intensity:',
  detailsOrigin: 'Origin:',
  detailsTreatments: 'Treatments:',
  length: 'Length: {0}',
  width: 'Width: {0}',
  height: 'Height: {0}',
  createJewelry: 'Create jewelry with stone',
  startFromStone: 'Create Jewelry With This Stone',
  startFromPair: 'Create Jewelry With These Stones',
  startFromSetting: 'Create Jewelry With This Setting',
  continueWithStone: 'Add stone to setting',
  continueWithPair: 'Add pair to setting',
  notCompatibleStone: 'This ruby is not compatible',
  notCompatiblePair: 'This rubies pair is not compatible',
  notCompatibleRingSetting: 'This ring setting is not compatible',
  notCompatibleNecklaceSetting: 'This necklace setting is not compatible',
  notCompatibleEarringSetting: 'This earrings setting is not compatible',
  notCompatiblePendantSetting: 'This pendant setting is not compatible',
  takeMeBackStone: 'Take me back to compatible rubies',
  takeMeBackPair: 'Take me back to compatible ruby pairs',
  takeMeBackRingSetting: 'Take me back to compatible ring settings',
  takeMeBackNecklaceSetting: 'Take me back to compatible necklace settings',
  takeMeBackEarringSetting: 'Take me back to compatible earrings settings',
  takeMeBackPendantSetting: 'Take me back to compatible pendant settings'
}
