export default {
  text:
    'It is fair to say that Kashmir sapphires set the standard by which all other blue sapphires are are judged. Kashmir sapphires are expensive and very rare because of the lack of new finds in the area, they are known for their highly saturated violet- blue color and “velvety” transparency, which is caused by the presence of silk. Kashmir is an antiquated term for what is now the Pakistan region.Kashmir’s sapphires were initially discovered when a landslide revealed the presence of large blue crystals, which were collected and traded by locals.',
  preTitle: 'Product origin'
}
