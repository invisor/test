export default {
  title: 'Quality, Value & Heart',
  row1: {
    anchor: '100% Natural Rubies',
    title: '100% Natural and Untreated',
    message:
      'Our untreated rubies meet all of the expectations of what a gemstone should be: truly rare, stunningly beautiful, and valuable in every sense.'
  },
  row2: {
    anchor: 'Free Shipping - Worldwide',
    title: 'Free Worldwide Shipping',
    message:
      "That's right! We're so certain you'll love what you receive that we cover shipping and insurance anywhere in the world!"
  },
  row3: {
    anchor: '14 Day No Hassle Return Policy',
    title: '14 Day No Hassle Return Policy',
    message:
      'We offer a 14 day return window to review any of our loose gemstones. Should you wish to return your stone(s), we will refund you the full price paid. If you have concerns or questions please contact us.'
  },
  row4: {
    anchor: 'Supports Local Charities',
    title: 'We Donate From Your Order!',
    message:
      "We love giving back. That's why we've created The Natural Sapphire Company Charitable Foundation. Having full time staff working in the mining regions where our stones originate; we're directly creating and administering charitable projects."
  }
}
