export default {
  label: 'Sizing guide',
  ringSizeTitle: "Don't Know Your Ring Size?",
  ringSizeMessage: `We have a convenient PDF that you can print out to help determine your exact ring size. This chart is based on the US
  system for ring sizing. It's important to note that we cannot guarantee the accuracy 100%. However when print at full
  size and used correctly, we have found this chart to be extremely accurate.`,
  ringSizeDownload: 'Download and Print our',
  ringSizingChart: 'Ring Sizing Chart',
  pdf: '(PDF)',
  converterTitle: 'Ring Size Conversion Chart',
  converterMessage:
    'We use the US system for ring sizing. Below is a ring size comparison chart for the different international systems.',
  usaSize: 'USA Size',
  diamInch: 'DIAM. INCH',
  diamMm: 'DIAM. MM',
  circumMm: 'CIRCUM. MM',
  british: 'British',
  french: 'French',
  german: 'German',
  japanese: 'Japan',
  swiss: 'Swiss'
}
