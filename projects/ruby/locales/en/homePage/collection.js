export default {
  ourCollection1: 'Our Ruby',
  ourCollection2: 'COLLECTION',
  bracelets: {
    title: 'Ruby Bracelets',
    text:
      'We offer a variety of classic ruby bracelets with diamonds to antique reproductions.',
    button: 'Shop now'
  },

  earrings: {
    title: 'Ruby Earrings',
    text:
      'Ruby earrings for everyday wear or a special event, view our stunning collection.',
    button: 'Shop now'
  },

  necklaces: {
    title: 'Ruby Necklaces & Pendants',
    text:
      'Our ruby necklaces and ruby pendants come in all shapes, sizes and colors, search our collection or make your own.',
    button: 'Shop now'
  },

  pairs: {
    title: 'Ruby Pairs',
    text:
      'Our staff gemologists have carefully selected ruby pairs so you can pick the perfect match for your jewelry with ease.',
    button: 'Shop now'
  },

  rings: {
    title: 'Ruby Rings',
    text:
      "Whether you are looking for a classic solitaire, a modern bezel setting, or a ruby and diamond halo design, you'll find the perfect ruby engagement ring in our collection.",
    button: 'Shop now'
  },

  settings: {
    title: 'Settings',
    text:
      "Interested in creating something special? You'll find the perfect setting for your custom ruby jewelry here.",
    button: 'Shop now'
  },

  stones: {
    title: 'Loose Rubies',
    text:
      'We’re proud to present a world class collection of fine rubies from various famous and exotic locations from around the earth.',
    button: 'Shop now'
  },

  weddingBands: {
    title: 'Ruby Wedding Bands',
    text:
      'Perfectly hand crafted ruby wedding bands are one of our specialities!',
    button: 'Shop now'
  }
}
