export default {
  smallText: 'ruby essentials',
  title: 'Something About Rubies',
  text:
    'Rubies are one of the most valuable precious stones, they have been coveted for centuries by royalty, gem collectors, larger-than-life personalities, and have experienced a resurgence in popularity in recent years as a centerpiece stone for an engagement ring.',
  readMore: 'Read more'
}
