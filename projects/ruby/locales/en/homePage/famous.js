export default {
  title1: 'Famous',
  title2: 'Rubies',
  slide1: {
    title: {
      row1: 'Rubies in',
      row2: 'Pop Culture'
    },
    text: 'Perhaps the most famous appearance of rubies were in slippers!',
    button: 'Read more'
  },
  slide2: {
    title: {
      row1: 'The Sunrise',
      row2: 'Ruby'
    },
    text:
      'The world’s most expensive ruby – actually, the most expensive precious gem that is not a diamond – the Sunrise Ruby, $30.3 million dollars',
    button: 'Read more'
  },
  slide3: {
    title: {
      row1: 'Elizabeth',
      row2: 'Taylor'
    },
    text:
      'No one else in Hollywood is as synonymous with exquisite jewelry as Elizabeth Taylor',
    button: 'Read more'
  }
}
