export default {
  create: 'Create Your Perfect',
  ring: 'Ruby ring',
  description:
    'We have thousands of exquisite, natural, untreated rubies to choose from, matched perfectly with our beautiful, custom designed, hand crafted settings.',
  ready: 'Ready to create?',
  chooseSetting: 'Choose a setting',
  or: 'or',
  chooseRuby: 'Choose a ruby',
  learnMore: 'Learn more'
}
