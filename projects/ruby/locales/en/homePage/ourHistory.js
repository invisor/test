export default {
  slide1: {
    subTitle: 'Our story & Heritage',
    title: 'Over 80 Years of History',
    text:
      "Since 1939 we have specialized in the finest natural sapphires. Our roots go back three generations, and now we're extending this legacy into natural rubies.",
    button: 'Read more'
  },
  slide2: {
    subTitle: 'Our story & Heritage',
    title: "How We're Different",
    text:
      'Providing quality, rarity, and value will always be the cornerstones of our business, but discover what else sets us apart in delivering the best jewelry purchase experiences.',
    button: 'Read more'
  }
}
