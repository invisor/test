export default {
  subTitle: 'Consectetur',
  title: 'Visit us on Instagram',
  text:
    'Our Instagram is a ruby lover’s dream where we showcase our current and past ruby jewelry and loose rubies before they were placed in lucky homes!',
  button: 'Follow us on instagram'
}
