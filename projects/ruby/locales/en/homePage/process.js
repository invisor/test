export default {
  ourProcess1: 'Our',
  ourProcess2: 'Process',
  button: 'Learn more about our FREE design process',
  step1: 'Choose your ruby',
  step2: 'Choose your setting',
  step3: 'Review & finalize your design',
  step4: 'Request a free preview',
  step5: 'Request a 3d model of your design',
  step5notice: '*3D model is optional and available for only $95'
}
