export default {
  text:
    'And the marvellous rose became crimson, like the rose of the eastern sky. Crimson was the girdle of petals, and crimson as a ruby was the heart',
  author: 'Oscar Wilde'
}
