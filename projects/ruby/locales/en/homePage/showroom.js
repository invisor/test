export default {
  title1: 'Visit Our ',
  title2: 'Showroom',
  text:
    'Enjoy a unique jewelry buying experience, flooded with natural light, in a comfortable and private setting for your viewing pleasure.',
  button: 'Book a tour'
}
