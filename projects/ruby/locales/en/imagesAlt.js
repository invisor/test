export default {
  heroRing:
    "Pigeon's blood vivid ruby and diamond ring 3.26ct certified by GIA",
  collectionRings:
    'Ruby engagement rings with pave set diamonds set in platinum',
  collectionEarrings:
    'Ruby earrings with diamonds set in a halo setting design in platinum',
  collectionWeddingBands:
    'Ruby and diamond wedding bands set in white gold and platinum',
  collectionNecklaces:
    '8.88ct Burmese ruby set in platinum with pave set diamonds',
  collectionBracelets:
    'Mozambique ruby bracelet with invisible set diamonds in 18k white gold',
  collectionStones: 'Rubies from Burma and Mozambique',
  collectionPairs: 'Pair of Burmese rubies untreated and certified',
  collectionSettings: 'Custom made settings for ruby rings',
  educationVideo: 'Learn more about Burmese and Mozambique rubies',
  ourStory: 'The Natural Ruby Company History',
  famousRubies: 'Famous Rubies',
  famousQuality: 'Fine Quality Rubies',
  famousElizabeth: 'Elizabeth Taylor Ruby Jewelry',
  packaging: 'Ruby rings in The Natural Ruby Company custom made packaging'
}
