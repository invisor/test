export default {
  subscription: {
    title: 'Stay up to date with our news',
    text: 'Subscribe to our newsletter for updates and special offers.',
    placeholder: 'Email address',
    button: 'Sign up'
  },
  bottom: {
    terms: 'Terms & Conditions',
    privacy: 'Privacy policy'
  },
  menu: {
    aboutUs: {
      title: 'About Us',
      item1: 'Why Choose Us?',
      item2: 'Charity Foundation',
      item3: 'Staff',
      item4: 'Blog',
      item5: 'Showroom',
      item6: 'Technology',
      item7: 'Workshop',
      item8: 'Laboratory',
      item9: 'In The Press'
    },
    customerCare: {
      title: 'Customer care',
      item1: 'Contact Us',
      item2: 'General Questions',
      item3: 'Return Policy',
      item4: 'Our Location',
      item5: 'Grading, Sizing & More',
      item6: 'Payment Policy',
      item7: 'Shipping',
      item8: 'Security & Fraud Policy',
      item9: 'Privacy Policy'
    },
    education: {
      title: 'Education',
      item1: 'The Nature Of Rubies',
      item2: 'The 4Cs Of Ruby Quality',
      item3: 'Ruby Rarity & Enduring Value',
      item4: 'Ruby Treatments & Enhancements',
      item5: 'Famous Rubies',
      item6: 'Ruby Buying Tips',
      item7: 'Caring for Rubies'
    }
  }
}
