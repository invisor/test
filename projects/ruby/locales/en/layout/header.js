export default {
  noResults: 'No items found',
  searchById: 'Search by item ID #',
  meta: {
    name: 'The Natural Ruby Company',
    description:
      'The Natural Ruby Company specializes in providing the finest natural rubies.'
  },
  language: 'Language',
  currency: 'Currency',
  contacts: {
    availableHours: 'Hours Available',
    days: 'Mon – Fri',
    showroom: {
      text: 'For a unique viewing experience visit our private Penthouse Showroom in Manhattan NYC.',
      button: 'Make Appointment'
    },
    locations: {
      title1: 'Showroom & Workshop',
      text1: '6 East 45th St, 20th Floor New York, NY 10017 United States',
      title2: 'CAD & Production',
      text2: '73/1/3 Council Avenue Demuwawatha, Ratnapura Sri Lanka'
    },
    phone: {
      usa: 'USA',
      usaPhone: '+1-212-221-6136',
      line1: 'For international customers, please contact us by email.',
      line2: 'We will get back to you during our regular business hours.'
    },
    chat: 'Chat with us',
    email: {
      title: "We're here to help!",
      text: 'We invite you to send us a message through our website at any time.',
      button: 'Contact us'
    }
  },
  searchLine: 'Search for product name or item ID #',
  firstLine: {
    left: {
      text1: 'Free Worldwide Shipping',
      text2: '14 Day No Hassle Returns',
      tooltipText1:
        "We're so certain you'll love what you receive that we cover shipping and insurance for all items delivered anywhere in the United States, plus <strong><u>all items over $300</u></strong> to anywhere in the World!",
      tooltipTitle1:
        'Free Worldwide Shipping Policy<br /> <small><i>(restrictions apply)</i></small>',
      tooltipText2:
        'We offer a 14 day return window for any of our loose gemstones, any finished ready-to-ship jewelry, and <strong><u>most</u></strong> made-to-order custom jewelry and bands. Should you choose to make a return, we will refund you the full price paid. Some restrictions may apply.',
      tooltipTitle2:
        '14 Day No Hassle Return Policy<br/> <small><i>(some restrictions may apply)</i></small>'
    },
    middle: {
      noRisk: {
        DE: 'Keine Kosten, kein Risiko, KOSTENLOSER Versand nach Deutschland',
        AU: 'G’day Australian visitors! Free Shipping​, Free Returns! No worries!​',
        BR: 'Sem custo, Sem Risco, Frete grátis para/de​ Brazil​!',
        CA: 'No Cost, No Risk, Free Shipping to​ USA + Canada​!',
        CN: '沒有任何费用,沒有风险,免费送货​至/从中国！',
        FI: 'Maksuton, Riskiä ei ole, Ilmainen toimitus että​/​alkaen​ Finland​!',
        FR: '​Aucun coût, Aucun risque, Livraison gratuite vers/à partir de​ France!',
        IT: 'Gratuitamente e senza alcun rischio, La spedizione gratuita da/per​ Italia​!',
        JP: '無料、無リスク、日本からの送料無料！',
        MY: 'Tiada kos, ​T​iada risiko, ​P​enghantaran percuma ke/dari​ Malaysia​!',
        NL: 'Geen kosten, ​Geen risico, ​G​ratis verzending naar / van​ Netherlands​!',
        NZ: 'No Cost, No Risk, Free Shipping to​ USA +​​ New Zealand​!',
        NO: 'ngen pris, Ingen risiko, Gratis frakt til / fra​ Norway​!',
        SG: 'No Cost, No Risk, Free Shipping to​ Singapore​!',
        ZA: '​No Cost, No Risk, Free Shipping to​ South Africa​​!',
        ES: 'Sin Costo, Sin Riesgo, Envío gratuito desde / hasta​ Spain​​!',
        CH: 'Keine Kosten, kein Risiko, Kostenloser Versand an/aus​ Switzerland​​​​​!',
        GB: 'No Cost, No Risk, Free Shipping to​ the UK​!',
        US: 'No Cost, No Risk, FREE Shipping!',
        KH: 'គ្មានតម្លៃគ្មានហានិភ័យគ្មានការដឹកជញ្ជូនដោយឥតគិតថ្លៃទៅកាន់ / មកពីប្រទេសកម្ពុជា!!',
        GR: 'Δεν υπάρχει κόστος, δεν υπάρχει κίνδυνος, δωρεάν αποστολή από / προς την Ελλάδα !',
        HK: '沒有任何費用,沒有風險,免費送貨​ ​至/從香港！',
        IE: 'No Cost, No Risk, Free Shipping to Ireland!',
        ID: 'Tanpa Biaya, Tanpa Resiko, Gratis pengiriman ke / dari Indonesia!',
        QA: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى قطر',
        RU: 'Бесплатная доставка в Российскую Федерацию. Без риска',
        SE: 'Ingen kostnad, Ingen risk, Gratis frakt till / från​ Sweden​!',
        TR: "Maliyet Yok, Risk Yok, Türkiye'ye Ücretsiz Kargo",
        AE: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى الإمارات العربية المتحدة',
        PL: 'Bez kosztów, bez ryzyka, bezpłatna wysyłka do / z Polski !',
        PT: 'Não, não há nenhum risco de ​ Envio gratuito de/para o Portugal!',
        HU: 'Nincs költség, nincs kockázat, Ingyenes szállítás a / a hundary!',
        IL: 'ללא עלות, ללא סיכון, משלוח חינם מ / ל Isarel',
        KR: '비용 없음, 위험 없음, 무료 배송 대한민국!',
        KW: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى الكويت',
        OM: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني إلى / من عمان',
        PH: 'No Cost, No Risk, Free Shipping Philippines!',
        MX: 'Sin costo, sin riesgos, envío gratuito a / desde México!',
        SA: 'لا تكلفة ، لا خطر ، الشحن المجاني إلى / من المملكة العربية السعودية!',
        AT: 'No Cost, No Risk, Free Shipping Austria!',
        EG: 'لا تكلفة ، لا خطر ، الشحن المجاني من وإلى مصر!',
        TH: 'ไม่มีความเสี่ยงไม่มีความเสี่ยงจัดส่งฟรีถึง / จากประเทศไทย!',
        VN: 'Không có chi phí, không có rủi ro, miễn phí vận chuyển đến / từ Việt Nam !',
        CY: 'No Cost, No Risk, Free Shipping Cyprus!',
        AR: 'Sin costo, sin riesgos, envío gratuito a / desde Argentina !',
        BN: 'Tiada kos, tiada risiko, penghantaran percuma ke / dari Brunei!',
        LU: 'Keen Präis, kee Risiko, gratis Liwwerung fir / aus Lëtzebuerg!',
        IS: 'Engin kostnaður, engin hætta, ókeypis sendingar til / frá Íslandi!',
        BH: 'لا تكلفة ، لا خطر ، الشحن المجاني إلى / من Braharin!',
        BE: 'No cost, no risk, free shipping to / from Belgium!',
        CL: 'Sin costo, sin riesgo, envío gratis a / desde Chile!',
        UY: '¡Sin costo, sin riesgo, envío gratis a / desde Uruguay!',
        HR: 'Bez troškova, bez rizika, besplatne dostave do / od Hrvatske!',
        PE: 'No cost, no risk, free shipping to / from Peru !',
        NG: 'No cost, no risk, free shipping to / from Nigeria !',
        LA: 'ບໍ່ມີຄ່າໃຊ້ຈ່າຍ, ບໍ່ມີຄວາມສ່ຽງ, ການຂົນສົ່ງຟຣີໄປ / ຈາກລາວ!'
      }
    }
  },
  secondLine: {
    favorites: 'Favorites',
    compare: 'Compare',
    cart: 'Cart',
    account: 'Account'
  }
}
