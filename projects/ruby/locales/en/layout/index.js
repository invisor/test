import header from './header'
import phones from './phones'
import mainMenu from './mainMenu'
import footer from './footer'

export default {
  header,
  phones,
  mainMenu,
  footer
}
