import stoneTypes from '../constants/stoneTypes'

const stonesList = {
  photo: 'Actual photo',
  weight: 'Carat',
  length: 'Length (MM)',
  width: 'Width (MM)',
  shape: 'Shape',
  origin: 'Origin',
  price: 'Price',
  compare: 'Compare',
  wishlist: 'Favorites'
}

const ringSettings = {
  photo: 'Actual photo',
  metal: 'Metal Name',
  compare: 'Compare',
  wishlist: 'Favorites'
}

const jewelryList = {
  photo: 'Actual photo',
  weight: 'Carat',
  metal: 'Metal Name',
  treatment: 'Treatment',
  shape: 'Shape',
  origin: 'Origin',
  price: 'Price',
  compare: 'Compare',
  wishlist: 'Favorites'
}

const bandsList = {
  photo: 'Actual photo',
  metalWeight: 'Metal weight',
  totalCarat: 'Total carat',
  metal: 'Metal Name',
  price: 'Price',
  compare: 'Compare',
  wishlist: 'Favorites',
  style: 'Style'
}

export default {
  and: ' and ',
  startingFrom: 'Starting from',
  more: 'More',
  less: 'Less',
  quickView: 'Quick view',
  moreDetails: 'View Details',
  itemId: 'Item ID',
  details: 'Details',
  stoneTypes,
  prev: 'Previous',
  next: 'Next',
  tableHeader: {
    id: 'ID',
    remove: 'Remove',
    stonesList,
    stonePairsList: stonesList,
    ringSettings,
    earringSettings: ringSettings,
    pendantSettings: ringSettings,
    necklaceSettings: ringSettings,
    braceletsList: jewelryList,
    earringsList: jewelryList,
    necklacesPendantsList: jewelryList,
    ringsList: jewelryList,
    weddingBands: bandsList,
    weddingBandsPlain: bandsList
  }
}
