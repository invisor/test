import filters from './filters'
import controls from './controls'
import stonesList from './stonesList'
import stonePairsList from './stonePairsList'
import ringSettings from './ringSettings'
import earringSettings from './earringSettings'
import pendantSettings from './pendantSettings'
import necklaceSettings from './necklaceSettings'
import braceletsList from './braceletsList'
import earringsList from './earringsList'
import necklacesPendantsList from './necklacesPendantsList'
import ringsList from './ringsList'
import common from './common'
import compareList from './compareList'
import wishlist from './wishlist'
import weddingBands from './weddingBands'
import weddingBandsPlain from './weddingBandsPlain'

export default {
  filters,
  controls,
  stonesList,
  stonePairsList,
  ringSettings,
  earringSettings,
  pendantSettings,
  necklaceSettings,
  braceletsList,
  earringsList,
  necklacesPendantsList,
  common,
  compareList,
  wishlist,
  ringsList,
  weddingBands,
  weddingBandsPlain
}
