import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound: 'No rings found | One ring found | {count} rings found'
  },
  ringSizePlaceholder: 'Select ring size',
  ringSizeError: 'Please select a ring size',
  engravingPlaceholder: 'Free optional engraving'
}
