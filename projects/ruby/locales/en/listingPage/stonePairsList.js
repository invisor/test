import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    itemsFound:
      'No ruby pairs found | One ruby pair found | {count} ruby pairs found',
    itemsStarFound:
      'No star ruby pairs found | One star ruby pair found | {count} star ruby pairs found',
    itemsCabochonFound:
      'No ruby cabochon pairs found | One ruby cabochon pair found | {count} ruby cabochon pairs found'
  },
  pair: 'Pair',
  prefilteredItems:
    'Displaying <strong>ONLY</strong> ruby pairs that are compatible with the setting you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> ruby pairs which means some may not be compatible with your setting.',
  showAllItems: 'No thanks, show me all ruby pairs.',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible ruby pairs.'
}
