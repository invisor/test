import stonesOrigins from '../constants/stonesOrigins'
import metalTypes from '../constants/metalTypes'
import treatments from '../constants/treatments'
import stoneCut from '../constants/stoneCut'
import stoneClarity from '../constants/stoneClarity'
import stoneColorIntensity from '../constants/stoneColorIntensity'
import bandStyle from '../constants/bandStyle'
import stoneShapes from '../constants/stoneShapes'

export default {
  sort: {
    sortBy: 'Sort by',
    itemsFound: 'No rubies found | One ruby found | {count} rubies found',
    itemsStarFound:
      'No star rubies found | One star ruby found | {count} star rubies found',
    itemsCabochonFound:
      'No cabochon rubies found | One cabochon ruby found | {count} cabochon rubies found',
    other: {
      featuredFirst: 'Featured First',
      newFirst: 'New First'
    },
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  localSort: {
    sortBy: 'Sort by',
    placeholder: 'Select',
    itemsFound: 'rubies pairs found',
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  prefilteredItems:
    'Displaying <strong>ONLY</strong> rubies that are compatible with the setting you have chosen.',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> rubies which means some may not be compatible with your setting.',
  showAllItems: 'No thanks, show me all rubies.',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible rubies.',
  stoneShapes,
  stonesOrigins,
  metalTypes,
  treatments,
  stoneCut,
  stoneClarity,
  stoneColorIntensity,
  bandStyle
}
