import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: 'Weight (Low to High)',
      highToLow: 'Weight (High to Low)'
    },
    itemsFound:
      'No wedding bands found | One wedding band found | {count} wedding bands found'
  },
  ringSizePlaceholder: 'Select ring size',
  ringSizeError: 'Please select a ring size',
  engravingPlaceholder: 'Free optional engraving'
}
