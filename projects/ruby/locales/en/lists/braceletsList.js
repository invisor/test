export default {
  price: 'Price:',
  itemId: 'Item ID:',
  cut: 'Cut:',
  origin: 'Origin:',
  carat: 'Carat:',
  treatment: 'Treatment:',
  metal: 'Metal type:'
}
