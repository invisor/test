export default {
  hero: {
    title: 'Find an Rubies',
    message:
      'Show me {stoneShapesFilter} rubies in the price range of {priceFilter}',
    andOther: 'and other',
    placeholder: 'all'
  },
  table: {
    actualPhoto: 'Actual photo',
    shape: 'Shape',
    colorIntensity: 'Color Intensity',
    clarity: 'Clarity',
    carat: 'Carat',
    origin: 'Origin',
    cut: 'Cut',
    price: 'Price',
    compare: 'Compare',
    wishlist: 'Wishlist',
    details: 'Details'
  },
  sort: {
    sortBy: 'Sort by',
    itemsFound: 'rubies found',
    other: {
      featuredFirst: 'Featured First',
      newFirst: 'New First'
    },
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  localSort: {
    sortBy: 'Sort by',
    placeholder: 'Select',
    itemsFound: 'rubies pairs found',
    price: {
      lowToHigh: 'Price (Low to High)',
      highToLow: 'Price (High to Low)'
    },
    carat: {
      lowToHigh: 'Carat (Low to High)',
      highToLow: 'Carat (High to Low)'
    }
  },
  lists: {
    result: 'Result',
    compare: 'Compare',
    wishlist: 'Wishlist'
  },
  carat: 'Carat:',
  changeView: 'Change view',
  hideNonCompatibleItems: 'Nevermind, take me back to compatible rubies.',
  itemId: 'Item ID:',
  intensity: 'Color intensity:',
  clarity: 'Clarity:',
  cut: 'Cut:',
  origin: 'Origin:',
  perCaratPrice: 'Per carat price:',
  dimensions: 'Dimensions:',
  notPrefilteredItems:
    'Displaying <strong>ALL</strong> rubies which means some may not be compatible with your setting.',
  prefilteredItems:
    'Displaying <strong>ONLY</strong> rubies that are compatible with the setting you have chosen.',
  price: 'Price:',
  quickView: 'Quick view',
  readMore: 'View more',
  readMoreTitle: "You've viewed {0} of {1} products",
  showAllItems: 'No thanks, show me all rubies.',
  weight: '{0} Ct.'
}
