import { companyName } from '~~/utils/definitions/defaults.js'

export default {
  index: {
    seo: {
      title: `${
        companyName[process.env.siteName]
      } - Ruby Rings & Jewelry Since 1939`,
      description:
        'We are the authority in ruby engagement rings, natural rubies, and ruby jewelry, since 1939. We have the largest collection of rubies found online.'
    },
    navPageTitle: 'Home'
  },
  'custom-design-ruby-rings-jewelry': {
    seo: {
      title: 'Design Custom Ruby Engagement Rings & Jewelry Online',
      description:
        'Looking for something special and unique? Let us help create the perfect custom Ruby Engagement Ring, Earrings, Necklace, or Pendant for you or a loved one.'
    },
    navPageTitle: 'Learn More'
  },
  'design-your-own-setting-mens-ring-settings': {
    seo: {
      title: "Custom Ruby Men's Ring Settings",
      description:
        "With dozens of bold ring settings to choose from - specifically designed for the modern man - we'e confident you'll find the perfect men's ruby ring setting for any occasion."
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-engagement-ring-settings': {
    seo: {
      title: 'Custom Ruby Engagement Ring Settings',
      description:
        'Custom made ruby engagement ring settings. We have hundreds of design styles from three stone ring, pave diamonds, and solitaire to name a few. Our ruby ring settings can be created in white and yellow gold, platinum, and palladium.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-earring-settings': {
    seo: {
      title: 'Custom Ruby Earring Settings',
      description:
        'From simple to elaborate earring settings, we offer it all! Choose among a wide variety of earring designs to fit your ruby pair.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-necklace-pendant-settings': {
    seo: {
      title: 'Custom Ruby Necklace & Pendant Settings',
      description:
        "Choose one of our ruby necklace or pendant settings to accentuate any ensemble. With our simple or elaborate designs, you'll find everything you're looking for!"
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-earring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-ruby-necklace-pendant-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-ruby-engagement-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-mens-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'ruby-jewelry-id': {
    seo: {
      title: 'Ruby {type} - {shape} {carat} Ct. - {metal} #{id}',
      description:
        'This beautiful {carat} Ct. {shape} Ruby {type} (#{id}) set in {metal} is the perfect piece of ruby jewelry for that someone special in your life.'
    },
    navPageTitle: 'Ruby {type} {metal}'
  },
  rubies: {
    seo: {
      title: 'Loose Rubies',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Loose Ruby Gemstones',
    navPageDescr:
      'While the Burmese rubies are the most famous, other ruby sources like Mozambique, Sri Lanka, and even Madagascar are capable of producing ultra fine quality rubies. View our selection from all over the world.'
  },
  'rubies-burmese-rubies': {
    seo: {
      title: 'Burmese rubies',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Burmese rubies',
    navPageDescr:
      'Burmese rubies are the oldest known source of fine quality rubies on planet earth. Written about since the dawn or recorded human history; rubies from Burma are the stuff of legends.'
  },
  'rubies-mozambique-rubies': {
    seo: {
      title: 'Mozambique Rubies',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Mozambique Rubies',
    navPageDescr:
      'Mozambique rubies are the single greatest gemological discovery of modern times. Rivaling the light and color reflection of all other ruby sources, Mozambique rubies are the future of all things rubies.'
  },
  'ruby-jewelry-burmese-ruby-rings': {
    seo: {
      title: 'Burmese Ruby Rings',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Burmese Ruby Rings',
    navPageDescr:
      'Burmese rubies are the oldest known source of fine quality rubies on planet earth. Since the dawn or recorded human history; rubies from Burma are legendary. View our collection of Burmese ruby rings below.'
  },
  'ruby-jewelry-mozambique-ruby-rings': {
    seo: {
      title: 'Mozambique Ruby Rings',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Mozambique Ruby Rings',
    navPageDescr:
      'Mozambique rubies are the single greatest gemological discovery of modern times. Rivaling the light and color reflection of all other ruby sources, Mozambique rubies are the future of all things rubies.'
  },
  'rubies-star-rubies': {
    seo: {
      title: 'Star Rubies',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Star Rubies',
    navPageDescr:
      'The star ruby is one of the rarest gemstones on earth. The visible star seen in a polished ruby often proves the ruby is unheated and in its natural state. Learn more about star rubies <a href="/education/all-about-star-rubies/">here</a>.'
  },
  'rubies-ruby-cabochon': {
    seo: {
      title: 'Cabochon Rubies',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Cabochon Rubies',
    navPageDescr:
      'The cabochon ruby is one of the oldest and most coveted gemstone used since ancient times. From the Egyptians to the royal jewels of kings and queens this simplistic cut of a cabochon ruby remains a beloved cut.'
  },
  'rubies-gemologist-recommended': {
    seo: {
      title: 'Gemologist Recommended',
      description:
        'We offer a large variety of beautiful, natural rubies. Between different hues and tones, and all type of shapes you will be sure to find your perfect ruby.'
    },
    navPageTitle: 'Gemologist Recommended',
    navPageDescr:
      'Our staff gemologists see thousands of rubies each year and can become very picky in what ruby speak to them. In this section we’ve asked them to suggest some of their personal favorites in our inventory.'
  },
  'rubies-id': {
    seo: {
      title: 'Loose Ruby - {shape} {carat} Ct. - #{id}',
      description:
        'Choose this Natural {carat} Ct. {shape} Ruby from {origin} (#{id}) ready to be set to create the perfect piece of jewelry for you or a loved one.'
    },
    navPageTitle: 'Ruby Gemstone'
  },
  'rubies-matched-pairs-of-rubies': {
    seo: {
      title: 'Ruby Pairs',
      description:
        'We offer a large variety of beautiful ruby pairs. Each ruby pair has been well-matched in shape, size, hue, and tone by our GIA certified gemologists.'
    },
    navPageTitle: 'Loose Ruby Pairs',
    navPageDescr:
      'Making ruby pairs can be challenging. It is not only the red hue, tone and saturation, but also the cut, carat-weight, and even clarity. We strive to make fine quality ruby pairs that compliment each other well.'
  },
  'rubies-star-ruby-pairs': {
    seo: {
      title: 'Star Ruby Pairs',
      description:
        'We offer a large variety of beautiful ruby pairs. Each ruby pair has been well-matched in shape, size, hue, and tone by our GIA certified gemologists.'
    },
    navPageTitle: 'Star Ruby Pairs',
    navPageDescr:
      'The star ruby is one of the rarest gemstones on earth. To then have a matching pair, is just simply astonishingly rare. To learn more about star rubies click <a href="/education/all-about-star-rubies/">here</a>.'
  },
  'rubies-matched-pairs-of-rubies-cabochon': {
    seo: {
      title: 'Ruby Cabochon Pairs',
      description:
        'We offer a large variety of beautiful ruby pairs. Each ruby pair has been well-matched in shape, size, hue, and tone by our GIA certified gemologists.'
    },
    navPageTitle: 'Ruby Cabochon Pairs',
    navPageDescr:
      'Pairs of cabochon rubies are unusual and difficult to match, below is a selection of options showcasing this beautiful cut seen in a natural ruby. Learn more about cabochon rubies <a href="/education/all-about-cabochon-rubies/">here</a>.'
  },
  'rubies-matched-pairs-of-rubies-id': {
    seo: {
      title: 'Ruby Pair - {shape} {carat} Ct. - #{id}',
      description:
        'Choose this Natural {carat} Ct. {shape} Ruby Pair from {origin} (#{id}), ready to be set to create the perfect piece of jewelry for you or a loved one.'
    },
    navPageTitle: 'Ruby Pair'
  },
  'ruby-jewelry-ruby-engagement-rings': {
    seo: {
      title: 'Ruby Engagement Rings',
      description:
        'Find the perfect engagement ring for your loved one. We offer expertly crafted, handmade ruby engagement rings, or design your own unique ruby ring!'
    },
    navPageTitle: 'Ruby Engagement Rings',
    navPageDescr:
      'Throughout history the rich, glowing red within a ruby has captivated Kings and Queens. We offer the finest rubies from Burma, Mozambique, Sri Lanka, Siam and Madagascar.  Ruby engagement ring are our specialty.'
  },
  'ruby-jewelry-ruby-rings': {
    seo: {
      title: 'Ruby Rings',
      description:
        'Find the perfect ruby ring for your loved one. We offer expertly crafted, handmade ruby rings, or design your own unique ruby ring!'
    },
    navPageTitle: 'Ruby Rings',
    navPageDescr:
      'We offer fine quality natural rubies from Mozambique, Burma, Sri Lanka, and Madagascar. With these sources comes a range of reds with modifying hues of pink, purple, orange, or a pure, pigeon blood red.'
  },
  'ruby-jewelry-mens-ruby-rings': {
    seo: {
      title: "Men's Ruby Rings",
      description:
        'Find the perfect ruby ring for your loved one. We offer expertly crafted, handmade ruby rings, or design your own unique ruby ring!'
    },
    navPageTitle: "Men's Ruby Rings",
    navPageDescr:
      "Browse hundreds of men's ruby rings with rubies originating from Burma, Mozambique and Madagascar. Whether you’re looking for a gift or a ring for everyday wear, you’ll find the perfect style in our collection."
  },
  'ruby-jewelry-star-ruby-rings': {
    seo: {
      title: 'Star Ruby Rings',
      description:
        'Find the perfect ruby ring for your loved one. We offer expertly crafted, handmade ruby rings, or design your own unique ruby ring!'
    },
    navPageTitle: 'Star Ruby Rings',
    navPageDescr:
      'Burmese star rubies are of the most expensive gemstones in the world, very few fine quality gems are known to exist. We also have star rubies from Vietnam and Sri Lanka. Learn more about star rubies <a href="/education/all-about-star-rubies/">here</a>.'
  },
  'ruby-wedding-rings-bands': {
    seo: {
      title: 'Ruby Wedding Bands',
      description:
        'Explore our expertly designed and handmade ruby and diamond wedding bands and stackable wedding rings with rubies and diamonds in all precious metal types.'
    },
    navPageTitle: 'Ruby and Diamond Wedding Bands',
    navPageDescr:
      'With rubies from Burma to Mozambique, we offer custom designs ruby wedding bands in the follow styles, eternity, infinity, bezel and simple prong settings.  We can also make custom ruby wedding bands and rings.'
  },
  'ruby-wedding-rings-bands-id': {
    seo: {
      title: '{metal} Ruby Band #{id}{metalTypeCode}',
      description:
        'Make this exquisite {metal} Ruby Band (#{id}{metalTypeCode}) into the perfect wedding band to match your one-of-a-kind ruby engagement ring.'
    },
    navPageTitle: '{metal} Ruby Band'
  },
  'wedding-bands-without-gemstone': {
    seo: {
      title: 'Plain Wedding Bands',
      description:
        'Plain wedding bands and rings are classic designs and anything but simple. From textured or two tone metal, to polished or matte design options, we have it all!'
    },
    navPageTitle: 'Plain Wedding Bands',
    navPageDescr:
      "Don't see what you're looking for? Contact our workshop for details on custom diamond and ruby wedding bands as a testament of your love."
  },
  'wedding-bands-without-gemstone-id': {
    seo: {
      title: '{metal} Plain Band #{id}{metalTypeCode}',
      description:
        'Make this exquisite {metal} Plain Band (#{id}{metalTypeCode}) into the perfect wedding band to match your one-of-a-kind ruby engagement ring.'
    },
    navPageTitle: '{metal} Plain Band'
  },
  'design-your-own': {
    seo: {
      title: 'Create Your Custom Ruby Ring in Our Simple Three-Step Process',
      description:
        'Get started on creating your custom ruby ring in our simple three-step process, designed to build you your perfect ruby ring in the exact right setting.'
    },
    navPageTitle: 'Design Your Own Ring'
  },
  'design-your-own-stones-single': {
    seo: {
      title: 'Choose An Ruby to Create Your Perfect Piece of Ruby Jewelry',
      description:
        "With the world's largest collection of natural rubies, you will surely be able to find the perfect ruby to add to you or your loved one's collection."
    },
    navPageTitle: 'Loose Rubies'
  },
  'design-your-own-stones-pair': {
    seo: {
      title: 'Choose An Ruby Pair to Create Your Perfect Ruby Jewelry',
      description:
        "Search through our handpicked ruby pairs to find the perfect jewelry set for you. You'll see a wide variety in hue, tone, color and shape. Let's get started!"
    },
    navPageTitle: 'Ruby Pairs'
  },
  'design-your-own-stones-single-filterName-filterValue': {
    seo: {
      title: 'Choose An Ruby to Create Your Perfect Piece of Ruby Jewelry',
      description:
        "With the world's largest collection of natural rubies, you will surely be able to find the perfect ruby to add to you or your loved one's collection."
    },
    navPageTitle: 'Loose Rubies'
  },
  'design-your-own-stones-pair-filterName-filterValue': {
    seo: {
      title: 'Ruby Pairs',
      description:
        "Search through our handpicked ruby pairs to find the perfect jewelry set for you. You'll see a wide variety in hue, tone, color and shape. Let's get started!"
    },
    navPageTitle: 'Ruby Pairs'
  },
  'design-your-own-review-id': {
    seo: {
      title: '{stoneType} {category} {weight} {metalName}',
      description:
        'Looking for something special? Let us help you to create the perfect custom Ruby {fullType} as you finalize your ruby gemstone and setting options.'
    },
    navPageTitle: 'Finalize Your {type}'
  },
  'ruby-jewelry-ruby-earrings': {
    seo: {
      title: 'Ruby Earrings',
      description:
        'With hundreds of designs in stock, find simple ruby studs to red-carpet-ready chandelier ruby earrings. Browse today, and find the perfect fit for you!'
    },
    navPageTitle: 'Ruby Earrings',
    navPageDescr:
      'Made with carefully matched pairs of red rubies, you can expect our ruby stud earrings to match perfectly. We also have ruby and diamond earrings set in glittering halos, modern bezels and more.'
  },
  'ruby-jewelry-ruby-necklaces-pendants': {
    seo: {
      title: 'Ruby Necklaces & Pendants',
      description:
        'Search our wide variety of ruby necklaces & pendants including classic styles & modern designs. We offer one-of-a-kind necklaces for everyone, including you!'
    },
    navPageTitle: 'Ruby Necklaces and Pendants',
    navPageDescr:
      'We also offer custom ruby necklace and pendants set in sparkling diamond halos to sleek, modern-looking bezels. Choose the perfect ruby and our expert design team can create a ruby necklace of your dreams.'
  },
  'ruby-jewelry-ruby-bracelets': {
    seo: {
      title: 'Ruby Bracelets',
      description:
        'Find a timeless look of elegance with our ruby bracelets. With countless designs and color combinations, you are sure to find the bracelet of your dreams.'
    },
    navPageTitle: 'Ruby Bracelets',
    navPageDescr:
      'Our ruby bracelets are made with a collection of stones carefully selected and set. We also offer ruby and diamond bracelets set in an assortment of precious metals that best compliments our rubies.'
  },
  cart: {
    seo: {
      title: 'Cart',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-index': {
    seo: {
      title: 'Cart',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-order-summary': {
    seo: {
      title: 'Order Summary',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Order summary'
  },
  'cart-index-secure-payment': {
    seo: {
      title: 'Secure Payment',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  'cart-index-payment-order': {
    seo: {
      title: 'Payment Order',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Shopping Cart'
  },
  account: {
    seo: {
      title: 'Account',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'My Account'
  },
  'account-notifications': {
    seo: {
      title: 'Email Notifications',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Notifications'
  },
  'account-recommendations': {
    seo: {
      title: 'Recommended for You',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Recommended for You'
  },
  'account-personal-details': {
    seo: {
      title: 'Personal & Login Details',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Personal & Login Details'
  },
  'account-preview-request-submissions': {
    seo: {
      title: 'Preview Request Submissions',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Preview Request Submissions'
  },
  'account-address-book': {
    seo: {
      title: 'Address book',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Address book'
  },
  'account-orders': {
    seo: {
      title: 'Review Your Order History',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Orders'
  },
  'account-change-password': {
    seo: {
      title: 'Change Your Password',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Change Your Password'
  },
  'account-reset-password': {
    seo: {
      title: 'Reset Your Password',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Reset Password'
  },
  'account-reset-password-token': {
    seo: {
      title: 'Reset Your Password',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Reset Password'
  },
  wishlist: {
    seo: {
      title: 'Favorite Rubies',
      description:
        'Keep track of your favorite loose rubies, settings, ruby pairs, and finished ruby jewelry pieces in one place. Compare them all before choosing the one!'
    },
    navPageTitle: 'Favorites'
  },
  'compare-list': {
    seo: {
      title: 'Compare list',
      description:
        'The authority in ruby engagement rings, natural rubies and ruby jewelry since 1939. We have the largest collection of rubies you will find online.'
    },
    navPageTitle: 'Compare list'
  },
  'contact-us': {
    seo: {
      title: 'Contacts',
      description:
        'Our team of ruby and jewelry experts is available via live chat, phone, and email. Contact us now for personal assistance.'
    },
    navPageTitle: 'Contact The Natural Ruby Company'
  },
  showroom: {
    seo: {
      title: 'Visit the Natural Ruby Company showroom',
      description:
        'Visit the Natural Ruby Company showroom at our Manhattan office in New York, NY to find your perfect ruby or ruby jewelry.'
    },
    navPageTitle: 'Visit the Natural Ruby Company showroom'
  },
  'showroom-preview': {
    seo: {
      title: 'Visit the Natural Ruby Company showroom',
      description:
        'Visit the Natural Ruby Company showroom at our Manhattan office in New York, NY to find your perfect ruby or ruby jewelry.'
    },
    navPageTitle: 'Visit the Natural Ruby Company showroom'
  },
  'showroom-preview-success': {
    seo: {
      title: 'Visit the Natural Ruby Company showroom',
      description:
        'Visit the Natural Ruby Company showroom at our Manhattan office in New York, NY to find your perfect ruby or ruby jewelry.'
    },
    navPageTitle: 'Visit the Natural Ruby Company showroom'
  },
  'our-staff': {
    seo: {
      title: 'Meet our the Natural Ruby Company teams',
      description:
        'Learn more about the talented professionals that make everything happen at The Natural Ruby Company.'
    },
    navPageTitle: 'Our Staff'
  },
  'michael-arnstein': {
    seo: {
      title: 'Michael Arnstein',
      description:
        'Learn more about the talented professionals that make everything happen at The Natural Ruby Company.'
    },
    navPageTitle: 'Michael Arnstein',
    navPageDescr: 'President - The Natural Ruby Company'
  },
  auth: {
    seo: {
      title: 'Create Account',
      description:
        'Create an account to track your order and save items of interest to your favorites for later purchase at thenaturalrubycompany.com'
    },
    navPageTitle: 'The Natural Ruby Company - Create Account'
  },
  'education-index-category-slug': {
    seo: {
      title: 'Education',
      description: ''
    },
    navPageTitle: 'The Natural Ruby Company'
  }
}
