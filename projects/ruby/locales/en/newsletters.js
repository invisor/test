export default {
  validation: {
    required: 'Please enter email address',
    invalid: 'Please enter valid email address'
  },
  success:
    'Thank you for subscribing to the thenaturalrubycompany.com newsletter',
  confirmButtonText: 'Ok',
  signUp: 'Sign up',
  email: 'Email address'
}
