export default {
  pageTitle: 'Our Staff',
  pageSubtitle:
    'The Natural Ruby Company specializes in providing the finest natural rubies.',
  pageMessage:
    'We owe our success to our dedicated staff, from salespeople to designers. Learn more about the talented professionals that make everything happen at The Natural Ruby Company below:',
  contactUsTitle: 'Contact Us',
  readMore: 'Read more',
  birthplace: 'Birthplace',
  residence: 'Residence',
  blog: 'Read Blog',
  previous: '< Previous Team Member',
  next: 'Next Team Member >',
  previousShort: '< Previous Member',
  nextShort: 'Next Member >'
}
