export default {
  header: {
    visitTitle: 'Visit The Natural Ruby Company',
    messageTitle: 'NYC Showroom',
    messageText:
      'We offer a unique buying experience in our Manhattan office. Our showroom occupies the penthouse floor of a midtown office building where we have created a comfortable and private setting for our guest to view our extensive ruby collection. A visit to our showroom and office allows customers to see firsthand how we have blended state of the art technology with the ancient art of hand crafting and designing jewelry.',
    city: 'New York city, USA',
    security: 'Security Requirements',
    review: 'Please review our {link} for all guests',
    galleryButton: 'Launch the showroom gallery'
  },
  sectionOne: {
    addressTitle: 'Our Showroom Opening Hours & Location',
    detailsTitle: 'Your details',
    companyName: 'The Natural Ruby Company',
    address:
      'Head Office, Showroom & Workshop<br>6 East 45th Street, 20th Floor<br>New York, NY 10017<br>United States',
    viewMap: 'View Map',
    getDirections: 'Get Directions',
    whenTitle: 'When would you like to visit our showroom?',
    clockLabel: 'Type the time you’d like to visit us'
  },
  sectionTwo: {
    sectionTitle: 'What specific items would you like to see?',
    message:
      'Select or start typing an item ID below. You can find the product ID numbers in the detail view of a product page or add a list from your {favorites} or {compare} lists',
    itemsAdded: '{0}/{1} items added',
    fromFavorites: 'Favorites',
    fromCompare: 'Compare',
    selectItemsPlaceholder: 'Select items'
  },
  sectionThree: {
    sectionTitle: 'Do you have other questions or special requests?'
  },
  sectionFour: {
    sectionTitle: 'Do you have an account with us?',
    sectionTitleAuth: 'Are you ready to finalise your request?',
    reviewButton: 'Review my request',
    submitButton: 'Submit my showroom request',
    loginTitle: 'Log in to complete your submission',
    authMessage:
      'Please check that you are happy with the details above and if so, please proceed to review your request.',
    loginMessage:
      'Logging in will allow you to see what other showroom visits you have scheduled and it will also allow you to change or cancel any show room visit request or confirmed visit.',
    guestTitle: 'Continue as a guest',
    guestMessage:
      'If you don’t have an account with us, we recommend you {signUp} so that you can easily request, view and manage your showroom visits. Otherwise, submit this request as a guest below.',
    signUp: 'sign up for one',
    or: 'OR'
  },
  preview: {
    previewTitle: 'Please review your showroom visit request',
    previewMessage:
      'Please ensure the details you have entered below are correct before you submit your request.',
    details: 'Your visit request details',
    contactDetails: 'Your visit request details',
    date: 'Date:',
    time: 'Time:',
    salesPerson: 'Preferred Salesperson:',
    back: 'Go back and edit my request'
  },
  success: {
    successTitle: 'Your visit request has been submitted!',
    successMessage:
      'One of our salespeople will be in touch shortly to confirm your showroom visit.',
    details: 'Your visit request details',
    contactDetails: '',
    back: 'Request to cancel my visit'
  },
  securityDialog: {
    title: 'Showroom, Office & Workshop Security Requirements',
    intro: `<p>Thanks for your interest in visiting our showroom! We can’t wait to have you stop by and view our natural ruby jewelry in person. When you visit our Showroom you’re literally getting an up-close glimpse into our daily workflow of our team at work. You’ll see team members handling rubies, settings, and finished jewelry working to provide every customer with their perfect ring, earring, necklace or pendant.</p>
    <p>We can’t wait to meet you. However, for security reasons, an appointment is required. Please review our security measures below if you plan to visit our office.</p>`,
    title1: 'We Value Your Privacy!',
    message1:
      '<p>Your personal information is not shared with any third parties. It is strictly used to ensure a safe and pleasurable buying experience as best as possible.</p>',
    title2: 'Showroom Hours: {0}',
    message2: `<ol><li>Once Again: Walk-ins are not allowed. If you’re in town please give us a call.</li>
    <li>Every visitor must present a valid government <strong>Photo ID</strong> such as a drivers license or passport.</li>
    <li>Your photo ID will be scanned for our security records.</li>
    <li>Visitors are scanned by metal detector and bags are subject to inspection.</li></ol>`,
    message21:
      '<p><strong>Returning Visitor?</strong> We apologize but you’ll still be subject to the security procedures above.</p>',
    title3: 'A Note About Our Security Practices',
    message3:
      '<p>We work with a 3rd-party to keep our office and showroom secure and safe at all times. Our 24/7 armed guard security and surveillance service includes, but not limited to:</p>',
    message31: `<ul>
    <li>16 High Resolution Video Cameras with Audio.</li>
    <li>10 Motion Sensors.</li>
    <li>18 Door and Window Sensors.</li>
    <li>6 Specially-located Vibration Sensors.</li>
    <li>10 Silent Call Buttons – Quick Police and 3rd-Party Armed Guard Response</li>
    <li>3 Double-Door Man-Trap Systems with Fail Safe Protection</li>
    <li>1 Intake Window with 2 Inch Thick, Bullet Proof Glass</li>
    <li>Steel Reinforced Wall and Door Placement</li>
    <li>High Security Office Building</li>
    <li>Full Time Armed Security Guards. 24 Hours, 7 Days a Week.</li>
    </ul>`,
    message32:
      '<p>Our independent 24/7 security monitoring company responds immediately if summoned. Remote access allows for instant monitoring and reporting supplying meticulous security detail.</p>',
    message33:
      '<p><strong>Notice</strong>: For safety reasons, an armed guard is on premises at all times during showroom hours.</p>'
  },
  form: {
    customerName: 'Name',
    phone: 'Phone',
    email: 'Email',
    date: 'Date',
    time: 'Time',
    itemIds: 'Item ID(s)',
    message: 'Message',
    submit: 'Submit',
    salesPerson: 'Preferred sales person',
    medicalMask: 'Would you like your salesperson to wear a mask?',
    medicalMaskYes: "I'd rather meet with someone with wearing masks",
    medicalMaskNo: "I'm ok without masks",
    itemIdPlaceholder: 'Please start to type Item ID'
  },
  swal: {
    title: 'Success',
    text: 'Your request has been submitted',
    ok: 'Ok'
  },
  formErrors: {
    customerName: 'Please enter name',
    phone: 'Please enter phone',
    email: {
      required: 'Please enter email',
      valid: 'Please enter correct email address'
    },
    date: 'Please enter date',
    time: 'Please enter time',
    itemIds: 'Please select items'
  },
  images: {
    image1:
      'Continue to explore the technology and craftsmanship performed onsite as we visit with the professional staff that makes NRC a leader in the industry.',
    image2:
      'Upon your arrival, you will enter the exterior of our bright and cheery penthouse salon with impressive Neoclassic architectural and design elements.',
    image3:
      "With your personal gemologist, you'll be escorted to a private room for the most intimate shopping experience.",
    image4:
      "In our showroom, you'll be treated to a relaxed atmosphere to help you be at ease to explore the jewelry at hand.",
    image5:
      'Our showroom has many areas to sit in and have some privacy while making your considerations.',
    image6:
      'Feel free to take any piece outside on our lovely balcony to see it in all its glory in natural sunlight.',
    image7:
      'Feel free to take any piece outside on our lovely balcony to see it in all its glory in natural sunlight.'
  }
}
