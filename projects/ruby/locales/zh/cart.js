import stoneShapes from './constants/stoneShapes'
import metalTypesFilter from './constants/metalTypesFilter'
import stoneTypes from './constants/stoneTypes'
import stoneCut from './constants/stoneCut'
import stoneClarity from './constants/stoneClarity'
import stoneColorIntensity from './constants/stoneColorIntensity'
import stoneColors from './constants/stoneColors'
import prongTypes from './constants/prongsNames'
import divisionNames from './constants/divisionNames'
import stonesOrigins from './constants/stonesOrigins'
import treatments from './constants/treatments'
import bandStyles from './constants/bandStyle'
import designStyles from './constants/designStylesFilter'

export default {
  viewItem: 'View item',
  editItem: 'Edit item',
  removeItem: 'Remove item',
  outOfStock: 'Out of stock:',
  outOfStockText:
    'The item has been added to your cart but please note it is currently out of stock and someone from our service team will be in touch soon with a more accurate estimated delivery date.',
  onHold: 'On hold:',
  onHoldText:
    'This item is currently on hold for another customer. Continue your order to be placed on a wait-list, and someone will contact you within 24 - 48 hours with an update on the status of this item. You will not be charged at this time.',
  countryPopup: {
    outOfStock: '缺货:',
    outOfStockText:
      '此商品已添加到您的购物车，但请注意它目前缺货，我们的服务团队将尽快与您联系，提供更准确的预计交货日期。',
    US1: '请注意：非美国客户不提供延长保修保护',
    US2: 'Please Note: Extend Warranty Protection is available for USA based customers. You may add a warranty to any applicable item in your <a class="is-underline" href="/cart/">shopping cart</a>.',
    BE: `Belgium has a few import prohibitions - to that which includes loose stones and jewelry.</br>
        You can read more on the government website <a class="is-underline" href="https://www.trade.gov/country-commercial-guides/belgium-customs-regulations" target="_blank">here</a>.<br>
        To view the import prohibitions and restrictions with shipping to Belgium, click <a class="is-underline" href="https://crossborder.fedex.com/us/assets/prohibited-restricted/belgium/index.shtml" target="_blank">here</a>.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`,
    IN: `India has a few import prohibitions- to that which includes loose stones and jewelry.</br>
        You can read more on the government website <a class="is-underline" href="https://commerce.gov.in/about-us/divisions/export-products-division/gems-and-jewellery/" target="_blank">here</a>.<br>
        To view the import prohibitions and restrictions with shipping to India, click <a class="is-underline" href="https://crossborder.fedex.com/us/assets/prohibited-restricted/india/index.shtml" target="_blank">here</a>.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`,
    IR: `Iran is unfortunately not a country that we are permitted to ship to.</br>
        We apologize if this causes any inconvenience, but we are happy to ship to another country of your choice.`
  },
  vatNotice: {
    note: '<strong>请注意:</strong>&nbsp; Import Customer, Duty/VAT/GST may apply',
    link: '查看我们的常见问题解答',
    faq: '有关进口到您所在国家/地区时可能获得的更多信息.',
    accept: '我了解我可能需要向政府支付的额外税款.'
  },
  paymentMethod: {
    1: '信用卡',
    3: '银行电汇',
    5: 'Check / ACH'
  },
  docs: {
    title: '报告和证明文件',
    message:
      '我们的宝石和珠宝报告详细说明了您物品的所有相关属性，并且被大多数保险提供商所接受，请在下面选择您希望接收这些报告的方法。',
    scan: '宝石扫描报告',
    scan3d: '3D 扫描报告',
    appraisal: '宝石鉴定',
    option1: '<strong>打印文件.</strong> 我希望将打印的副本发送到我的收货地址',
    option2:
      '<strong>环保文件.</strong> 我想帮助拯救地球，请通过电子邮件向我发送 PDF 副本',
    option3: '<strong>不需要文件.</strong> 我不希望收到任何文件.*',
    notice:
      '*如果您以后再决定想要您的文件，只需将您的请求通过电子邮件发送给我们，我们会将其发送给您。'
  },
  expandedCart: {
    close: '关闭',
    itemId: '货品编号 ',
    total: '总计：',
    totalIn: '总计{0}：',
    seeCart: '查看我的购物车',
    checkout: '结算',
    pay: '安全付款',
    slideSwitcherText1: '查看另外添加的{0}项',
    slideSwitcherText2: '查看下一个添加的项目',
    wax: {
      ring: '戒指蜡模型',
      earring: '耳环蜡模型',
      pendant: '吊坠蜡模型',
      necklace: '项链蜡模型',
      default: '蜡模型'
    }
  },
  item: {
    itemId: '货品编号：',
    designId: '设计编号:',
    vedicSetting: 'Vedic Style:',
    vedicSettingSelected: 'Stone Touching Skin',
    metal: '金属：',
    shape: '形状：',
    weight: '重量：',
    color: '颜色：',
    cut: '切工：',
    clarity: '净度：',
    widthMm: '{0} mm',
    size: '尺寸：',
    paveStones: '铺镶钻石:',
    centerStone: '主石：',
    centerStones: '{0} x {1} {2} {3}(s)',
    colorIntensity: '颜色强度:',
    origin: '产地:',
    totalPerCt: '{0} 总价 ({1} 每克拉)',
    sideStone: '副石：',
    sideStones: '{0} x {1} {2} {3}(s)',
    stoneColor: '宝石颜色：',
    stoneShape: '宝石形状：',
    centStCt: '主宝石重量：',
    sideStCt: '副宝石重量：',
    prodTime: '生产时间：',
    prodTimeDays: '{0}到{1}天',
    finish: '完成样式：',
    engraving: '雕刻：',
    sidestoneSelections: '副石：',
    totalWeight: '总重量：',
    stoneId: '宝石编号：',
    settingId: '镶嵌编号：',
    grade: '等级：{0}',
    eachStoneWeight: '单石重量：',
    total: '总计',
    perCarat: '每克拉',
    prongs: 'Prongs:',
    ringWidth: 'Width:',
    stoneType: 'Stone type:',
    quantity: 'Quantity:',
    jewelrySideStones: 'Side stones:',
    totalMetalWeight: 'Total metal weight:',
    totalStonesWeight: 'Total stones weight:',
    paveStonesWeight: 'Pave stones weight:',
    treatment: 'Treatment:',
    dimensions: 'Dimensions (MM):',
    length: 'Length: {0}',
    height: 'Height: {0}',
    width: 'Width: {0}',
    seeMore: 'See more details',
    seeLess: 'See less details',
    starAppearance: 'Star appearance:',
    style: 'Style:',
    stoneShapes: {
      ...stoneShapes
    },
    metalTypes: {
      ...metalTypesFilter
    },
    stoneTypeName: stoneTypes,
    stoneCut,
    stoneClarity,
    stoneColorIntensity,
    stoneColors,
    prongTypes,
    stonesOrigins,
    treatments,
    bandStyles,
    designStyles
  },
  waxTitles: {
    ringWaxModel: '戒指 3D 打印模型',
    earringWaxModel: '耳环 3D 打印模型',
    pendantWaxModel: '吊坠 3D 打印模型',
    necklaceWaxModel: '项链 3D 打印模型',
    waxModel: '3D 打印模型'
  },
  band: '戒指',
  plainBand: '素圈戒指',
  pair: '对',
  addressForm: {
    divisionNames,
    salesTeam: '已经和我们的工作人员取得联系？',
    customerInformation: '顾客信息',
    deliveryAddress: '配送地址',
    shippingAddressLabel: '从地址簿中选择送货地址或输入新地址',
    shippingAddressPlaceholder: '选择配送地址',
    email: '邮箱地址',
    firstNameLabel: '名字',
    lastNameLabel: '姓氏',
    addressLine1: '街道名称/区名称',
    addressLine2: '房间号/门牌号',
    shippingCity: '配送城市',
    state: '省',
    billingCity: '城市',
    regions: '州/省/县',
    pleaseSelect: '请选择',
    zip: '邮编',
    country: '国家',
    phoneNumber: '电话号码',
    billingAddress: '账单地址',
    asShipping: '和配送地址一致 ',
    billingAddressLabel: '从地址簿中选择账单地址或输入新地址',
    billingAddressPlaceholder: '选择账单地址',
    deliveryOptions: '配送选项',
    comments: '留言或者咨询',
    commentsTitle: '如果你对订单细节有任何疑问，请在此留言',
    payment: '支付方式',
    errorTitle: '抱歉，出错了',
    fixErrors: '请更正以下指示的任何错误，然后重试：',
    ok: '好的',
    rules: {
      email: {
        required: '需要一个有效的邮箱地址',
        template: '请输入一个有效的邮箱地址'
      },
      firstName: {
        required: '请输入您的名字'
      },
      billingFirstName: {
        required: '请输入您的账单名字'
      },
      lastName: {
        required: '请输入您的姓氏'
      },
      billingLastName: {
        required: '请输入一个账单名字'
      },
      shippingStreet1: {
        required: '请输入一个有效的配送地址'
      },
      billingStreet1: {
        required: '请输入一个有效的账单地址'
      },
      shippingCity: {
        required: '请输入您的配送城市'
      },
      billingCity: {
        required: '请输入一个账单城市'
      },
      shippingState: {
        required: '请输入一个有效的配送省'
      },
      billingState: {
        required: '请输入一个有效的账单省'
      },
      shippingCountryId: {
        required: '请选择配送国家',
        notProhibitedCountry: '无法配送的国家'
      },
      billingCountryId: {
        required: '请选择账单城市'
      },
      shippingZipCode: {
        required: '请输入一个有效的配送区号'
      },
      billingZipCode: {
        required: '请输入一个有效的账单区号'
      },
      phone: {
        required:
          '请提供一个电话号码，以便您的订单有任何问题时我们可以用电话联系。'
      },
      delivery: {
        required: '请选择配送方式'
      },
      acceptVAT: {
        required: '请应用这些规则'
      }
    }
  },
  cartControls: {
    edit: 'Edit cart',
    backToCart: '返回到购物车',
    backToAddress: '返回到地址&配送',
    checkout: '安全支付',
    signIn: '登录',
    signInAs: '登录',
    asGuest: '以访客身份继续访问',
    guest: '访客',
    signOut: '退出',
    continue: 'Continue browsing',
    itemsCount: 'Shopping cart ({count} items)',
    hold: '暂不购买'
  },
  cartTotal: {
    pay: '使用以下方式安全付款：',
    subtotal: '小计：',
    shipping: '配送：',
    subtotalIn: '小计：{0}:',
    shippingIn: '配送：{0}:'
  },
  screens: {
    shoppingCart: '购物车',
    securePayment: '安全支付',
    orderSummary: '订单汇总',
    addressShipping: '地址&配送',
    paymentOrder: '支付&订单'
  },
  orderSummary: {
    thankYou: '感谢您的购买',
    emailSent: '我们将会发送一份带收据的确认邮件给您。',
    order: '订单编号: ',
    thankYouOrder: '感谢您的购买',
    orderNumber: '订单编号为：',
    receiveEmail: '您将会收到一封确认邮件',
    ok: '好的'
  },
  paymentForm: {
    paymentsOptions: '付款方式',
    paymentsMessage: `<p>我们将会很快联系您，通过<b>汇款</b> 或 <b>信用基金</b> 完成付款。</p>
    <p><b>请注意：</b>国内和国际订单均只接受美元汇款。</p>`,
    cardDetails: '卡信息',
    cardholder: '持卡人姓名(如卡上所示)',
    cardNumber: '卡号',
    expirationDate: '有效期',
    ccv: 'CVV号码',
    ccvDescription: 'CVV号是您的信用卡背面签名面板旁边的3位数字。',
    terms: '通过验证您的订单，您接受我们所有',
    termsLink: '条款',
    proceed: '完成订单',
    ccvExpError: '您用于订单的信用卡已过期',
    rules: {
      paymentOptionId: {
        required: '请选择有效的付款方式'
      },
      billingName: {
        required: '请输入与卡上显示的名称完全相同的名称',
        range: '长度应为5到100'
      },
      cardNumber: {
        required: '必须提供有效的信用卡号',
        type: '请输入一个有效的信用卡号码',
        range: '您输入的信用卡号似乎无效'
      },
      ccExpMonth: {
        required: '有效的信用卡到期月份为必填项',
        between: '必须介于{0}和{1}之间'
      },
      ccExpYear: {
        required: '有效的信用卡到期年份为必填项',
        between: '必须介于{0}和{1}之间'
      },
      cvvCode: {
        requiredAmex: '请在卡背面输入4位数字',
        required: '请在卡背面输入3-4位数字',
        type: '该卡的安全码无效',
        range: '请在卡背面输入3-4位数字'
      }
    },
    swal: {
      title: '抱歉，出错了',
      message: '请根据下面的提示进行信息修改并重试',
      ok: '好的'
    }
  },
  shoppingCart: {
    item: '商品',
    itemPrice: '商品价格',
    remove: '删除'
  },
  cartSidebar: {
    in: ' {0}',
    orderSummary: '订单汇总',
    totalInfo:
      '*Please Note: The price shown above is a close approximation of your order total after converting from USD to {0}. Please note that we process all charges in USD, and final charge may vary due to associated credit card fees and exchange rate fluctuations.',
    subtotal: '小计',
    tax: '税({0})',
    shipping: '配送',
    orderTotal: '总计',
    paySecurely: '安全支付',
    deliveryInfo:
      '如果您需要星期六送货，请致电<b> 1-212-221-6136 </b>或通过电子邮件<b> info@thenaturalrubycompany.com </b>和客户服务部门确认。周日和节假日不提供服务。',
    infoBlock: {
      whatNext: {
        title: '接下来是什么？',
        description:
          '下订单后，我们的销售团队将审核所有详细信息，如果需要更多信息，我们将尽快与您联系。如果有问题，请阅读我们的“FAQ”，或随时与我们联系。'
      },
      returnPolicy: {
        title: '我们的退货政策',
        description:
          '我们提供14天的检查期，供您检查红宝石或珠宝成品，如果您对商品不完全满意，则可以退货并获得全额退款，有关这些政策以及所有“自定义珠宝订单”的更多信息，请查看我们的完整退货政策。'
      },
      shippingInformation: {
        title: '配送信息',
        description:
          '您的订单将采用独特包装，并拥有全部的保险，您的订单寄出后，我们会将跟踪信息发送到您提供的邮箱。请注意！收货时需要签名。有关更多信息，请查看我们的完整运输政策。'
      },
      privacySecurity: {
        title: '您的隐私和安全',
        description:
          '我们网站上的所有交易均通过SSL安全技术执行，您的信用卡，和个人信息有关数据以及所有其他电子信息的安全性和私密性永远不会被窃取或滥用。'
      },
      labReports: {
        title: '第三方实验室报告',
        description:
          '我们将内部报告与所有红宝石和珠宝一起发送，如果您的商品还具有第三方实验室认证(来自GIA或其他机构), 确认购买后，将单独发送此文档，由于第三方实验室报告无法替换，因此我们只能在收到客户告知他们将确定购买物品后，才发送给他们。'
      }
    }
  },
  empty: '您的购物车是空的！',
  continueShopping: '继续购物，稍后再结帐！'
}
