export default {
  settings: {
    settingRing: 'Ring',
    settingEarring: 'Earrings',
    settingNecklace: 'Necklace',
    settingPendant: 'Pendant'
  },
  stone: '宝石',
  pair: '对',
  ring: '戒指',
  mensRing: "Men's Ring",
  earring: 'Earrings',
  pendant: '吊坠',
  necklace: '项链',
  bracelet: '手链',
  weddingBand: '结婚戒指',
  plainBand: '素圈戒指',
  settingRing: '镶嵌_戒指',
  settingEarring: '镶嵌_耳环',
  settingNecklace: '镶嵌_项链',
  settingPendant: '镶嵌_吊坠',
  custom: '自定义',
  setting: '镶嵌',
  wax: '蜡'
}
