export default {
  tcw: '<span>{0}</span>&nbsp;<span class="tooltip-light">Ct.Tw.<span class="tooltiptext">Total Carat Weight</span></span>',
  totalCaratWeight: 'Total Carat Weight',
  tcwPlain: '{0} Ct.Tw.',
  'tcw-t':
    '<span>{0}</span>&nbsp;<span class="tooltip-light tooltipless">Ct.Tw.</span>',
  unknown: '未知',
  videoMessage:
    '此视频为实物拍摄，我们不使用“库存”照片，所拍摄的视频就是该实物',
  close: '关闭',
  priceConversionTitle: '货币换算为{currency}',
  priceConversionMessage:
    "We detected that you are visiting our website from {country}. As a courtesy, we have made a close approximation of this item's price after converting from USD to {currency}. Please note that we process all charges in USD, and final prices may vary due to associated credit card fees and exchange rate fluctuations.",
  priceConversionOriginal: '美元价格： {price}',
  cookieMessage:
    '我们使用Cookie来个性化内容和广告，提供社交媒体功能并分析我们的流量,我们还会与我们的社交媒体共享有关您对我们网站使用情况的信息， 广告和分析合作伙伴，可以将其与您提供给他们的其他信息或您通过使用其服务收集到的其他信息相结合。',
  cookieAccept: '我接受',
  cookieMore: '了解更多...',
  topBanner: {
    rubies: '红宝石',
    emeralds: '祖母绿',
    sapphires: '蓝宝石'
  },
  whyPhone: 'Why are we asking for a phone number?',
  whyPhoneContent:
    "Don't worry - we won't call you unless you specifically ask us to, or in the event that we have not heard back on a request via email. Also, please know that we respect your privacy. Your contact information is kept secure and and is never shared or sold."
}
