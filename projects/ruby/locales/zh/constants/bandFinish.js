export default {
  crossSatin: '十字缎',
  hammer: '手工制作',
  highPolish: '高抛光',
  sandBlast: '喷沙',
  satinFinish: '缎面处理',
  stoneFinish: '石材饰面',
  wireMatte: '金属丝哑光'
}
