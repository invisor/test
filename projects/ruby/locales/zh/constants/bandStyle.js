export default {
  antique: '复古',
  classic: '经典',
  eternity: '永恒',
  mens: '男式',
  modern: '现代'
}
