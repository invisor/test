export default {
  nA: '无',
  deepRed: '深红色',
  red: '红色',
  vividRedToDeepRed: '浅红到深红',
  darkRed: '暗红色',
  intenseRed: '深红色',
  intenseRedToVividRed: '深红到鲜红',
  vividPinkRed: '鲜艳的粉红色',
  vividRed: '鲜艳的红色',
  vividRedPigeonsBlood: '鲜艳的红色-“鸽子血”',
  vividRedImperialRed: '鲜艳的红-“帝王红”',
  purplishRed: 'Purplish Red',
  vividRedVibrant: '鲜艳的红色-“充满活力”'
}
