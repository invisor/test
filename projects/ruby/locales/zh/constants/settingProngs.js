export default {
  ballProng: '球状齿',
  doubleClaw: '双钩状齿',
  flatTab: '平面标签',
  doubleRound: '双球状齿',
  singleClaw: '单钩状齿',
  vProng: 'V状齿',
  none: ''
}
