export default {
  fractureFilling: '裂缝填充',
  noEnhancement: '无优化处理',
  insignificantClarityEnhancement: '微度净度优化处理',
  minorClarityEnhancement: '轻度净度优化处理',
  moderateClarityEnhancement: '中度净度优化处理',
  significantClarityEnhancement: '重度净度优化处理',
  dyed: '染色的',
  enhancedWithUnidentifiedMethods: '未知方式处理',
  heatTreated: '热处理',
  none: '无'
}
