import stoneShapes from '../constants/stoneShapes'
import stonesOrigins from '../constants/stonesOrigins'
import filtersOrigins from '../constants/filtersOrigins'
import treatments from '../constants/treatments'
import stoneTypes from '../constants/stoneTypes'
import stoneColors from '~/locales/zh/constants/stoneColors'
import bandStyle from '~/locales/zh/constants/bandStyle'
import bandFinish from '~/locales/zh/constants/bandFinish'

export default {
  prongsNotice:
    'The item displayed above includes a stone set with {0} prongs. Please note the actual item you receive may look different depending on your prong selection.',
  availability: {
    0: 'Available',
    1: 'Delisted',
    2: 'Call for Status',
    3: 'Sold',
    4: 'On Hold'
  },
  confirmation: {
    ringSize:
      'Please reconfirm your ring size is a size {ringSize} by clicking the confirm button below',
    inCart:
      'A design using the same stone is currently in your shopping cart. By continuing, the item in your cart will be replaced by this new design.',
    confirm: 'Confirm',
    cancel: 'Cancel'
  },
  addedToCart: 'Added to {cart}',
  cart: 'cart',
  viewFullDetails: 'View full product details',
  chain: '18" 包含链子',
  soldSorry: '很抱歉，此商品已售罄，如果您有其他问题，请联系我们。',
  soldDelivery: ' - Order now for delivery by ',
  soldDeliveryTooltip:
    "But don't worry, we're working to bring this popular {category} back in stock as quickly as possible. Order now for delivery by {date}",
  thisIsMto: '此商品可正常购买',
  onHold: '暂时不能购买',
  soldOut: '已出售',
  outOfStock: '缺货',
  ring: '戒指',
  earring: '耳环',
  pendant: '吊坠',
  necklace: '项链',
  preview: 'All',
  stone: '宝石',
  setting: '镶嵌',
  reports: '报告',
  available: '可购买',
  freeShipping: '免邮费',
  noHasslePolicy: '14天无忧退货政策',
  noHasslePolicyText:
    '我们提供14天的退货窗口，以查看我们的红宝石、珠宝或您独一无二的定制的红宝石珠宝。如果您希望退货，我们将全额退款给您，除非我们在开始生产前特别通知您任何可能的进货费用。我们的商品很少有补货费用。如果您有疑问，请与我们联系。',
  stonePrice: '宝石价格',
  pairPrice: '宝石对价格',
  settingPrice: '镶嵌价格',
  designId: '设计编号',
  itemId: '货品编号',
  stoneItemId: 'Ruby Item ID',
  settingItemId: 'Setting Item ID',
  totalPrice: '总价',
  calculatedPrice: '在下一步计算价格',
  calculatedPriceTitle: '价格在哪里？',
  calculatedPriceText:
    '每一件定制珠宝都是独一无二的，以匹配您所选择的宝石。因此，最终价格只能在您选择了一个兼容的宝石和所有可用的选项之后进行计算，其中可能包括:金属类型，戒指大小，副石和/或铺钻石的重量。只需继续选择下面的这个镶嵌。我们会在下一步为您自动计算价格',
  price: '价格',
  toCartStone: '将裸石加入购物车',
  toCartPair: 'Add loose stones to cart',
  toSettingStone: '使用这个宝石来创作首饰',
  toCartPairs: '加入购物车',
  toSettingPairs: '添加到耳环上',
  inCart: '这个货品目前已经在您的',
  inCartStone: '这个宝石目前已经在您的',
  shoppingCart: '购物车',
  videoShot: '使用iPhone XS拍摄',
  metal: '金属',
  vedic: 'Vedic Setting Style',
  vedicText:
    "This setting can accommodate a 'Vedic' style stone placement. Vedic settings are usually chosen for their ability to set the gemstone low, so it specifically touches the skin of the wearer.",
  vedicTextCTA:
    'If you would like the gemstone to touch the skin when set please check this box to confirm.',
  chooseVedic: 'Place gemstone touching skin',
  vedicLearnMore: 'Learn more about Rubies and Vedic astrology',
  vedicNotSelected: 'Not Selected',
  vedicSelected: 'Gemstone Touching Skin',
  prongs: 'Prongs',
  size: '戒指尺寸',
  finish: '完成',
  ringWidth: '婚戒宽度 (mm)',
  sideStones: '选择宝石大小',
  sizingGuide: '尺寸参考',
  addEngraving: '添加雕刻',
  paveStone: '密镶钻石',
  totalPaveWeight: '总估计 {0} 重量: {1}',
  optional: '可选',
  characters: '{0}/15最多字符，刻在戒指内侧',
  engravingPlaceholder: '雕刻（可选）',
  productionTime: '生产时间',
  productionTimePeriod: ' {0} 到 {1} 天',
  totalMetalWeight: '金属总重量',
  grade: '等级',
  stoneWeight: '重量',
  weight: '单石重量',
  stoneTypeName: stoneTypes,
  paveStones: {
    sapphire: '蓝宝石',
    ruby: '红宝石',
    diamond: '钻石',
    emerald: '祖母绿宝石'
  },
  ringDetails: {
    ringSize: '选择戒指尺寸',
    chooseRingSize: '选择戒指尺寸',
    ringWidth: '选择戒指宽度',
    selectSize: '请选择一个戒指尺寸'
  },
  settingTypePicker: {
    title: '您想用红宝石创作什么首饰呢？',
    continue: '继续',
    ringsList: '戒指',
    earringsList: '耳环',
    pendantsList: '吊坠',
    necklacesList: '项链',
    necklacesPendantsList: 'Necklaces & Pendants'
  },
  share: {
    facebook: '通过Facebook分享',
    twitter: '通过Twitter分享',
    googlePlus: '通过Google plus分享',
    pinterest: '通过Pinterest分享',
    email: '使用邮件分享',
    copyLink: '复制链接',
    linkCopied: '链接已复制到粘贴板',
    linkCopyError: '出错了，请手动复制链接'
  },
  help: {
    name: '需要帮助？',
    call: '电话咨询',
    email: '邮件联系',
    chat: '在线沟通！'
  },
  product: '产品',
  description: '说明',
  details: '详情',
  overview: '概述',
  stoneDetails: '宝石详情',
  stoneShapesName: {
    ...stoneShapes
  },
  originName: {
    ...filtersOrigins
  },
  stonesOrigins,
  treatmentsName: {
    ...treatments
  },
  bandFinish,
  bandStyle,
  stoneColors,
  itemOverview: {
    itemId: '货品编号:',
    weight: '重量:',
    totalWeight: '总重量:',
    price: '价格:',
    totalPrice: '总价:',
    settingPrice: '镶嵌价格:',
    perCaratPrice: '每克拉价格:',
    metalType: '金属类型:',
    centerStones: '主石:',
    sideStones: '副石:',
    paveStones: '预估计密镶钻石总重量：'
  },
  stoneParams: {
    undefined: {
      tooltip: ''
    },
    perCaratPrice: {
      tooltip: '了解每克拉价格',
      title: '每克拉价格',
      text: `<p> "每克拉价格" 是一个珠宝贸易术语，解释了如何计算宝石价格。 优质宝石的定价是将它们的重量乘以“每克拉”价格（例如：2.50ct.x $ 1000 per / ct = $ 2500总成本）。</p>
      <p>每克拉价格越高，宝石中4C（切割，净度，克拉尺寸和颜色）的固有质量就越高。</p>
      <p>例如，净度非常差的1.00克拉红宝石，单价为10美元/克拉；而净度极佳的1.00克拉红宝石，单价为100美元/克拉，即使它们的大小相同。普通净度的1.00克拉红宝石，单价约为50美元/克拉。</p>`
    },
    color: {
      tooltip: '了解颜色',
      title: '颜色',
      text: '<p>所有物品都用灯光拍摄，旨在复制强烈的直射阳光。 这是评估所有宝石颜色的宝石学标准。 我们使用 5000-6000 开尔文 LED 光源来复制头顶直射阳光。</p>'
    },
    shape: {
      tooltip: '了解红宝石的形状',
      title: '形状',
      text1:
        '下图显示了常见的宝石形状。最受欢迎的彩色宝石形状有圆形，垫型，雷德恩型和公主方型。如果您想了解有关每种形状特征的更多信息，请致电我们与宝石专家交谈，他们将很乐意帮助您选择您的红宝石。',
      text2: `要查看每种形状的尺寸PDF，请单击 <a
      href="https://image.thenaturalsapphirecompany.com/site-files/nrc_stone_dimensions.pdf"
      target="_blank">这里</a>`
    },
    dimensions: {
      tooltip: '了解红宝石的尺寸'
    },
    clarity: {
      tooltip: '了解红宝石的净度',
      title: '净度',
      text: `<p></p><p><strong>肉眼可见的清晰</strong>：不仔细看的话，红宝石没有肉眼可见的杂质。</p>
      <p><strong>非常轻微杂质</strong>：肉眼观察，夹杂物会略微可见，但不会阻碍观察者从正面观看宝石的光彩。</p>
      <p><strong>轻微杂质</strong>：肉眼可以见轻微的杂质，略微影响宝石的色泽。</p>
      <p><strong>包含杂质</strong>: 有明显的杂质，严重或完全阻碍宝石内的反射光彩。</p>`
    },
    cut: {
      tooltip: '了解祖母绿的切面',
      title: '切面',
      text: '以下是用于评估和分类祖母绿宝石切割的常用术语和特征。'
    },
    colorIntensity: {
      tooltip: '了解关于红宝石的颜色饱和度',
      title: '颜色饱和度表',
      text: `<p>颜色饱和度:</p>
      <p><strong>正红：</strong> 在红宝石的颜色分类中，红宝石呈现纯红色，宝石反射出充分的光彩和光线。</p>
      <p><strong>鸽血红：</strong>是定义红宝石拥有最好的颜色的一种术语，是一种较深的血红色。</p>
      <p><strong>淡红:</strong> 比正红色稍微淡一些，这种颜色在业内被称为“开放色彩”。</p>
      <p><strong>粉红:</strong> 红宝石中带有桃红色或者紫红色的色调。</p>
      <p><strong>暗红:</strong> 暗饱和度的红宝石几乎没有光从宝石中逸出，颜色接近不透明。</p>`
    },
    origin: {
      tooltip: '了解关于红宝石的产地',
      title: '产地',
      text: `<p><strong>莫桑比克</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/mozambique-ruby-mines/">这里</a>.</p>
      <p><strong>缅甸</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/myanmar-ruby-mines/">这里</a>.</p>
      <p><strong>马达加斯加</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/madagascar-ruby-mines/">这里</a>.</p>
      <p><strong>泰国(暹罗)</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/thailand-ruby-mines/">这里</a>.</p>
      <p><strong>越南</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/vietnam-ruby-mines/">这里</a>.</p>
      <p><strong>坦桑尼亚</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/tanzania-ruby-mines/">这里</a>.</p>
      <p><strong>肯尼亚</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/kenya-ruby-mines/">这里</a>.</p>
      <p><strong>柬埔寨(佩林)</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/cambodia-ruby-mines/">这里</a>.</p>
      <p><strong>阿富汗</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/afghanistan-ruby-mines/">这里</a>.</p>
      <p><strong>巴基斯坦</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/pakistan-ruby-mines/">这里</a>.</p>
      <p><strong>斯里兰卡(锡兰)</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/sri-lanka-ruby-mines/">这里</a>.</p>
      <p><strong>马拉维</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/malawi-ruby-mines/">这里</a>.</p>
      <p><strong>老挝</strong> - 了解更多 <a class="has-border" target="_blank" href="/education/mining-locations-rubies/laos-ruby-mines/">这里</a>.</p>`
    },
    treatments: {
      tooltip: '了解红宝石的优化处理',
      title: '优化处理',
      text: `<p>红宝石的优化处理：</p>
      <p><strong>无优化:</strong> 红宝石没有任何的优化处理或热加工。 颜色和清晰度完全是由自然界通过自然地质过程创造的。</p>
      <p><strong>热处理：</strong> 热处理用于改善天然红宝石的颜色和清晰度，因为它们的红色纯度较差，或含有抑制光反射的杂质。热加工处理是不需要添加任何添加剂，将红宝石加热到1500-1700°C, 这种热暴露通常可以提高清晰度和颜色。</p>
      <p><strong>孔洞填充:</strong> 通过固定宝石内的裂缝和空腔来提高宝石的清晰度和耐用性。这些处理方法可以使低价值，劣质红宝石更具市场价值。如果一个红宝石的缝隙中填充有玻璃，应该会很容易的被发现，这些玻璃填充的红宝石会便宜得多。</p>
      <p><strong>扩散或玻璃填充:</strong> 扩散是一种增色处理，其中浅色的红宝石在加热过程中会暴露于元素中，从而使晶体颜色变红。这些是非常便宜的宝石，以这种方式处理时必须要公开。玻璃填充的红宝石也被认为是质量很差的宝石，如果以这种方式处理，也必须予以公开。</p>`
    }
  },
  stoneType: '宝石类型：',
  quantity: '数量:',
  color: '颜色：',
  clarity: '净度：',
  shape: '形状：',
  cut: '切工:',
  totalWeight: '总重量:',
  colorIntensity: '颜色饱和度:',
  detailsDimensions: '尺寸 (MM):',
  chainLength: '链长 (Inches):',
  braceletLength: '长度 (Inches):',
  origin: '产地：',
  treatment: '处理:',
  detailsItemId: '货品编号:',
  detailsWeight: '重量:',
  detailsColor: '颜色:',
  detailsClarity: '净度：',
  detailsShape: '形状：',
  detailsPerCaratPrice: '价格/每克拉：',
  detailsCut: '切工：',
  starAppearance: '星相:',
  detailsColorIntensity: '颜色饱和度：',
  detailsOrigin: '产地：',
  detailsTreatments: '处理：',
  length: '长度: {0}',
  width: '宽度: {0}',
  height: '高度: {0}',
  createJewelry: '使用宝石制作首饰',
  startFromStone: 'Create Jewelry With This Stone',
  startFromPair: 'Create Jewelry With These Stones',
  startFromSetting: 'Create Jewelry With This Setting',
  continueWithStone: '将宝石添加到镶嵌',
  continueWithPair: '将这对宝石添加到镶嵌',
  notCompatibleStone: '这个红宝石不匹配',
  notCompatiblePair: '这对红宝石不匹配',
  notCompatibleRingSetting: '这个戒指镶嵌不匹配',
  notCompatibleNecklaceSetting: '这个项链镶嵌不匹配',
  notCompatibleEarringSetting: '这个耳环镶嵌不匹配',
  notCompatiblePendantSetting: '这个吊坠镶嵌不匹配',
  takeMeBackStone: '返回到匹配的红宝石',
  takeMeBackPair: '返回到匹配的红宝石对',
  takeMeBackRingSetting: '返回到匹配的戒指镶嵌',
  takeMeBackNecklaceSetting: '返回到匹配的项链镶嵌',
  takeMeBackEarringSetting: '返回到匹配的耳环镶嵌',
  takeMeBackPendantSetting: '返回到匹配的吊坠镶嵌'
}
