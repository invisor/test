export default {
  designId: '设计编号',
  haveQuestion: '对此产品有疑问？',
  subTitle:
    '没问题，在这里我们会提供帮助！只要完成这个简短的表格，有人会尽快给你答复。如果你想立即与工作人员交谈，请拨打+1-212-221-6136',
  price: '价格：',
  totalPrice: '总价:',
  warningText1: '不用担心。',
  warningText2: '我们的客户服务代表不会收取费用。',
  warningText3: '我们不会出售或泄露客户的信息',
  submit: '提交问题',
  itemId: '货品编号:',
  weight: '重量：',
  origin: '产地：',
  color: '颜色：',
  shape: '形状：',
  metalType: '金属类型：',
  centerStone: '主宝石',
  centerStoneWeight: '主宝石重量：',
  centerStoneOrigin: '主宝石产地：',
  centerStoneShape: '主宝石形状：',
  metalWeight: '金属重量：',
  styleName: '样式名称：',
  stonesWeight: '宝石重量：',
  swal: {
    title: '您的请求已经被提交',
    text: '谢谢您的请求，我们会尽快与您联系',
    confirmButtonText: '好的'
  },
  form: {
    customerName: '姓名',
    phone: '电话',
    email: '邮箱',
    question: '您的问题'
  },
  formErrors: {
    customerName: '请输入姓名',
    phone: '请输入电话',
    email: {
      required: '请输入邮箱',
      valid: '请输入正确的邮箱地址'
    }
  }
}
