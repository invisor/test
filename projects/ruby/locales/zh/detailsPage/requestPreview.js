export default {
  title: '申请 3D渲染',
  submit: '提交申请',
  alreadySent: {
    text1: '我们的记录显示您已经提交过此定制珠宝的3D渲染请求。',
    text2:
      '如果您丢失了照片，或者忘记将照片放在和何处，或者觉得这个消息显示有误，请通过 <strong>+1-212-869-1165</strong>联系我们，我们将竭诚为您服务。',
    text3: '谢谢,<br>天然红宝石公司',
    limitText1:
      '您已经达到了此服务每天提供三次请求的限制，感谢您的理解，因为3D 渲染图像这项工作，是一项劳动密集型工作。',
    limitText2:
      '如果您认为此信息有误，或者您仍希望收到此珠宝创作的自定义 3D 渲染图像，请致电 <strong>+1-212-869-1165</strong> 与我们联系，我们将很乐意提供帮助。',
    limitText3: '谢谢,<br>天然红宝石公司'
  },
  swal: {
    title: '您的请求已经被提交',
    text: '请允许在24小时内，通过电子邮件收到您定制的珠宝创作的组合3D渲染',
    confirmButtonText: '好的'
  },
  form: {
    customerName: '姓名',
    phone: '电话',
    email: '邮箱',
    reEmail: 'Re-enter email address',
    forWhom: '为谁选购',
    instagramName: 'Instagram',
    country: '国家',
    occasion: '场合',
    timeframe: '配送时间',
    teamMember: '成员',
    deliveryDate: '配送日期',
    comments: '留言'
  },
  formErrors: {
    customerName: '请输入姓名',
    phone: '请输入电话',
    email: {
      required: '请输入邮箱',
      valid: '请输入正确的邮箱地址'
    },
    country: '请输入国家',
    occasion: '请输入使用场合',
    timeframe: '请输入配送时间',
    deliveryDate: '请输入配送日期'
  },
  placeholders: {
    country: '请选择',
    occasion: '请选择',
    timeframe: '请选择',
    deliveryDate: '请选择一个日期',
    comments: '请输入留言'
  },
  occasionsList: {
    engagement: '订婚',
    anniversary: '周年纪念日',
    giftForLovedOne: '给心爱人的礼物',
    birthday: '生日',
    graduation: '毕业',
    personalPurchase: '个人购买',
    vedicAstrology: '占星',
    weddingNeedRingBand: '婚礼需要/结婚戒指',
    other: '其他'
  },
  timeframesList: {
    asSoonAsPossible: '尽快配送',
    inTheNextFewWeeks: '接下来的几周之内',
    inTheNextFewMonths: '接下来的几个月之内',
    sometimeInTheNext6Months: '接下来的半年之内',
    '6Months': '半年以后',
    justBrowsing: '只是随便看看'
  },
  text1:
    '填写下面的表格，我们的CAD设计师团队将为您创造的珠宝饰品创建3D渲染。请耐心等待大约24小时，我们将通过电子邮件向您发送3D预览。',
  text2: '不用担心，我们不会出售或泄露客户的信息。',
  text3: '我们的销售团队不会收取费用',
  occasion: '您购买的货品是用于什么场合？',
  timeframe: '您希望什么时间段货品能够配达？',
  teamMember: '已经和我们的员工沟有所通过？',
  deliveryDate: '您希望在某个特定的日期送达货品吗？',
  deliveryDateNotice:
    '<strong>Note</strong>: We cannot guarantee delivery dates until the order is placed and the date is confirmed with our production team',
  prongs: 'Selected prong style',
  questions: '您还有任何问题和请求吗？',
  note1:
    'Note: Please limit your request for this complimentary service to three (3) total combinations.',
  note2: '感谢您的谅解，3D渲染是个相当耗费时力的工作。'
}
