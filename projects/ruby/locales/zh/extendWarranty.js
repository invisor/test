export default {
  term: '条款: {0}.',
  remove: '取消延保',
  years: '{0} Year Protection',
  lifetime: '终身延保'
}
