export default {
  askDesigner: {
    submit: '提交',
    label: {
      name: '姓名',
      email: '邮箱',
      phone: '电话',
      message: '您有什么问题需要解答呢？'
    },
    error: {
      required: '必填项',
      email: '邮箱不正确',
      reEmailRequired: 'Please enter the email again',
      reEmail: 'Email does not match the confirm email'
    }
  },
  askAbout: {
    submit: '提交',
    label: {
      name: '姓名',
      email: '邮箱',
      phone: '电话',
      message: '您的问题'
    },
    error: {
      required: '必填项',
      email: '邮箱不正确'
    }
  }
}
