export default {
  ourCollection1: '我们的红宝石 ',
  ourCollection2: ' 系列',
  bracelets: {
    title: '手链',
    text: '我们提供各种带有钻石的经典红宝石手链，可以成为古董复制品。',
    button: '现在购买'
  },

  earrings: {
    title: '耳环',
    text: '日常佩戴或用于特殊活动的红宝石耳环，可以查看我们的惊艳系列。',
    button: '现在购买'
  },

  necklaces: {
    title: '项链 & 吊坠',
    text:
      '我们的红宝石项链和红宝石吊坠有各种形状，大小和颜色，可以查找我们的系列或自己设计制作。',
    button: '现在购买'
  },

  pairs: {
    title: '红宝石对',
    text:
      '我们的宝石专家精心挑选了成对的宝石，因此您可以轻松挑选最适合您珠宝的宝石。',
    button: '现在购买'
  },

  rings: {
    title: '戒指',
    text:
      '无论您是寻找经典的独粒宝石戒指，现代的表圈镶托还是红宝石和钻石光环设计，您都可以在我们的系列中找到完美的红宝石订婚戒指。',
    button: '现在购买'
  },

  settings: {
    title: '镶嵌',
    text:
      '有兴趣创建一些特别的东西吗？ 您会在这里找到定制红宝石珠宝的完美镶嵌。',
    button: '现在购买'
  },

  stones: {
    title: '红宝石裸石',
    text:
      '我们可以很自豪地向您展示世界一流的红宝石，它们来自世界各地，各种著名和具有异国风情的地方。',
    button: '现在购买'
  },

  weddingBands: {
    title: '订婚戒指',
    text: '完美的手工制作的红宝石婚戒是我们的特色之一！',
    button: '现在购买'
  }
}
