export default {
  title1: '著名的 ',
  title2: ' 红宝石',
  slide1: {
    title: {
      row1: '红宝石在',
      row2: '流行文化'
    },
    text: '也许最著名的红宝石的出现是拖鞋！',
    button: '阅读更多'
  },
  slide2: {
    title: {
      row1: '日出',
      row2: '红宝石'
    },
    text:
      '世界上最昂贵的红宝石-实际上，最昂贵的宝石而不是钻石-日出红宝石，价值3030万美元',
    button: '阅读更多'
  },
  slide3: {
    title: {
      row1: '伊丽莎白',
      row2: '泰勒'
    },
    text:
      '好莱坞没有其他人像伊丽莎白·泰勒（Elizabeth Taylor）一样是精致珠宝的代名词',
    button: '阅读更多'
  }
}
