export default {
  slide1: {
    subTitle: '我们的包装',
    title: '细节之美',
    text:
      '我们的包装、报告和资料都是精美、简洁、深受好评的。我们的信誉是我们最大的财富，请您放心，我们不会偷工减料，请查看我们所有红宝石产品的详尽报告和特殊包装。 ',
    tab1name: '包装外盒',
    tab1text:
      '我们的手工制作的锌合金戒指盒，它的制作效果就像手工制作的红宝石戒指和红宝石珠宝一样，给顾客留下非常深刻的印象。 每个戒指盒的重量将近一磅！',
    tab2name: '我们的质量承诺',
    tab2text:
      '如果您选择天然红宝石公司给您自己或家人购买物品，我们会非常有信心，这次购物会给您留下美好的印象。'
  },
  slide2: {
    subTitle: '我们的包装',
    title: '细节之美',
    text:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur consectetur lacus nec mauris pellentesque, non varius ex mattis. Donec mauris ipsum.',
    tab1name: '包装外盒',
    tab1text:
      'Praesent vitae erat in ligula suscipit condimentum. Nullam suscipit ipsum sed nibh ultrices lobortis.',
    tab2name: '我们的质量承诺',
    tab2text:
      'In ac lacinia urna. Cras blandit, ante ac feugiat dictum, nunc velit pulvinar ipsum, id tristique tortor lorem non sem.'
  }
}
