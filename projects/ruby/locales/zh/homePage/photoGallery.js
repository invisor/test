export default {
  subTitle: '奉献',
  title: '在 Instagram上访问我们',
  text:
    '我们的Instagram是红宝石爱好者的梦想，在那里我们展示了有史以来所有的红宝石珠宝，以及还没有出售的裸红宝石！',
  button: '关注我们Instagram'
}
