export default {
  heroRing: '3.26克拉的鸽血红红宝石和钻石戒指，获得了GIA认证',
  collectionRings: '镶嵌有铂金密镶的红宝石订婚戒指',
  collectionEarrings: '铂金光环镶托设计中，镶有钻石的红宝石耳环',
  collectionWeddingBands: '红宝石和钻石结婚戒指镶入白金和铂金中',
  collectionNecklaces: '8.88ct铂金镶嵌缅甸红宝石，密钉镶钻石',
  collectionBracelets: '莫桑比克红宝石手链，配隐形18k白色黄金镶托钻石',
  collectionStones: '来自缅甸和莫桑比克的红宝石',
  collectionPairs: '一对未经处理和认证的缅甸红宝石',
  collectionSettings: '定制的红宝石戒指镶嵌',
  educationVideo: '了解有关缅甸和莫桑比克红宝石的更多信息',
  ourStory: '天然红宝石公司的历史',
  famousRubies: '著名的红宝石',
  famousQuality: '优质红宝石',
  famousElizabeth: '伊丽莎白·泰勒红宝石珠宝',
  packaging: '天然红宝石公司定制包装中的红宝石戒指'
}
