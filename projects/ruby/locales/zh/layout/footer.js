export default {
  subscription: {
    title: '及时了解我们的更新',
    text: '订阅我们的时事邮件，了解更新和特别的优惠',
    placeholder: '邮箱地址',
    button: '登录'
  },
  bottom: {
    terms: '条款 & 条件',
    privacy: '隐私政策'
  },
  menu: {
    aboutUs: {
      title: '关于我们',
      item1: '为什么选择我们？',
      item2: '慈善基金会',
      item3: '团队',
      item4: '博客',
      item5: '陈列室',
      item6: '技术',
      item7: '工作室',
      item8: '实验室',
      item9: '新闻'
    },
    customerCare: {
      title: '常见问题',
      item1: '联系我们',
      item2: '常见问题',
      item3: '退回政策',
      item4: '我们的地址',
      item5: '等级,尺寸&更多',
      item6: '支付政策',
      item7: '运输',
      item8: '安全和防诈骗政策',
      item9: '隐私政策'
    },
    education: {
      title: '相关知识',
      item1: '红宝石',
      item2: '红宝石质量的4C',
      item3: '红宝石稀有度和持久价值',
      item4: '红宝石的处理和增强',
      item5: '著名的红宝石',
      item6: '红宝石购买提示',
      item7: '红宝石的保养'
    }
  }
}
