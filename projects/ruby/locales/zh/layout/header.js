export default {
  noResults: '未找到任何商品',
  searchById: '按货品编号进行搜索#',
  meta: {
    name: '天然红宝石公司',
    description: '天然红宝石公司专门提供最好的天然红宝石。'
  },
  language: '语言',
  currency: '货币',
  contacts: {
    availableHours: '预约时间',
    days: '周一 – 周五',
    showroom: {
      text: '如需独特的观赏体验，请前往纽约曼哈顿的私人顶层展厅。',
      button: '预约'
    },
    locations: {
      title1: '陈列室 & 工作室',
      text1: '6 East 45th St, 20th Floor New York, NY 10017 United States',
      title2: 'CAD & 产品部门',
      text2: '73/1/3 斯里兰卡拉特纳普勒Demuwawatha市政厅'
    },
    phone: {
      usa: '美国',
      usaPhone: '+1-212-221-6136',
      line1: '对于国际客户，请通过电子邮件与我们联系。',
      line2: '我们将会在正常工作时间与您联系。'
    },
    chat: '在线沟通',
    email: {
      title: '我们在这里为您提供帮助！',
      text: '我们邀请您，可以随时通过我们的网站，向我们发送消息。',
      button: '咨询我们'
    }
  },
  searchLine: '通过货品名称或编号进行搜索#',
  firstLine: {
    left: {
      text1: '全球免费送货',
      text2: '14天无忧退货',
      tooltipText1:
        "我们非常确定您会喜欢您收到的商品，我们为在美国任何地方运送的所有物品提供运费和保险，以及运送到世界任何地方的<strong><u>所有超过300美元的物品</u></strong>！",
      tooltipTitle1:
        '全球免费送货政策<br/> <small><i>(特殊情况除外)</i></small>',
      tooltipText2:
        '我们为每个裸石、成品珠宝以及<strong><u>大多数</u></strong>定制珠宝和戒指提供14天退货期。如果您选择退货，我们将全额退还您支付的费用，特殊情况除外。',
      tooltipTitle2:
        '14天无忧退货政策<br/> <small><i>(特殊情况除外)</i></small>'
    },
    middle: {
      noRisk: {
        DE: 'Keine Kosten, kein Risiko, KOSTENLOSER Versand nach Deutschland',
        AU: 'G’day Australian visitors! Free Shipping​, Free Returns! No worries!​',
        BR: 'Sem custo, Sem Risco, Frete grátis para/de​ Brazil​!',
        CA: 'No Cost, No Risk, Free Shipping to​ USA + Canada​!',
        CN: '沒有任何费用,沒有风险,免费送货​至/从中国！',
        FI: 'Maksuton, Riskiä ei ole, Ilmainen toimitus että​/​alkaen​ Finland​!',
        FR: '​Aucun coût, Aucun risque, Livraison gratuite vers/à partir de​ France!',
        IT: 'Gratuitamente e senza alcun rischio, La spedizione gratuita da/per​ Italia​!',
        JP: '無料、無リスク、日本からの送料無料！',
        MY: 'Tiada kos, ​T​iada risiko, ​P​enghantaran percuma ke/dari​ Malaysia​!',
        NL: 'Geen kosten, ​Geen risico, ​G​ratis verzending naar / van​ Netherlands​!',
        NZ: 'No Cost, No Risk, Free Shipping to​ USA +​​ New Zealand​!',
        NO: 'ngen pris, Ingen risiko, Gratis frakt til / fra​ Norway​!',
        SG: 'No Cost, No Risk, Free Shipping to​ Singapore​!',
        ZA: '​No Cost, No Risk, Free Shipping to​ South Africa​​!',
        ES: 'Sin Costo, Sin Riesgo, Envío gratuito desde / hasta​ Spain​​!',
        CH: 'Keine Kosten, kein Risiko, Kostenloser Versand an/aus​ Switzerland​​​​​!',
        GB: 'No Cost, No Risk, Free Shipping to​ the UK​!',
        US: 'No Cost, No Risk, FREE Shipping!',
        KH: 'គ្មានតម្លៃគ្មានហានិភ័យគ្មានការដឹកជញ្ជូនដោយឥតគិតថ្លៃទៅកាន់ / មកពីប្រទេសកម្ពុជា!!',
        GR: 'Δεν υπάρχει κόστος, δεν υπάρχει κίνδυνος, δωρεάν αποστολή από / προς την Ελλάδα !',
        HK: '沒有任何費用,沒有風險,免費送貨​ ​至/從香港！',
        IE: 'No Cost, No Risk, Free Shipping to Ireland!',
        ID: 'Tanpa Biaya, Tanpa Resiko, Gratis pengiriman ke / dari Indonesia!',
        QA: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى قطر',
        RU: 'Бесплатная доставка в Российскую Федерацию. Без риска',
        SE: 'Ingen kostnad, Ingen risk, Gratis frakt till / från​ Sweden​!',
        TR: "Maliyet Yok, Risk Yok, Türkiye'ye Ücretsiz Kargo",
        AE: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى الإمارات العربية المتحدة',
        PL: 'Bez kosztów, bez ryzyka, bezpłatna wysyłka do / z Polski !',
        PT: 'Não, não há nenhum risco de ​ Envio gratuito de/para o Portugal!',
        HU: 'Nincs költség, nincs kockázat, Ingyenes szállítás a / a hundary!',
        IL: 'ללא עלות, ללא סיכון, משלוח חינם מ / ל Isarel',
        KR: '비용 없음, 위험 없음, 무료 배송 대한민국!',
        KW: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني من وإلى الكويت',
        OM: 'بدون تكلفة ، بدون مخاطرة ، شحن مجاني إلى / من عمان',
        PH: 'No Cost, No Risk, Free Shipping Philippines!',
        MX: 'Sin costo, sin riesgos, envío gratuito a / desde México!',
        SA: 'لا تكلفة ، لا خطر ، الشحن المجاني إلى / من المملكة العربية السعودية!',
        AT: 'No Cost, No Risk, Free Shipping Austria!',
        EG: 'لا تكلفة ، لا خطر ، الشحن المجاني من وإلى مصر!',
        TH: 'ไม่มีความเสี่ยงไม่มีความเสี่ยงจัดส่งฟรีถึง / จากประเทศไทย!',
        VN: 'Không có chi phí, không có rủi ro, miễn phí vận chuyển đến / từ Việt Nam !',
        CY: 'No Cost, No Risk, Free Shipping Cyprus!',
        AR: 'Sin costo, sin riesgos, envío gratuito a / desde Argentina !',
        BN: 'Tiada kos, tiada risiko, penghantaran percuma ke / dari Brunei!',
        LU: 'Keen Präis, kee Risiko, gratis Liwwerung fir / aus Lëtzebuerg!',
        IS: 'Engin kostnaður, engin hætta, ókeypis sendingar til / frá Íslandi!',
        BH: 'لا تكلفة ، لا خطر ، الشحن المجاني إلى / من Braharin!',
        BE: 'No cost, no risk, free shipping to / from Belgium!',
        CL: 'Sin costo, sin riesgo, envío gratis a / desde Chile!',
        UY: '¡Sin costo, sin riesgo, envío gratis a / desde Uruguay!',
        HR: 'Bez troškova, bez rizika, besplatne dostave do / od Hrvatske!',
        PE: 'No cost, no risk, free shipping to / from Peru !',
        NG: 'No cost, no risk, free shipping to / from Nigeria !',
        LA: 'ບໍ່ມີຄ່າໃຊ້ຈ່າຍ, ບໍ່ມີຄວາມສ່ຽງ, ການຂົນສົ່ງຟຣີໄປ / ຈາກລາວ!'
      }
    }
  },
  secondLine: {
    favorites: '收藏夹',
    compare: '对比',
    cart: '购物车',
    account: '账号'
  }
}
