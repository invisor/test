export default {
  us: 'USA: +1-212-221-6136',
  other: {
    line1: '对于国际客户，请通过电子邮件与我们联系。',
    line2: '我们将会在正常工作时间与您联系。'
  }
}
