export default {
  topImage: {
    subTitle: '使用我们的免费设计服务来',
    title: '制作独一无二的珠宝',
    message:
      '数以千计的精美天然红宝石可供选择，并通过我们精美、定制设计、手工制作的镶嵌与您的独特规格相匹配。',
    button: '开始'
  },
  firstBlock: {
    title: '您的定制珠宝从{link}开始',
    settings: '镶嵌',
    message:
      '首先，让我们了解一下您想要制作什么类型的珠宝，从数种不同金属制成的数千种独特的手工制作镶嵌中进行选择。'
  },
  secondBlock: {
    title: '接下来，从我们的100％纯天然中选择{link}',
    stones: '红宝石',
    message:
      '从我们收集的来自莫桑比克，缅甸，马达加斯加以及地球上更多神秘地区的优质红宝石中进行选择。'
  },
  thirdBlock: {
    title: '预览您的作品并下订单',
    message:
      '完成完美珠宝的制作后，您可以在下订单之前请求免费预览,我们的团队将与您一对一地合作，以确保您的创作完美地实现。'
  },
  fourthBlock: {
    title: '完美的礼物，拥有精致的包装，',
    message:
      '每件商品均与精美的定制珠宝盒一起运输，并附有证明和报告，以证明其质量和真实性。'
  },
  fifthBlock: {
    title: '准备定制设计精美红宝石首饰？',
    buttonStones: '选择一个红宝石',
    buttonPairs: '选择一对红宝石'
  }
}
