import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound:
      '没有找到耳环镶嵌 | 找到1款耳环镶嵌 | {count} 款耳环镶嵌被找到了'
  },
  prefilteredItems: '<strong>仅</strong> 显示匹配您选择的红宝石对的耳环镶嵌。',
  notPrefilteredItems:
    '显示<strong>全部</strong>的耳环镶嵌，意味着有些镶嵌可能和您选择的红宝石对不匹配。',
  showAllItems: '请显示全部的耳环镶嵌。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的耳环镶嵌。'
}
