import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound:
      'No necklace or pendant settings found | One necklace or pendant setting found | {count} necklace and pendant settings found'
  },
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的红宝石的项链镶嵌。',
  notPrefilteredItems:
    '显示<strong>全部</strong>的项链镶嵌，意味着有些镶嵌可能和您选择的红宝石不匹配。',
  showAllItems: '请显示全部的项链镶嵌。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的项链镶嵌'
}
