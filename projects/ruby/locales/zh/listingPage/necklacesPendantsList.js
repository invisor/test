import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound:
      '没有找到项链或吊坠 | 找到1款项链或吊坠 | {count} 款项链或吊坠被找到了'
  }
}
