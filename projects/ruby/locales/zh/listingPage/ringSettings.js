import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    weight: {
      lowToHigh: '重量 (由低到高)',
      highToLow: '重量 (由高到低)'
    },
    itemsFound:
      '没有找到戒指镶嵌 | 找到1款戒指镶嵌 | {count} 款戒指镶嵌被找到了'
  },
  ringSizePlaceholder: '选择戒指的尺寸',
  ringSizeError: '请选择一个戒指尺寸',
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的红宝石的戒指镶嵌。',
  notPrefilteredItems:
    '显示<strong>全部</strong>的戒指镶嵌，意味着有些镶嵌可能和您选择的红宝石不匹配。',
  showAllItems: '请显示全部的戒指镶嵌。 ',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的戒指镶嵌'
}
