import stonesList from './stonesList'
export default {
  ...stonesList,
  sort: {
    ...stonesList.sort,
    itemsFound:
      '没有找到红宝石对 | 找到1款红宝石对 | {count} 款红宝石对被找到了',
    itemsStarFound:
      '没有找到星光红宝石对 | 找到1款星光红宝石对 | {count} 款星光红宝石对被找到了',
    itemsCabochonFound:
      '没有找到天然圆形红宝石对 | 找到1款天然圆形红宝石对 | {count} 款天然圆形红宝石对被找到了'
  },
  pair: '对',
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的镶嵌的红宝石对。',
  notPrefilteredItems:
    '显示 <strong>全部</strong> 红宝石对，意味着有些红宝石对可能和您选择的镶嵌不匹配。',
  showAllItems: '请显示全部的红宝石对。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的红宝石对'
}
