import stonesOrigins from '../constants/stonesOrigins'
import metalTypes from '../constants/metalTypes'
import treatments from '../constants/treatments'
import stoneCut from '../constants/stoneCut'
import stoneClarity from '../constants/stoneClarity'
import stoneColorIntensity from '../constants/stoneColorIntensity'
import bandStyle from '../constants/bandStyle'
import stoneShapes from '../constants/stoneShapes'

export default {
  sort: {
    sortBy: '排序方式',
    itemsFound: '没有找到红宝石 | 找到1款红宝石 | {count} 款红宝石被找到了',
    itemsStarFound:
      '没有找到星光红宝石 | 找到1款星光红宝石 | {count} 款星光红宝石被找到了',
    itemsCabochonFound:
      '没有找到天然圆形红宝石 | 找到1款天然圆形红宝石 | {count} 款天然圆形红宝石被找到了',
    other: {
      featuredFirst: '精选优先',
      newFirst: '最新优先'
    },
    price: {
      lowToHigh: '价格 (由低到高)',
      highToLow: '价格 (由高到低)'
    },
    carat: {
      lowToHigh: '克拉 (由低到高)',
      highToLow: '克拉 (由高到低)'
    }
  },
  localSort: {
    sortBy: '排序方式',
    placeholder: '选择',
    itemsFound: '款红宝石对被找到了',
    price: {
      lowToHigh: '价格 (由低到高)',
      highToLow: '价格 (由高到低)'
    },
    carat: {
      lowToHigh: '克拉 (由低到高)',
      highToLow: '克拉 (由高到低)'
    }
  },
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的镶嵌的红宝石。',
  notPrefilteredItems:
    '显示 <strong>全部</strong>红宝石，意味着有些红宝石可能和您选择的镶嵌不匹配。',
  showAllItems: '请显示全部的红宝石。',
  hideNonCompatibleItems: '不用担心，我们将返回到匹配的红宝石',
  stoneShapes,
  stonesOrigins,
  metalTypes,
  treatments,
  stoneCut,
  stoneClarity,
  stoneColorIntensity,
  bandStyle
}
