export default {
  price: '价格：',
  itemId: '货品编号',
  cut: '切工：',
  origin: '产地：',
  carat: '克拉：',
  treatment: '处理：',
  metal: '金属类型：'
}
