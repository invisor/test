import stonesList from './stonesList'
import ringSettings from './ringSettings'
import earringSettings from './earringSettings'
import pendantSettings from './pendantSettings'
import necklaceSettings from './necklaceSettings'
import braceletsList from './braceletsList'

export default {
  stonesList,
  stonePairsList: stonesList,
  ringSettings,
  earringSettings,
  pendantSettings,
  necklaceSettings,
  braceletsList,
  earringsList: braceletsList,
  necklacesPendantsList: braceletsList,
  ringsList: braceletsList
}
