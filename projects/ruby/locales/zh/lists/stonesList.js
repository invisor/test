export default {
  hero: {
    title: '查找红宝石',
    message: '显示在{priceFilter}这个价格范围的{stoneShapesFilter}红宝石',
    andOther: '和其他',
    placeholder: '全部'
  },
  table: {
    actualPhoto: '真实照片',
    shape: '形状',
    colorIntensity: '颜色饱和度',
    clarity: '净度',
    carat: '克拉',
    origin: '产地',
    cut: '切工',
    price: '价格',
    compare: '对比列表',
    wishlist: '收藏夹',
    details: '详情'
  },
  sort: {
    sortBy: '排序方式',
    itemsFound: '款红宝石被找到了',
    other: {
      featuredFirst: '精选优先',
      newFirst: '最新优先'
    },
    price: {
      lowToHigh: '价格（从低到高）',
      highToLow: '价格（从高到低）'
    },
    carat: {
      lowToHigh: '重量（从低到高）',
      highToLow: '重量（从高到低）'
    }
  },
  localSort: {
    sortBy: '排序方式',
    placeholder: '选择',
    itemsFound: '款宝石对被找到了',
    price: {
      lowToHigh: '价格（从低到高）',
      highToLow: '价格（从高到低）'
    },
    carat: {
      lowToHigh: '重量（从低到高）',
      highToLow: '重量（从高到低）'
    }
  },
  lists: {
    result: '结果',
    compare: '对比列表',
    wishlist: '收藏夹'
  },
  carat: '克拉：',
  changeView: '切换视图',
  hideNonCompatibleItems: '没关系，返回到匹配的红宝石',
  itemId: '货品编号：',
  intensity: '颜色饱和度：',
  clarity: '净度：',
  cut: '切工：',
  origin: '产地：',
  perCaratPrice: '每克拉价格：',
  dimensions: '尺寸：',
  notPrefilteredItems:
    '显示<strong>所有</strong> 红宝石，意味着有些红宝石可能和您所选的镶嵌无法匹配',
  prefilteredItems: '<strong>仅</strong>显示匹配您选择的镶嵌的红宝石。',
  price: '价格：',
  quickView: '快速浏览',
  readMore: '查看更多',
  readMoreTitle: '您已查看{1}个产品中的{0}个',
  showAllItems: '不用了，给我看看所有的红宝石。',
  weight: '{0}克拉'
}
