import { companyName } from '~~/utils/definitions/defaults.js'

export default {
  index: {
    seo: {
      title: `${
        companyName[process.env.siteName]
      } - Ruby Rings & Jewelry Since 1939`,
      description:
        '自 1939 年以来,我们就是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '首页'
  },
  'custom-design-ruby-rings-jewelry': {
    seo: {
      title: '在线设计定制红宝石订婚戒指和珠宝',
      description:
        '寻找特别或独特的东西？ 让我们帮助您或亲人打造完美的定制红宝石订婚戒指、耳环、项链或吊坠。'
    },
    navPageTitle: '了解更多'
  },
  'design-your-own-setting-mens-ring-settings': {
    seo: {
      title: "Custom Ruby Men's Ring Settings",
      description:
        "With dozens of bold ring settings to choose from - specifically designed for the modern man - we'e confident you'll find the perfect men's ruby ring setting for any occasion."
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-engagement-ring-settings': {
    seo: {
      title: 'Custom Ruby Engagement Ring Settings',
      description:
        'Custom made ruby engagement ring settings. We have hundreds of design styles from three stone ring, pave diamonds, and solitaire to name a few. Our ruby ring settings can be created in white and yellow gold, platinum, and palladium.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-earring-settings': {
    seo: {
      title: 'Custom Ruby Earring Settings',
      description:
        'From simple to elaborate earring settings, we offer it all! Choose among a wide variety of earring designs to fit your ruby pair.'
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-necklace-pendant-settings': {
    seo: {
      title: 'Custom Ruby Necklace & Pendant Settings',
      description:
        "Choose one of our ruby necklace or pendant settings to accentuate any ensemble. With our simple or elaborate designs, you'll find everything you're looking for!"
    },
    navPageTitle: '{fullType}',
    navPageDescr:
      'We have an expansive catalogue of setting designs for ruby rings, necklaces, pendants, and earrings. Custom order is our speciality!'
  },
  'design-your-own-setting-ruby-earring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-ruby-necklace-pendant-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-ruby-engagement-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'design-your-own-setting-mens-ring-settings-id': {
    seo: {
      title: '{metal} {style} {type} Setting #{id}{metalTypeCode}',
      description:
        'Create your perfect custom ruby {type} with this beautifully crafted, handmade {metal} Pave {type} Setting #{id}{metalTypeCode}.'
    },
    navPageTitle: '{metal} {style} {type} Setting'
  },
  'ruby-jewelry-id': {
    seo: {
      title: '红宝石{type} - {shape} {carat} 克拉 - {metal} #{id}',
      description:
        '这美丽的 {carat} 克拉、{shape} 红宝石 {type} (#{id}) 镶嵌在 {metal} 中，是您生命中特别的人的完美红宝石首饰。'
    },
    navPageTitle: '红宝石 {type} {metal}'
  },
  rubies: {
    seo: {
      title: '红宝石裸石',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '红宝石裸石',
    navPageDescr:
      '缅甸红宝石是最著名的，但莫桑比克、斯里兰卡甚至马达加斯加等其他红宝石产地也能够生产出超优质的红宝石。请浏览我们来自世界各地的红宝石。'
  },
  'rubies-burmese-rubies': {
    seo: {
      title: '缅甸红宝石',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '缅甸红宝石',
    navPageDescr:
      '从远古的人类历史记载标明：缅甸红宝石是地球上已知最古老的优质红宝石来源，是传说中的宝石。'
  },
  'rubies-mozambique-rubies': {
    seo: {
      title: '莫桑比克红宝石',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '莫桑比克红宝石',
    navPageDescr:
      '莫桑比克红宝石是现代最伟大的宝石学发现。 地球上没有任何地方能够提供如此稳定的质量和数量的宝石级红宝石。 与所有其他红宝石来源的光和颜色反射相媲美，莫桑比克红宝石被认为是所有红宝石的未来。 在下方查看我们世界级的莫桑比克红宝石系列。'
  },
  'ruby-jewelry-burmese-ruby-rings': {
    seo: {
      title: '莫桑比克红宝石戒指',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '缅甸红宝石戒指',
    navPageDescr:
      '缅甸红宝石是地球上已知最古老的优质红宝石来源。从远古的人类历史记载标明，来自缅甸的红宝石具有传奇色彩。请浏览我们来自世界各地的红宝石'
  },
  'ruby-jewelry-mozambique-ruby-rings': {
    seo: {
      title: '莫桑比克红宝石戒指',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '莫桑比克红宝石戒指',
    navPageDescr:
      '莫桑比克红宝石是现代最伟大的宝石学发现，与所有其他红宝石来源的光和颜色反射相媲美，莫桑比克红宝石是所有红宝石的未来。'
  },
  'rubies-star-rubies': {
    seo: {
      title: '星光红宝石',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '星光红宝石',
    navPageDescr:
      '星光红宝石是地球上最稀有的宝石之一。 抛光的星光红宝石戒指中可见的星星通常证明红宝石未经加热且处于自然状态。 在<a href="/education/all-about-star-rubies/">这里</a>了解更多关于星光红宝石的信息.'
  },
  'rubies-ruby-cabochon': {
    seo: {
      title: '天然圆形红宝石',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '天然圆形红宝石对',
    navPageDescr:
      '凸圆形红宝石是自古以来使用的最古老、最令人垂涎的宝石之一。 从埃及人到国王和王后的皇家珠宝，这种简单的凸圆形红宝石切割至今仍是稀有红宝石宝石的心爱切割。 <a href="/education/all-about-cabochon-rubies/">这里</a>了解更多信息。'
  },
  'rubies-gemologist-recommended': {
    seo: {
      title: '宝石专家推荐',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '宝石专家推荐',
    navPageDescr:
      '我们的宝石专家每年都会查看数千颗红宝石，他们对宝石的信息非常挑剔。在这部分，我们的宝石专家会在清单中推荐一些他们个人的最爱'
  },
  'rubies-id': {
    seo: {
      title: '红宝石 - {shape} {carat} Ct. - #{id}',
      description:
        '选择这个天然的{carat}克拉、来自{origin} (#{id})的 {shape}红宝石，为您或您所爱的人打造完美的珠宝。'
    },
    navPageTitle: '红宝石'
  },
  'rubies-matched-pairs-of-rubies': {
    seo: {
      title: '红宝石对',
      description:
        '我们提供种类繁多的美丽天然红宝石,在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '红宝石对',
    navPageDescr:
      '制作红宝石对可能具有一定的挑战性，不仅是红色的色调、色调和饱和度，还有切工、克拉重量，甚至净度。我们努力制造出能够很好地相互补充的优质红宝石对。'
  },
  'rubies-star-ruby-pairs': {
    seo: {
      title: '星光红宝石对',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '星光红宝石对',
    navPageDescr:
      '星光红宝石是地球上最稀有的宝石之一,如果有一对配对,简直是惊人的罕见。了解更多有关星光红宝石,请点击<a href="/education/all-about-star-rubies/">这里</a>.'
  },
  'rubies-matched-pairs-of-rubies-cabochon': {
    seo: {
      title: '天然圆形红宝石对',
      description:
        '我们提供种类繁多的美丽天然红宝石，在不同的色调和色调以及所有类型的形状之间，您一定会找到完美的红宝石。'
    },
    navPageTitle: '天然圆形红宝石对',
    navPageDescr:
      '成对的凸圆形红宝石不寻常且难以匹配,以下是一系列选项,展示了天然红宝石中这种美丽的切工。了解更多关于凸圆形红宝石的信息,请点击<a href="/education/all-about-cabochon-rubies/">这里</a>.'
  },
  'rubies-matched-pairs-of-rubies-id': {
    seo: {
      title: '红宝石对 - {shape} {carat} Ct. - #{id}',
      description:
        '选择这颗天然{carat} 克拉、 来自{origin} (#{id})的{shape}红宝石对，为您或您所爱的人打造完美的珠宝。'
    },
    navPageTitle: '红宝石对'
  },
  'ruby-jewelry-ruby-engagement-rings': {
    seo: {
      title: '红宝石订婚戒指',
      description:
        '为您所爱的人寻找完美的订婚戒指。 我们提供精心制作的手工红宝石订婚戒指，或设计您自己独特的红宝石戒指！'
    },
    navPageTitle: '红宝石订婚戒指',
    navPageDescr:
      '纵观历史，红宝石中丰富的、发光的红色一直吸引着国王和王后。我们提供来自缅甸、莫桑比克、斯里兰卡、暹罗和马达加斯加的上等红宝石。红宝石订婚戒指是我们的专长。'
  },
  'ruby-jewelry-ruby-rings': {
    seo: {
      title: '红宝石戒指',
      description:
        '为您所爱的人寻找完美的红宝石戒指。 我们提供专业制作的手工红宝石戒指，或设计您自己独特的红宝石戒指！'
    },
    navPageTitle: '红宝石戒指',
    navPageDescr:
      '我们提供来自莫桑比克、缅甸、斯里兰卡和马达加斯加的优质天然红宝石。 随着这些来源的出现，一系列红色带有修饰色调的粉红色、紫色、橙色或纯鸽血红。'
  },
  'ruby-jewelry-star-ruby-rings': {
    seo: {
      title: '星光红宝石戒指',
      description:
        '为您所爱的人寻找完美的红宝石戒指。 我们提供专业制作的手工红宝石戒指，或设计您自己独特的红宝石戒指！'
    },
    navPageTitle: '星光红宝石戒指',
    navPageDescr:
      '星光红宝石是地球上最稀有的宝石之一。 抛光的星光红宝石戒指中可见的星星通常证明红宝石未经加热且处于自然状态。 缅甸星光红宝石是世界上最昂贵的宝石，已知很少有优质宝石存在。我们也有来自越南和斯里兰卡的星光红宝石。 在<a href="/education/all-about-star-rubies/">这里</a>了解更多关于星光红宝石的信息.'
  },
  'ruby-wedding-rings-bands': {
    seo: {
      title: '红宝石婚戒',
      description:
        '探索我们专业设计和手工制作的红宝石和钻石结婚戒指，以及镶嵌所有贵金属类型的红宝石和钻石的可叠放结婚戒指。'
    },
    navPageTitle: '红宝石婚戒',
    navPageDescr:
      '无论是缅甸红宝石或者是莫桑比克的红宝石，我们都提供定制设计的红宝石婚戒，款式包括：永恒、无限、包边和简单的爪镶。我们还可以制作定制的红宝石婚戒和戒指。'
  },
  'ruby-wedding-rings-bands-id': {
    seo: {
      title: '{metal} 红宝石婚戒 #{id}{metalTypeCode}',
      description:
        '将这款精美的 {metal} 红宝石戒指 (#{id}{metalTypeCode}) 制作成完美的结婚戒指，制作成您独一无二的红宝石订婚戒指。'
    },
    navPageTitle: '{metal} 红宝石婚戒'
  },
  'wedding-bands-without-gemstone': {
    seo: {
      title: '素圈婚戒',
      description:
        '素圈的结婚戒指和带宝石的戒指都是经典设计，但又不简单，从纹理金属或双色调金属，到抛光或哑光设计选项，我们应有尽有！'
    },
    navPageTitle: '素圈婚戒',
    navPageDescr:
      '没有看到您要查找的内容？ 联系我们的工作室，了解定制钻石和红宝石婚戒的详细信息，以证明您的爱。'
  },
  'wedding-bands-without-gemstone-id': {
    seo: {
      title: '{metal} 素圈戒指 #{id}{metalTypeCode}',
      description:
        '将这款精美的 {metal}素圈戒指(#{id}{metalTypeCode}) 制成完美的结婚戒指，以匹配您独一无二的红宝石订婚戒指。'
    },
    navPageTitle: '{metal} 素圈戒指'
  },
  'design-your-own': {
    seo: {
      title: '在我们简单的三步过程中创建您的定制红宝石戒指',
      description:
        '开始在我们简单的三步过程中创建您的定制红宝石戒指,旨在为您打造完美的红宝石戒指。'
    },
    navPageTitle: '设计你自己的戒指'
  },
  'design-your-own-stones-single': {
    seo: {
      title: '选择一颗红宝石来打造您的完美红宝石首饰',
      description:
        '在拥有世界上最大的天然红宝石收藏中,一定能为您或您的爱人找到完美的红宝石'
    },
    navPageTitle: '红宝石裸石'
  },
  'design-your-own-stones-pair': {
    seo: {
      title: '选择一颗红宝石来打造您的完美红宝石首饰',
      description:
        '搜索我们精心挑选的红宝石对,为您找到完美的珠宝套装。 你会看到各种各样的色调、色调、颜色和形状。 让我们开始吧!'
    },
    navPageTitle: '红宝石对'
  },
  'design-your-own-stones-single-filterName-filterValue': {
    seo: {
      title: '选择一颗红宝石来打造您的完美红宝石首饰',
      description:
        '在拥有世界上最大的天然红宝石收藏中,一定能为您或您的爱人找到完美的红宝石'
    },
    navPageTitle: '红宝石裸石'
  },
  'design-your-own-stones-pair-filterName-filterValue': {
    seo: {
      title: '红宝石对',
      description:
        '搜索我们精心挑选的红宝石对,为您找到完美的珠宝组合，你会看到各种各样的色调、色调、颜色和形状。让我们开始吧!'
    },
    navPageTitle: '红宝石对'
  },
  'design-your-own-review-id': {
    seo: {
      title: '{stoneType} {category} {weight} {metalName}',
      description:
        '寻找特别的东西?当您最终确定您的红宝石宝石和镶嵌选项时,让我们帮助您创建完美的定制红宝石{fullType}。'
    },
    navPageTitle: '完成您的 {type}'
  },
  'ruby-jewelry-ruby-earrings': {
    seo: {
      title: '红宝石耳环',
      description:
        '库存有数百种设计,找到简单的红宝石耳钉到红地毯上的枝形吊灯红宝石耳环。 立即浏览,找到最适合您的!'
    },
    navPageTitle: '红宝石耳环',
    navPageDescr:
      '由精心搭配的一对红宝石制成,您可以期待我们的红宝石耳钉能够完美搭配。我们还提供镶嵌在闪亮光环、现代表圈等中的红宝石和钻石耳环。'
  },
  'ruby-jewelry-ruby-necklaces-pendants': {
    seo: {
      title: '红宝石项链 & 吊坠',
      description:
        '搜索我们种类繁多的红宝石项链和吊坠，包括经典款式和现代设计。我们为包括您在内的所有顾客提供独一无二的项链！'
    },
    navPageTitle: '红宝石项链 & 吊坠',
    navPageDescr:
      '我们还提供定制的红宝石项链和吊坠，这些项链和吊坠镶嵌在闪闪发光的钻石光环中，以及时尚、现代的表圈。 选择完美的红宝石，我们的专业设计团队可以打造您梦想中的红宝石项链。'
  },
  'ruby-jewelry-ruby-bracelets': {
    seo: {
      title: '红宝石手链',
      description:
        '用我们的红宝石手链寻找永恒的优雅外观。无数的设计和颜色组合,你一定会找到你梦想中的手链。'
    },
    navPageTitle: '红宝石手链',
    navPageDescr:
      '我们的红宝石手链由一系列精心挑选和镶嵌的宝石制成。 我们还提供镶嵌在各种贵金属中的红宝石和钻石手链，与我们的红宝石相得益彰。'
  },
  cart: {
    seo: {
      title: '购物车',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-index': {
    seo: {
      title: '购物车',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-order-summary': {
    seo: {
      title: '订单汇总',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '订单汇总'
  },
  'cart-index-secure-payment': {
    seo: {
      title: '安全支付',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  'cart-index-payment-order': {
    seo: {
      title: '付款单',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '购物车'
  },
  account: {
    seo: {
      title: '账户',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '我的账户'
  },
  'account-notifications': {
    seo: {
      title: '邮件通知',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '通知'
  },
  'account-recommendations': {
    seo: {
      title: '为您推荐',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '为您推荐'
  },
  'account-personal-details': {
    seo: {
      title: '个人 & 登录详情',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '个人 & 登录详情'
  },
  'account-preview-request-submissions': {
    seo: {
      title: '预览请求提交',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '预览请求提交'
  },
  'account-address-book': {
    seo: {
      title: '地址簿',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '地址簿'
  },
  'account-orders': {
    seo: {
      title: '查看历史订单',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '订单'
  },
  'account-change-password': {
    seo: {
      title: '修改密码',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '修改密码'
  },
  'account-reset-password': {
    seo: {
      title: '重置密码',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '重置密码'
  },
  'account-reset-password-token': {
    seo: {
      title: '重置密码',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '重置密码'
  },
  wishlist: {
    seo: {
      title: '最喜欢的红宝石',
      description:
        '将您最喜爱的红宝石裸石、镶嵌、红宝石对和成品红宝石首饰放在一起。 在选择之前可以将它们进行比较！'
    },
    navPageTitle: '收藏夹'
  },
  'compare-list': {
    seo: {
      title: '对比列表',
      description:
        '自 1939 年以来,我们一直是红宝石订婚戒指、天然红宝石和红宝石首饰的权威,我们拥有最多的在线红宝石收藏。'
    },
    navPageTitle: '对比列表'
  },
  'contact-us': {
    seo: {
      title: 'Contacts',
      description:
        '我们的红宝石和珠宝专家团队可通过实时聊天、电话和电子邮件联系。立即联系我们获取帮助。'
    },
    navPageTitle: '联系天然红宝石公司'
  },
  showroom: {
    seo: {
      title: '参观天然红宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的 Natural Ruby Company 陈列室,寻找您心仪的红宝石或红宝石首饰。'
    },
    navPageTitle: '参观天然红宝石公司的陈列室'
  },
  'showroom-preview': {
    seo: {
      title: '参观天然红宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的 Natural Ruby Company 陈列室,寻找您心仪的红宝石或红宝石首饰。'
    },
    navPageTitle: '参观天然红宝石公司的陈列室'
  },
  'showroom-preview-success': {
    seo: {
      title: '参观天然红宝石公司的陈列室',
      description:
        '参观位于纽约州纽约市曼哈顿办事处的 Natural Ruby Company 陈列室,寻找您心仪的红宝石或红宝石首饰。'
    },
    navPageTitle: '参观天然红宝石公司的陈列室'
  },
  'our-staff': {
    seo: {
      title: '认识我们的团队',
      description:
        '了解更多有关Natural Ruby公司的优秀专业人士,他们让一切都正常运行'
    },
    navPageTitle: '我们的团队'
  },
  'michael-arnstein': {
    seo: {
      title: 'Michael Arnstein',
      description:
        '了解更多有关Natural Ruby公司的优秀专业人士,他们让一切都正常运行'
    },
    navPageTitle: 'Michael Arnstein',
    navPageDescr: '董事长 - 天然红宝石公司'
  },
  auth: {
    seo: {
      title: '注册账户',
      description:
        '注册一个帐户,可以跟踪您的订单并将感兴趣的物品保存到您的收藏夹中,以便以后在 thenaturalrubycompany.com 上购买'
    },
    navPageTitle: '天然红宝石公司 - 注册账户'
  },
  'education-index-category-slug': {
    seo: {
      title: 'Education',
      description: ''
    },
    navPageTitle: '天然红宝石公司'
  }
}
