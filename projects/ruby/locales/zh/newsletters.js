export default {
  validation: {
    required: '请输入邮箱地址',
    invalid: '请输入有效的邮箱地址'
  },
  success: '感谢您订阅thenaturalrubycompany.com新闻通讯',
  confirmButtonText: '好的',
  signUp: '注册',
  email: '邮箱地址'
}
