export default {
  pageTitle: '我们的工作人员',
  pageSubtitle: '天然红宝石公司专门提供最好的天然红宝石。',
  pageMessage:
    '我们的成功归功于我们敬业的员工，从销售人员到设计师。 了解有关才华横溢的专业人士的更多信息',
  contactUsTitle: '联系我们',
  readMore: '了解更多',
  birthplace: '出生地',
  residence: '地址',
  blog: '阅读博客',
  previous: '< 上一个',
  next: '下一个 >',
  previousShort: '< 上一个',
  nextShort: '下一个>'
}
