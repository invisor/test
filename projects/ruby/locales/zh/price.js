export default {
  priceConversionTitle: '货币转换成{currency}',
  priceConversionMessage:
    "We detected that you are visiting our website from {country}. As a courtesy, we have made a close approximation of this item's price after converting from USD to {currency}. Please note that we process all charges in USD, and final prices may vary due to associated credit card fees and exchange rate fluctuations.",
  priceConversionOriginal: '美元价格： {price}'
}
