import metalTypes from './constants/metalTypesFilter'
import stoneShapes from './constants/stoneShapes'

export default {
  pair: '红宝石对',
  itemId: '货品编号',
  centerStone: '主石',
  ruby: '红宝石',
  weight: '{0} 克拉',
  Setting_Ring: '戒指',
  Setting_Earring: '耳环',
  Setting_Pendant: '吊坠',
  Setting_Necklace: '项链',
  metalTypes,
  stoneShapes
}
