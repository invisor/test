import nuxtConfig, { apiHost, cacheTime, isDev } from '../../nuxt.config.js'
import sh from '../../sitemap'
import { getFeedItem, getSupplementalItem } from '../../utils/sitemapUtils'
import { host } from '../../utils/definitions/defaults'

const siteName = 'ruby'
export const prodDomain = 'thenaturalrubycompany.com'
export const devDomain = 'staging.thenaturalrubycompany.com'
export const wpDomain = 'about.thenaturalrubycompany.com'

export default {
  ...nuxtConfig,

  srcDir: __dirname,

  buildDir: '.nuxt/ruby',

  env: {
    siteName,
    prodDomain,
    devDomain,
    version: '1.0.9', // change to clear localstorage
    isDev,
    merchantId: 547143324, // google merchant id
    extendStoreId: '5b751067-d33c-4d2d-ae11-deb5ae42c62d', // production
    reCaptchaKey: isDev
      ? '6LeaXRseAAAAAJxsVHz1YLEJ5-cvc-DVHlif9_lq'
      : '6LcbVk0eAAAAAAW1n7Rf0pQi7nETmwdHkXd6C5Ey',
    googleMapKey: isDev
      ? 'AIzaSyCAnjgwbUptL6Y2hZrrspZMi9T6KsIODO0'
      : 'AIzaSyA5x4jW2IR7g8qvuE4AH0p6896PzEM_dUg',
    wpDomain: `https://${wpDomain}`,
    video360Url: 'https://360images.thenaturalsapphirecompany.com',
    host: getHost(),
    assetsHost: 'https://asset.thenaturalrubycompany.com',
    wpAPIHost: 'https://about.thenaturalrubycompany.com/',
    ipStackHost: 'https://api.ipstack.com',
    ipStackKey: '40df478658bc3ac5f7d64ce5d38325da',
    fbAppId: null,
    detailsSizeVideoHost: isDev
      ? 'https://d27qoi3p1edcyf.cloudfront.net'
      : 'https://d2kh7hn62dcf23.cloudfront.net', // size video on details page
    promo: 'valentine' // to disable use null, false or '' value. To enable enter banner name
  },

  sitemap: {
    path: '/sitemap.xml',
    cacheTime,
    hostname: getHost(),
    exclude: [
      '/account/**',
      '/auth',
      '/order-history',
      '/education',
      '/zh/account/**',
      '/zh/auth',
      '/zh/order-history',
      '/zh/education'
    ],

    async routes() {
      const port = ':' + (isDev ? 80 : 443)
      const proto = (isDev ? 'http' : 'https') + '://'
      const getRoutes = sh.getRoutes.bind(sh)

      const routes = [
        ...(await getRoutes(proto + apiHost + port)),
        ...(await sh.getEduRoutes(wpDomain, prodDomain))
      ]

      return routes
    },

    filter({ routes }) {
      return routes.filter((r) => r)
    }
  },

  auth: {
    resetOnError: true, // user will be automatically logged out if an error happens. (For example when token expired)
    plugins: ['../../plugins/auth'],
    redirect: {
      login: '/auth/',
      logout: false,
      home: false
    },
    cookie: {
      prefix: 'auth.',
      options: {
        path: '/',
        secure: !isDev
      }
    },
    strategies: {
      register: {
        scheme: 'local',
        token: {
          maxAge: 60 * 300, // 300 minutes
          property: 'token',
          type: 'Bearer'
        },
        user: {
          property: false,
          autoFetch: true
        },
        endpoints: {
          login: {
            url: `/public/${siteName}/account/signup`,
            method: 'post'
          },
          user: {
            url: `/public/${siteName}/account/profile`,
            method: 'get'
          },
          logout: false
        }
      },
      login: {
        scheme: 'local',
        token: {
          maxAge: 60 * 300, // 300 minutes
          property: 'token',
          type: 'Bearer'
        },
        user: {
          property: false,
          autoFetch: true
        },
        endpoints: {
          login: {
            url: `/public/${siteName}/account/auth`,
            method: 'post'
          },
          user: {
            url: `/public/${siteName}/account/profile`,
            method: 'get'
          },
          logout: false
        }
      }
    }
  },

  robots: () => {
    if (!isDev) {
      return {
        UserAgent: '*',
        CrawlDelay: 2,
        Disallow: ['/account/', '/account/reset-password/', '/order-history/'],
        Sitemap: `https://${prodDomain}/sitemap.xml`
      }
    }
    return {
      UserAgent: '*',
      Disallow: '/'
    }
  },

  feed: [
    // A default feed configuration object
    {
      path: '/googlefeed.xml', // The route to your feed.
      async create(feed) {
        const port = ':' + (isDev ? 80 : 443)
        const proto = (isDev ? 'http' : 'https') + '://'

        feed.options = {
          title: 'The Natural Ruby Company',
          link: `${getHost()}/googlefeed.xml`,
          description: 'Google Merchant Center Product Feed',
          namespaces: [
            {
              name: 'xmlns:g',
              link: 'http://base.google.com/ns/1.0'
            }
          ]
        }

        const items = await sh.getItems(proto + apiHost + port)

        items.forEach((item) => {
          feed.items.push(getFeedItem(item))
        })

        return feed
      }, // The create function (see below)
      cacheTime, // How long should the feed be cached
      data: [] // Will be passed as 2nd argument to `create` function
    },
    {
      path: '/googlesupplemental.xml', // The route to your feed.
      async create(feed) {
        const port = ':' + (isDev ? 80 : 443)
        const proto = (isDev ? 'http' : 'https') + '://'

        feed.options = {
          title: 'The Natural Ruby Company',
          link: `${getHost()}/googlesupplemental.xml`,
          description: 'Google Merchant Center Supplemental Feed',
          namespaces: [
            {
              name: 'xmlns:g',
              link: 'http://base.google.com/ns/1.0'
            }
          ]
        }

        const items = await sh.getItems(
          proto + apiHost + port,
          '?all=supplemental'
        )

        items.forEach((item) => {
          feed.items.push(getSupplementalItem(item))
        })

        return feed
      }, // The create function (see below)
      cacheTime, // How long should the feed be cached
      data: [] // Will be passed as 2nd argument to `create` function
    }
  ]

  // components: [
  //   { path: '~/components', extensions: ['vue'] }, // default level is 0. Level 0 will overwrite level 1
  //   { path: '~~/components', level: 1, extensions: ['vue'] } // level 1
  //   // { path: '~~/components', level: 1, extensions: ['vue'] } // level 1
  // ]
}

export function getHost() {
  return isDev ? `http://${devDomain}` : host
}
