window.accordion = function($ = jQuery) {
  $('body')
    .find('.sp-easy-accordion')
    .each(function() {
      const accordionId = $(this).attr('id')
      const _this = $(this)
      const eaActive = _this.data('ea-active')
      const eaMode = _this.data('ea-mode')
      const preloader = _this.data('preloader')
      const selector = '#' + accordionId + ' > .ea-card > .sp-collapse'
      if (eaMode === 'vertical') {
        if (eaActive === 'ea-click') {
          $('#' + accordionId).each(function() {
            $('#' + accordionId + ' > .ea-card > .ea-header').on(
              'click',
              function() {
                $(selector).on('hide.bs.spcollapse', function(e) {
                  $(this)
                    .parent('.ea-card')
                    .removeClass('ea-expand')
                  $(this)
                    .siblings('.ea-header')
                    .find('.ea-expand-icon')
                    .addClass('fa-plus')
                    .removeClass('fa-minus')
                  e.stopPropagation()
                })
                $(selector).on('show.bs.spcollapse', function(e) {
                  $(this)
                    .parent('.ea-card')
                    .addClass('ea-expand')
                  $(this)
                    .siblings('.ea-header')
                    .find('.ea-expand-icon')
                    .addClass('fa-minus')
                    .removeClass('fa-plus')
                  e.stopPropagation()
                })
              }
            )
          })
          $('#' + accordionId + ' > .ea-card .ea-header a ').click(function(
            event
          ) {
            event.preventDefault()
          })
        }
        if (eaActive === 'ea-hover') {
          $('#' + accordionId + ' > .ea-card').mouseover(function() {
            $(this)
              .children('.sp-collapse')
              .spcollapse('show')
          })
          $(selector).on('hide.bs.spcollapse', function(e) {
            $(this)
              .parent('.ea-card')
              .removeClass('ea-expand')
            $(this)
              .siblings('.ea-header')
              .find('.ea-expand-icon')
              .addClass('fa-plus')
              .removeClass('fa-minus')
            e.stopPropagation()
          })
          $(selector).on('show.bs.spcollapse', function(e) {
            $(this)
              .parent('.ea-card')
              .addClass('ea-expand')
            $(this)
              .siblings('.ea-header')
              .find('.ea-expand-icon')
              .addClass('fa-minus')
              .removeClass('fa-plus')
            e.stopPropagation()
          })
        }
      }
      if (preloader === 1) {
        const preloaderId = $('.accordion-preloader').attr('id')
        $(document).ready(function() {
          $('#' + preloaderId)
            .animate({ opacity: 0 }, 500)
            .remove()
          $('#' + accordionId)
            .find('.ea-card')
            .animate({ opacity: 1 }, 500)
        })
      }
    })
}
