export default function(req, res, next) {
  // res.setHeader(
  //   'Cache-Control',
  //   'public, max-age=1, stale-while-revalidate=31536000'
  // )
  // res.setHeader('Cache-Control', 'public, max-age=10, s-maxage=20')
  res.setHeader('Cache-Control', 'max-age=31536000')
  next()
}
