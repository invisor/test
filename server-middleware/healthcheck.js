// AWS health check
const defaults = {
  path: '/hc',
  contentType: 'text/plain',
  healthy: () => {
    return 'OK'
  }
}

export default function(req, res, next) {
  res.setHeader('Content-Type', defaults.contentType)
  res.end(defaults.healthy())
}
