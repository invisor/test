const axios = require('axios')
const utils = require('./utils/sitemapUtils')
const securityHeaders = require('./utils/helpers/securityHeaders.js')

const sitemapHelper = {
  async getItems(apiHost, params = '') {
    const request = {
      method: 'get',
      url: `${apiHost}/public/${process.env.siteName}/metadata/sitemap${params}`
    }
    const { data } = await axios.get(request.url, {
      headers: securityHeaders.default(request)
    })
    return Object.values(data).flat()
  },
  async getRoutes(apiHost) {
    const routes = []
    const items = await this.getItems(apiHost, '?all=true')

    items.forEach((item) => {
      const detailsPath = utils.getDetailsPath(item)
      routes.push(detailsPath)
      routes.push('zh/' + detailsPath)
    })

    return routes
  },
  getEduRoutes(wpDomain, prodDomain) {
    const routes = []

    return Promise.all([
      axios.get(`https://${wpDomain}/wp-json/wp/v2/pages/?per_page=100`),
      axios.get(`https://${wpDomain}/wp-json/wp/v2/posts/?per_page=100`)
    ]).then((results) => {
      const pagesAndPosts = [...results[0].data, ...results[1].data]
      const parentIds = pagesAndPosts.map((p) => p.parent)

      pagesAndPosts.forEach((p) => {
        const type =
          p.parent === 0 && parentIds.includes(p.id) ? 'list' : p.type
        const link =
          p.link.replace(
            wpDomain,
            process.env.isDev ? `${prodDomain}/education` : 'education'
          ) + (type === 'page' ? '' : `?type=${type}`)
        routes.push(link)
      })

      return routes
    })
  }
}

module.exports = sitemapHelper
