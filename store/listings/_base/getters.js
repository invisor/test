export default {
  pageItems: (store) => (pageNumber) => {
    return store.pagesList[pageNumber].page
  },
  loadedPages: (store) => {
    return store.pagesList.map((i) => i.pageNumber).sort((a, b) => a - b)
  }
}
