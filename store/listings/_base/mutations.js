import { ITEMS_LIST } from './constants'

export default {
  bindNamespace(state, name) {
    state._namespace = name
  },

  changeItemsOrder(state, value) {
    state.itemsList = value
  },

  [ITEMS_LIST.GET_ITEMS_LIST](state, listData) {
    state.totalItemsCount = listData.totalCount
    if (listData.pageNumber > state.lastLoadedPageNumber) {
      // next page
      state.pagesList.push(listData)
    } else {
      // previous page
      state.pagesList.unshift(listData)
    }
    state.totalPagesCount = listData.pageCount
    state.lastLoadedPageNumber = listData.pageNumber
    state.itemsHash = this.$h.guid()
  },

  [ITEMS_LIST.DROP_ITEMS_LIST](state) {
    state.pagesList = []
    state.loadingPages = []
    state.lastLoadedPageNumber = 0
    state.totalItemsCount = 0
  },

  [ITEMS_LIST.SWITCH_ITEMS_LIST_VIEW](state, viewType) {
    state.listView = viewType
  },

  [ITEMS_LIST.SET_LOADING](state, st) {
    state.loading = st
  },

  [ITEMS_LIST.ADD_TO_LOADING_LIST](state, pageNumber) {
    state.loadingPages.push(pageNumber)
  },

  [ITEMS_LIST.REMOVE_FROM_LOADING_LIST](state, pageNumber) {
    const index = state.loadingPages.indexOf(pageNumber)
    if (index > -1) state.loadingPages.splice(index, 1)
  }
}
