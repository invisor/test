import cloneDeep from 'lodash/cloneDeep'
import mutations from '../_base/mutations'

export default cloneDeep(mutations)
