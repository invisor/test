import cloneDeep from 'lodash/cloneDeep'
import actions from '../ringsList/actions'

export default cloneDeep(actions)
