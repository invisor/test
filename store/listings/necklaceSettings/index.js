import storeState from './state'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export const state = () => ({ ...storeState })

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
