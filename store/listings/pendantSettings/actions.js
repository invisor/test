import cloneDeep from 'lodash/cloneDeep'
import actions from '../ringSettings/actions'

export default cloneDeep(actions)
