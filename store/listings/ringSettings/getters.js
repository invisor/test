import cloneDeep from 'lodash/cloneDeep'
import getters from '../_base/getters'

export default cloneDeep(getters)
