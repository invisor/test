import cloneDeep from 'lodash/cloneDeep'
import actions from '../stonesList/actions'
import { ITEMS_LIST } from '../_base/constants'
import {
  defaultGemListQuery,
  defaultPageSize
} from '~~/utils/definitions/defaults'
import { serializeFilter } from '~~/utils/serializators'
import { normalizeItemsListData } from '~~/utils/normalizers'

export default {
  ...cloneDeep(actions),

  async getPage({ commit, rootState, state, getters, dispatch }, params) {
    if (await dispatch('skipFetch', params)) return {}

    commit(ITEMS_LIST.SET_LOADING, true)
    commit(ITEMS_LIST.ADD_TO_LOADING_LIST, params.pageNumber)
    const query = serializeFilter(state._namespace, rootState.filters, params)

    const pageQuery = {
      ...cloneDeep(defaultGemListQuery),
      pageNumber: params.pageNumber,
      pageSize: params.pageSize || defaultPageSize
    }

    const listData = await this.$api.listings.fetchPairsList({
      ...pageQuery,
      ...{ query }
    })

    if (!params.keepPages || params.force) {
      commit(ITEMS_LIST.DROP_ITEMS_LIST)
    }

    if (params.beforeAddItemsCallback) params.beforeAddItemsCallback()

    const normalizedData = normalizeItemsListData(listData, params.pageNumber)

    commit(ITEMS_LIST.GET_ITEMS_LIST, normalizedData)

    if (params.afterAddItemsCallback) params.afterAddItemsCallback()

    commit(ITEMS_LIST.SET_LOADING, false)
    commit(ITEMS_LIST.REMOVE_FROM_LOADING_LIST, params.pageNumber)

    return normalizedData
  }
}
