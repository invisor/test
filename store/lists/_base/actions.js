import forEach from 'lodash/forEach'
import LIST from './constants'
import {
  cookiesOpts,
  productListTypeByCategory
} from '~~/utils/definitions/defaults'
import {
  serializeBaseItems,
  serializeCustomItem,
  serializeRemoveListItems,
  serializeWishlistItems
} from '~~/utils/serializators'
import {
  normalizeBaseItem,
  normalizeBandsItemsWishlist,
  normalizeCustomItemWishlist,
  normalizePlainBandsItemsWishlist,
  normalizeSkip,
  normalizeWaxItemCart
} from '~~/utils/normalizers'
import {
  normalizeBandsItemsCompare,
  normalizeCustomItemCompare,
  normalizePlainBandsItemsCompare
} from '~~/utils/normalizers/compareList'
import { getItemId } from '~~/utils/utils'

const normalizersMap = {
  favorites: {
    weddingBands: normalizeBandsItemsWishlist,
    weddingBandsPlain: normalizePlainBandsItemsWishlist,
    custom: normalizeCustomItemWishlist
  },
  compareList: {
    weddingBands: normalizeBandsItemsCompare,
    weddingBandsPlain: normalizePlainBandsItemsCompare,
    custom: normalizeCustomItemCompare
  },
  cart: {
    weddingBands: normalizeBandsItemsCompare,
    weddingBandsPlain: normalizePlainBandsItemsCompare,
    custom: normalizeCustomItemCompare,
    wax: normalizeWaxItemCart
  }
}

function normalizeItemsMap(namespace, list) {
  switch (list) {
    case 'weddingBands':
    case 'weddingBandsPlain':
    case 'custom':
    case 'wax':
      return normalizersMap[namespace][list]
    default:
      return normalizeSkip
  }
}

export default {
  setBaseItems({ state, commit }, items) {
    commit(LIST.ADD_BASE_ITEMS, normalizeBaseItem(items))
  },

  async getItemsFromBase({ state, commit }, items) {
    commit(LIST.FETCHING_ITEMS, true)
    const response = await this.$api[state._namespace].fetchListItems(
      serializeBaseItems(items, state._namespace)
    )

    const fullItems =
      response[`${state._namespace.replace('favorites', 'favorite')}Data`]

    forEach(fullItems, (itemsList) => {
      itemsList.forEach((i) => {
        let item = { ...i, serverSync: true }
        const productListType = productListTypeByCategory[item.category]
        item = normalizeItemsMap(state._namespace, productListType)(item)

        commit(LIST.REPLACE_ITEM_SILENT, { item, type: productListType })
      })
    })
    commit(LIST.REMOVE_PLACEHOLDERS, items)
    commit(LIST.FETCHING_ITEMS, false)
  },

  changeItemsOrder({ commit }, { type, value }) {
    commit(LIST.CHANGE_ITEMS_ORDER, { type, value })
  },

  /**
   * Mark all items as unsynced to fetch actual prices from lists
   * @param commit
   */
  markItemsAsNotSynced({ commit }) {
    commit(LIST.MARK_ALL_AS_NOT_SYNCED)
  },

  /**
   * NOT PRICES! Sync new added and updated locally items with the server
   * @param commit
   */
  markItemsAsSynced({ commit }) {
    commit(LIST.MARK_ALL_AS_SYNCED)
  },

  async pushAllListItemsToServer({ rootState, getters, dispatch }) {
    if (!rootState.auth.loggedIn) return
    const notSyncedItems = getters.allListItems.filter(
      (item) => item.itemToSync && !item.placeholder
    )
    if (notSyncedItems.length) {
      await dispatch('addToServerListForce', { item: notSyncedItems })
      dispatch('markItemsAsSynced')
    }
  },

  addToServerList({ commit, getters, state, rootState, dispatch }, items) {
    if (!rootState.auth.loggedIn) return
    const listItems = (Array.isArray(items) ? items : [items]).filter(
      (item) => item
    )
    if (!listItems.length) return
    if (listItems.length === 1) {
      if (isInList(getters.allListItems, listItems[0])) {
        // await dispatch('removeFromServerList', listItems[0])
        return
      }
    }
    this.$api[state._namespace]
      .mergeList(serializeWishlistItems(listItems))
      .then((response) => {
        listItems.forEach((listItem) => {
          const item = { ...listItem, serverSync: true }
          const productListType = productListTypeByCategory[item.category]
          this.$cookies.set(
            `${state._namespace}Items`,
            Number(getters.allListItems.length) + 1,
            cookiesOpts
          )
          commit(LIST.ADD_ITEM, { item, type: productListType })
        })
      })
  },

  async addToServerListForce(
    { state, commit, getters, rootState, dispatch },
    { item, update }
  ) {
    const listItems = (Array.isArray(item) ? item : [item]).filter(
      (item) => item
    )
    if (!listItems.length) return
    if (rootState.auth.loggedIn)
      await this.$api[state._namespace].mergeList(
        serializeWishlistItems(listItems)
      )
    if (update === undefined) {
      listItems.forEach((listItem) => {
        const item = { ...listItem, serverSync: true }
        const productListType = productListTypeByCategory[item.category]
        commit(LIST.ADD_ITEM, { item, type: productListType })
      })
    }
  },

  removeFromServerList(
    { state, commit, getters, rootState },
    { item, silent }
  ) {
    if (!rootState.auth.loggedIn) return
    this.$api[state._namespace]
      .removeFromList({
        data: serializeRemoveListItems([item])
      })
      .then((response) => {
        this.$cookies.set(
          `${state._namespace}Items`,
          Number(getters.allListItems.length) - 1,
          cookiesOpts
        )
        if (silent) {
          commit(LIST.REMOVE_ITEM_SILENT, { item })
          return
        }
        commit(LIST.REMOVE_ITEM, { item })
      })
  },

  async addToList(
    { state, commit, getters, rootState, dispatch },
    { item, serverSync = true }
  ) {
    if (serverSync) await dispatch('addToServerList', item)
    if (isInList(getters.allListItems, item)) {
      if (rootState.auth.loggedIn) {
        await dispatch('removeFromServerList', { item })
      } else commit(LIST.REMOVE_ITEM, { item })
      this.$cookies.set(
        `${state._namespace}Items`,
        Number(getters.allListItems.length),
        cookiesOpts
      )
      return
    }
    this.$cookies.set(
      `${state._namespace}Items`,
      Number(getters.allListItems.length) + 1,
      cookiesOpts
    )
    commit(LIST.ADD_ITEM, { item })
  },

  async getFinalOptions({ commit, state }, item) {
    const query = serializeCustomItem(item)
    const finalOptions = await this.$api.customItem.fetchFinalOptions(query)
    commit(LIST.SET_FINAL_OPTIONS, { item, finalOptions })
  },

  async setSettingOptions(
    { commit, rootState, dispatch },
    { type, id, item, options } // for custom items
  ) {
    if (rootState.auth.loggedIn) {
      await dispatch('addToServerListForce', { item, update: false })
    }
    commit(LIST.SET_SETTING_OPTIONS, { type, id, options })
  },

  setSettingOptionsWithoutServerUpdate(
    { commit, rootGetters, dispatch },
    { type, id, item, options } // for custom items
  ) {
    commit(LIST.SET_SETTING_OPTIONS, { type, id, options })
  },

  setSideStonesOptions(
    { commit },
    { type, id, index, value, sideType } // for custom items
  ) {
    commit(LIST.SET_SIDE_STONE_OPTIONS, {
      type,
      id,
      index,
      value,
      sideType
    })
  },

  async updateCustomItemPrice(
    { commit, rootState, dispatch, state },
    { id, price, type } // for custom items
  ) {
    commit(LIST.UPDATE_CUSTOM_ITEM_PRICE, { id, price, type })
    const index = state[`${type}Items`].findIndex((el) => el.id === id)
    if (index > -1) {
      if (rootState.auth.loggedIn) {
        // update server wishlist only after price was updated
        await dispatch('addToServerListForce', {
          item: state[`${type}Items`][index],
          update: false
        })
      }
    }
  },

  async updateListItem({ commit, rootState, dispatch }, { item, silent }) {
    if (rootState.auth.loggedIn) {
      await dispatch('addToServerListForce', { item, update: false })
    }
    if (silent) {
      commit(LIST.UPDATE_ITEM_SILENT, { item })
      return
    }
    commit(LIST.UPDATE_ITEM, { item })
  },

  async replaceListItem(
    { state, commit, rootState, dispatch },
    { index, item }
  ) {
    if (rootState.auth.loggedIn) {
      await dispatch('addToServerListForce', { item, update: false }) // add new item
    }
    commit(LIST.REPLACE_ITEM, { index, item })
  },

  async removeFromList({ state, commit, getters, dispatch, rootState }, item) {
    if (rootState.auth.loggedIn) {
      await dispatch('removeFromServerList', { item })
      return
    }
    const productListType = productListTypeByCategory[item.category]
    this.$cookies.set(
      `${state._namespace}Items`,
      Number(getters.allListItems.length) - 1,
      cookiesOpts
    )
    commit(LIST.REMOVE_ITEM, { item, type: productListType })
  },

  async removeFromListSilent(
    { state, commit, getters, dispatch, rootState },
    item
  ) {
    if (rootState.auth.loggedIn) {
      await dispatch('removeFromServerList', { item })
      return
    }
    const productListType = productListTypeByCategory[item.category]
    this.$cookies.set(
      `${state._namespace}Items`,
      Number(getters.allListItems.length) - 1,
      cookiesOpts
    )
    commit(LIST.REMOVE_ITEM_SILENT, { item, type: productListType })
  },

  updatePrices({ commit }, prices) {
    commit(LIST.UPDATE_PRICES, prices)
  }
}

export function isInList(list, item) {
  return list.some((el) => getItemId(el) === getItemId(item))
}
