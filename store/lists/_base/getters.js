import { getItemId } from '~~/utils/utils'

export default {
  stonesListItems: (store) => store.stonesListItems,
  stonePairsListItems: (store) => store.stonePairsListItems,
  braceletsListItems: (store) => store.braceletsListItems,
  earringsListItems: (store) => store.earringsListItems,
  necklacesPendantsListItems: (store) => store.necklacesPendantsListItems,
  ringsListItems: (store) => store.ringsListItems,
  weddingBandsItems: (store) => store.weddingBandsItems,
  weddingBandsPlainItems: (store) => store.weddingBandsPlainItems,
  ringSettingsItems: (store) => store.ringSettingsItems,
  earringSettingsItems: (store) => store.earringSettingsItems,
  pendantSettingsItems: (store) => store.pendantSettingsItems,
  necklaceSettingsItems: (store) => store.necklaceSettingsItems,
  customItems: (store) => store.customItems,
  allListItems: (store) => {
    return [
      ...store.stonesListItems,
      ...store.stonePairsListItems,
      ...store.braceletsListItems,
      ...store.earringsListItems,
      ...store.necklacesPendantsListItems,
      ...store.ringsListItems,
      ...store.weddingBandsItems,
      ...store.weddingBandsPlainItems,
      ...store.ringSettingsItems,
      ...store.earringSettingsItems,
      ...store.pendantSettingsItems,
      ...store.necklaceSettingsItems,
      ...store.customItems
    ]
  },
  inList: (store, getters) => (item) =>
    getters.allListItems.some(
      (i) => getItemId(i) === getItemId(item) || i.guid === item.guid
    )
}
