export const lists = {
  stonesListItems: [],
  stonePairsListItems: [],
  braceletsListItems: [],
  earringsListItems: [],
  necklacesPendantsListItems: [],
  ringsListItems: [],
  weddingBandsItems: [],
  weddingBandsPlainItems: [],
  ringSettingsItems: [],
  earringSettingsItems: [],
  pendantSettingsItems: [],
  necklaceSettingsItems: [],
  customItems: []
}

export default {
  _namespace: '',
  fetchingItems: false,
  ...lists
}
