import cloneDeep from 'lodash/cloneDeep'
import actions, { isInList } from '../_base/actions'
import LIST from './constants'
import { serializeOrderData } from '~~/utils/serializators'
import { normalizeOrder } from '~~/utils/normalizers/order'
import { serializeTaxesAddress } from '~~/utils/serializators/taxJar'
import { cookiesOpts } from '~~/utils/definitions/defaults.js'

const metadataCache = 'metadata-cache'

export default {
  ...actions,

  setShippingBillingCountry({ commit, rootState }) {
    commit(LIST.SET_SHIPPING_BILLING_ADDRESS, rootState.countryCode)
  },

  setTaxHash({ commit }, hash) {
    commit(LIST.SET_TAX_HASH, hash)
  },

  async calculateTax({ state, commit, getters }) {
    try {
      const tax = await this.$api.cart.taxes(
        serializeTaxesAddress(state, getters)
      )
      const st =
        state.metaData.states.us.find(
          (s) => s.key === getters.addressForm.shippingState
        ) || {}

      commit(LIST.SET_TAX, { ...tax, state: st })
    } catch (e) {
      commit(LIST.BACKEND_TAX_CALC_MODE, true)
    }
  },

  removeTax({ state, commit }) {
    if (!state.tax) return
    commit(LIST.REMOVE_TAX)
  },

  addExtend({ commit }, { item, plan }) {
    commit(LIST.ADD_EXTEND, { item, plan })
  },

  removeExtend({ commit }, { item }) {
    commit(LIST.REMOVE_EXTEND, { item })
  },

  async addToList(
    { state, commit, getters, rootState, dispatch },
    { item, serverSync = true, extendPlan }
  ) {
    if (serverSync) await dispatch('addToServerList', { ...item, extendPlan })
    if (isInList(getters.allListItems, item)) {
      if (rootState.auth.loggedIn) {
        await dispatch('removeFromServerList', { item })
      } else commit(LIST.REMOVE_ITEM, { item })
      this.$cookies.set(
        `${state._namespace}Items`,
        Number(getters.allListItems.length),
        cookiesOpts
      )
      return
    }
    this.$cookies.set(
      `${state._namespace}Items`,
      Number(getters.allListItems.length) + 1,
      cookiesOpts
    )
    commit(LIST.ADD_ITEM, { item: { ...item, extendPlan } })
  },

  async fetchMetaData({ commit, state }, id) {
    if (this.$cookies.get(metadataCache)) return
    const metaData = await this.$api.cart.fetchCartMetadata()
    this.$cookies.set(metadataCache, true, {
      maxAge: 60 * 60 * 4,
      ...cookiesOpts
    })
    commit(LIST.SET_META_DATA, metaData)
  },

  setDocumentationType({ commit }, type) {
    commit(LIST.SET_DOCUMENTATION_TYPE, type)
  },

  clearShippingRates({ commit }) {
    commit(LIST.CLEAR_SHIPPING_RATES)
  },

  confirmConversion({ commit }) {
    commit(LIST.CONFIRM_CONVERSION)
  },

  async postOrder({ commit, rootState, state, getters, dispatch }) {
    const orderData = serializeOrderData(state, getters)
    try {
      const orderType = rootState.auth.loggedIn ? 'Auth' : ''
      const response = await this.$api.cart[`make${orderType}Order`](orderData)
      if (typeof response === 'object' && response.id) {
        commit(LIST.RESET_CARD)
        dispatch('flushCart')
        commit(LIST.ORDER_SUCCESS, normalizeOrder(response))
        commit(LIST.SET_ORDER_RECEIVED, true)
        commit(LIST.SET_ORDER_CONFIRMED_STATE, false)
      }
      return Promise.resolve(response)
    } catch (e) {
      return Promise.reject(e)
    }
  },

  flushCart({ commit, getters, dispatch, rootState }) {
    if (rootState.auth.loggedIn) {
      cloneDeep(getters.items).forEach((item) => {
        dispatch('removeFromServerList', { item, silent: true })
      })
    }
    commit(LIST.FLUSH_CART)
  },

  setFormValue({ commit, getters }, data) {
    commit(LIST.SET_FORM_VALUE, data)
    if (data.shippingRateId) {
      const id = data.shippingRateId
      const subTotal = getters.getSubTotal
      commit(LIST.SET_SHIPPING, { id, subTotal })
    }
  },

  setPaymentFormValue({ commit }, data) {
    commit(LIST.SET_PAYMENT_FORM_VALUE, data)
  },

  updateAddressForm({ commit }, formData) {
    commit(LIST.UPDATE_ADDRESS_FORM_VALUE, formData)
  },

  resetDirty({ commit }) {
    commit(LIST.SET_DIRTY, false)
  }
}
