import get from 'lodash/get'
import sortBy from 'lodash/sortBy'
import getters from '../_base/getters'
import {
  defaultWaxPrice,
  jewelryByCategory,
  showDocsOptionsEdge,
  // stateTaxes,
  stonesByCategory
} from '~~/utils/definitions/defaults'
import { getLocalPrice } from '~~/utils/utils'
export default {
  ...getters,

  checkoutStep: (store) => store.checkoutStep,
  shipping: (store) => store.shipping,
  shippingLocal: (store) => store.shippingLocal,
  metaData: (store) => store.metaData,
  shippingCountry: (store) => store.addressForm.shippingCountryId,
  paymentForm: (store) => store.paymentForm,
  orderSummary: (store) => store.orderSummary,
  showExpandedCart: (store) => store.showExpandedCart,
  isOrderReceived: (store) => store.isOrderReceived,
  isOrderConfirmed: (store) => store.isOrderConfirmed,
  fetchCheckout: (store) => store.fetchCheckout,
  isBankWire: (store) => store.paymentForm.paymentOptionId === 3,
  conversionConfirmed: (store) => store.conversionConfirmed,
  docType: (store) => store.paymentForm.docType,
  localCurrency: (store) => store.metaData.localCurrency,
  pickUpDeliverySelected: (store) => store.addressForm.shippingRateId === 38,
  allListItems: (store) => {
    return [
      ...store.stonesListItems,
      ...store.stonePairsListItems,
      ...store.braceletsListItems,
      ...store.earringsListItems,
      ...store.necklacesPendantsListItems,
      ...store.ringsListItems,
      ...store.weddingBandsItems,
      ...store.weddingBandsPlainItems,
      ...store.ringSettingsItems,
      ...store.earringSettingsItems,
      ...store.pendantSettingsItems,
      ...store.necklaceSettingsItems,
      ...store.customItems,
      ...store.waxItems,
    ]
  },
  selectedUSState: (state, getters) => {
    return state.metaData.states?.us?.find(
      (s) => s.key === getters.addressForm.shippingState
    )
  },
  addressForm: (store, getters) => {
    if (getters.pickUpDeliverySelected)
      return { ...store.addressForm, ...store.companyAddress }
    return store.addressForm
  },
  items: (store, getters) => {
    return sortBy(getters.allListItems, ['addedAt'])
  },
  mixedPrices: (
    store, // Items in cart in different currencies
    getters,
    rootState
  ) => {
    if (!getters.items.length) return false
    const defaultCurrencyCode =
      rootState.app.localCurrency.currencyCode || 'USD'
    return getters.items.some((item) => {
      return (
        (get(item, 'localCurrency.currencyCode', '') ||
          get(item, 'finalPrice.localCurrency.currencyCode', '')) !==
        defaultCurrencyCode
      )
    })
  },
  getSubTotal: (store, getters) => {
    const isUS = getters.addressForm.shippingCountryId === 'US'
    return getters.items.reduce((acc, current) => {
      let price
      const extendPrice = isUS ? (current.extendPlan?.price || 0) / 100 : 0

      if (current.category === 'Wax') return acc + Number(defaultWaxPrice)

      if (current.finalPrice) {
        if (current.finalPrice.discountAmt) {
          const stonePrice = current.finalPrice.stonePrice
          const settingPrice = current.finalPrice.settingPrice
          return acc + Number(stonePrice) + Number(settingPrice) + extendPrice
        }
        if (
          store.paymentForm.paymentOptionId === 3 &&
          current.customStone.bankWirePrice
        ) {
          const stonePrice = current.customStone.bankWirePrice
          const settingPrice = current.finalPrice.settingPrice
          return acc + Number(stonePrice) + Number(settingPrice) + extendPrice
        }
        price = current.finalPrice.totalPrice
      } else {
        if (store.paymentForm.paymentOptionId === 3 && current.bankWirePrice)
          return acc + Number(current.bankWirePrice) + extendPrice
        price = current.discountPrice
          ? current.discountPrice
          : current.price || current.totalPrice
      }
      return acc + Number(price) + extendPrice
    }, 0)
  },
  getSubTotalLocal: (store, getters, rootState) => {
    const { localRate, usdRate } = rootState.app.localCurrency
    return getLocalPrice(getters.getSubTotal, usdRate, localRate)
  },
  getTax: (store, getters) => {
    // if (store.backendTaxCalcMode) return 0
    if (getters.addressForm.shippingCountryId !== 'US') return 0
    if (!getters.selectedUSState?.tax) return 0
    return store.tax
  },
  getTaxLocal: (store, getters, rootState) => {
    const { localRate, usdRate } = rootState.app.localCurrency

    return getLocalPrice(getters.getTax, usdRate, localRate)
  },

  getTotalWithoutTax: (store, getters) => {
    const subtotal = getters.getSubTotal
    return subtotal + store.shipping
  },

  getTotal: (store, getters) => {
    const subtotal = getters.getSubTotal
    return subtotal + store.shipping + getters.getTax
  },

  showDocsOptions: (store, getters) => {
    const priceCondition = getters.getTotal < showDocsOptionsEdge
    const itemsCondition = getters.items.some((item) =>
      [...jewelryByCategory, ...stonesByCategory, 'Custom'].includes(
        item.category
      )
    )
    return priceCondition && itemsCondition
  },

  getTotalLocal: (store, getters, rootState) => {
    const { localRate, usdRate } = rootState.app.localCurrency

    return getLocalPrice(getters.getTotal, usdRate, localRate)
  },
}
