import Vue from 'vue'
import mutations from '../_base/mutations'
import { lists } from '../_base/state'
import LIST from './constants'
import { productListTypeByCategory } from '~~/utils/definitions/defaults'
import { capitalize } from '~~/utils/formatters'
export default {
  ...mutations,

  [LIST.SET_SHIPPING_BILLING_ADDRESS](state, countryCode) {
    const { shippingCountryId } = state.addressForm
    if (shippingCountryId !== countryCode) {
      state.addressForm.shippingCountryId = countryCode
      state.addressForm.billingCountryId = countryCode
      state.addressForm.shippingState = ''
      state.addressForm.billingState = ''
    }
  },

  [LIST.SET_TAX](state, data) {
    state.tax = data.tax?.amount_to_collect || 0
  },

  [LIST.REMOVE_TAX](state) {
    state.tax = 0
    state.taxHash = ''
  },

  [LIST.BACKEND_TAX_CALC_MODE](state, mode) {
    state.backendTaxCalcMode = mode
  },

  [LIST.SET_TAX_HASH](state, hash) {
    state.taxHash = hash
  },

  [LIST.FLUSH_CART](state) {
    Object.keys(lists).forEach((key) => {
      state[key] = []
    })
  },

  [LIST.ADD_EXTEND](state, { item, plan }) {
    const listType = productListTypeByCategory[item.category]
    const index = state[`${listType}Items`].findIndex(
      (i) => i.guid === item.guid
    )
    if (index > -1) {
      Vue.set(state[`${listType}Items`][index], 'extendPlan', plan)
      // state[`${listType}Items`][index].extendPlan = normalizeExtendPlan(plan)
    }
  },

  [LIST.REMOVE_EXTEND](state, { item }) {
    const listType = productListTypeByCategory[item.category]
    const index = state[`${listType}Items`].findIndex(
      (i) => i.guid === item.guid
    )
    if (index > -1) {
      state[`${listType}Items`][index].extendPlan = null
    }
  },

  [LIST.CLEAR_SHIPPING_RATES](state) {
    state.metaData.shippingRates = null
  },

  [LIST.SET_DOCUMENTATION_TYPE](state, type) {
    state.paymentForm.docType = type
  },

  [LIST.SET_DIRTY](state, value) {
    state.isDirty = value
  },

  [LIST.CONFIRM_CONVERSION](state) {
    state.conversionConfirmed = true
  },

  [LIST.SET_FORM_VALUE](state, data) {
    Object.keys(data).forEach((key) => {
      state.addressForm[key] = data[key]
    })
  },

  [LIST.SET_PAYMENT_FORM_VALUE](state, data) {
    Object.keys(data).forEach((key) => {
      state.paymentForm[key] = data[key]
    })
  },

  [LIST.SHOW_EXPANDED_CART](state, bool) {
    state.showExpandedCart = bool
  },

  [LIST.FETCH_CHECKOUT](state, bool) {
    state.fetchCheckout = bool
  },

  [LIST.SET_ORDER_CONFIRMED_STATE](state, bool) {
    state.isOrderConfirmed = bool
  },

  [LIST.SET_ORDER_RECEIVED](state, bool) {
    state.isOrderReceived = bool
  },

  [LIST.RESET_CARD](state) {
    Object.keys(state.paymentForm).forEach((field) => {
      if (['paymentOptionId', 'docType'].includes(field)) return
      state.paymentForm[field] = ''
    })
    state.backendTaxCalcMode = false
    state.taxHash = ''
  },

  [LIST.SET_META_DATA](state, metaData) {
    state.metaData = metaData
  },

  [LIST.UPDATE_ADDRESS_FORM_VALUE](state, { formType, formData }) {
    Object.keys(formData).forEach((key) => {
      if (formType === 'shipping' && key === 'phone') {
        state.addressForm.phone = formData[key]
        return
      }
      state.addressForm[`${formType}${capitalize(key)}`] = formData[key]
    })
  },

  [LIST.UPDATE_FORM_VALUE](state, formData) {
    state[formData.formName][formData.fieldName] = formData.value
  },

  [LIST.ORDER_SUCCESS](state, orderSummary) {
    state.orderSummary = orderSummary
  },

  [LIST.SET_SHIPPING](state, { id, subTotal }) {
    let rates =
      state.addressForm.shippingCountryId === 'US'
        ? state.metaData.shippingRates.filter((i) => i.domestic)
        : state.metaData.shippingRates.filter((i) => !i.domestic)

    rates = rates.filter(
      (i) =>
        (i.maxOrderTotal >= subTotal || i.maxOrderTotal === null) &&
        i.minOrderTotal <= subTotal
    )

    const shippingPriceObj = rates.find((r) => r.id === id)

    if (shippingPriceObj) {
      const shippingPrice = shippingPriceObj.price

      state.shipping = shippingPrice

      const { localRate, usdRate } = this.state.app.localCurrency
      state.shippingLocal = this.$h.getLocalPrice(
        shippingPrice,
        usdRate,
        localRate
      )
    }
  }
}
