import cloneDeep from 'lodash/cloneDeep'
import state from '../_base/state'

export default {
  ...cloneDeep(state),
  lastAddedItemType: null
}
