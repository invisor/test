import Vue from 'vue'
import flatten from 'lodash/flatten'
import omit from 'lodash/omit'
import {
  normalizeFiltersResponse,
  normalizeJewelryFiltersResponse,
  normalizeWeddingBandsPlainFiltersResponse,
  normalizeWeddingBandsFiltersResponse,
  normalizeRingSettingsFiltersResponse,
  normalizeEarringSettingsFiltersResponse,
  normalizePendantSettingsFiltersResponse,
  normalizeNecklaceSettingsFiltersResponse
} from '~~/utils/normalizers'
import { settings, jewelry, bands, stones } from '~~/utils/definitions/defaults'
import { capitalize } from '~~/utils/formatters'

const normalizeSettingsMap = {
  ring: normalizeRingSettingsFiltersResponse,
  earring: normalizeEarringSettingsFiltersResponse,
  pendant: normalizePendantSettingsFiltersResponse,
  necklace: normalizeNecklaceSettingsFiltersResponse
}

export const FILTERS = {
  SET_AREA_FILTERS: 'SET_AREA_FILTERS',
  SET_FILTERS: 'SET_FILTERS',
  CLEAR_ALL_FILTERS: 'CLEAR_ALL_FILTERS',
  CLEAR_ORIGIN_FILTER: 'CLEAR_ORIGIN_FILTER',
  CLEAR_STYLES_FILTER: 'CLEAR_STYLES_FILTER',
  CHANGE_PREFILTER_STATE: 'CHANGE_PREFILTER_STATE',
  SET_DIRTY_LISTS: 'SET_DIRTY_LISTS',
  CLEAN_DIRTY_LISTS: 'CLEAN_DIRTY_LISTS',
  SET_DATA_TYPE: 'SET_DATA_TYPE',
  REMOVE_SELECTED_FILTER: 'REMOVE_SELECTED_FILTER'
}

/**
 * All existed item types
 * @type {string[]}
 */
const itemsTypes = [...stones, ...settings, ...jewelry, ...bands]

export const state = () => ({
  usePrefilter: true,
  dataType: {
    ringsList: 0,
    earringsList: 0,
    necklacesPendantsList: 0
  },
  dirtyLists: false,
  selectedFilters: {
    ringsList: {},
    stonesList: {},
    stonePairsList: {},
    earringsList: {},
    necklacesPendantsList: {},
    braceletsList: {},
    weddingBandsPlain: {},
    weddingBands: {},
    ringSettings: {},
    earringSettings: {},
    pendantSettings: {},
    necklaceSettings: {}
  },
  sourceFilters: {
    ringsList: {},
    stonesList: {},
    stonePairsList: {},
    earringsList: {},
    necklacesPendantsList: {},
    braceletsList: {},
    weddingBandsPlain: {},
    weddingBands: {},
    ringSettings: {},
    earringSettings: {},
    pendantSettings: {},
    necklaceSettings: {}
  }
})

export const getters = {
  usePrefilter: (store) => store.usePrefilter,
  dirtyLists: (store) => store.dirtyLists,

  hasFilters: (state) => (listType) => {
    const values = Object.values(
      omit(state.selectedFilters[listType], ['sortBy', 'order'])
    )
    return !!flatten(values).length
  },

  selectedFilters: (state) => (listType) => {
    return state.selectedFilters[listType]
  },

  /**
   * Filters received from server
   */
  ...(function () {
    const filters = {}
    itemsTypes.forEach((type) => {
      filters[`${type}SourceFilters`] = (store) => store.sourceFilters[type]
    })
    return filters
  })()
}

export const actions = {
  removeSelectedFilter({ commit }, params) {
    commit(FILTERS.REMOVE_SELECTED_FILTER, params)
  },

  setAreasFilters({ commit }, { listType, filterName, value }) {
    commit(FILTERS.SET_AREA_FILTERS, { listType, filterName, value })
  },

  setDataType({ commit }, { listType, value }) {
    commit(FILTERS.SET_DATA_TYPE, { listType, value })
  },

  disablePrefilter({ commit }) {
    commit(FILTERS.CHANGE_PREFILTER_STATE, false)
  },

  enablePrefilter({ commit }) {
    commit(FILTERS.CHANGE_PREFILTER_STATE, true)
  },

  setDirtyLists({ commit }) {
    commit(FILTERS.SET_DIRTY_LISTS)
  },

  cleanDirtyLists({ commit }) {
    commit(FILTERS.CLEAN_DIRTY_LISTS)
  },

  async setFilters({ commit, state }, params) {
    const { itemsType, query } = params
    const { usePrefilter } = state

    if (jewelry.includes(itemsType)) {
      const itemsMap = {
        braceletsList: 'Bracelet',
        earringsList: 'Earring',
        necklacesPendantsList: 'Necklace',
        ringsList: 'Ring'
      }

      const queryString = [
        itemsMap[itemsType],
        getQueryString(state, params)
      ].join('?')

      const filters = await this.$api.filters.fetchJewelryFilters(queryString)

      commit(FILTERS.SET_FILTERS, {
        filters: normalizeJewelryFiltersResponse(filters),
        itemsType
      })
      return
    }

    if (settings.includes(itemsType)) {
      const type = itemsType.replace('Settings', '')
      let queryString = `/${type}${
        usePrefilter && query && query.stoneId ? `/${query.stoneId}` : ''
      }`
      queryString = [queryString, getQueryString(state, params)].join('?')
      const filters = await this.$api.filters.fetchSettingsFilters(queryString)

      commit(FILTERS.SET_FILTERS, {
        filters: normalizeSettingsMap[type](filters),
        itemsType
      })
      return
    }

    if (itemsType === 'weddingBandsPlain') {
      const filters = await this.$api.filters.fetchPlainBandsFilters()
      commit(FILTERS.SET_FILTERS, {
        filters: normalizeWeddingBandsPlainFiltersResponse(filters),
        itemsType
      })
      return
    }

    if (itemsType === 'weddingBands') {
      const filters = await this.$api.filters.fetchBandsFilters()
      commit(FILTERS.SET_FILTERS, {
        filters: normalizeWeddingBandsFiltersResponse(filters),
        itemsType
      })
      return
    }

    if (
      itemsType === 'stonesList' ||
      (itemsType === 'stonePairsList' && query && query.settingId)
    ) {
      let queryString =
        usePrefilter && query && query.settingId
          ? `/setting/${query.settingId}`
          : ''
      queryString = [queryString, getQueryString(state, params)].join('?')

      const filters = await this.$api.filters.fetchStonesFilters(queryString)

      commit(FILTERS.SET_FILTERS, {
        filters: normalizeFiltersResponse(filters),
        itemsType
      })
      return
    }

    if (itemsType === 'stonePairsList') {
      let queryString = ''

      queryString = [queryString, getQueryString(state, params)].join('?')

      const filters = await this.$api.filters.fetchPairsFilters(queryString)

      commit(FILTERS.SET_FILTERS, {
        filters: normalizeFiltersResponse(filters),
        itemsType
      })
    }
  },

  clearAllFilters({ commit }, itemsType) {
    commit(FILTERS.CLEAR_ALL_FILTERS, itemsType)
  },

  clearOriginFilter({ commit }, itemsType) {
    commit(FILTERS.CLEAR_ORIGIN_FILTER, itemsType)
  },

  clearStyleFilter({ commit }, itemsType) {
    commit(FILTERS.CLEAR_STYLES_FILTER, itemsType)
  }
}

export const mutations = {
  [FILTERS.REMOVE_SELECTED_FILTER](
    state,
    { listType, valuesToRemove, filterType }
  ) {
    if (filterType.includes('Range')) {
      state.selectedFilters[listType][filterType] = []
      return
    }

    valuesToRemove.forEach((value) => {
      const index = state.selectedFilters[listType][filterType].findIndex(
        (v) => v === value
      )
      state.selectedFilters[listType][filterType].splice(index, 1)
    })
  },

  [FILTERS.SET_DATA_TYPE](state, { listType, value }) {
    state.dataType[listType] = value
  },

  [FILTERS.SET_AREA_FILTERS](state, { listType, filterName, value }) {
    Vue.set(state.selectedFilters[listType], filterName, value)
  },

  [FILTERS.CHANGE_PREFILTER_STATE](state, result) {
    if (result && !state.usePrefilter) state.dirtyLists = true
    state.usePrefilter = result
  },

  [FILTERS.SET_DIRTY_LISTS](state) {
    state.dirtyLists = true
  },

  [FILTERS.CLEAN_DIRTY_LISTS](state) {
    state.dirtyLists = false
  },

  [FILTERS.SET_FILTERS](state, { filters, itemsType }) {
    state.sourceFilters[itemsType] = filters
  },

  [FILTERS.CLEAR_ALL_FILTERS](state, itemsType) {
    state.selectedFilters[itemsType] = {}
  },

  [FILTERS.CLEAR_ORIGIN_FILTER](state, itemsType) {
    delete state.selectedFilters[itemsType].stoneOrigins
  },

  [FILTERS.CLEAR_STYLES_FILTER](state, itemsType) {
    delete state.selectedFilters[itemsType].styles
  }
}

function getQueryString(state, params) {
  const query = []

  const {
    itemsType,
    itemsSubType: cuttingstyle,
    isStar,
    origin,
    style,
    webCategory
  } = params
  const availability = state.dataType[itemsType]

  const paramsMap = {
    cuttingstyle: capitalize(cuttingstyle || ''),
    isStar,
    origin,
    style,
    webCategory,
    availability
  }

  Object.keys(paramsMap).forEach((param) => {
    if (paramsMap[param]) query.push(`${param}=${paramsMap[param]}`)
  })
  return query.join('&')
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
