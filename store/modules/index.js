// import { format } from 'date-fns'
import { getMeta } from '~~/utils/metaUtils'
import { cookiesOpts } from '~~/utils/definitions/defaults.js'

export const ROOT = {
  SET_META: 'SET_META',
  SET_PAGE_TITLE: 'SET_PAGE_TITLE',
  SET_USER_LANG: 'SET_USER_LANG',
  SET_USER_COUNTRY_NAME: 'SET_USER_COUNTRY_NAME',
  SET_LOADING_STATE: 'SET_LOADING_STATE',
  SET_TYPE_LIST: 'SET_TYPE_LIST',
  SET_FETCHED: 'SET_FETCHED'
}

const defaultCountryCode = 'US'
const defaultCountryName = 'United States'

/* LOG SYSTEM */
// const { resolve } = require('path')
// const fs = require('fs')
// const logFileName = 'ipstack.txt'
// const logPath = resolve('./', 'static', logFileName)
//
// function accessLog(req, ip) {
//   if (process.client) return
//   const agent = req.headers['user-agent']
//   const dateNow = format(new Date(), 'yyyy/MM/dd HH:mm:ss')
//   const logString = `${linesCount()} | ${dateNow} | ${ip} | ${agent}\n`
//   writeFile(logString)
// }
//
// function writeFile(line) {
//   const data = fs.readFileSync(logPath)
//   const fd = fs.openSync(logPath, 'w+')
//   const insert = Buffer.from(line)
//   fs.writeSync(fd, insert, 0, insert.length, 0)
//   fs.writeSync(fd, data, 0, data.length, insert.length)
//   fs.close(fd, (err) => {
//     if (err) throw err
//   })
// }
//
// function linesCount() {
//   const text = fs.readFileSync(logPath).toString()
//   const lines = text.split('\n')
//   return lines.length
// }

/* !LOG SYSTEM */

async function getCountryCode({ ip, req, route }) {
  const cookieLocale = this.$cookies.get('country_code')
  const cookieCountry = this.$cookies.get('country_name')

  if (process.env.isDev && route.query.forceCountry) {
    return {
      locale: route.query.forceCountry.toUpperCase(),
      country: 'Synthetic Country'
    }
  }

  if (cookieLocale && cookieCountry)
    return { locale: cookieLocale, country: cookieCountry }

  if (process.env.isDev)
    return { locale: defaultCountryCode, country: defaultCountryName }
  // if (isDev) return { locale: 'RU', country: 'Russia' }

  try {
    // accessLog(req, ip)
    const data = await this.$api.app.fetchCountryInfo(ip.trim())
    const locale = data.country_code || defaultCountryCode
    const country = data.country_name || defaultCountryName
    this.$cookies.set('country_code', locale, {
      maxAge: 60 * 60 * 24 * 7,
      ...cookiesOpts
    })
    this.$cookies.set('country_name', country, {
      maxAge: 60 * 60 * 24 * 7,
      ...cookiesOpts
    })

    return { locale, country }
  } catch (e) {
    console.log('Getting locale error', e)
  }

  return { locale: defaultCountryCode, country: defaultCountryName }
}

export const state = () => ({
  loading: false,
  typeList: null,
  fetched: false, // loaded data from local storage
  countryCode: defaultCountryCode,
  countryName: defaultCountryName,
  meta: [],
  pageTitle: ''
})

export const getters = {
  typeList: (store) => store.typeList,
  fetched: (store) => store.fetched,
  loading: (store) => store.loading,
  countryCode: (store) => store.countryCode || defaultCountryCode,
  countryName: (store) => store.countryName,
  meta: (store) => store.meta,
  pageTitle: (store) => store.pageTitle
}

export const actions = {
  async nuxtServerInit({ dispatch }, { $sentry }) {
    try {
      await dispatch('app/fetchInitialData')
    } catch (e) {
      if ($sentry) $sentry.captureException(e)
      console.log(e)
    }
  },

  getMeta({ commit }, context) {
    const { meta, title } = getMeta(context)
    commit(ROOT.SET_META, meta)
    commit(ROOT.SET_PAGE_TITLE, title)
  },
  /**
   * @param commit
   * @param {Object} params
   * @param {String} params.ip
   * @param {Object} params.req
   * @param {Object} params.route
   * @returns {Promise<*>}
   */
  async getCountryCode({ commit }, params) {
    const { locale, country } = await getCountryCode.call(this, params)

    commit(ROOT.SET_USER_LANG, locale)
    commit(ROOT.SET_USER_COUNTRY_NAME, country)
    return locale
  },

  globalLoading({ commit }, state) {
    commit(ROOT.SET_LOADING_STATE, state)
  },

  setTypeList({ commit }, typeList) {
    commit(ROOT.SET_TYPE_LIST, typeList)
  },

  setFetched({ commit }) {
    commit(ROOT.SET_FETCHED)
  }
}

export const mutations = {
  [ROOT.SET_USER_LANG](state, locale) {
    state.countryCode = locale
  },

  [ROOT.SET_META](state, meta) {
    state.meta = meta
  },

  [ROOT.SET_PAGE_TITLE](state, pageTitle) {
    state.pageTitle = pageTitle
  },

  [ROOT.SET_USER_COUNTRY_NAME](state, country) {
    state.countryName = country
  },

  [ROOT.SET_LOADING_STATE](state, val) {
    state.loading = val
  },

  [ROOT.SET_TYPE_LIST](state, typeList) {
    state.typeList = typeList
  },

  [ROOT.SET_FETCHED](state) {
    state.fetched = true
  }
}

export default {
  // namespaced: true,
  state,
  getters,
  actions,
  mutations
}
