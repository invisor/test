export const host = 'https://thenaturalrubycompany.com/'
export const companyName = {
  ruby: 'The Natural Ruby Company',
  emerald: 'The Natural Emerald Company',
  sapphire: 'The Natural Sapphire Company'
}

export const title = companyName[process.env.siteName]
// export const title = `${companyName[process.env.siteName]} - Ruby Rings & Jewelry Since 1939`
export const description =
  'We are the authority in ruby engagement rings, natural ruby, and ruby jewelry, since 1939. We have the largest collection of rubies found online.'

export const reportTypes = [3, 4, 10] // reports thumbnails types

export const prohibitedCountries = ['BE', 'IN', 'IR']

export const mensStyleID = 83
export const mensStyleRingID = "Men's"

export const cookiesOpts = {
  sameSite: 'lax',
  secure: !process.env.isDev
}

export const {
  productListTypeByCategory,
  jewelry,
  settings,
  bands,
  bandsByCategory,
  settingsByCategory
} = require('../sitemapUtils')

export const menuOpenDelay = 200
export const updateItemsListDelay = 500
export const video360Speed = 0.5 // from 0 to 1
export const defaultWaxPrice = 95
export const showDocsOptionsEdge = 5000

export const companyAddress = {
  shippingStreet1: '6 East 45th Street',
  shippingStreet2: '20th Floor',
  shippingCity: 'New York',
  shippingZipCode: '10017',
  shippingState: 'NY',
  shippingCountryId: 'US'
}

export const stateTaxes = {
  NY: 8.88
}

export const cartRoutes = [
  // 'cart-index', // on this page cart item will be synced automatically
  'cart-index-secure-payment',
  'cart-index-payment-order'
]

export const jewelryByCategory = [
  'Ring',
  'Earring',
  'Pendant',
  'Necklace',
  'Bracelet'
]

export const itemTypeByListingType = {
  ringSettings: 'Ring',
  ringsList: 'Ring',
  earringSettings: 'Earring',
  earringsList: 'Earring',
  pendantSettings: 'Pendant',
  necklaceSettings: 'Necklace',
  necklacesPendantsList: 'Pendant'
}

export const settingsListByCategory = {
  ring: 'ringSettings',
  earring: 'earringSettings',
  pendant: 'pendantSettings',
  necklace: 'necklaceSettings',
  Ring: 'ringSettings',
  Earring: 'earringSettings',
  Pendant: 'pendantSettings',
  Necklace: 'necklaceSettings'
}

export const routesMap = {
  ruby: {
    Setting_Earring: 'ruby-earring-settings',
    Setting_Ring: 'ruby-engagement-ring-settings',
    Setting_Pendant: 'ruby-pendants-settings',
    Setting_Necklace: 'ruby-necklaces-settings'
  },
  emerald: {
    Setting_Earring: 'emerald-earring-settings',
    Setting_Ring: 'emerald-engagement-ring-settings',
    Setting_Pendant: 'emerald-pendants-settings',
    Setting_Necklace: 'emerald-necklaces-settings'
  }
}

export const previewSettingsRoutesMap = {
  ruby: {
    Earring: 'ruby-earring-settings',
    Ring: 'ruby-engagement-ring-settings',
    Pendant: 'ruby-pendants-settings',
    Necklace: 'ruby-necklaces-settings'
  },
  emerald: {
    Earring: 'emerald-earring-settings',
    Ring: 'emerald-engagement-ring-settings',
    Pendant: 'emerald-pendants-settings',
    Necklace: 'emerald-necklaces-settings'
  }
}

export const settingsListBySettingUrl = {
  ruby: {
    'design-your-own-setting-ruby-engagement-ring-settings': 'ringSettings',
    'design-your-own-setting-mens-ring-settings': 'ringSettings',
    'design-your-own-setting-ruby-earring-settings': 'earringSettings',
    'design-your-own-setting-ruby-necklace-pendant-settings':
      'necklaceSettings',
    'design-your-own-setting-ruby-pendant-settings': 'necklaceSettings',
    'design-your-own-setting-ruby-necklace-settings': 'necklaceSettings'
  },
  emerald: {
    'design-your-own-setting-emerald-engagement-ring-settings': 'ringSettings',
    'design-your-own-setting-mens-ring-settings': 'ringSettings',
    'design-your-own-setting-emerald-earring-settings': 'earringSettings',
    'design-your-own-setting-emerald-necklace-pendant-settings':
      'necklaceSettings',
    'design-your-own-setting-emerald-pendants-settings': 'necklaceSettings',
    'design-your-own-setting-emerald-necklaces-settings': 'necklaceSettings'
  }
}

export const itemsTypeByRouteName = {
  stonesList: [
    'rubies',
    'rubies-star-rubies',
    'rubies-ruby-cabochon',
    'rubies-gemologist-recommended',
    'rubies-burmese-rubies',
    'rubies-mozambique-rubies',

    'emeralds',
    'emeralds-star-emeralds',
    'emeralds-emerald-cabochon',
    'emeralds-gemologist-recommended',
    'emeralds-colombian-emeralds',
    'emeralds-zambian-emeralds'
  ],
  stonePairsList: [
    'rubies-matched-pairs-of-rubies',
    'rubies-star-ruby-pairs',
    'rubies-matched-pairs-of-rubies-cabochon',

    'emeralds-matched-pairs-of-emeralds',
    'emeralds-star-emerald-pairs',
    'emeralds-matched-pairs-of-emeralds-cabochon'
  ],
  braceletsList: [
    'ruby-jewelry-ruby-bracelets',

    'emerald-jewelry-emerald-bracelets'
  ],
  earringSettings: [
    'design-your-own-setting-ruby-earring-settings',

    'design-your-own-setting-emerald-earring-settings'
  ],
  earringsList: [
    'ruby-jewelry-ruby-earrings',

    'emerald-jewelry-emerald-earrings'
  ],
  necklaceSettings: [
    'design-your-own-setting-ruby-necklace-pendant-settings',

    'design-your-own-setting-emerald-necklaces-settings'
  ],
  necklacesPendantsList: [
    'ruby-jewelry-ruby-necklaces-pendants',

    'emerald-jewelry-emerald-necklaces-pendants'
  ],
  pendantSettings: [
    'design-your-own-setting-ruby-necklace-pendant-settings',

    'design-your-own-setting-emerald-pendants-settings'
  ],
  ringSettings: [
    'design-your-own-setting-ruby-engagement-ring-settings',
    'design-your-own-setting-mens-ring-settings',

    'design-your-own-setting-emerald-engagement-ring-settings'
  ],
  ringsList: [
    'ruby-jewelry-ruby-rings',
    'ruby-jewelry-ruby-engagement-rings',
    'ruby-jewelry-star-ruby-rings',
    'ruby-jewelry-burmese-ruby-rings',
    'ruby-jewelry-mozambique-ruby-rings',
    'ruby-jewelry-mens-ruby-rings',

    'emerald-jewelry-emerald-rings',
    'emerald-jewelry-engagement-emerald-rings',
    'emerald-jewelry-colombian-emerald-rings',
    'emerald-jewelry-zambian-emerald-rings',
    'emerald-jewelry-mens-emerald-rings'
  ],
  weddingBands: ['ruby-wedding-rings-bands', 'emerald-wedding-rings-bands'],
  weddingBandsPlain: ['wedding-bands-without-gemstone']
}

export const stones = ['stonePairsList', 'stonesList']

export const settingsItems = [
  'Setting_Ring',
  'Setting_Earring',
  'Setting_Pendant',
  'Setting_Necklace'
]

export const listsForPrefilter = [
  'rubies',
  'rubies-ruby-cabochon',
  'rubies-burmese-rubies',
  'rubies-mozambique-rubies',
  'rubies-star-rubies',
  'rubies-matched-pairs-of-rubies',
  'rubies-matched-pairs-of-rubies-cabochon',
  'rubies-star-ruby-pairs',
  'design-your-own-setting-ruby-engagement-ring-settings',
  'design-your-own-setting-ruby-necklace-pendant-settings',
  'design-your-own-setting-ruby-ruby-earring-settings',
  'design-your-own-setting-mens-ring-settings',

  'emeralds',
  'emeralds-emerald-cabochon',
  'emeralds-colombian-emeralds',
  'emeralds-zambian-emeralds',
  'emeralds-matched-pairs-of-emeralds',
  'emeralds-matched-pairs-of-emeralds-cabochon',
  'design-your-own-setting-emerald-engagement-ring-settings',
  'design-your-own-setting-emerald-pendants-settings',
  'design-your-own-setting-emerald-necklaces-settings',
  'design-your-own-setting-emerald-earring-settings',
  'design-your-own-setting-mens-ring-settings'
]

export const defaultAddresses = {
  shippingAddress: null,
  billingAddress: null
}

export const accountForm = {
  firstName: null,
  lastName: null,
  email: null,
  phone: null,
  mailingList: false,
  gender: null
}

export const profileForm = {
  billingAddress: {},
  email: null,
  firstName: null,
  gender: null,
  lastName: null,
  mailingList: true,
  phone: null,
  shippingAddress: {}
}

export const defaultQuickSearchValues = {
  stones: [],
  jewelry: [],
  pairs: [],
  bands: [],
  settings: []
}

export const defaultFiltersValues = {
  stoneShapesFilter: [],
  stoneOriginsFilter: [],
  priceRangeFilter: [],
  caratRangeFilter: [],
  lengthRangeFilter: [],
  widthRangeFilter: [],
  sortByWeight: 0,
  sortByPrice: 0,
  featuredFirst: 1,
  newFirst: 0
}

export const defaultJewelryFiltersValues = {
  metalTypesFilter: [],
  stoneShapesFilter: [],
  stoneOriginsFilter: [],
  priceRangeFilter: [],
  caratRangeFilter: [],
  treatmentsFilter: [],
  categoriesFilter: [],
  sortByWeight: 0,
  sortByPrice: 0,
  featuredFirst: 1,
  newFirst: 0
}

export const defaultWeddingBandsPlainFiltersValues = {
  metalTypesFilter: [],
  stylesFilter: [],
  sortBy: 'FeaturedFirst',
  sortOrder: 1
}

export const defaultWeddingBandsFiltersValues = {
  metalTypesFilter: [],
  stoneTypesFilter: [],
  stoneShapesFilter: [],
  sortBy: 'FeaturedFirst',
  sortOrder: 1
}

export const defaultSettingsFiltersValues = {
  metalTypesFilter: [],
  centerStoneShapesFilter: [],
  sideStoneShapesFilter: [],
  designStylesFilter: [],
  sortBy: 'FeaturedFirst',
  sortOrder: 1
}

export const defaultFiltersMap = {
  ringsList: defaultJewelryFiltersValues,
  stonesList: defaultFiltersValues,
  stonePairsList: defaultFiltersValues,
  earringsList: defaultJewelryFiltersValues,
  necklacesPendantsList: defaultJewelryFiltersValues,
  braceletsList: defaultJewelryFiltersValues,
  weddingBandsPlain: defaultWeddingBandsPlainFiltersValues,
  weddingBands: defaultWeddingBandsFiltersValues,
  ringSettings: defaultSettingsFiltersValues,
  earringSettings: defaultSettingsFiltersValues,
  pendantSettings: defaultSettingsFiltersValues,
  necklaceSettings: defaultSettingsFiltersValues
}

export const defaultPageSize = 60
export const tabletPageSize = 60

export const defaultCustomDesignsListQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: 15,
  query: {
    settingId: null,
    metalType: [],
    category: [],
    origins: [],
    styles: [],
    centreStoneShapes: [],
    price: {
      min: null,
      max: null
    },
    weight: {
      min: null,
      max: null
    },
    treatments: []
  }
}

// query template. Default values: defaultFiltersValues const
export const defaultGemListQuery = {
  pageNumber: 1,
  calculateCount: true,
  previousPages: false,
  pageSize: defaultPageSize,
  query: {
    id: null,
    settingId: null,
    shapes: [],
    clarity: [],
    intensity: [],
    labColors: [],
    labTypes: [],
    origins: [],
    treatments: [],
    price: {
      min: null,
      max: null
    },
    weight: {
      min: null,
      max: null
    },
    width: {
      min: null,
      max: null
    },
    length: {
      min: null,
      max: null
    },
    cuttingStyles: [],
    isStar: 0,
    webCategory: 0,
    sortByWeight: 0,
    sortByPrice: 0,
    featuredFirst: 1,
    newFirst: 0
  }
}

// query template. Default values: defaultJewelryFiltersValues const
export const defaultJewelryListQuery = {
  pageNumber: 1,
  calculateCount: true,
  previousPages: false,
  pageSize: defaultPageSize,
  query: {
    id: null,
    dataType: 0,
    metals: [],
    categories: [],
    clarity: [],
    intensity: [],
    shapes: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    cuttingStyles: [],
    labColors: [],
    labTypes: [],
    origins: [],
    price: {
      min: null,
      max: null
    },
    weight: {
      min: null,
      max: null
    },
    treatments: [],
    sortByWeight: 0,
    sortByPrice: 0,
    featuredFirst: 1,
    newFirst: 0
  }
}

// query template. Default values: defaultWeddingBandsPlainFiltersValues const
export const defaultWeddingBandsPlainQuery = {
  pageNumber: 1,
  calculateCount: true,
  previousPages: false,
  pageSize: defaultPageSize,
  query: {
    id: null,
    categoryId: 1,
    metals: [],
    styles: [],
    stoneTypes: [],
    stoneShapes: [],
    bandWidth: {
      min: null,
      max: null
    },
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

// query template. Default values: defaultWeddingBandsFiltersValues const
export const defaultWeddingBandsQuery = {
  pageNumber: 1,
  calculateCount: true,
  previousPages: false,
  pageSize: defaultPageSize,
  query: {
    id: null,
    categoryId: 2,
    metals: [],
    styles: [],
    stoneTypes: [],
    stoneShapes: [],
    bandWidth: {
      min: null,
      max: null
    },
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

// query template. Default values: defaultSettingsFiltersValues const
export const defaultSettingsQuery = {
  pageNumber: 1,
  calculateCount: true,
  previousPages: false,
  pageSize: defaultPageSize,
  query: {
    category: 'ring',
    stoneId: null,
    metals: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

export const defaultWeddingBandsSideStonesQuery = {
  bandId: null,
  bandWidth: 0
}

export const defaultShareLinkQuery = {
  stoneId: null,
  stoneSetId: null,
  settingId: null,
  bandId: null,
  jewelryItemId: null,
  finishedDesign: false,
  engraving: null,
  metalTypeCode: null,
  ringSize: null,
  width: null,
  finish: null,
  sideStones: []
}

export const defaultShareFormQuery = {
  senderName: null,
  senderEmail: null,
  message: null,
  recipientsEmail: [],
  receiveCopy: true,
  pageLink: null,
  options: {}
}

export const defaultWeddingBandsPlainDetails = {
  id: null,
  metalTypeCode: null,
  size: 0,
  width: 0,
  sideStones: []
}

export const defaultListItem = {
  stoneId: null,
  stoneSetId: null,
  jewelryItemId: null,
  bandId: null,
  settingId: null,
  isWax: false,
  finishedDesign: false,
  metalTypeCode: null,
  guid: null
}

export const itemsWithSelectedOptions = [
  'Ring',
  'Plain Band',
  'Wedding Band',
  'Setting_Ring',
  'Custom' // custom jewelry
]

export const itemIdByCategory = {
  Ring: 'jewelryItemId',
  Stone: 'stoneId',
  Pair: 'stoneSetId',
  Earring: 'jewelryItemId',
  Pendant: 'jewelryItemId',
  Necklace: 'jewelryItemId',
  Bracelet: 'jewelryItemId',
  'Plain Band': 'bandId',
  'Wedding Band': 'bandId',
  Setting_Ring: 'settingId',
  Setting_Earring: 'settingId',
  Setting_Pendant: 'settingId',
  Setting_Necklace: 'settingId'
}

export const urlShapeToFilter = {
  emeraldCut: 'Emerald Cut',
  oval: 'Oval',
  asscher: 'Asscher',
  round: 'Round',
  cushion: 'Cushion',
  radiant: 'Radiant',
  heart: 'Heart'
}

export const urlOriginToFilter = {
  colombia: 'Colombia',
  zambia: 'Zambia',
  afghanistan: 'Afghanistan',
  ethiopia: 'Ethiopia',
  madagascar: 'Madagascar',
  pakistan: 'Pakistan',
  russia: 'Russia',
  brazil: 'Brazil',
  zimbabwe: 'Zimbabwe',
  mozambique: 'Mozambique',
  burmaMyanmar: 'Burma (Myanmar)',
  thailandSiam: 'Thailand (Siam)',
  ceylon: 'Ceylon',
  montana: 'Montana',
  nigeria: 'Nigeria',
  vietnam: 'Vietnam',
  tajikistan: 'Tajikistan',
  thailand: 'Thailand',
  tanzania: 'Tanzania',
  sriLankaCeylon: 'Sri Lanka (Ceylon)',
  cambodia: 'Cambodia',
  australia: 'Australia',
  kashmir: 'Kashmir'
}

export const stonesByCategory = ['Stone', 'Pair']

export const metalsFinishMap = {
  hammer: 'details/metal-finish/hammer@2x.jpg',
  hammerFull: 'details/metal-finish/hammerFull@2x.jpg',
  highPolish: 'details/metal-finish/highPolish@2x.jpg',
  highPolishFull: 'details/metal-finish/highPolishFull@2x.jpg',
  sandBlast: 'details/metal-finish/sandBlast@2x.jpg',
  sandBlastFull: 'details/metal-finish/sandBlastFull@2x.jpg',
  satin: 'details/metal-finish/satin@2x.jpg',
  satinFull: 'details/metal-finish/satinFull@2x.jpg',
  crossSatin: 'details/metal-finish/crossSatin@2x.jpg',
  crossSatinFull: 'details/metal-finish/crossSatinFull@2x.jpg',
  satinFinish: 'details/metal-finish/satinFinish@2x.jpg',
  satinFinishFull: 'details/metal-finish/satinFinishFull@2x.jpg',
  stoneFinish: 'details/metal-finish/stoneFinish@2x.jpg',
  stoneFinishFull: 'details/metal-finish/stoneFinishFull@2x.jpg',
  wireMatte: 'details/metal-finish/wireMatt@2x.jpg',
  wireMatteFull: 'details/metal-finish/wireMattFull@2x.jpg'
}
