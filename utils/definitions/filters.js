// There is no featured first b/c this is default sort
export const urlToStoreSort = {
  price: 'sortByPrice',
  weight: 'sortByWeight',
  'new-first': 'newFirst'
}

export const newSortingNamesMap = {
  'featured-first': 'FeaturedFirst',
  price: 'Price',
  'metal-weight': 'MetalWeight',
  'new-first': 'NewFirst',
  'carat-weight': 'TotalCarat'
}

export const urlToStoreFilters = {
  clarity: 'clarity',
  intensity: 'intensity',
  'cutting-styles': 'cuttingStyles',
  'lab-colors': 'labColors',
  'lab-types': 'labTypes',
  availability: 'availability',
  'length-max': 'lengthRange',
  'length-min': 'lengthRange',
  origins: 'stoneOrigins',
  'price-max': 'priceRange',
  'price-min': 'priceRange',
  shapes: 'stoneShapes',
  'weight-max': 'caratRange',
  'weight-min': 'caratRange',
  'width-max': 'widthRange',
  'width-min': 'widthRange',
  'band-width-max': 'bandWidthRange',
  'band-width-min': 'bandWidthRange',
  metals: 'metalTypes',
  treatments: 'treatments',
  'stone-types': 'stoneTypes',
  'design-styles': 'designStyles',
  styles: 'styles',
  'center-stone-shapes': 'centerStoneShapes',
  'side-stone-shapes': 'sideStoneShapes',
  'sort-by': 'sortBy',
  order: 'order'
}

export const filtersNamesMap = {
  clarity: 'clarity',
  cuttingStyles: 'cutting-styles',
  intensity: 'intensity',
  labColors: 'lab-colors',
  labTypes: 'lab-types',
  availability: 'availability',
  caratRange: 'weight',
  lengthRange: 'length',
  priceRange: 'price',
  stoneOrigins: 'origins',
  stoneShapes: 'shapes',
  widthRange: 'width',
  bandWidthRange: 'band-width',
  metalTypes: 'metals',
  treatments: 'treatments',
  categories: 'categories',
  centerStoneShapes: 'center-stone-shapes',
  sideStoneShapes: 'side-stone-shapes',
  designStyles: 'design-styles',
  styles: 'styles',
  stoneTypes: 'stone-types',
  sortBy: 'sort-by',
  order: 'order'
}

export const sourceRangeFiltersNamesMap = {
  bandWidthRange: 'bandWidthRange',
  priceRange: 'priceRange',
  caratRange: 'caratRange',
  lengthRange: 'dimensions.lengthRange',
  widthRange: 'dimensions.widthRange'
}

export const notAreaFilters = ['availability']

export const validUrlFilterNames = [
  'clarity',
  'intensity',
  'cutting-styles',
  'lab-colors',
  'lab-types',
  // 'availability',
  'length-max',
  'length-min',
  'origins',
  'price-max',
  'price-min',
  'shapes',
  'weight-max',
  'weight-min',
  'width-max',
  'width-min',
  'band-width-max',
  'band-width-min',
  'metals',
  'treatments',
  'stone-types',
  'styles',
  'design-styles',
  'center-stone-shapes',
  'side-stone-shapes',
  'sort-by',
  'order'
]
