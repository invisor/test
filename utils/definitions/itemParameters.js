export const sideStonesAvailableParameters = [
  'weight',
  'grade',
  'position',
  'clarity'
]

export const availableParameters = [
  'prong',
  'ringSize',
  'vedicSetting',
  'engraving',
  'finish',
  'ringWidth',
  ...sideStonesAvailableParameters
]

export const sideStonesParametersNamesMap = {
  weight: 'w',
  grade: 'g',
  position: 'p',
  clarity: 'c'
}

export const parametersNamesMap = {
  ringSize: 'rs',
  vedicSetting: 'vs',
  engraving: 'e',
  finish: 'f',
  ringWidth: 'rw',
  prong: 'pr',
  ...sideStonesParametersNamesMap
}
