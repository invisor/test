export default function getMenuList() {
  return [
    {
      id: '4dcc72cf-8a19-4e57-a3d6-05e97e404c70',
      name: this.$t('account.menu.myAccount'),
      routeName: 'account'
    },
    {
      id: '8458ea83-74d2-4b53-9de3-557d9e31e20d',
      name: this.$t('account.menu.changePassword'),
      routeName: 'account-change-password'
    },
    {
      id: 'd33ccde1-f666-4ada-8433-b0e1fb545799',
      name: this.$t('account.menu.orderHistory'),
      routeName: 'account-orders'
    },
    {
      id: 'cc3a93f1-684c-4a5f-b0f1-8b0ddef95fb9',
      name: this.$t('account.menu.wishList'),
      routeName: 'wishlist'
    }
  ]
}
