export default function getMenuList() {
  return [
    {
      id: 'abb1b5ab-4d70-43d1-aa01-4dec94616855',
      name: this.$t('layout.mainMenu.create'),
      icon: 'ring',
      view: 'dyo',
      leftItems: {
        shopByDesignStyle: [
          {
            id: '4cbf4859-16a3-47de-bc79-2617fb06d97b',
            icon: 'antiqueRing',
            text: this.$t('layout.mainMenu.antique'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 32, // antique
                metals: 5
              }
            }
          },
          {
            id: '63d3c3b5-62ef-475f-b274-3bd5a74acee2',
            icon: 'haloRing',
            text: this.$t('layout.mainMenu.halo'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 30, // halo
                metals: 5
              }
            }
          },
          {
            id: '11f9bfe9-f35a-45d6-adec-0af66159564f',
            icon: 'paveRing',
            text: this.$t('layout.mainMenu.pave'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 29, // pave
                metals: 5
              }
            }
          },
          {
            id: '9ed7f56d-e841-4d8a-ab05-aa0c2f0f2e0b',
            icon: 'accentStonesRing',
            text: this.$t('layout.mainMenu.accentStones'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 31, // side stone
                metals: 5
              }
            }
          },
          {
            id: 'fb73b54a-cdf2-45f6-a37e-bbd994d1a764',
            icon: 'solitaireRing',
            text: this.$t('layout.mainMenu.solitaire'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 27, // solitaire
                metals: 5
              }
            }
          },
          {
            id: '10783c15-db77-4432-8a4e-b53461e0f527',
            icon: 'threeStoneRing',
            text: this.$t('layout.mainMenu.threeStone'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 28, // three stone
                metals: 5
              }
            }
          },
          {
            id: '70697df2-778d-40fa-a2b4-f71d703f97cc',
            icon: 'weddingSetRing',
            text: this.$t('layout.mainMenu.weddingSet'),
            to: {
              name: 'design-your-own-setting-emerald-engagement-ring-settings',
              query: {
                'design-styles': 33, // wedding set
                metals: 5
              }
            }
          }
        ]
      },
      rightItems: {
        shopByCarat: [
          {
            id: '470eb57e-17f0-4864-bf5b-e732eea7cf19',
            text: `1-2 ${this.$t('layout.mainMenu.carat')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 1,
                'weight-max': 2
              }
            }
          },
          {
            id: '56a80421-bc5e-4e5c-8267-320a14572db9',
            text: `2-3 ${this.$t('layout.mainMenu.carat')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 2,
                'weight-max': 3
              }
            }
          },
          {
            id: 'ff503849-ff10-42b6-a12b-d5527e642c14',
            text: `3-4 ${this.$t('layout.mainMenu.carat')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 3,
                'weight-max': 4
              }
            }
          },
          {
            id: '6474c047-074a-4f2d-a4c1-47982300199b',
            text: `4-5 ${this.$t('layout.mainMenu.carat')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 4,
                'weight-max': 5
              }
            }
          },
          {
            id: '03dcaa20-398d-4621-908a-21e7b7f46bc2',
            text: `5-10 ${this.$t('layout.mainMenu.carat')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 5,
                'weight-max': 10
              }
            }
          },
          {
            id: '5482d91e-6524-40c0-834b-0b589baf7307',
            text: `10 ${this.$t('layout.mainMenu.carat')} ${this.$t(
              'layout.mainMenu.andAbove'
            )}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'weight-min': 10
              }
            }
          }
        ],
        shopByPrice: [
          {
            id: '7669b486-aca9-4d3d-8aea-8b1972c2d710',
            text: '$1,000-2,000 ',
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'price-min': 1000,
                'price-max': 2000
              }
            }
          },
          {
            id: '64627943-45b6-432a-97d3-6d7d8d1add5b',
            text: '$2,000-5,000 ',
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'price-min': 2000,
                'price-max': 5000
              }
            }
          },
          {
            id: 'd8d873cf-3cdc-46fe-9d8f-2c0e8edb0b01',
            text: '$5000-10,000 ',
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'price-min': 5000,
                'price-max': 10000
              }
            }
          },
          {
            id: '6a1bcc05-ca43-4a61-92c5-314ebb2f0957',
            text: `10,000 ${this.$t('layout.mainMenu.andAbove')}`,
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                'price-min': 10000
              }
            }
          }
        ],
        shopByShape: [
          {
            id: 'e0f6b809-b85b-419f-ba4e-9a9aa54ec151',
            text: this.$t('layout.mainMenu.emeraldCut'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Emerald Cut'
              }
            }
          },
          {
            id: '8c46a733-07d3-4b71-b325-b53ecb1d14d0',
            text: this.$t('layout.mainMenu.oval'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Oval'
              }
            }
          },
          {
            id: '0a42bd42-5800-4b33-a042-25571c842033',
            text: this.$t('layout.mainMenu.asscher'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Asscher'
              }
            }
          },
          {
            id: 'c46b7631-41a6-4f7a-9f4e-a5833c56b5d5',
            text: this.$t('layout.mainMenu.round'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Round'
              }
            }
          },
          {
            id: '14a02841-365b-4e4b-a3c1-d267ff89b1b7',
            text: this.$t('layout.mainMenu.cushion'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Cushion'
              }
            }
          },
          {
            id: 'f776b968-0333-4a8e-9d48-932b8527c96c',
            text: this.$t('layout.mainMenu.radiant'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Radiant'
              }
            }
          },
          {
            id: 'e4d62558-3b18-4c35-bbb6-90755b821da8',
            text: this.$t('layout.mainMenu.heart'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                shapes: 'Heart'
              }
            }
          }
        ],
        shopByOrigin: [
          {
            id: '5d10ffd3-1eec-4615-9eb9-b58fdc6338e0',
            text: this.$t('layout.mainMenu.zambia'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Zambia'
              }
            }
          },
          {
            id: 'f0d71880-640b-4960-b6e1-4d820a76b953',
            text: this.$t('layout.mainMenu.colombia'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Colombia'
              }
            }
          },
          {
            id: '03451967-e00b-48d8-aeb4-c892b2456a25',
            text: this.$t('layout.mainMenu.ethiopia'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Ethiopia'
              }
            }
          },
          {
            id: 'e5481d10-c851-4f61-b769-b34542b7a777',
            text: this.$t('layout.mainMenu.brazil'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Brazil'
              }
            }
          },
          {
            id: '58f97fa6-8a76-4030-92ed-a390da37c686',
            text: this.$t('layout.mainMenu.zimbabwe'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Zimbabwe'
              }
            }
          },
          {
            id: '51af2382-c3a3-4180-9800-7ea6b0cf8419',
            text: this.$t('layout.mainMenu.afghanistan'),
            to: {
              name: 'emeralds',
              query: {
                'design-your-own': 1,
                origins: 'Afghanistan'
              }
            }
          }
        ]
      }
    },
    {
      id: '76d7c1bb-dc79-4933-aa73-482aae06a0be',
      name: this.$t('layout.mainMenu.emeraldRings'),
      view: 'simple',
      width: 600,
      class: 'rings',
      // img: '~/assets/images/hi-res/mainMenu/rings@2x.png',
      items: [
        {
          id: '86e6d2fe-601d-4966-a8ca-4c96803166ca',
          text: this.$t('layout.mainMenu.emeraldRings'),
          to: {
            name: 'emerald-jewelry-emerald-rings'
          }
        },
        {
          id: '5ed4caa1-a462-47c5-8edd-b15a211c4f42',
          text: this.$t('layout.mainMenu.emeraldEngagementRings'),
          to: {
            name: 'emerald-jewelry-engagement-emerald-rings'
          }
        },
        {
          id: 'c9084c4e-86c6-48b9-a6f6-c548acda6fd1',
          text: this.$t('layout.mainMenu.emeraldWeddingBands'),
          to: {
            name: 'emerald-wedding-rings-bands'
          }
        }
      ]
    },
    {
      id: 'b1a89ee9-7e21-483c-ab36-11e26cd7bdd6',
      name: this.$t('layout.mainMenu.jewelry'),
      view: 'simple',
      width: 600,
      class: 'jewelries',
      items: [
        {
          id: '3ce29a56-1f99-4174-a690-890f9b1f6c4d',
          text: this.$t('layout.mainMenu.rings'),
          itemType: 'ring',
          to: {
            name: 'emerald-jewelry-engagement-emerald-rings'
          }
        },
        {
          id: 'a28ee56c-ba9c-44bc-8ae7-e56b99d0a8d9',
          text: this.$t('layout.mainMenu.earrings'),
          itemType: 'earrings',
          to: {
            name: 'emerald-jewelry-emerald-earrings'
          }
        },
        {
          id: '2976be59-c95a-4ac3-b372-7a0b2a5f98de',
          text: this.$t('layout.mainMenu.necklacesAndPendants'),
          itemType: 'pendant',
          to: {
            name: 'emerald-jewelry-emerald-necklaces-pendants'
          }
        },
        {
          id: '140e3bfd-d9bf-45cd-abd7-69c5bb7097c7',
          text: this.$t('layout.mainMenu.bracelets'),
          itemType: 'bracelet',
          to: {
            name: 'emerald-jewelry-emerald-bracelets'
          }
        },
        {
          id: 'c9084c4e-86c6-48b9-a6f6-c548acda6fd1',
          text: this.$t('layout.mainMenu.weddingBands'),
          itemType: 'wedding-band',
          to: {
            name: 'emerald-wedding-rings-bands'
          }
        },
        {
          id: '6c9b0e8b-d63b-4a56-8483-88f8254092c3',
          text: this.$t('layout.mainMenu.weddingBandsPlain'),
          itemType: 'plain-band',
          to: {
            name: 'wedding-bands-without-gemstone'
          }
        },
        {
          id: '14dc5c30-75ac-4465-9308-be3e199f69d0',
          text: this.$t('layout.mainMenu.menSRings'),
          to: {
            name: 'emerald-jewelry-mens-emerald-rings'
          }
        }
      ]
    },
    {
      id: '0cfe184b-8351-46d7-8a3c-e21f02b484eb',
      name: this.$t('layout.mainMenu.looseEmeralds'),
      view: 'simple',
      width: 600,
      class: 'stones',
      items: [
        {
          id: '99c622a1-f0fd-4276-826e-d1308baa9f9a',
          text: this.$t('layout.mainMenu.emeralds'),
          to: 'emeralds'
        },
        {
          id: 'd5f74069-d1d2-4318-8dd6-7be893201ce5',
          text: this.$t('layout.mainMenu.pairsOfEmeralds'),
          to: 'emeralds-matched-pairs-of-emeralds'
        },
        {
          id: '3aa3547b-e5f9-4b4c-b97d-d169ffc04925',
          text: this.$t('layout.mainMenu.emeraldCabochon'),
          to: 'emeralds-emerald-cabochon'
        },
        {
          id: '6660e021-89ad-49da-9d6e-d3faae185b25',
          text: this.$t('layout.mainMenu.pairsCabochon'),
          to: 'emeralds-matched-pairs-of-emeralds-cabochon'
        },
        {
          id: '8d3738d5-ac09-4d03-a81d-817dbc14785d',
          text: this.$t('layout.mainMenu.gemologistRecommended'),
          to: 'emeralds-gemologist-recommended'
        }
      ]
    },
    {
      id: '8c4981dd-a77f-413d-acc9-991969a71a25',
      name: this.$t('layout.mainMenu.origin'),
      view: 'simple',
      width: 600,
      class: 'origin',
      items: [
        {
          id: '99c622a1-f0fd-4276-826e-d1308baa9f9a',
          text: this.$t('layout.mainMenu.colombianEmeralds'),
          to: 'emeralds-colombian-emeralds'
        },
        {
          id: 'd5f74069-d1d2-4318-8dd6-7be893201ce5',
          text: this.$t('layout.mainMenu.colombianEmeraldRings'),
          to: 'emerald-jewelry-colombian-emerald-rings'
        },
        {
          id: '45bf23b4-21b6-460c-9d98-5a461e371181',
          text: this.$t('layout.mainMenu.zambianEmeralds'),
          to: 'emeralds-zambian-emeralds'
        },
        {
          id: '0f09403a-de69-4380-9113-3fe2124d6981',
          text: this.$t('layout.mainMenu.zambianEmeraldRings'),
          to: 'emerald-jewelry-zambian-emerald-rings'
        }
      ]
    },
    {
      id: '60e529c2-657f-40b7-9caa-c9869ef00796',
      name: this.$t('layout.mainMenu.settings'),
      view: 'simple',
      width: 600,
      class: 'settings',
      items: [
        {
          id: '00cfd293-c661-4159-892a-ffbc4560a0c1',
          text: this.$t('layout.mainMenu.rings'),
          to: {
            name: 'design-your-own-setting-emerald-engagement-ring-settings',
            query: {
              metals: 5
            }
          }
        },
        {
          id: '44ff0399-3671-47ed-a099-cf92f1c46623',
          text: this.$t('layout.mainMenu.earrings'),
          to: {
            name: 'design-your-own-setting-emerald-earring-settings',
            query: {
              metals: 5
            }
          }
        },
        {
          id: '90af71f5-36d6-46a2-957d-6aafcdd42c89',
          text: this.$t('layout.mainMenu.necklacesAndPendants'),
          to: {
            name: 'design-your-own-setting-emerald-necklace-pendant-settings',
            query: {
              metals: 5
            }
          }
        },
        {
          id: 'aa6e4286-6b7b-4ad2-abd5-6fc43c1afada',
          text: this.$t('layout.mainMenu.menSRings'),
          to: {
            name: 'design-your-own-setting-mens-ring-settings',
            query: {
              metals: 5
            }
          }
        }
      ]
    },
    {
      id: '120e017c-5938-4665-a7aa-8078593972b8',
      name: this.$t('layout.mainMenu.faq'),
      view: 'simple',
      width: 400,
      class: 'faq',
      // img: '~/assets/images/original/main-menu/faq-item.jpg',
      items: [
        {
          id: 'e4193696-9fc8-40f7-81fc-e90c6293c662',
          text: this.$t('layout.mainMenu.general'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#gq'
          },
          type: 'nuxt'
        },
        {
          id: '1423c374-73c3-4a9a-80ca-83fc877a5814',
          text: this.$t('layout.mainMenu.shipping'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#shipping'
          },
          type: 'nuxt'
        },
        {
          id: '5986e08f-b3c8-4191-b23d-faacc5a2923d',
          text: this.$t('layout.mainMenu.taxes'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#usa-taxes'
          },
          type: 'nuxt'
        },
        {
          id: '640754e8-a523-4dc7-ad2f-e1f13508942a',
          text: this.$t('layout.mainMenu.payment'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#payment'
          },
          type: 'nuxt'
        },
        {
          id: 'c71c77e0-a4ba-4453-aca3-da2ce8f36b95',
          text: this.$t('layout.mainMenu.ringSizing'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#ring-sizing'
          },
          type: 'nuxt'
        },
        {
          id: '117a4200-9fb1-4289-8eed-98787a8ebf23',
          text: this.$t('layout.mainMenu.return'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#return-policy'
          },
          type: 'nuxt'
        },
        {
          id: 'bcaf9bf9-9d73-4b2e-8c2a-d7288a4c9ee6',
          text: this.$t('layout.mainMenu.location'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#our-location'
          },
          type: 'nuxt'
        },
        {
          id: 'e469f072-64b7-4f46-b72e-e8f80ef8cb8f',
          text: this.$t('layout.mainMenu.security'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#security'
          },
          type: 'nuxt'
        },
        {
          id: '8bef422e-7668-4c6c-aa40-fc34b8e482e8',
          text: this.$t('layout.mainMenu.privacy'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#privacy-policy'
          },
          type: 'nuxt'
        }
      ]
    },
    {
      id: 'caa191f8-c3ba-4d30-bcca-45a2e0010d45',
      name: this.$t('layout.mainMenu.about'),
      view: 'simple',
      width: 400,
      class: 'about',
      items: [
        {
          id: 'c0e9e731-9f9f-4046-a790-9c90009ab708',
          text: this.$t('layout.mainMenu.different'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'why-choose-us'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'a0e5e8f8-0815-4598-9748-369c3db30332',
          text: this.$t('layout.mainMenu.workshop'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-workshop'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'f9ffa566-cb5a-49ac-8717-a3aafff884c3',
          text: this.$t('layout.mainMenu.ourHistory'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-history'
            }
          },
          type: 'nuxt'
        },
        {
          id: '73cace3f-5fdf-42ff-aa0f-0741b75fec23',
          text: this.$t('layout.mainMenu.ourShowroom'),
          to: {
            name: 'showroom'
          },
          type: 'nuxt'
        },
        {
          id: '3f86a269-36a5-4611-8b40-c27e34937999',
          text: this.$t('layout.mainMenu.ourStaff'),
          to: {
            name: 'our-staff'
          },
          type: 'nuxt'
        },
        {
          id: '812092be-4006-442d-bc01-799a1c6a3f7e',
          text: this.$t('layout.mainMenu.ourTechnology'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-technology'
            }
          },
          type: 'nuxt'
        },
        {
          id: '96ac6ab2-3938-49c6-ad4e-8d7a31c8f2af',
          text: this.$t('layout.mainMenu.gemLab'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-laboratory'
            }
          },
          type: 'nuxt'
        },
        {
          id: '98323554-9988-491d-a55a-885f61061a22',
          text: this.$t('layout.mainMenu.contactUs'),
          to: {
            name: 'contact-us'
          },
          type: 'nuxt'
        }
      ]
    },
    {
      id: '3a4c88be-02df-4d95-9d0d-d02529355658',
      name: this.$t('layout.mainMenu.education'),
      view: 'simple',
      width: 400,
      class: 'education',
      items: [
        {
          id: 'b7eb3f90-b714-49c1-aae8-927030bffb28',
          text: this.$t('layout.mainMenu.emeraldEducation'),
          to: {
            name: 'education-index'
          },
          type: 'nuxt'
        },
        {
          id: '0ded91cf-0609-4ff4-9611-35598c539f39',
          text: this.$t('layout.mainMenu.essentials'),
          to: {
            name: 'education-index-category-slug',
            params: {
              category: 'emerald-characteristics',
              slug: 'the-nature-of-emeralds'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'ff79d03c-6573-4c13-a88f-37073b90d1d1',
          text: this.$t('layout.mainMenu.miningLocations'),
          // to: `${process.env.wpDomain}/mining-locations/`,
          to: {
            name: 'education-index-category',
            params: {
              category: 'mining-locations'
            },
            query: {
              type: 'list'
            }
          },
          type: 'nuxt'
        },
        {
          id: '1df13546-9fa9-4094-8287-3aa4e8b3cf3b',
          text: this.$t('layout.mainMenu.historyAndUsege'),
          // to: `${process.env.wpDomain}/emerald-history/`,
          to: {
            name: 'education-index-category',
            params: {
              category: 'emerald-history'
            },
            query: {
              type: 'list'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'b8baa940-0b09-428b-a8be-d70641d17c7b',
          text: this.$t('layout.mainMenu.emeraldsJewelry'),
          to: {
            name: 'education-index-category',
            params: {
              category: 'emerald-jewelry'
            },
            query: {
              type: 'list'
            }
          },
          type: 'nuxt'
          // to: `${process.env.wpDomain}/emerald-jewelry/`,
          // type: 'native'
        }
      ]
    }
  ]
}
