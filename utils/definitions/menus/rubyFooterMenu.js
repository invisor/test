export default function getMenuList() {
  return [
    {
      id: 'fee921d2-8c5a-44d1-b4ba-106b2a8cd99f',
      name: this.$t('layout.footer.menu.aboutUs.title'),
      items: [
        {
          id: 'fa05a0b6-8d67-4787-8378-fada19c481f0',
          name: this.$t('layout.footer.menu.aboutUs.item1'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'why-choose-us'
            }
          },
          type: 'nuxt'
        },
        {
          id: '92a2617a-18a5-4592-b10d-525f5aae056d',
          name: this.$t('layout.footer.menu.aboutUs.item2'),
          link: 'https://www.instagram.com/naturalsapphire.charity/',
          type: 'native',
          target: '_blank'
        },
        {
          id: '09f65f87-c563-4b47-92bd-b50147abe224',
          name: this.$t('layout.footer.menu.aboutUs.item3'),
          link: {
            name: 'our-staff'
          },
          type: 'nuxt'
        },
        // {
        //   id: '133b3820-1265-4128-9267-0dfd4f092bfd',
        //   name: this.$t('layout.footer.menu.aboutUs.item4'),
        //   link: `${process.env.wpDomain}`,
        //   type: 'native'
        // },
        {
          id: '892f63be-54e4-4776-bc54-7d643b66887f',
          name: this.$t('layout.footer.menu.aboutUs.item5'),
          link: {
            name: 'showroom'
          },
          type: 'nuxt'
        },
        {
          id: 'da5fa3f6-e1aa-4933-bb51-000fa2c2ef20',
          name: this.$t('layout.footer.menu.aboutUs.item6'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-technology'
            }
          },
          type: 'nuxt'
        },
        {
          id: '8169e91a-23cb-4554-926a-9759382bc1d8',
          name: this.$t('layout.footer.menu.aboutUs.item7'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-workshop'
            }
          },
          type: 'nuxt'
        },
        {
          id: '611860e0-ed1c-4c0c-b486-5c8d0c84c8c5',
          name: this.$t('layout.footer.menu.aboutUs.item8'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'about-us',
              slug: 'our-laboratory'
            }
          },
          type: 'nuxt'
        },
        {
          id: '67226e5c-5fe0-4d64-893c-6ed6130e0339',
          name: this.$t('layout.footer.menu.aboutUs.item9'),
          link:
            'https://www.thenaturalsapphirecompany.com/education/about-us/in-the-press/',
          type: 'native',
          target: '_blank'
        }
      ]
    },
    {
      id: 'dfb23c2c-46af-44a0-b8fd-7025e6fe4d4a',
      name: this.$t('layout.footer.menu.customerCare.title'),
      items: [
        {
          id: '95f130ba-8e63-45e3-983f-31468553570f',
          name: this.$t('layout.footer.menu.customerCare.item1'),
          link: {
            name: 'contact-us'
          },
          type: 'nuxt'
        },
        {
          id: '29e74e8e-19ac-4db8-a661-4d13412e11ec',
          name: this.$t('layout.footer.menu.customerCare.item2'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#gq'
          },
          type: 'nuxt'
        },
        {
          id: 'a5df0a0f-1aba-4015-a84e-6e0e4cad3eb8',
          name: this.$t('layout.footer.menu.customerCare.item3'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#return-policy'
          },
          type: 'nuxt'
        },
        {
          id: '1d824567-662c-4f83-b70c-3bc7e12a6a59',
          name: this.$t('layout.footer.menu.customerCare.item4'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#our-location'
          },
          type: 'nuxt'
        },
        {
          id: '8733db24-0e56-49ee-840b-76b9c1863e2d',
          name: this.$t('layout.footer.menu.customerCare.item5'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#ring-sizing'
          },
          type: 'nuxt'
        },
        {
          id: '1b701f18-8016-417a-9723-434f80cae13d',
          name: this.$t('layout.footer.menu.customerCare.item6'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#payment'
          },
          type: 'nuxt'
        },
        {
          id: '47f6a84d-9c85-48f3-a1b1-4da66a1860e7',
          name: this.$t('layout.footer.menu.customerCare.item7'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#shipping'
          },
          type: 'nuxt'
        },
        {
          id: '088e5c91-360f-45dd-ba6c-1151bd28b57f',
          name: this.$t('layout.footer.menu.customerCare.item8'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#security'
          },
          type: 'nuxt'
        },
        {
          id: 'b119f9c8-1e0b-4e60-a10a-a7e27ea1833b',
          name: this.$t('layout.footer.menu.customerCare.item9'),
          link: {
            name: 'education-index-category',
            params: {
              category: 'frequently-asked-questions'
            },
            hash: '#privacy-policy'
          },
          type: 'nuxt'
        }
      ]
    },
    {
      id: 'dcf16ebd-96cc-4361-9c5f-18892f08adff',
      name: this.$t('layout.footer.menu.education.title'),
      items: [
        {
          id: 'c37f099e-4516-4a2a-a62d-eb93dce9480b',
          name: this.$t('layout.footer.menu.education.item1'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-characteristics',
              slug: 'the-nature-of-rubies'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'cb56a315-3c69-493f-8cf7-b49dc34992a5',
          name: this.$t('layout.footer.menu.education.item2'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-characteristics',
              slug: 'judging-ruby-quality'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'b021db00-3715-4b48-99cb-678b3a1ef44a',
          name: this.$t('layout.footer.menu.education.item3'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-characteristics',
              slug: 'ruby-rarity-value'
            }
          },
          type: 'nuxt'
        },
        {
          id: '68b1cb14-51a6-4385-b60d-1d11fa31577a',
          name: this.$t('layout.footer.menu.education.item4'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-characteristics',
              slug: 'ruby-treatments-enhancements'
            }
          },
          type: 'nuxt'
        },
        {
          id: 'bb336b49-92fa-437f-aaf1-3a2b7f3146fa',
          name: this.$t('layout.footer.menu.education.item5'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-history',
              slug: 'famous-rubies'
            }
          },
          type: 'nuxt'
        },
        {
          id: '2c2b890e-22f0-4632-906b-3f417e818738',
          name: this.$t('layout.footer.menu.education.item6'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-jewelry',
              slug: 'ruby-buying-tips'
            }
          },
          type: 'nuxt'
        },
        {
          id: '70151e67-a386-4728-a4c0-4b92f0535cc6',
          name: this.$t('layout.footer.menu.education.item7'),
          link: {
            name: 'education-index-category-slug',
            params: {
              category: 'ruby-jewelry',
              slug: 'caring-for-ruby-jewelry'
            }
          },
          type: 'nuxt'
        }
      ]
    }
  ]
}
