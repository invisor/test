import get from 'lodash/get'
import invert from 'lodash/invert'
import BaseFetchDetails from './_baseFetch'
import { parametersNamesMap } from '~~/utils/definitions/itemParameters'
import { getDetailsPath, getPreferableRingSize } from '~~/utils/utils'

export default class FetchDetails extends BaseFetchDetails {
  get pathPattern() {
    return /(JS\d+|js\d+)([A-Za-z\d]+)-(.+)$/gm
  }

  async parseRoute() {
    const path = this.route.params.id
    this.source = this.route.query.source

    const result = this.pathPattern.exec(path)
    if (result) {
      this.settingId = result[1]
      this.metalTypeCode = result[2]
      this.stoneId = result[3]

      this.stoneParams = {
        id: this.stoneId,
        type: this.stoneId.toLowerCase().includes('pr')
          ? 'stonePairsList'
          : 'stonesList'
      }

      this.settingParams = {
        id: this.settingId,
        metalTypeCode: this.metalTypeCode,
        type: 'ringSettings'
      }

      const data = {
        settingId: this.settingId,
        metalCode: this.metalTypeCode,
        stoneId: this.stoneId
      }

      await this.isItemsMatch(data)
    } else {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }

  async fetch() {
    await this.parseRoute()
    if (this.isNewItem()) {
      // it's a new item. Wee need to reset selected options and etc before fetch new one
      this.store.dispatch('customItem/confirmRingSizeReset')
      this.store.dispatch('customItem/resetSelectedOptions')
    }
    try {
      await this.fetchDetails()
    } catch (e) {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
    this.getMetaData()
  }

  isNewItem() {
    const newId = `${this.settingId}-${this.stoneId}`.toLowerCase()
    const oldId =
      `${this.customSetting.id}-${this.customStone.id}`.toLowerCase()

    return newId !== oldId
  }

  async isItemsMatch(data) {
    const result = await this.store.dispatch('customItem/isStoneMatch', data)
    return result
  }

  validateDetailsPath() {
    const validPath = this.localePath(
      getDetailsPath(this.store.getters['customItem/customItem'])
    )
    if (validPath !== this.route.path) this.context.redirect(301, validPath)
  }

  get customStone() {
    return this.store.getters['customItem/customStone']
  }

  get customSetting() {
    return this.store.getters['customItem/customSetting']
  }

  // item was created manually and we shouldn't reset selected options
  get isCustomItem() {
    return (
      this.stoneId.toUpperCase() === this.customStone.id &&
      this.settingId.toUpperCase() === this.customSetting.id &&
      this.metalTypeCode.toUpperCase() === this.customSetting.metalTypeCode
    )
  }

  get isFetched() {
    return this.isCustomItem && !this.store.state.customItem.isDirty
  }

  async getAvailableDesigns() {
    await this.store.dispatch('customItem/getAvailableDesigns')
  }

  async fetchDetails() {
    await this.fetchDetailsSources()
    await this.getAvailableDesigns()
    this.validateDetailsPath()
  }

  async fetchFromGround() {
    if (this.isFetched) return
    const preferableRingSizeId = getPreferableRingSize(this.cookies)
    const query = {
      id: this.settingId,
      stoneId: this.stoneId,
      metalTypeCode: this.metalTypeCode
    }

    if (preferableRingSizeId) query.size = preferableRingSizeId

    if (!this.store.state.customItem.isItemsMatch) return
    await this.getFinalOptions(query)
    await this.store.dispatch('customItem/setItemGuid', this.guid)
    this.setRouteParams()
    await this.fetchPrice()
  }

  get fetchedItem() {
    return this.store.getters['productDetails/productDetails']
  }

  async fetchStone() {
    await this.store.dispatch('productDetails/fetchDetails', this.stoneParams) // Fetch custom stone
    await this.store.dispatch('customItem/setCustomStone', this.fetchedItem)
  }

  async fetchSetting() {
    await this.store.dispatch('productDetails/fetchDetails', this.settingParams) // Fetch custom setting
    await this.store.dispatch('customItem/setCustomSetting', this.fetchedItem)
  }

  async getFinalOptions(query) {
    await this.store.dispatch('customItem/getFinalOptions', query)
  }

  setRouteParams() {
    const queryToStore = invert(parametersNamesMap)
    Object.keys(this.route.query).forEach((p) => {
      const paramName = queryToStore[p]
      if (paramName) {
        const pattern = /weight|grade|position|clarity/
        if (pattern.test(paramName)) {
          // parameters can be received as string or as array from browser url
          if (Array.isArray(this.route.query[p])) {
            this.route.query[p].forEach((v, index) => {
              const value = ['grade', 'clarity'].includes(paramName)
                ? Number(this.route.query[p][index])
                : this.route.query[p][index]
              this[`${paramName}SetParam`](paramName, value, index)
            })
            return
          }
          const value = ['grade', 'clarity'].includes(paramName)
            ? Number(this.route.query[p])
            : this.route.query[p]
          this[`${paramName}SetParam`](paramName, value, 0)
          return
        }
        const numValue = Number(this.route.query[p])
        const value = Number.isNaN(numValue) ? this.route.query[p] : numValue
        this[`${paramName}SetParam`](paramName, value)
      }
    })
  }

  ringSizeSetParam(key, value) {
    const { sizes } = this.store.state.customItem.customSetting
    if (sizes) {
      const val = sizes.find((size) => size.key === value)
      if (val)
        this.store.dispatch('customItem/setSettingOptions', { [key]: val })
    }
  }

  vedicSettingSetParam(key, value) {
    this.store.dispatch('customItem/setSettingOptions', { [key]: !!value })
  }

  positionSetParam(key, value, index) {
    const { position } = get(
      this.store,
      `state.customItem.sideStones[${index}]`,
      ''
    )
    if (position) {
      this.store.dispatch('customItem/setSideStonesOptions', {
        index,
        value: position,
        type: 'position'
      })
    }
  }

  claritySetParam(key, value, index) {
    const { clarities } = get(
      this.store,
      `state.customItem.sideStones[${index}]`,
      {}
    )
    if (clarities) {
      const val = clarities.find((clarity) => clarity.id === Number(value))
      if (val)
        this.store.dispatch('customItem/setSideStonesOptions', {
          index,
          value: val,
          type: 'clarity'
        })
    }
  }

  prongSetParam(key, value) {
    const { prongs } = this.store.state.customItem.customSetting
    if (prongs) {
      const val = prongs.find((prong) => prong.id === Number(value))
      if (val)
        this.store.dispatch('customItem/setSettingOptions', { [key]: val })
    }
  }

  gradeSetParam(key, value, index) {
    const { grades } = get(
      this.store,
      `state.customItem.sideStones[${index}]`,
      {}
    )
    if (grades) {
      const val = grades.find((grade) => grade.id === Number(value))
      if (val)
        this.store.dispatch('customItem/setSideStonesOptions', {
          index,
          value: val,
          type: 'grade'
        })
    }
  }

  weightSetParam(key, value, index) {
    const { weights } = get(
      this.store,
      `state.customItem.sideStones[${index}]`,
      {}
    )
    if (weights) {
      const val = weights.find((weight) => weight.id === value)
      if (val)
        this.store.dispatch('customItem/setSideStonesOptions', {
          index,
          value: val,
          type: 'weight'
        })
    }
  }

  engravingSetParam(key, value) {
    const val = String(value)
    if (val.trim())
      this.store.dispatch('customItem/setSettingOptions', { [key]: val })
  }

  async fetchPrice() {
    await this.store.dispatch('customItem/fetchPrice') // Fetch price
  }

  async setItemDetails(item) {
    await this.store.dispatch('customItem/setCustomItem', item)
  }

  async cartFetchTry() {
    this.store.dispatch('customItem/resetCustomItem')
    const cartItems = this.store.getters['cart/items']
    const item = cartItems.find(
      (item) =>
        item.id ===
        `${this.settingId}${this.metalTypeCode}-${this.stoneId}`.toUpperCase()
    )
    if (item) {
      await this.setItemDetails(item)
      return true
    }
    return false
  }

  async wishlistFetchTry() {
    this.store.dispatch('customItem/resetCustomItem')
    const wishlistItems = this.store.getters['favorites/customItems']
    const item = wishlistItems.find(
      (item) =>
        item.id ===
        `${this.settingId}${this.metalTypeCode}-${this.stoneId}`.toUpperCase()
    )

    if (item) {
      if (item.fromServer) {
        const { guid } = this.route.query
        // set up guid to update custom item from details
        if (guid) this.store.dispatch('customItem/setItemGuid', guid)
        return false
      }
      await this.setItemDetails(item)
      return true
    }
    return false
  }

  async compareFetchTry() {
    this.store.dispatch('customItem/resetCustomItem')
    const compareItems = this.store.getters['compareList/customItems']
    const item = compareItems.find(
      (item) =>
        item.id ===
        `${this.settingId}${this.metalTypeCode}-${this.stoneId}`.toUpperCase()
    )
    if (item) {
      await this.setItemDetails(item)
      return true
    }
    return false
  }
}
