import invert from 'lodash/invert'
import BaseFetchDetails from './_baseFetch'
import { parametersNamesMap } from '~~/utils/definitions/itemParameters'

export default class FetchDetails extends BaseFetchDetails {
  get pathPattern() {
    return /([a-zA-Z\d]+)$/gm
  }

  parseRoute() {
    const path = this.route.params.id
    this.type = 'braceletsList'

    const result = this.pathPattern.exec(path)
    if (result) {
      this.id = result[1]?.toUpperCase()
    } else {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }

  async fetchDetails() {
    await this.fetchDetailsOptions()
    await this.fetchDetailsSources()
    this.validateDetailsPath()
  }

  async fetchItemDetails() {
    const params = {
      type: this.type,
      id: this.id
    }

    const queryToStore = invert(parametersNamesMap)

    Object.keys(this.route.query).forEach((p) => {
      const param = queryToStore[p]
      if (param) {
        const numValue = Number(this.route.query[p])
        params[param] = Number.isNaN(numValue) ? this.route.query[p] : numValue
      }
    })

    await this.store.dispatch('productDetails/fetchDetails', params)
  }

  async fetchDetailsOptions() {
    await this.store.dispatch('productDetails/fetchDetailsOptions')
  }
}
