import StoneFetch from './StoneFetch'

export default class FetchDetails extends StoneFetch {
  get pathPattern() {
    return /([a-zA-Z\d]+-[urec])$/gm
  }

  parseRoute() {
    const path = this.route.params.id
    this.type = 'stonePairsList'

    const result = this.pathPattern.exec(path)
    if (result) {
      this.id = result[1]?.toUpperCase()
    } else if (!this.redirectTry())
      this.error({ statusCode: 404, message: 'This page could not be found' })
  }
}
