import WeddingBandFetch from './WeddingBandFetch'

export default class FetchDetails extends WeddingBandFetch {
  parseRoute() {
    const path = this.route.params.id
    this.source = this.route.query.source
    this.guid = this.route.query.guid
    this.type = 'weddingBandsPlain'

    const result = this.pathPattern.exec(path)
    if (result) {
      this.id = result[1]?.toUpperCase()
      this.metalTypeCode = result[2]?.toUpperCase()
      this.fullId = this.id + this.metalTypeCode
    } else {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }
}
