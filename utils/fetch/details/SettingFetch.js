import invert from 'lodash/invert'
import BaseFetchDetails from './_baseFetch'
import { parametersNamesMap } from '~~/utils/definitions/itemParameters'
import { settingsListByCategory } from '~~/utils/definitions/defaults'

export default class FetchDetails extends BaseFetchDetails {
  get pathPattern() {
    return /([a-z]+)-(js\d+)(.*)$/gm
  }

  parseRoute() {
    const path = this.route.params.id
    this.source = this.route.query.source

    const result = this.pathPattern.exec(path)

    if (result) {
      this.type = settingsListByCategory[result[1]]
      this.id = result[2]?.toUpperCase()
      this.metalTypeCode = result[3]?.toUpperCase()
    } else {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }

  async fetchDetails() {
    // this.store.dispatch('productDetails/resetSelectedOptions')
    await this.fetchDetailsSources()
    await this.checkSettingMatch()
    this.validateDetailsPath()
  }

  async setItemDetails(item) {
    await this.store.dispatch('productDetails/setDetailsItem', { item })
  }

  async fetchItemDetails() {
    const params = {
      type: this.type,
      id: this.id,
      metalTypeCode: this.metalTypeCode
    }

    if (this.parent) params.parent = this.parent

    const queryToStore = invert(parametersNamesMap)

    Object.keys(this.route.query).forEach((p) => {
      const param = queryToStore[p]
      if (param) {
        const numValue = Number(this.route.query[p])
        params[param] = Number.isNaN(numValue) ? this.route.query[p] : numValue
      }
    })

    // await this.store.dispatch('productDetails/setCurrentGuid', this.guid)
    await this.store.dispatch('productDetails/fetchDetails', params)
  }

  async checkSettingMatch() {
    const details = this.store.getters['productDetails/productDetails']
    await this.store.dispatch('customItem/checkSettingMatch', details)
  }
}
