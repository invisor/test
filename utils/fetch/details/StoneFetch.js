import BaseFetchDetails from './_baseFetch'

export default class FetchDetails extends BaseFetchDetails {
  get pathPattern() {
    return /([a-zA-Z\d]+)$/gm
  }

  parseRoute() {
    const path = this.route.params.id
    this.type = 'stonesList'

    const result = this.pathPattern.exec(path)
    if (result) {
      this.id = result[1]?.toUpperCase()
    } else if (!this.redirectTry())
      this.error({ statusCode: 404, message: 'This page could not be found' })
  }

  async fetchDetails() {
    await this.fetchDetailsSources()
    await this.checkSettingMatch()
    this.validateDetailsPath()
  }

  redirectTry() {
    const path = this.route.params.id
    if (this.pathPattern.exec(path)) {
      this.context.redirect(301, this.route.path.replace('_', '-'))
      return true
    }
    return false
  }

  async fetchItemDetails() {
    const params = {
      type: this.type,
      id: this.id
    }
    await this.store.dispatch('productDetails/fetchDetails', params)
  }

  async checkSettingMatch() {
    const details = this.store.getters['productDetails/productDetails']
    await this.store.dispatch('customItem/checkSettingMatch', details)
  }
}
