import invert from 'lodash/invert'
import BaseFetchDetails from './_baseFetch'
import { parametersNamesMap } from '~~/utils/definitions/itemParameters'

export default class FetchDetails extends BaseFetchDetails {
  get pathPattern() {
    return /([jbJB]+\d+)(.*)$/gm
  }

  /**
   * Some items should be searched by id some by guid.
   * Bands should be searched by guid for example
   * @returns {string}
   */
  get searchItemBy() {
    return 'guid'
  }

  async fetch() {
    await this.parseRoute()
    try {
      this.store.dispatch('productDetails/confirmRingSizeReset')
      await this.fetchDetails()
    } catch (e) {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
    this.getMetaData()
  }

  parseRoute() {
    const path = this.route.params.id
    this.source = this.route.query.source
    this.guid = this.route.query.guid
    this.type = 'weddingBands'

    const result = this.pathPattern.exec(path)
    if (result) {
      this.id = result[1]?.toUpperCase()
      this.metalTypeCode = result[2]?.toUpperCase()
      this.fullId = this.id + this.metalTypeCode
    } else {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }

  async fetchDetails() {
    // this.store.dispatch('productDetails/resetSelectedOptions')
    await this.fetchDetailsSources()
    this.validateDetailsPath()
  }

  async setItemDetails(item) {
    await this.store.dispatch('productDetails/setDetailsItem', { item })
  }

  async fetchItemDetails() {
    const params = {
      type: this.type,
      id: this.id,
      metalTypeCode: this.metalTypeCode
    }

    const queryToStore = invert(parametersNamesMap)

    Object.keys(this.route.query).forEach((p) => {
      const paramName = queryToStore[p]
      if (paramName) {
        if (/weight|grade|position|clarity/.test(paramName)) {
          params[paramName] = Array.isArray(this.route.query[p])
            ? this.route.query[p]
            : [this.route.query[p]]
          return
        }
        const numValue = Number(this.route.query[p])
        params[paramName] = Number.isNaN(numValue)
          ? this.route.query[p]
          : numValue
      }
    })
    await this.store.dispatch('productDetails/setCurrentGuid', this.guid)
    await this.store.dispatch('productDetails/fetchDetails', params)
  }
}
