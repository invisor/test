import { getItemId, getDetailsPath, guid } from '~~/utils/utils'

export default class BaseFetchDetails {
  constructor(options) {
    const { context, category } = options
    this.options = options
    this.context = context
    this.cookies = context.$cookies
    this.localePath = context.app.localePath
    this.store = context.store
    this.route = context.route
    this.error = context.error
    this.category = category
    this.source = this.route.query.source
    this.guid = context.route.query.guid || guid()
    this.parent = this.route.query.parent || null
  }

  /**
   * Some items should be searched by id some by guid.
   * Bands should be searched by guid for example
   * @returns {string}
   */
  get searchItemBy() {
    return 'id'
  }

  getMetaData() {
    this.store.dispatch('getMeta', this.context)
  }

  parseRoute() {}
  fetchDetails() {}
  fetchItemDetails() {}

  validateDetailsPath() {
    const validPath = this.localePath(
      getDetailsPath(
        this.store.getters['productDetails/productDetails'],
        this.route
      )
    )
    if (validPath !== this.route.path) this.context.redirect(301, validPath)
  }

  async fetch() {
    await this.parseRoute()
    try {
      await this.fetchDetails()
    } catch (e) {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
    this.getMetaData()
  }

  async fetchDetailsSources() {
    if (this.source === 'cart' && (await this.cartFetchTry())) return
    if (this.source === 'wishlist' && (await this.wishlistFetchTry())) return
    if (this.source === 'compare' && (await this.compareFetchTry())) return

    try {
      await this.fetchFromGround()
    } catch (e) {
      this.error({ statusCode: 404, message: 'This page could not be found' })
    }
  }

  async fetchFromGround() {
    await this.fetchItemDetails()
  }

  async cartFetchTry() {
    const cartItems = this.store.getters['cart/items']
    const item = cartItems.find(
      (item) => getItemId(item, this.searchItemBy) === this[this.searchItemBy]
    )
    if (item) {
      await this.setItemDetails(item)
      return true
    }
    return false
  }

  async wishlistFetchTry() {
    const wishlistItems = this.store.getters['favorites/allListItems']
    const item = wishlistItems.find(
      (item) => getItemId(item, this.searchItemBy) === this[this.searchItemBy]
    )

    if (item) {
      if (item.fromServer) {
        const { guid } = this.route.query
        // set up guid to update item from details
        if (guid) this.store.dispatch('productDetails/setCurrentGuid', guid)
        return false
      }
      await this.setItemDetails(item)
      return true
    }
    return false
  }

  async compareFetchTry() {
    const compareItems = this.store.getters['compareList/allListItems']
    const item = compareItems.find(
      (item) => getItemId(item, this.searchItemBy) === this[this.searchItemBy]
    )
    // TODO ID в нижнем регистре
    if (item) {
      await this.setItemDetails(item)
      return true
    }
    return false
  }

  async setItemDetails(item) {
    await this.store.dispatch('productDetails/setDetails', item)
  }
}
