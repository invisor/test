import camelCase from 'lodash/camelCase'
import WeddingBandFetch from './WeddingBandFetch'
import PlainBandFetch from './PlainBandFetch'
import StoneFetch from './StoneFetch'
import PairFetch from './PairFetch'
import JewelryFetch from './JewelryFetch'
import SettingFetch from './SettingFetch'
import CustomFetch from './CustomFetch'

export default function fetchDetails(options) {
  this.options = options
  this.itemsCategories = [
    'Ring',
    'Stone',
    'Pair',
    'Earring',
    'Pendant',
    'Necklace',
    'Bracelet',
    'Plain Band',
    'Wedding Band',
    'Setting_Ring',
    'Setting_Earring',
    'Setting_Pendant',
    'Setting_Necklace',
    'Custom'
  ]
  this.map = {
    weddingBand: WeddingBandFetch,
    plainBand: PlainBandFetch,
    stone: StoneFetch,
    pair: PairFetch,
    ring: JewelryFetch,
    earring: JewelryFetch,
    necklace: JewelryFetch,
    pendant: JewelryFetch,
    bracelet: JewelryFetch,
    settingRing: SettingFetch,
    settingEarring: SettingFetch,
    settingPendant: SettingFetch,
    settingNecklace: SettingFetch,
    custom: CustomFetch
  }
}

fetchDetails.prototype.fetch = async function() {
  try {
    this.validate()
  } catch (e) {
    console.log(e)
    return
  }
  const f = new this.map[camelCase(this.options.category)](this.options)
  await f.fetch()
}

fetchDetails.prototype.validate = function() {
  if (!this.options.context) throw new Error('Context is not defined')
  if (!this.options.category) throw new Error('Category is not defined')
  if (!this.itemsCategories.includes(this.options.category))
    throw new Error('Wrong category')
}
