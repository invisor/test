import isEmpty from 'lodash/isEmpty'
import pick from 'lodash/pick'
import get from 'lodash/get'
import {
  sourceRangeFiltersNamesMap,
  validUrlFilterNames
} from '~~/utils/definitions/filters'
import { normalizeUrlFilters } from '~~/utils/normalizers/filters'
import { tabletPageSize } from '~~/utils/definitions/defaults'
import { query } from '~~/utils/utils'

export default class BaseFetchListing {
  constructor(options) {
    const { context, type, subType, origin, style, webCategory, isStar } =
      options
    this.options = options
    this.context = context
    this.store = context.store
    this.route = context.route
    this.error = context.error
    this.type = type
    this.subType = subType
    this.origin = origin
    this.style = style
    this.isStar = isStar // 1 or undefined
    this.webCategory = webCategory
  }

  async fetch() {
    this.setLastListingPage()
    this.setStaticData()
    await this.setFilters()
    this.setUrlFilters()
    await this.getItems()
  }

  /**
   * Check out if selected filters still exist in source filters
   * Some filters may disappear during DYO process
   * Remove unallowed filters
   */
  validateFilters() {
    const sourceFilters =
      this.store.getters[`filters/${this.type}SourceFilters`]
    const selectedFilters = this.store.getters['filters/selectedFilters'](
      this.type
    )
    Object.keys(selectedFilters).forEach((filterType) => {
      if (filterType.includes('Range')) {
        this.validateRangeFilter(
          this.getFiltersSource(sourceFilters, filterType),
          selectedFilters[filterType],
          filterType
        )
        return
      }
      this.validateSimpleFilter(
        this.getFiltersSource(sourceFilters, filterType),
        selectedFilters[filterType],
        filterType
      )
    })
  }

  getFiltersSource(sourceFilters, filterType) {
    if (['lengthRange', 'widthRange'].includes(filterType))
      return sourceFilters.dimensions[filterType]
    return sourceFilters[filterType] || []
  }

  validateSimpleFilter(source, values, filterType) {
    if (!source.length) return
    const filterValues = source.map((f) => f.name || f.key)
    const valuesToRemove = values.filter((v) => !filterValues.includes(v))
    if (valuesToRemove.length)
      this.removeFilterFromSelected({ valuesToRemove, filterType })
  }

  validateRangeFilter(source, values, filterType) {
    const { min, max } = source[0]
    const [minV, maxV] = values
    if (minV < min || maxV > max) this.removeFilterFromSelected({ filterType })
  }

  removeFilterFromSelected({ valuesToRemove, filterType }) {
    this.store.dispatch('filters/removeSelectedFilter', {
      listType: this.type,
      valuesToRemove,
      filterType
    })
  }

  setLastListingPage() {
    this.store.dispatch('navigation/setLastListingPage', this.route)
  }

  resetOriginFilter() {
    this.store.dispatch('filters/clearOriginFilter', this.type)
  }

  resetStyleFilter() {
    this.store.dispatch('filters/clearStyleFilter', this.type)
  }

  async setFilters() {
    await this.store.dispatch('filters/setFilters', {
      itemsType: this.type,
      query: query(this.store, this.route)
    })
    this.store.dispatch('filters/setDirtyLists')
  }

  setUrlFilters() {
    const filtersFromUrl = this.getFromUrl()

    const normFilters = normalizeUrlFilters(filtersFromUrl)

    if (!isEmpty(normFilters)) this.store.dispatch('filters/setDirtyLists')

    Object.keys(normFilters).forEach((filterName) => {
      const value = normFilters[filterName]

      if (/Range/.test(filterName)) {
        const borders = this.getFilterBorders(filterName)
        for (let i = 0; i < value.length; i++) {
          if (value[i] === null) {
            value[i] = borders[i]
          }
        }
      }

      this.store.dispatch('filters/setAreasFilters', {
        listType: this.type,
        filterName,
        value
      })
    })
  }

  get sourceFilters() {
    return this.store.state.filters.sourceFilters[this.type]
  }

  getFromUrl() {
    return pick(this.route.query, [...validUrlFilterNames, 'availability'])
  }

  getFilterBorders(filterName) {
    const borders = get(
      this.sourceFilters,
      sourceRangeFiltersNamesMap[filterName],
      [{}]
    )[0]
    return [borders.min, borders.max]
  }

  setStaticData() {
    this.store.dispatch('getMeta', this.context)
    this.store.dispatch('setTypeList', this.type)
  }

  async getItems() {
    const pageNumber = parseInt(this.route.query.page) || 1
    const prefilterQuery = query(this.store, this.route)

    const params = {
      prefilterQuery,
      pageNumber,
      force: this.options.force,
      origin: this.origin,
      itemsSubType: this.subType,
      isStar: this.isStar,
      webCategory: this.webCategory
    }

    if (this.context.isTablet) params.pageSize = tabletPageSize

    const data = await this.store.dispatch(`${this.type}/getPage`, params)

    if (data.pageCount && pageNumber > data.pageCount)
      this.error({ statusCode: 404, message: 'This page could not be found' })

    if (this.store.getters['filters/dirtyLists'])
      this.store.dispatch('filters/cleanDirtyLists')
  }
}
