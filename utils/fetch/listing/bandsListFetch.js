import isEmpty from 'lodash/isEmpty'
import Vue from 'vue'
import BaseFetchListing from './_baseFetch'
import { tabletPageSize } from '~~/utils/definitions/defaults'

export default class FetchListing extends BaseFetchListing {
  async fetch() {
    this.setLastListingPage()
    this.setStaticData()
    await this.setFilters()
    this.setUrlFilters()
    await this.getItems()
  }

  async setFilters() {
    if (isEmpty(this.store.getters[`filters/${this.type}SourceFilters`]))
      await this.store.dispatch('filters/setFilters', { itemsType: this.type })
    Vue.prototype.$bus.$emit('filters-ready')
  }

  async getItems() {
    const pageNumber = parseInt(this.route.query.page) || 1

    const params = {
      pageNumber,
      force: this.options.force
    }

    if (this.context.isTablet) params.pageSize = tabletPageSize
    const data = await this.store.dispatch(`${this.type}/getPage`, params)

    if (data.pageCount && pageNumber > data.pageCount)
      this.error({ statusCode: 404, message: 'This page could not be found' })
  }
}
