import StonesListFetch from './stonesListFetch'
import StonePairsListFetch from './stonePairsListFetch'
import SettingsListFetch from './settingsListFetch'
import JewelryListFetch from './jewelryListFetch'
import BandsListFetch from './bandsListFetch'
import { settings, jewelry, bands, stones } from '~~/utils/definitions/defaults'

export default function fetchListing(options) {
  this.options = options
  this.itemsTypes = [...stones, ...settings, ...jewelry, ...bands]
  this.map = {
    stonesList: StonesListFetch,
    stonePairsList: StonePairsListFetch,
    ringSettings: SettingsListFetch,
    earringSettings: SettingsListFetch,
    pendantSettings: SettingsListFetch,
    necklaceSettings: SettingsListFetch,
    braceletsList: JewelryListFetch,
    earringsList: JewelryListFetch,
    necklacesPendantsList: JewelryListFetch,
    ringsList: JewelryListFetch,
    weddingBands: BandsListFetch,
    weddingBandsPlain: BandsListFetch
  }
}

fetchListing.prototype.fetch = async function() {
  try {
    this.validate()
  } catch (e) {
    console.log(e)
    return
  }
  const f = new this.map[this.options.type](this.options)
  await f.fetch()
}

fetchListing.prototype.validate = function() {
  if (!this.options.context) throw new Error('Context is not defined')
  if (!this.options.type) throw new Error('Type is not defined')
  if (!this.itemsTypes.includes(this.options.type))
    throw new Error('Wrong type')
}
