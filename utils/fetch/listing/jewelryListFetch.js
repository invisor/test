import isEmpty from 'lodash/isEmpty'
import Vue from 'vue'
import BaseFetchListing from './_baseFetch'
import { tabletPageSize } from '~~/utils/definitions/defaults'
import { normalizeUrlFilters } from '~~/utils/normalizers/filters'

export default class FetchListing extends BaseFetchListing {
  async fetch() {
    this.setLastListingPage()
    this.setStaticData()
    this.resetDataType()
    this.setUrlFilters()
    await this.setFilters()
    if (this.origin) this.resetOriginFilter()
    if (this.style) this.resetStyleFilter()
    await this.getItems()
  }

  async setFilters() {
    await this.store.dispatch('filters/setFilters', {
      itemsType: this.type,
      isStar: this.isStar,
      origin: this.origin,
      style: this.style
    })
    Vue.prototype.$bus.$emit('filters-ready')
  }

  // reset availability parameter to "all" on new listing page
  resetDataType() {
    this.store.dispatch('filters/setDataType', {
      listType: this.type,
      value: 0
    })
  }

  setUrlFilters() {
    const filtersFromUrl = this.getFromUrl()

    const normFilters = normalizeUrlFilters(filtersFromUrl)

    const availability = Number(filtersFromUrl.availability)
    if (availability && [1, 2].includes(availability))
      this.store.dispatch('filters/setDataType', {
        listType: this.type,
        value: availability
      })

    if (!isEmpty(normFilters)) this.store.dispatch('filters/setDirtyLists')

    Object.keys(normFilters).forEach((filterName) => {
      const value = normFilters[filterName]

      if (/Range/.test(filterName)) {
        const borders = this.getFilterBorders(filterName)
        for (let i = 0; i < value.length; i++) {
          if (value[i] === null) {
            value[i] = borders[i]
          }
        }
      }

      this.store.dispatch('filters/setAreasFilters', {
        listType: this.type,
        filterName,
        value
      })
    })
  }

  async getItems() {
    const pageNumber = parseInt(this.route.query.page) || 1

    const params = {
      pageNumber,
      isStar: this.isStar,
      force: this.options.force,
      origin: this.origin,
      style: this.style
    }

    if (this.context.isTablet) params.pageSize = tabletPageSize
    const data = await this.store.dispatch(`${this.type}/getPage`, params)

    if (data.pageCount && pageNumber > data.pageCount)
      this.error({ statusCode: 404, message: 'This page could not be found' })
  }
}
