import Vue from 'vue'
import BaseFetchListing from './_baseFetch'
import { tabletPageSize } from '~~/utils/definitions/defaults'
import { query } from '~~/utils/utils'

export default class FetchListing extends BaseFetchListing {
  async fetch() {
    this.setLastListingPage()
    this.setStaticData()
    await this.setFilters()
    this.setUrlFilters()
    this.validateFilters()
    if (this.style) this.resetStyleFilter()
    await this.getItems()
  }

  async setFilters() {
    await this.store.dispatch('filters/setFilters', {
      force: this.options.force,
      itemsType: this.type,
      style: this.style,
      query: query(this.store, this.route)
    })
    Vue.prototype.$bus.$emit('filters-ready')
    this.store.dispatch('filters/setDirtyLists')
  }

  async getItems() {
    const pageNumber = parseInt(this.route.query.page) || 1

    const params = {
      prefilterQuery: query(this.store, this.route),
      force: this.options.force,
      style: this.style,
      pageNumber
    }

    if (this.context.isTablet) params.pageSize = tabletPageSize
    const data = await this.store.dispatch(`${this.type}/getPage`, params)

    if (data.pageCount && pageNumber > data.pageCount)
      this.error({ statusCode: 404, message: 'This page could not be found' })

    if (this.store.getters['filters/dirtyLists'])
      this.store.dispatch('filters/cleanDirtyLists')
  }
}
