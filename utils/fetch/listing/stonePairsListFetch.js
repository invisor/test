import Vue from 'vue'
import BaseFetchListing from './_baseFetch'
import { query } from '~~/utils/utils'

export default class FetchListing extends BaseFetchListing {
  async fetch() {
    this.setLastListingPage()
    this.setStaticData()
    await this.setFilters()
    this.setUrlFilters()
    this.validateFilters()
    await this.getItems()
  }

  async setFilters() {
    await this.store.dispatch('filters/setFilters', {
      force: this.options.force,
      itemsType: this.type,
      itemsSubType: this.subType,
      isStar: this.isStar,
      query: query(this.store, this.route)
    })
    Vue.prototype.$bus.$emit('filters-ready')
    this.store.dispatch('filters/setDirtyLists')
  }
}
