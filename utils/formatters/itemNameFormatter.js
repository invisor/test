import camelCase from 'lodash/camelCase'
import get from 'lodash/get'
import { caratFormatter, tcwFormatter } from './index'

const itemsNameMap = {
  Stone: stoneNameFormatter,
  Pair: pairNameFormatter,
  Ring: jewelryNameFormatter,
  Earring: jewelryNameFormatter,
  Pendant: jewelryNameFormatter,
  Necklace: jewelryNameFormatter,
  Bracelet: jewelryNameFormatter,
  'Wedding Band': bandsNameFormatter,
  'Plain Band': bandsNameFormatter,
  Setting_Ring: settingsNameFormatter,
  Setting_Earring: settingsNameFormatter,
  Setting_Necklace: settingsNameFormatter,
  Setting_Pendant: settingsNameFormatter,
  Custom: customNameFormatter,
  Wax: waxNameFormatter
}

export default function itemNameFormatter(item, removeHtmlTags = false) {
  if (item.title) return item.title
  const func = itemsNameMap[item.category]
  if (!func) return 'Jewelry'
  if (removeHtmlTags)
    return func
      .call(this, item)
      .replace(/<[^>]*>?/gm, '')
      .replace('Total Carat Weight', '')
  return func.call(this, item)
}

function stoneNameFormatter(item) {
  const weight = caratFormatter(item.weight)

  if (this?.$t)
    return `${weight} ${this.$t(
      `detailsPage.stoneTypeName.${camelCase(item.stoneType)}`
    )} ${this.$t('detailsPage.from')} ${this.$t(
      `detailsPage.originName.${camelCase(item.origin)}`
    )}`

  return [weight, item.stoneType, 'from', item.origin].join(' ')
}

function pairNameFormatter(item) {
  const weight = tcwFormatter.call(this, item.totalWeight || item.weight)
  const stoneType = item.stoneType || get(item, 'stones[0].stoneType', '')
  const origin = item.origin || get(item, 'stones[0].origin', '')

  if (this?.$t)
    return `${weight} ${this.$t(
      `detailsPage.stoneTypeName.${camelCase(stoneType)}`
    )} ${this.$t('detailsPage.pairFrom')} ${this.$t(
      `detailsPage.originName.${camelCase(origin)}`
    )}`

  return [weight, stoneType, 'from', origin].join(' ')
}

function bandsNameFormatter(item) {
  const metalName = this?.$t
    ? this.$t(`detailsPage.metalTypesNames.${camelCase(item.metalName)}`)
    : item.metalName
  const stones = item.sidestoneOptions
    ? item.sidestoneOptions
        .map((s) =>
          this?.$t
            ? this.$t(`detailsPage.stoneTypeName.${camelCase(s.stoneType)}`)
            : s.stoneType
        )
        .reverse()
        .join(this?.$t ? ` ${this.$t('detailsPage.and')} ` : ' and ')
    : ''
  const category = this?.$t
    ? this.$t(`categories.${camelCase(item.category)}`)
    : item.category

  return [metalName, stones, category].filter((p) => p).join(' ')
}

function settingsNameFormatter(item) {
  const category = item.category.replace('Setting_', '')

  if (this?.$t)
    return `${this.$t(
      `detailsPage.metalTypesNames.${camelCase(item.metalName)}`
    )} ${this.$t('detailsPage.setting', [
      this.$t(`categories.${camelCase(category.toLowerCase())}`)
    ])}`

  return [item.metalName, category].join(' ')
}

function waxNameFormatter(item) {
  const category = item.customSetting.category.replace('Setting_', '')

  if (this?.$t)
    return `${this.$t(
      `categories.${camelCase(category.toLowerCase())}`
    )} Wax Model`
  return [category, 'Wax Model'].join(' ')
}

function customNameFormatter(item) {
  return customItemNameFormatter.call(this, item).replace('<br /> ', '')
}

function jewelryNameFormatter(item) {
  const weightFormatter = ['Earring', 'Bracelet'].includes(item.category)
    ? tcwFormatter
    : caratFormatter

  const weight = weightFormatter.call(
    this,
    item.weight ||
      item.stoneWeight ||
      get(item, 'centerStones[0].totalWeight', 0)
  )
  const centerStoneType = get(item, 'centerStones[0].stoneType', 'Ruby')
  const sideStoneType = get(item, 'sideStones[0].stoneType', '')
  const metalName = get(item, 'metal.value', '') || item.metalName

  if (!this?.$t) {
    let category = item.category
    if (category === 'Earring') category = 'Earrings'

    return [
      weight,
      centerStoneType,
      sideStoneType ? `and ${sideStoneType}` : '',
      category,
      '-',
      metalName
    ].join(' ')
  }

  const centerStone = this.$t(
    `detailsPage.stoneTypeName.${camelCase(centerStoneType)}`
  )
  const sideStone = sideStoneType
    ? `${this.$t('detailsPage.and')} ${this.$t(
        `detailsPage.stoneTypeName.${camelCase(sideStoneType)}`
      )}`
    : ''

  const category = this.$t(`categories.${camelCase(item.category)}`)

  const metal = this.$t(`detailsPage.metalTypesNames.${camelCase(metalName)}`)

  return [weight, centerStone, sideStone, category, '-', metal].join(' ')
}

function customItemNameFormatter(item) {
  if (!item.customSetting.category) return ''
  // unsuitable items
  // the same as isItemsMatch in utils helper
  if (
    (item.customSetting.category === 'Setting_Earring' &&
      item.customStone.category !== 'Pair') ||
    (item.customSetting.category !== 'Setting_Earring' &&
      item.customStone.category === 'Pair')
  )
    return ''

  const weight = item.customStone.totalWeight || item.customStone.weight
  const stoneType =
    item.customStone.stoneType ||
    get(item, 'customStone.stones[0].stoneType', 'Ruby')
  const settingCategory = item.customSetting.category.replace('Setting_', '')
  const weightFormatter = ['Earring', 'Bracelet'].includes(settingCategory)
    ? tcwFormatter
    : caratFormatter

  if (this?.$t)
    return `${this.$t(
      `detailsPage.stoneTypeName.${camelCase(stoneType)}`
    )} ${this.$t(
      `categories.${camelCase(settingCategory.toLowerCase())}`
    )} <br /> ${weightFormatter.call(this, weight)} ${this.$t(
      `detailsPage.metalTypesNames.${camelCase(item.customSetting.metalName)}`
    )}`

  return [
    stoneType,
    settingCategory,
    weightFormatter.call(this, weight),
    item.customSetting.metalName
  ].join(' ')
}
