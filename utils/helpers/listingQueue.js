const defaultConfig = {
  delay: 500
}

export default class Queue {
  constructor(params) {
    const config = {
      ...defaultConfig,
      ...params
    }

    this.delay = config.delay
    this.queue = []
    this.running = false
    this.proceed = false
  }

  add(cb) {
    this.queue.push(() => {
      const finished = new Promise((resolve, reject) => {
        this.proceed = true
        cb().then(() => {
          this.queue.shift()
          this.proceed = false
          resolve()
        })
      })

      finished.then(this.dequeue.bind(this), () => {})
    })

    return this
  }

  dequeue() {
    if (this.proceed) return

    this.running = this.queue[0]

    if (this.running) {
      this.running()
    }

    return this.running
  }

  get next() {
    return this.dequeue
  }
}
