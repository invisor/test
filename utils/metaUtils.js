import get from 'lodash/get'
import camelCase from 'lodash/camelCase'
import { normRouteName, removeTagsFromString } from '~~/utils/utils'
import { caratFormatter, tcwFormatter } from '~~/utils/formatters/index.js'

const settings = [
  'Setting_Ring',
  'Setting_Earring',
  'Setting_Pendant',
  'Setting_Necklace'
]

const jewelry = ['Earring', 'Pendant', 'Necklace', 'Bracelet', 'Ring']

const stones = ['Stone', 'Pair']

const bands = ['Plain Band', 'Wedding Band']

const settingsTypesMap = {
  Setting_Ring: 'Ring',
  Setting_Earring: 'Earring',
  Setting_Pendant: 'Pendant',
  Setting_Necklace: 'Necklace'
}

const productPages = [
  'design-your-own-setting-ruby-engagement-ring-settings-id',
  'design-your-own-setting-ruby-necklace-pendant-settings-id',
  'design-your-own-setting-ruby-ruby-earring-settings-id',
  'design-your-own-setting-mens-ring-settings-id',
  'design-your-own-review-id',
  'ruby-jewelry-id',
  'rubies-id',
  'rubies-matched-pairs-of-rubies-id',
  'wedding-bands-without-gemstone-id',
  'ruby-wedding-rings-bands-id'
]

const defaultTitle = ''
const defaultDescription = ''
const defaultPageTitle = ''

export function getMeta(context) {
  const title = pageTitle.call(context)

  const meta = [
    {
      name: 'title',
      content: title
    },
    {
      name: 'description',
      content: pageDescription.call(context)
    },
    {
      name: 'navigation-page-title',
      content: navPageTitle.call(context)
    },
    {
      name: 'navigation-page-description',
      content: navPageDescr.call(context)
    }
  ]

  if (productPages.includes(routeName.call(context))) {
    /* Twitter */
    meta.push({
      name: 'twitter:card',
      content: 'summary'
    })
    /* !Twitter */
    /* Pinterest */
    meta.push({
      property: 'product:price:amount',
      content: getProductPrice.call(context)
    })
    meta.push({
      property: 'product:price:currency',
      content: 'USD'
    })
    meta.push({
      property: 'og:image_url',
      content: productImage.call(context)
    })
    /* !Pinterest */
    meta.push({
      property: 'og:url',
      content: pageLink.call(context)
    })
    meta.push({ property: 'og:type', content: 'product' })
    meta.push({ property: 'og:title', content: title })
    meta.push({
      property: 'og:description',
      content: pageDescription.call(context)
    })
    meta.push({
      property: 'og:image',
      content: productImage.call(context)
    })
    meta.push({
      property: 'fb:app_id',
      content: process.env.fbAppId
    })
  } else {
    meta.push({
      property: 'og:image',
      content: 'https://thenaturalrubycompany.com/img/logo.jpg'
    })
  }

  return {
    meta,
    title
  }
}

function routeName() {
  return normRouteName(this.route.name)
}

function pageDescription() {
  const route = routeName.call(this)
  if (route === 'design-your-own-review-id') {
    const product = this.store.getters['customItem/customItem']
    const description =
      product.preview?.productDescription ||
      [
        product.customStone.productDescription,
        product.customSetting.productDescription
      ]
        .join(' ')
        .trim()
    if (description) return removeTagsFromString(description)
  } else {
    const product = this.store.getters['productDetails/productDetails']
    if (product.productDescription) return product.productDescription
  }

  return (
    this.app.i18n.t(
      `metadata.${routeName.call(this)}.seo.description`,
      itemData.call(this)
    ) || defaultDescription
  )
}

function navPageTitle() {
  return (
    this.app.i18n.t(
      `metadata.${routeName.call(this)}.navPageTitle`,
      itemData.call(this)
    ) || defaultPageTitle
  )
}

function navPageDescr() {
  if (this.app.i18n.te(`metadata.${routeName.call(this)}.navPageDescr`))
    return this.app.i18n.t(
      `metadata.${routeName.call(this)}.navPageDescr`,
      itemData.call(this)
    )
  return ''
}

function pageLink() {
  if (!productPages.includes(routeName.call(this))) return ''
  return `${process.env.host}${this.route.path}`
}

function pageTitle() {
  return (
    this.app.i18n.t(
      `metadata.${routeName.call(this)}.seo.title`,
      itemData.call(this)
    ) || defaultTitle
  )
}

function itemData() {
  const route = routeName.call(this)
  const product = this.store.getters['productDetails/productDetails']
  if (route === 'design-your-own-review-id') {
    // custom item
    return getCustomItemMeta.call(this)
  }
  if (settings.includes(product.category)) {
    return getSettingsMeta.call(this)
  }
  if (jewelry.includes(product.category)) {
    return getJewelryMeta.call(this)
  }
  if (stones.includes(product.category)) {
    return getStonesMeta.call(this)
  }
  if (bands.includes(product.category)) {
    return getBandsMeta.call(this)
  }
  // if (/^design-your-own-setting/g.test(route)) {
  //   // settings list
  //   return getSettingsListMeta.call(this)
  // }
  return {}
}

function getSettingsMeta() {
  const product = this.store.getters['productDetails/productDetails']
  const type = this.app.i18n.t(
    `categories.${camelCase(settingsTypesMap[product.category])}`
  )
  const metal = this.app.i18n.t(
    `detailsPage.metalTypesNames.${camelCase(product.metalName)}`
  )
  const s = get(product, 'styleNames[0]', null)
  const style = s
    ? this.app.i18n.t(`detailsPage.designStylesFilter.${camelCase(s)}`)
    : ''
  const { id, metalTypeCode } = product
  return { metal, type, id, style, metalTypeCode }
}

function getJewelryMeta() {
  const product = this.store.getters['productDetails/productDetails']
  const type = this.app.i18n.t(`categories.${camelCase(product.category)}`)
  const carat = product.centerStones[0].totalWeight
  const shape = this.app.i18n.t(
    `detailsPage.stoneShapesName.${camelCase(product.centerStones[0].shape)}`
  )
  const id = product.id
  const metal = this.app.i18n.t(
    `detailsPage.metalTypesNames.${camelCase(product.metal.value)}`
  )
  return { carat, id, type, metal, shape }
}

function getStonesMeta() {
  const product = this.store.getters['productDetails/productDetails']
  const origin = this.app.i18n.t(
    `detailsPage.originName.${camelCase(
      product.origin || get(product, 'stones[0].origin', '')
    )}`
  )
  const carat = product.weight || get(product, 'totalWeight', '')
  const shape = this.app.i18n.t(
    `detailsPage.stoneShapesName.${camelCase(
      product.shape || get(product, 'stones[0].shape', '')
    )}`
  )
  const id = product.id
  return { carat, id, origin, shape }
}

function getBandsMeta() {
  const product = this.store.getters['productDetails/productDetails']
  const metal = this.app.i18n.t(
    `detailsPage.metalTypesNames.${camelCase(product.metalName)}`
  )
  const { id, metalTypeCode } = product
  return { metal, id, metalTypeCode }
}

function getCustomItemMeta() {
  const stone = this.store.getters['customItem/customStone']
  const setting = this.store.getters['customItem/customSetting']
  const metalName = setting.metalName
  const stoneType = stone.stoneType || get(stone, 'stones[0].stoneType', 'Ruby')
  const category = setting.category.replace('Setting_', '').toLowerCase()
  const weight = stone.totalWeight || stone.weight
  const weightFormatter = category === 'earring' ? tcwFormatter : caratFormatter
  return {
    stoneType: this.app.i18n.t(
      `detailsPage.stoneTypeName.${camelCase(stoneType)}`
    ),
    metalName: this.app.i18n.t(
      `detailsPage.metalTypesNames.${camelCase(metalName)}`
    ),
    weight: weightFormatter.call(this, weight),
    category: this.app.i18n.t(`categories.${category}`),
    fullType: this.app.i18n.t(`breadcrumbs.review.${category}`)
  }
}

function productImage() {
  const isCustomReview = /design-your-own-review/.test(this.route.name)

  const previewThumbnails = this.store.getters['customItem/previewThumbnails']
  const settingThumbnails = this.store.getters['customItem/settingThumbnails']
  const productThumbnails =
    this.store.getters['productDetails/productThumbnails']

  const customItemThumbnailsSource = previewThumbnails.length
    ? previewThumbnails
    : settingThumbnails
  const thumbnails = isCustomReview
    ? customItemThumbnailsSource
    : productThumbnails
  return get(thumbnails, '[0].large', '')
}

function getProductPrice() {
  const isCustomReview = /design-your-own-review/.test(this.route.name)
  if (isCustomReview) {
    return this.store.getters['customItem/finalPrice'].totalPrice
  }
  return this.store.getters['productDetails/productDetails'].price
}
