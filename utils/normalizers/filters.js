import {
  notAreaFilters,
  urlToStoreFilters,
  validUrlFilterNames
} from '~~/utils/definitions/filters'

export function normalizeUrlFilters(filters) {
  const pattern = /([\w-]+)(-max|-min)/
  // const pattern = new RegExp('([\\w-]+)(-max|-min)')
  const normFilters = {}
  Object.keys(filters).forEach((filterName) => {
    if (!validUrlFilterNames.includes(filterName)) return
    if (pattern.test(filterName)) {
      const name = urlToStoreFilters[filterName]
      const ext = filterName.match(pattern)[2].toLowerCase()
      normFilters[name] = normFilters[name] || [null, null]
      normFilters[name][ext === '-min' ? 0 : 1] = Number(filters[filterName])
      return
    }
    if (typeof filters[filterName] === 'string') {
      normFilters[urlToStoreFilters[filterName]] = convertToNumber([
        filters[filterName]
      ])
      return
    }
    normFilters[urlToStoreFilters[filterName]] = convertToNumber(
      filters[filterName]
    )
  })
  notAreaFilters.forEach((f) => {
    delete normFilters[f]
  })
  return normFilters
}

function convertToNumber(array) {
  return array.map((el) => {
    if (!isNaN(Number(el))) return Number(el)
    return el
  })
}
