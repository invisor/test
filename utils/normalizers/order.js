import cloneDeep from 'lodash/cloneDeep'

export function normalizeOrder(data) {
  const response = cloneDeep(data)
  const custom = []

  for (let i = response.settings.length - 1; i >= 0; i--) {
    const customItem = {}
    customItem.customSetting = response.settings[i]
    const stoneArray =
      customItem.customSetting.category === 'Setting_Earring'
        ? response.pairs
        : response.stones
    const j = stoneArray.findIndex(
      (stone) => stone.id === customItem.customSetting.centerStoneId
    )
    if (j > -1) {
      customItem.customStone = stoneArray[j]
      customItem.category = 'Custom'
      customItem.id =
        customItem.customSetting.previewRequestID ||
        `${customItem.customSetting.id}-${customItem.customStone.id}`
      customItem.finalPrice = getFinalPrice(customItem)

      response.settings.splice(i, 1)
      stoneArray.splice(j, 1)

      custom.push(customItem)
    }
  }

  response.custom = custom
  return response
}

function getFinalPrice(item) {
  const price = {
    localCurrency: {},
    settingPrice: item.customSetting.price,
    stonePrice: item.customStone.discountPrice,
    originalStonePrice: item.customStone.price,
    discountAmt: item.customStone.discountAmt,
    originalTotalPrice: item.customSetting.price + item.customStone.price,
    totalPrice: item.customSetting.price + item.customStone.discountPrice
  }
  if (item.customSetting.localCurrency)
    price.localCurrency = {
      countryCode: item.customSetting.localCurrency.countryCode,
      currencyCode: item.customSetting.localCurrency.currencyCode,
      symbol: item.customSetting.localCurrency.symbol,
      settingPrice: item.customSetting.localCurrency.price,
      originalStonePrice: item.customStone.localCurrency.price,
      stonePrice: item.customStone.localCurrency.discountPrice,
      discountAmt: item.customStone.localCurrency.discountAmt,
      originalTotalPrice:
        item.customStone.localCurrency.price +
        item.customSetting.localCurrency.price,
      totalPrice:
        item.customStone.localCurrency.discountPrice +
        item.customSetting.localCurrency.price,
      usdRate: null
    }
  return price
}
