import get from 'lodash/get'
import cloneDeep from 'lodash/cloneDeep'
import {
  defaultGemListQuery,
  defaultJewelryListQuery,
  defaultSettingsQuery,
  defaultWeddingBandsPlainQuery,
  defaultWeddingBandsQuery
} from '~~/utils/definitions/defaults'
import {
  newSortingNamesMap,
  urlToStoreSort
} from '~~/utils/definitions/filters'
import { capitalize } from '~~/utils/formatters'

export default function (listingType, storeFilters, params) {
  return serializationsMap[listingType](listingType, storeFilters, params)
}

const serializationsMap = {
  stonesList: serializeStoneFilters,
  stonePairsList: serializeStoneFilters,
  braceletsList: serializeJewelryFilters,
  earringsList: serializeJewelryFilters,
  necklacesPendantsList: serializeJewelryFilters,
  ringsList: serializeJewelryFilters,
  ringSettings: serializeSettingsFilters,
  pendantSettings: serializeSettingsFilters,
  earringSettings: serializeSettingsFilters,
  necklaceSettings: serializeSettingsFilters,
  weddingBands: serializeWeddingBandsFilters,
  weddingBandsPlain: serializeWeddingBandsPlainFilters
}

const filtersNameMap = {
  metalTypes: 'metals',
  priceRange: 'price',
  caratRange: 'weight',
  widthRange: 'width',
  lengthRange: 'length',
  bandWidthRange: 'bandWidth',
  stoneShapes: 'shapes',
  stoneOrigins: 'origins',
  designStyles: 'styles'
}

const filtersNameBandsMap = {
  ...filtersNameMap,
  stoneShapes: 'stoneShapes'
}

function fillQuery(defaultQuery, selectedFilters, filtersNameMap) {
  Object.keys(selectedFilters).forEach((key) => {
    const filterName = filtersNameMap[key] || key
    if (key.includes('Range')) {
      const [min, max] = selectedFilters[key]
      defaultQuery[filterName] = { min: min || null, max: max || null }
      return
    }
    defaultQuery[filterName] = selectedFilters[key]
  })
}

function serializeSettingsFilters(itemsType, filters, params) {
  const prefilter = get(params, 'prefilterQuery', {})

  let query = cloneDeep(defaultSettingsQuery.query)

  if (filters.usePrefilter)
    query = {
      ...query,
      ...prefilter
    }

  const selectedFilters = filters.selectedFilters[itemsType]

  fillQuery(query, selectedFilters, filtersNameMap)

  query.category = itemsType.replace('Settings', '')

  const { sortBy, order } = selectedFilters

  if (sortBy) {
    query.sortBy = newSortingNamesMap[selectedFilters.sortBy[0]]
    query.sortOrder = order[0] === 'asc' ? 1 : 2
  }

  if (params.style) query.styles.push(params.style)

  return query
}

function serializeStoneFilters(itemsType, filters, params) {
  const prefilter = get(params, 'prefilterQuery', {})

  let query = cloneDeep(defaultGemListQuery.query)

  if (filters.usePrefilter)
    query = {
      ...query,
      ...prefilter
    }

  const selectedFilters = filters.selectedFilters[itemsType]

  fillQuery(query, selectedFilters, filtersNameMap)

  const { sortBy, order } = selectedFilters
  if (sortBy) {
    query[urlToStoreSort[sortBy[0]]] = order[0] === 'asc' ? 1 : 2
    query.featuredFirst = 0
  }

  if (params.itemsSubType)
    query.cuttingStyles.push(capitalize(params.itemsSubType))

  if (params.isStar) query.isStar = params.isStar

  if (params.webCategory) query.webCategory = params.webCategory

  return query
}

export function serializeJewelryFilters(itemsType, filters, params) {
  const query = cloneDeep(defaultJewelryListQuery.query)

  const selectedFilters = filters.selectedFilters[itemsType]

  query.dataType = filters.dataType[itemsType]

  fillQuery(query, selectedFilters, filtersNameMap)

  const { sortBy, order } = selectedFilters
  if (sortBy) {
    query[urlToStoreSort[sortBy[0]]] = order[0] === 'asc' ? 1 : 2
    query.featuredFirst = 0
  }

  query.isStar = !!params.isStar

  if (params.style) query.styles.push(params.style)

  return query
}

export function serializeWeddingBandsPlainFilters(itemsType, filters) {
  const query = cloneDeep(defaultWeddingBandsPlainQuery.query)

  const selectedFilters = filters.selectedFilters[itemsType]

  fillQuery(query, selectedFilters, filtersNameBandsMap)

  const { sortBy, order } = selectedFilters

  if (sortBy) {
    query.sortBy = newSortingNamesMap[selectedFilters.sortBy[0]]
    query.sortOrder = order[0] === 'asc' ? 1 : 2
  }

  return query
}

export function serializeWeddingBandsFilters(itemsType, filters) {
  const query = cloneDeep(defaultWeddingBandsQuery.query)

  const selectedFilters = filters.selectedFilters[itemsType]

  fillQuery(query, selectedFilters, filtersNameBandsMap)

  const { sortBy, order } = selectedFilters

  if (sortBy) {
    query.sortBy = newSortingNamesMap[selectedFilters.sortBy[0]]
    query.sortOrder = order[0] === 'asc' ? 1 : 2
  }

  return query
}
