import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import { protocol } from '../nuxt.config'
import { caratURLFormatter, itemNameFormatterNoLocale } from './formatters'

export const stoneType = 'ruby'
const maxItemsCount = 1000

export const defaultGemListQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    id: null,
    settingId: null,
    shapes: [],
    origins: [],
    price: {
      min: null,
      max: null
    },
    weight: {
      min: null,
      max: null
    },
    width: {
      min: null,
      max: null
    },
    length: {
      min: null,
      max: null
    },
    sortByWeight: 0,
    sortByPrice: 0,
    featuredFirst: 0,
    newFirst: 1
  }
}

export const defaultJewelryListQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    id: null,
    metals: [],
    categories: ['Ring', 'Earring', 'Necklace', 'Pendant', 'Bracelet'],
    shapes: [],
    price: {
      min: null,
      max: null
    },
    weight: {
      min: null,
      max: null
    },
    treatments: [],
    sortByWeight: 0,
    sortByPrice: 0,
    featuredFirst: 0,
    newFirst: 1
  }
}

export const defaultWeddingBandsPlainQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    id: null,
    categoryId: 1,
    metals: [],
    styles: [],
    stoneTypes: [],
    stoneShapes: [],
    sortBy: 'NewFirst',
    sortOrder: 1
  }
}

export const defaultWeddingBandsQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    id: null,
    categoryId: 2,
    metals: [],
    styles: [],
    stoneTypes: [],
    stoneShapes: [],
    sortBy: 'NewFirst',
    sortOrder: 1
  }
}

export const defaultRingSettingsQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    category: 'ring',
    stoneId: null,
    metals: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

export const defaultEarringSettingsQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    category: 'earring',
    stoneId: null,
    metals: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

export const defaultPendantSettingsQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    category: 'pendant',
    stoneId: null,
    metals: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

export const defaultNecklaceSettingsQuery = {
  pageNumber: 1,
  calculateCount: false,
  pageSize: maxItemsCount,
  query: {
    category: 'necklace',
    stoneId: null,
    metals: [],
    centerStoneShapes: [],
    sideStoneShapes: [],
    styles: [],
    sortBy: 'FeaturedFirst',
    sortOrder: 1
  }
}

export const productListTypeByCategory = {
  Ring: 'ringsList',
  Stone: 'stonesList',
  Pair: 'stonePairsList',
  Earring: 'earringsList',
  Pendant: 'necklacesPendantsList',
  Necklace: 'necklacesPendantsList',
  Bracelet: 'braceletsList',
  'Plain Band': 'weddingBandsPlain',
  'Wedding Band': 'weddingBands',
  Setting_Ring: 'ringSettings',
  Setting_Earring: 'earringSettings',
  Setting_Pendant: 'pendantSettings',
  Setting_Necklace: 'necklaceSettings',
  Custom: 'custom', // custom jewelry
  Wax: 'wax' // custom jewelry wax model
}

export const jewelry = [
  'braceletsList',
  'earringsList',
  'necklacesPendantsList',
  'ringsList'
]

export const previewJewelry = [
  'earringsList',
  'necklacesPendantsList',
  'ringsList'
]

export const settings = [
  'ringSettings',
  'earringSettings',
  'pendantSettings',
  'necklaceSettings'
]

export const bands = ['weddingBandsPlain', 'weddingBands']

export const bandsByCategory = ['Plain Band', 'Wedding Band']

export const settingsByCategory = [
  'Setting_Ring',
  'Setting_Earring',
  'Setting_Pendant',
  'Setting_Necklace'
]

const stonesListPathMap = {
  ruby: 'rubies',
  emerald: 'emeralds'
}

const stonePairsListPathMap = {
  ruby: 'rubies/matched-pairs-of-rubies',
  emerald: 'emeralds/matched-pairs-of-emeralds'
}

const weddingBandsPathMap = {
  ruby: 'ruby-wedding-rings-bands',
  emerald: 'emerald-wedding-rings-bands'
}

const jewelryPathMap = {
  ruby: 'ruby-jewelry',
  emerald: 'emerald-jewelry'
}

const settingsPathMap = {
  ruby: {
    Setting_Ring: 'ruby-engagement-ring-settings',
    Setting_Earring: 'ruby-earring-settings',
    Setting_Pendant: 'ruby-necklace-pendant-settings',
    Setting_Necklace: 'ruby-necklace-pendant-settings'
  },
  emerald: {
    Setting_Ring: 'emerald-engagement-ring-settings',
    Setting_Earring: 'emerald-earring-settings',
    Setting_Pendant: 'emerald-necklace-pendant-settings',
    Setting_Necklace: 'emerald-necklace-pendant-settings'
  }
}

export const normRouteName = function (name) {
  if (!name) return ''
  return name.replace(/_{3}\w{2}/gi, '')
}

class DetailsPath {
  constructor(item, route) {
    this.item = item
    this.route = route
    this.dataType = item.dataType
    this.path = 'index'
    this.type = productListTypeByCategory[item.category]
    this.id =
      item.id && typeof item.id === 'string'
        ? item.id.toLowerCase()
        : getItemId({
            customStone: item.customStone,
            customSetting: item.customSetting
          })
  }

  getPath() {
    if (!this.item) return this.path
    return `/${this[`${this.type}Path`]()}/`
  }

  stonesListPath() {
    const path = stonesListPathMap[process.env.siteName]
    const carat = caratURLFormatter(this.item.weight)
    const origin = this.originNormalize(this.item.origin)
    const shape = this.shapeNormalize(this.item.shape)

    return `${path}/${carat}ct-${origin}-${shape}-${stoneType}-${this.id}`
  }

  stonePairsListPath() {
    const path = stonePairsListPathMap[process.env.siteName]
    const carat = caratURLFormatter(this.item.weight || this.item.totalWeight)
    const origin = this.originNormalize(
      this.item.origin || get(this.item, 'stones[0].origin', '')
    )
    const shape = this.shapeNormalize(
      this.item.shape || get(this.item, 'stones[0].shape', '')
    )

    return `${path}/${carat}ct-${origin}-${shape}-${stoneType}-${this.id}`
  }

  weddingBandsPath() {
    const path = weddingBandsPathMap[process.env.siteName]
    const bandType = 'band'
    const metalTypeCode = this.item.metalTypeCode.toLowerCase()
    const metalName = this.metalNameNormalize(
      this.item.metalName || this.item.metalType
    )

    return `${path}/${metalName}-${bandType}-${this.id}${metalTypeCode}`
  }

  weddingBandsPlainPath() {
    const path = 'wedding-bands-without-gemstone'
    const bandType = 'plain-band'
    const metalTypeCode = this.item.metalTypeCode.toLowerCase()
    const metalName = this.metalNameNormalize(
      this.item.metalName || this.item.metalType
    )

    return `${path}/${metalName}-${bandType}-${this.id}${metalTypeCode}`
  }

  ringsListPath() {
    return this.jewelryPath()
  }

  earringsListPath() {
    return this.jewelryPath()
  }

  necklacesPendantsListPath() {
    return this.jewelryPath()
  }

  braceletsListPath() {
    return this.jewelryPath()
  }

  ringSettingsPath() {
    return this.settingsPath()
  }

  earringSettingsPath() {
    return this.settingsPath()
  }

  pendantSettingsPath() {
    return this.settingsPath()
  }

  necklaceSettingsPath() {
    return this.settingsPath()
  }

  waxPath() {
    return this.customPath()
  }

  customPath() {
    if (isEmpty(this.item.customSetting) || isEmpty(this.item.customStone))
      return 'design-your-own'
    const settingId = getItemId(this.item.customSetting).toLowerCase()
    const stoneId = getItemId(this.item.customStone).toLowerCase()
    const color = this.colorNormalize(
      this.item.customStone.color ||
        get(this.item, 'customStone.stones[0].color', '')
    )
    let jewelryType = this.item.customSetting.category.replace('Setting_', '')
    if (this.item.customSetting.category === 'Setting_Earring')
      jewelryType = 'earrings'
    if (['Pendant', 'Necklace'].includes(jewelryType))
      jewelryType = 'necklace-pendant'
    const shape = this.shapeNormalize(
      this.item.customStone.shape ||
        get(this.item, 'customStone.stones[0].shape', '')
    )
    const carat = caratURLFormatter(
      this.item.customStone?.weight ||
        this.item.customStone?.totalWeight ||
        this.item.weight // fot sitemap and feeds
    )
    const metalName = this.metalNameNormalize(this.item.customSetting.metalName)
    const customId =
      `${color}-${stoneType}-${jewelryType}-${shape}-${carat}ct-${metalName}-${settingId}-${stoneId}`.toLowerCase()
    return `design-your-own/review/${customId}`
  }

  customPreviewPath() {
    const color = this.colorNormalize(this.item.color || '')
    let category = this.item.category || ''
    if (this.item.category === 'Earring') category = 'earrings'
    const shape = this.item.shape || ''
    const weight = this.item.stoneWeight || this.item.weight
    const stoneWeight = weight ? caratURLFormatter(weight) : ''
    const metalName = this.metalNameNormalize(this.item.metalName)
    const customId =
      `${color}-${stoneType}-${category}-${shape}-${stoneWeight}ct-${metalName}-${this.id}`.toLowerCase()
    return `design-your-own/review/${customId}`
  }

  jewelryPath() {
    if (this.dataType === 2) return this.customPreviewPath()
    // if (this.dataType === 2) return `design-your-own/review/${this.id}`
    const path = jewelryPathMap[process.env.siteName]
    const carat = caratURLFormatter(
      this.item.stoneWeight ||
        this.item.weight ||
        this.item.centerStoneWeight ||
        this.item.centerStones[0].totalWeight
    )
    let category = this.item.category.toLowerCase()
    if (this.item.category === 'Earring') category = 'earrings'
    if (['Pendant', 'Necklace'].includes(this.item.category))
      category = 'necklace-pendant'
    return `${path}/${carat}ct-ruby-${category}-${this.id}`
  }

  settingsPath() {
    const prefix = 'design-your-own/setting/'
    let path = ''
    if (/mens-ring-settings/g.test(normRouteName(this.route?.name))) {
      path = 'mens-ring-settings'
    } else {
      path = settingsPathMap[process.env.siteName][this.item.category]
    }
    const metalName = this.metalNameNormalize(
      this.item.metalName || this.item.metalType
    )

    const styleName = this.styleNormalize(get(this.item, 'styleNames[0]', ''))

    const metalTypeCode = (this.item.metalTypeCode || '').toLowerCase()
    const category = this.categoryNormalize(this.item.category)
    return `${prefix + path}/${[
      metalName,
      styleName,
      category,
      this.id + metalTypeCode
    ]
      .filter((p) => p)
      .join('-')}`
  }

  categoryNormalize(category) {
    return category.replace('Setting_', '').toLowerCase()
  }

  styleNormalize(style) {
    if (!style) return ''
    return style
      .replace('(', '')
      .replace(')', '')
      .replace(/\s+/g, '-')
      .toLowerCase()
  }

  colorNormalize(color) {
    if (!color) return ''
    return color
      .replace('(', '')
      .replace(')', '')
      .replace(/\s+/g, '-')
      .toLowerCase()
  }

  originNormalize(origin) {
    if (!origin) return ''
    return origin
      .replace('(', '')
      .replace(')', '')
      .replace(/\s+/g, '-')
      .toLowerCase()
  }

  shapeNormalize(shape) {
    if (!shape) return ''
    return shape
      .replace('(', '')
      .replace(')', '')
      .replace(/\s+/g, '-')
      .toLowerCase()
  }

  metalNameNormalize(metalName) {
    if (!metalName) return ''
    return metalName
      .replace('(', '')
      .replace(')', '')
      .replace('&', '')
      .replace(/\s+/g, '-')
      .toLowerCase()
  }
}

export const getDetailsPath = function (item, route) {
  const path = new DetailsPath(item, route)
  return path.getPath()
}

export const getItemId = function (item, idBy) {
  if (idBy) return item[idBy]
  if (item.dataType === 2) return item.id // MTO listing items
  if (item.category === 'Custom' || (item.customStone && item.customSetting))
    return getCustomItemId(item)
  const metalTypeCode = item.metalTypeCode || ''
  return `${item.id}${metalTypeCode}`
}

export function toIsoString(date) {
  const tzo = -date.getTimezoneOffset()
  const dif = tzo >= 0 ? '+' : '-'
  const pad = function (num) {
    const norm = Math.floor(Math.abs(num))
    return (norm < 10 ? '0' : '') + norm
  }

  return (
    date.getFullYear() +
    '-' +
    pad(date.getMonth() + 1) +
    '-' +
    pad(date.getDate()) +
    'T' +
    pad(date.getHours()) +
    ':' +
    pad(date.getMinutes()) +
    ':' +
    pad(date.getSeconds()) +
    dif +
    pad(tzo / 60) +
    ':' +
    pad(tzo % 60)
  )
}

export const getFeedItem = function (item) {
  const host = `${protocol}://${
    process.env.isDev ? process.env.devDomain : process.env.prodDomain
  }`

  const id =
    item.dataType === 2 ? item.designId : item.id + (item.metalTypeCode || '')
  const color =
    item.dataType === 2
      ? `${item.color}/${item.metalName}`
      : item.color || item.metalName

  const feedItem = {
    'g:id': id,
    'g:title': itemNameFormatterNoLocale(item),
    'g:image_link': item.imagePath,
    'g:custom_label_1': getCustomLabel(item),
    'g:availability': item.itemStatus === 3 ? 'out_of_stock' : 'in_stock',
    'g:description': item.description || '',
    'g:price': item.price + ' USD',
    'g:link': host + getDetailsPath(item),
    'g:mpn': id,
    'g:gender': getProductGender(item),
    'g:age_group': 'adult',
    'g:brand': 'The Natural Ruby Company',
    'g:product_type': getProductType(item),
    'g:custom_label_0': getLabel(item),
    'g:google_product_category': getGoogleProductCategory(item),
    'g:condition': item.condition,
    'g:color': color,
    'g:min_handling_time': getMinTime(item),
    'g:max_handling_time': getMaxTime(item)
  }

  if (item.gtinCode) feedItem['g:gtin'] = item.gtinCode

  if (item.discountPrice) {
    feedItem['g:sale_price'] = item.discountPrice + ' USD'
    feedItem['g:sale_price_effective_date'] = `${toIsoString(
      new Date(item.saleStart * 1000)
    )}/${toIsoString(new Date(item.saleEnd * 1000))}`
  }

  return feedItem
}

export const getSupplementalItem = function (item) {
  const id =
    item.dataType === 2 ? item.id : item.id + (item.metalTypeCode || '')

  const feedItem = {
    'g:id': id
  }

  if (item.images) feedItem['g:additional_image_link'] = item.images

  return feedItem
}

const priceRanges = {
  100: '0-100',
  250: '100-250',
  500: '250-500',
  1000: '500-1000',
  1500: '1000-1500',
  2000: '1500-2000',
  2500: '2000-2500',
  3000: '2500-3000',
  4000: '3000-4000',
  5000: '4000-5000',
  7500: '5000-7500',
  10000: '7500-10000',
  15000: '10000-15000',
  20000: '15000-20000',
  25000: '20000-25000',
  50000: '25000-50000',
  100000: '50000-100000'
}

function getCustomLabel(item) {
  const vPrice = item.discountPrice || item.price
  if (vPrice > Object.keys(priceRanges).pop()) return '100000+'
  let priceRange = Object.values(priceRanges).pop()
  Object.keys(priceRanges)
    .reverse()
    .some((key) => {
      if (vPrice <= key) priceRange = priceRanges[key]
      return vPrice > key
    })
  return priceRange
}

function getProductGender(item) {
  const { category } = item
  if (item.styleNames?.includes("Men's")) return 'male'
  if (['Stone', 'Pair'].includes(category)) return 'unisex'
  return 'female'
}

function getProductType(item) {
  const { category } = item
  let type = 'Home > Ruby Jewelry'
  if (['Wedding Band', 'Plain Band'].includes(category))
    type = 'Home > Wedding Rings & Bands > Ruby Wedding Bands'
  if (['Pendant', 'Necklace'].includes(category))
    type = 'Home > Ruby Jewelry > Ruby Necklaces & Pendants'
  if (category === 'Stone') type = 'Home > Loose Rubies > Rubies'
  if (category === 'Pair') type = 'Home > Loose Rubies > Ruby Pairs'
  if (category === 'Ring') type = 'Home > Ruby Jewelry > Ruby Rings'
  if (category === 'Earring') type = 'Home > Ruby Jewelry > Ruby Earrings'
  if (category === 'Bracelet') type = 'Home > Ruby Jewelry > Ruby Bracelets'
  return type
}

function getLabel(item) {
  const { category } = item
  const jewelry = ['Pendant', 'Necklace', 'Ring', 'Earring', 'Bracelet']
  if (['Wedding Band', 'Plain Band'].includes(category)) return 'WeddingBand'
  if (['Stone', 'Pair'].includes(category)) return 'Stone'
  if (item.dataType === 2) return 'MadeToOrderJewelry'
  if (jewelry.includes(category)) return 'ReadyToShipJewelry'
  return 'Jewelry'
}

const defaultMinDays = {
  Stone: 1,
  Pair: 1,
  Pendant: 1,
  Necklace: 1,
  Earring: 1,
  Ring: 1,
  Bracelet: 1,
  Custom: 14,
  'Wedding Band': 14,
  'Plain Band': 14
}

const defaultMaxDays = {
  Stone: 3,
  Pair: 3,
  Pendant: 4,
  Necklace: 4,
  Earring: 4,
  Ring: 4,
  Bracelet: 4,
  Custom: 28,
  'Wedding Band': 28,
  'Plain Band': 28
}

function getMinTime(item) {
  if (item.productionTimeFrom) return item.productionTimeFrom
  if (item.dataType === 2) return defaultMinDays.Custom
  return defaultMinDays[item.category]
}

function getMaxTime(item) {
  if (item.productionTimeTo) return item.productionTimeTo
  if (item.dataType === 2) return defaultMaxDays.Custom
  return defaultMaxDays[item.category]
}

function getGoogleProductCategory(item) {
  const { category } = item
  let gCategory = 188
  if (category === 'Bracelet') gCategory = 191
  if (['Ring', 'Wedding Band', 'Plain Band'].includes(category)) gCategory = 200
  if (category === 'Earring') gCategory = 194
  if (['Pendant', 'Necklace'].includes(category)) gCategory = 196

  return gCategory
}

function getCustomItemId({ customStone, customSetting }) {
  if (isEmpty(customStone) || isEmpty(customSetting)) return null
  return `${customSetting.id}${customSetting.metalTypeCode}-${customStone.id}`
}
