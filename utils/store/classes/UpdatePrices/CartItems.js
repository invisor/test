import UpdateWishlistItems from './WishlistItems'
import { bandsByCategory, settingsItems } from '~~/utils/definitions/defaults'

export default class UpdateCartItems extends UpdateWishlistItems {
  get itemsList() {
    const excludeItems = [...bandsByCategory, ...settingsItems]
    return this.store.getters['cart/items'].filter(
      (i) =>
        !excludeItems.includes(i.category) && !i.serverSync && !i.placeholder
    )
  }

  /**
   * Update prices
   * @param {array} prices
   */
  updatePrices(prices) {
    this.store.dispatch('cart/updatePrices', prices)
  }
}
