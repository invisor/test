import { bandsByCategory, settingsItems } from '~~/utils/definitions/defaults'

export default class UpdateWishlistItems {
  constructor(context) {
    this.store = context.store
    this.simpleItemsList = [] // stones, jewelry
    this.customItemsList = [] // custom items and MTO
  }

  get itemsList() {
    const excludeItems = [...bandsByCategory, ...settingsItems]
    return this.store.getters['favorites/allListItems'].filter(
      (i) =>
        !excludeItems.includes(i.category) && !i.serverSync && !i.placeholder
    )
  }

  async update() {
    if (!this.itemsList.length) return
    await this.updateSimpleItems()
    await this.updateCustomItems()
  }

  async updateSimpleItems() {
    const simpleItemsList = this.itemsList
      .filter((item) => item.category !== 'Custom')
      .map((item) => item.id)
    if (simpleItemsList.length) {
      const prices = await this.store.dispatch(
        'productDetails/fetchItemsPrices',
        simpleItemsList
      )
      this.updatePrices(prices)
    }
  }

  async updateCustomItems() {
    const customItemsList = this.itemsList.filter(
      (item) => item.category === 'Custom'
    )
    if (customItemsList.length) {
      const prices = await this.store.dispatch(
        'customItem/fetchItemsPrices',
        customItemsList
      )
      this.updatePrices(prices)
    }
  }

  /**
   * Update prices
   * @param {array} prices
   */
  updatePrices(prices) {
    this.store.dispatch('favorites/updatePrices', prices)
  }
}
