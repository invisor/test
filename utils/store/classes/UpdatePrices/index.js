import UpdateWishlistItems from './WishlistItems'
import UpdateCompareItems from './CompareItems'
import UpdateCartItems from './CartItems'

export default async function(context, type) {
  const mapLists = {
    wishlist: UpdateWishlistItems,
    compare: UpdateCompareItems,
    cart: UpdateCartItems
  }

  const items = new mapLists[type](context)
  await items.update()
}
