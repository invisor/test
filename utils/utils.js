import isEmpty from 'lodash/isEmpty'
import omit from 'lodash/omit'
import pick from 'lodash/pick'
import cloneDeep from 'lodash/cloneDeep'
import invert from 'lodash/invert'
import {
  productListTypeByCategory,
  defaultWaxPrice,
  cartRoutes,
  cookiesOpts,
  mensStyleID,
  mensStyleRingID,
  itemsTypeByRouteName
} from './definitions/defaults'
import { validUrlFilterNames } from '~~/utils/definitions/filters'
import { parametersNamesMap } from '~~/utils/definitions/itemParameters'

export const {
  getDetailsPath,
  getItemId,
  normRouteName
} = require('~~/utils/sitemapUtils')

export const siteColorsMap = {
  primary: {
    ruby: '#fe5084',
    emerald: '#49a37d'
  },
  darkPrimary: {
    ruby: '#d21868',
    emerald: '#007F4E'
  }
}

export function getSiteColor(color) {
  return siteColorsMap[color]?.[process.env.siteName] || color
}

export function setPreferableRingSize(cookies, value) {
  cookies.set('ring-size', value, cookiesOpts)
}

export function removeTagsFromString(str) {
  // remove html tags from string
  return str.replace(/<(?:.|\n)*?>/gm, '')
}

/**
 * @param route {Object}
 * @returns {boolean}
 */
export function isCartRoute(route) {
  return cartRoutes.includes(normRouteName(route.name))
}

export function getPreferableRingSize(cookies) {
  const size = parseFloat(cookies.get('ring-size'))
  return isNaN(size) ? null : size
}

// get element offset from top of the document
export function getOffset(el) {
  const rect = el.getBoundingClientRect()
  const scrollLeft = window.pageXOffset || document.documentElement.scrollLeft
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop
  return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

export function isItemsMatch(item) {
  return !(
    (item.customSetting.category === 'Setting_Earring' &&
      item.customStone.category !== 'Pair') ||
    (item.customSetting.category !== 'Setting_Earring' &&
      item.customStone.category === 'Pair')
  )
}

export function htmlDecode(input) {
  return input.replace(/&#(\d+);/g, function (match, dec) {
    return String.fromCharCode(dec)
  })
}

export function getListingItemsOrigin(route) {
  const name = normRouteName(route.name)
  if (
    ['rubies-burmese-rubies', 'ruby-jewelry-burmese-ruby-rings'].includes(name)
  )
    return 'Burma (Myanmar)'
  if (
    ['rubies-mozambique-rubies', 'ruby-jewelry-mozambique-ruby-rings'].includes(
      name
    )
  )
    return 'Mozambique'
  return null
}

export function getListingItemsWebCategory(route) {
  const name = normRouteName(route.name)
  if (['rubies-gemologist-recommended'].includes(name)) return 2

  return 0
}

export function getListingItemsStyle(route) {
  const name = normRouteName(route.name)
  if (name === 'design-your-own-setting-mens-ring-settings') return mensStyleID
  if (name === 'ruby-jewelry-mens-ruby-rings') return mensStyleRingID

  return null
}

export function getListingItemsSubType(route) {
  const name = normRouteName(route.name)
  if (
    [
      'rubies-ruby-cabochon',
      'rubies-star-rubies',
      'rubies-matched-pairs-of-rubies-cabochon',
      'rubies-star-ruby-pairs'
    ].includes(name)
  )
    return 'cabochon'
  return null
}

export function isListingItemsHaveStar(route) {
  const name = normRouteName(route.name)
  if (
    [
      'ruby-jewelry-star-ruby-rings',
      'rubies-star-rubies',
      'rubies-star-ruby-pairs'
    ].includes(name)
  )
    return 1
  return null
}

export function getLastDetailsPath(params) {
  if (params.isDetails) return params.path?.path || null
  return null
}

export function isTokenValid(tokenExpiration) {
  return tokenExpiration > +new Date()
}

export function normRoute(route) {
  const r = cloneDeep(pick(route, ['name', 'params', 'query']))
  r.name = normRouteName(r.name)
  return r
}

export function getIconName(metal) {
  if (!metal) return 'PLMetal'
  if (metal === 'Palladium') return 'PLMetal'
  if (metal === 'Platinum 950') return 'PTMetal'
  const metalData = metal.split(' ')
  const carat = metalData[0] === '18K' ? 'EighteenK' : 'FourteenK'
  const reverse =
    metalData[1] === 'White' && metalData[3] === 'Yellow' ? '' : 'Reverse'
  return `${carat}${metalData[2] === '&' ? `Double${reverse}` : ''}Metal`
}

export function getIconColor(metal) {
  if (!metal) return '#898989'
  if (['Palladium', 'Platinum 950'].includes(metal)) return '#898989'
  const metalColor = metal.split(' ')
  if (metalColor[1] === 'White') return '#898989'
  if (metalColor[1] === 'Yellow') return '#e4aa09'
  if (metalColor[1] === 'Rose') return '#ff8282'
}

export function getPrice(item, isBankWire) {
  if (isBankWire === undefined) throw new Error('Error: isBankWire not setted')
  let price
  if (item.category === 'Wax') {
    return defaultWaxPrice
  } else if (item.finalPrice) {
    if (isBankWire && item.customStone.bankWirePrice) {
      const stonePrice = item.customStone.bankWirePrice
      const settingPrice = item.finalPrice.settingPrice
      return Number(stonePrice) + Number(settingPrice)
    }
    if (item.customStone.discountPrice) {
      const stonePrice = item.customStone.discountPrice
      const settingPrice = item.finalPrice.settingPrice
      return Number(stonePrice) + Number(settingPrice)
    }
    price = item.finalPrice.totalPrice
  } else {
    if (isBankWire && item.bankWirePrice) return Number(item.bankWirePrice)
    if (item.discountPrice) return item.discountPrice
    price = item.price || item.totalPrice
  }
  return price
}

export function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1)
  }
  return (
    s4() +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    '-' +
    s4() +
    s4() +
    s4()
  )
}

export function getSettingsPickerPath(type) {
  let defaultPath = { name: 'design-your-own' }
  if (type === 'ringsList')
    defaultPath = {
      name: 'design-your-own-setting-ruby-engagement-ring-settings'
    }
  if (['pendantsList', 'necklacesList'].includes(type))
    defaultPath = {
      name: 'design-your-own-setting-ruby-necklace-pendant-settings'
    }

  if (type === 'earringsList')
    defaultPath = {
      name: 'design-your-own-setting-ruby-earring-settings'
    }
  return defaultPath
}

export function getCustomPathByCategory(step) {
  const functionsMap = {
    ruby: rubyGetCustomPathByCategory,
    emerald: emeraldGetCustomPathByCategory
  }
  return functionsMap[process.env.siteName](step)
}

function rubyGetCustomPathByCategory(step) {
  const category = productListTypeByCategory[step.itemData.category]

  if (category === 'earringSettings')
    return {
      name: 'design-your-own-setting-ruby-earring-settings'
    }
  if (['pendantSettings', 'necklaceSettings'].includes(category))
    return {
      name: 'design-your-own-setting-ruby-necklace-pendant-settings'
    }
  if (category === 'ringSettings')
    return {
      name: 'design-your-own-setting-ruby-engagement-ring-settings'
    }
  if (category === 'stonesList') {
    const route = {
      name: 'rubies',
      query: { 'design-your-own': 1 }
    }
    if (step.itemData.cuttingStyle === 'Cabochon') {
      route.name = 'rubies-ruby-cabochon'
      if (step.itemData.isStar) route.name = 'rubies-star-rubies'
    }
    return route
  }
  if (category === 'stonePairsList') {
    const route = {
      name: 'rubies-matched-pairs-of-rubies',
      query: { 'design-your-own': 1 }
    }
    if (step.itemData.cuttingStyle === 'Cabochon') {
      route.name = 'rubies-matched-pairs-of-rubies-cabochon'
      if (step.itemData.isStar) route.name = 'rubies-star-ruby-pairs'
    }
    return route
  }
}

function emeraldGetCustomPathByCategory(step) {
  const category = productListTypeByCategory[step.itemData.category]

  if (category === 'earringSettings')
    return {
      name: 'design-your-own-setting-emerald-earring-settings'
    }
  if (['pendantSettings', 'necklaceSettings'].includes(category))
    return {
      name: 'design-your-own-setting-emerald-necklace-pendant-settings'
    }
  if (category === 'ringSettings')
    return {
      name: 'design-your-own-setting-emerald-engagement-ring-settings'
    }
  if (category === 'stonesList') {
    const route = {
      name: 'emeralds',
      query: { 'design-your-own': 1 }
    }
    if (step.itemData.cuttingStyle === 'Cabochon')
      route.name = 'emeralds-emerald-cabochon'
    return route
  }
  if (category === 'stonePairsList') {
    const route = {
      name: 'emeralds-matched-pairs-of-emeralds',
      query: { 'design-your-own': 1 }
    }
    if (step.itemData.cuttingStyle === 'Cabochon')
      route.name = 'emeralds-matched-pairs-of-emeralds-cabochon'
    return route
  }
}

export class CustomNextPath {
  constructor(data) {
    this.data = {
      customStone: data.customStone,
      customSetting: data.customSetting
    }
    this.customStone = data.customStone
    this.customSetting = data.customSetting
    this.defaultRoute = null
    this.type = data.type
    this.route = data.route
    this.query = data.query || {}
  }

  get() {
    const isSettingEmpty = isEmpty(this.customSetting)
    const isStoneEmpty = isEmpty(this.customStone)

    if (!isStoneEmpty && !isSettingEmpty) return this.getCustomRoute()

    if (isStoneEmpty && isSettingEmpty)
      return {
        name: 'design-your-own'
      }

    if (isStoneEmpty) return this.getStonesRoute()

    if (isSettingEmpty) return this.getSettingsRoute()
  }

  getDefaultRoute() {
    const settings = [
      'ringsList',
      'pendantsList',
      'necklacesList',
      'earringsList'
    ]
    if (this.type && settings.includes(this.type))
      this.defaultRoute = this[`${this.type}DefaultRoute`]()
  }

  getCustomRoute() {
    const { source } = this.route.query
    const path = getDetailsPath({
      ...this.data,
      category: 'Custom'
    })
    const route = {
      path,
      query: this.query
    }
    if (source === 'share') route.query.source = 'share'
    return route // all settings are filled
  }

  getStonesRoute() {
    const routeNamesMap = {
      ruby: {
        stones: 'rubies',
        pairs: 'rubies-matched-pairs-of-rubies'
      },
      emerald: {
        stones: 'emeralds',
        pairs: 'emeralds-matched-pairs-of-emeralds'
      }
    }
    if (
      productListTypeByCategory[this.customSetting.category] ===
      'earringSettings'
    )
      // for earring setting only pair stones
      return {
        name: routeNamesMap[process.env.siteName].pairs,
        query: { 'design-your-own': 1 }
      }
    return {
      name: routeNamesMap[process.env.siteName].stones,
      query: { 'design-your-own': 1 }
    }
  }

  getSettingsRoute() {
    // detect which setting page should redirect to
    if (
      productListTypeByCategory[this.customStone.category] === 'stonePairsList'
    )
      return this.earringsListDefaultRoute() // for pair stones only earring settings
    this.getDefaultRoute()
    return this.defaultRoute || this.ringsListDefaultRoute() // need to detect which type settings is needed
  }

  ringsListDefaultRoute() {
    const routeNamesMap = {
      ruby: 'design-your-own-setting-ruby-engagement-ring-settings',
      emerald: 'design-your-own-setting-emerald-engagement-ring-settings'
    }
    return {
      name: routeNamesMap[process.env.siteName]
    }
  }

  pendantsListDefaultRoute() {
    const routeNamesMap = {
      ruby: 'design-your-own-setting-ruby-necklace-pendant-settings',
      emerald: 'design-your-own-setting-emerald-necklace-pendant-settings'
    }
    return {
      name: routeNamesMap[process.env.siteName]
    }
  }

  necklacesListDefaultRoute() {
    const routeNamesMap = {
      ruby: 'design-your-own-setting-ruby-necklace-pendant-settings',
      emerald: 'design-your-own-setting-emerald-necklace-pendant-settings'
    }
    return {
      name: routeNamesMap[process.env.siteName]
    }
  }

  earringsListDefaultRoute() {
    const routeNamesMap = {
      ruby: 'design-your-own-setting-ruby-earring-settings',
      emerald: 'design-your-own-setting-emerald-earring-settings'
    }
    return {
      name: routeNamesMap[process.env.siteName]
    }
  }
}

export function generateId() {
  return String(Math.round(Math.random() * 10000000000))
}

export function getItemFullId(item) {
  const metalTypeCode = item.metalTypeCode ? item.metalTypeCode : ''
  return `${item.id || ''}${metalTypeCode}`
}

export function getLocalPrice(usdPrice, usdRate, conversionRate) {
  if (!usdRate || !conversionRate) return usdPrice
  return (usdPrice / usdRate) * conversionRate
}

export function getOriginalPrice(localPrice, usdRate, conversionRate) {
  if (!usdRate || !conversionRate) return localPrice
  return (localPrice / conversionRate) * usdRate
}

export function query(store, route) {
  const customSetting = store.getters['customItem/customSetting']
  const customStone = store.getters['customItem/customStone']

  const routeName = normRouteName(route.name)
  const isStonesPage = itemsTypeByRouteName.stonesList.includes(routeName)
  const isSettingsPage = /design-your-own-setting/.test(routeName)

  if (!isEmpty(customSetting) && isStonesPage)
    return { settingId: customSetting.id }

  if (!isEmpty(customStone) && isSettingsPage)
    return { stoneId: customStone.id }

  return {}
}

export function flushQuery(query, params = []) {
  const itemsParams = Object.keys(invert(parametersNamesMap))
  let paramsList = ['page', ...validUrlFilterNames, ...itemsParams]
  if (params.length) paramsList = params
  return omit(query, paramsList)
}
